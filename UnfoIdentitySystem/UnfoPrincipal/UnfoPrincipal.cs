﻿using DataModel;
using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Newtonsoft.Json;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace UnfoIdentitySystem.UnfoPrincipal
{
    internal interface IUnfoPrincipal : IPrincipal, IShortUser
    {
        bool IsInRole(Role role);
    }

    public sealed class UnfoIdentity : GenericIdentity
    {
        public UnfoIdentity(IUser<Guid> unfoPrincipal)
            : base(unfoPrincipal.UserName)
        {
            var claimUserId = new Claim(ClaimTypes.NameIdentifier, unfoPrincipal.Id.ToString(), typeof(Guid).Name);
            AddClaim(claimUserId);
        }

        public UnfoIdentity(string name, string type)
            : base(name, type)
        {
        }

        private UnfoIdentity(GenericIdentity identity)
            : base(identity)
        {
        }
    }

    [Serializable]
    public class UnfoPrincipal : IUnfoPrincipal
    {
        private readonly List<string> _roles;

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string someRole)
        {
            if (someRole == null || this._roles == null)
                return false;

            return _roles.Any(role => role == someRole);
        }

        public bool IsInRole(Role role)
        {
            return IsInRole(role.ToString());
        }

        /// <summary>
        /// Проверка юзера есть у него одна из перечисленых ролей или нет
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public bool IsInRole(params Role[] roles)
        {
            return roles.Any(r => _roles != null && _roles.Contains(r.ToString()));
        }

        /// <summary>
        /// Проверка юзера содержит ли он все перечисленные роли
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public bool IsInRoles(params Role[] roles)
        {
            return roles.All(r => _roles != null && _roles.Contains(r.ToString())); ;
        }

        public UnfoPrincipal(UnfoPrincipalSerializeModel deserializedModel)
        {
            Id = deserializedModel.Id;
            UserName = deserializedModel.UserName;
            SurName = deserializedModel.SurName;
            FirstName = deserializedModel.FirstName;
            FatherName = deserializedModel.FatherName;
            Location = deserializedModel.Location;
            Email = deserializedModel.Email;
            PhoneNumber = deserializedModel.PhoneNumber;
            PhotoId = deserializedModel.PhotoId;

            this._roles = deserializedModel.Roles;
            HasPassword = deserializedModel.HasPassword;
            IsEmailConfirmed = deserializedModel.IsEmailConfirmed;

            this.Identity = new UnfoIdentity(this);

            TrySetModeratorOfCurrentEventRole();
        }

        public UnfoPrincipal()
        {
            Identity = new GenericIdentity("");
        }

        #region Интерфейсные свойства IShortUser

        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string SurName { get; set; }

        public string FirstName { get; set; }

        public string FatherName { get; set; }
        public string Location { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Guid PhotoId { get; set; }

        #endregion Интерфейсные свойства IShortUser

        public bool IsEmailConfirmed { get; set; }

        public bool HasPassword { get; set; }

        public TimeZoneInfo TimeZone { get; set; }

        private void TrySetModeratorOfCurrentEventRole()
        {
            var context = new UnfoDbContext();
            var currentUser = context.Users.Where(user => user.Id == Id);
            var isModeratorOfCurrentEvent = currentUser.Any(user => user.ModeratedEvents.Any(events => events.EventID == IdentityCurrentEvent.ID && !events.IsDelete));
            if (isModeratorOfCurrentEvent) _roles.Add(Role.ModeratorOfCurrentEvent.ToString());
            context.Dispose();
        }

        public string ToJson()
        {
            var serializeModel = new CookieUnfoPrincipal(this, _roles);
            return JsonConvert.SerializeObject(serializeModel);
        }
    }

    public class UnfoPrincipalSerializeModel : IShortUser
    {
        #region Интерфейсные свойства

        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string SurName { get; set; }

        public string FirstName { get; set; }

        public string FatherName { get; set; }
        public string Location { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Guid PhotoId { get; set; }

        #endregion Интерфейсные свойства

        public List<string> Roles { get; set; }

        public bool HasPassword { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public UnfoPrincipalSerializeModel()
        {
        }

        public UnfoPrincipalSerializeModel(UnfoUser dbUser, params Role[] userRoles)
        {
            Id = dbUser.Id;
            UserName = dbUser.UserName;
            SurName = dbUser.SurName;
            FirstName = dbUser.FirstName;
            FatherName = dbUser.FatherName;
            Location = dbUser.Location;
            Email = dbUser.Email;
            PhoneNumber = dbUser.PhoneNumber;
            PhotoId = dbUser.PhotoId;

            Roles = userRoles.Select(r => r.ToString()).ToList();
            HasPassword = dbUser.PasswordHash != null;
            IsEmailConfirmed = dbUser.EmailConfirmed;
        }
    }

    public class CookieUnfoPrincipal : IShortUser
    {
        #region Implementation of IUser<out Guid>

        /// <summary>
        /// Unique key for the user
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Unique username
        /// </summary>
        public string UserName { get; set; }

        #endregion

        #region Implementation of IShortUser

        /// <summary>
        /// Фамилия
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string FatherName { get; set; }

        public string Location { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public Guid PhotoId { get; set; }
        public List<string> Roles { get; set; }
        #endregion

        public CookieUnfoPrincipal(IShortUser userInfo, List<string> roles)
        {
            Id = userInfo.Id;
            Roles = roles;
            UserName = userInfo.UserName;
            SurName = userInfo.SurName;
            FirstName = userInfo.FirstName;
            FatherName = userInfo.FatherName;
            Location = userInfo.Location;
            Email = userInfo.Email;
            PhoneNumber = userInfo.PhoneNumber;
            PhotoId = userInfo.PhotoId;
        }
    }
}