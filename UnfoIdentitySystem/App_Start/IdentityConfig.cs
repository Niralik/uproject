﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace UnfoIdentitySystem
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class UnfoUserManager : UserManager<UnfoUser,Guid>
    {
        public UnfoUserManager(IUserStore<UnfoUser, Guid> store)
            : base(store)
        {
        }

        public static UnfoUserManager Create(IdentityFactoryOptions<UnfoUserManager> options, IOwinContext context)
        {
            var manager = new UnfoUserManager(new UserStore<UnfoUser, UnfoRole, Guid, UnfoUserLogin, UnfoUserRole, UnfoUserClaim>(context.Get<UnfoDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<UnfoUser, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 5,
                //RequireNonLetterOrDigit = true,
                //RequireDigit = true,
                //RequireLowercase = true,
                //RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(15);
            manager.MaxFailedAccessAttemptsBeforeLockout = GlobalVariables.MAX_FAILED_ATTEMPS;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            //manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<UnfoUser>
            //{
            //    MessageFormat = "Your security code is: {0}"
            //});
            //manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<UnfoUser>
            //{
            //    Subject = "SecurityCode",
            //    BodyFormat = "Your security code is {0}"
            //});
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<UnfoUser, Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class UnfoRoleManager : RoleManager<UnfoRole, Guid>
    {
        public UnfoRoleManager(IRoleStore<UnfoRole, Guid> roleStore)
            : base(roleStore)
        {
        }

        public static UnfoRoleManager Create(IdentityFactoryOptions<UnfoRoleManager> options, IOwinContext context)
        {
            return new UnfoRoleManager(new RoleStore<UnfoRole, Guid, UnfoUserRole>(context.Get<UnfoDbContext>()));
        }
    }
    public class UnfoSignInManager : SignInManager<UnfoUser, Guid>
    {
        public UnfoSignInManager(UnfoUserManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager) { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(UnfoUser user)
        {
            return user.GenerateUserIdentityAsync((UnfoUserManager)UserManager, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static UnfoSignInManager Create(IdentityFactoryOptions<UnfoSignInManager> options, IOwinContext context)
        {
            return new UnfoSignInManager(context.GetUserManager<UnfoUserManager>(), context.Authentication);
        }

       

    }

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.

            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            return Task.FromResult(0);
        }
    }
}