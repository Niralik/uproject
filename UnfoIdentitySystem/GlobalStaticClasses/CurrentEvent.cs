using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DataModel;

namespace UnfoIdentitySystem.GlobalStaticClasses
{
    /// <summary>
    /// Текущее глобальное событие
    /// </summary>
    public static class IdentityCurrentEvent
    {
        public static Guid ID { get; set; }

        public static UEvent UEvent { get; set; }

        public static bool HasAccess { get; set; }

        public static void Initialize(UEvent @event)
        {
            if (@event == null) return;
            UEvent = @event;
            ID = @event.ID;
        }

       
    }
}