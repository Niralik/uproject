﻿using Duke.Owin.VkontakteMiddleware;
using Duke.Owin.VkontakteMiddleware.Provider;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Twitter;
using Owin.Security.Providers.GooglePlus;
using Owin.Security.Providers.GooglePlus.Provider;
using System.Security.Claims;

namespace UnfoIdentitySystem
{
    public static class ExternalLoginProvidersOptions
    {
        public static TwitterAuthenticationOptions Twitter
        {
            get
            {
                var twitterOptions = new TwitterAuthenticationOptions();
                twitterOptions.ConsumerKey = "foJL5ZOcqNIiJvXLFfvakNpeu";
                twitterOptions.ConsumerSecret = "snaj0XRIcSVzevXBZjwOThYv0ZygMEMnLp8oyrIhwtxSESQh5I";
                twitterOptions.BackchannelCertificateValidator = new CertificateSubjectKeyIdentifierValidator(
                    new[]
                    {
                        "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
                        "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
                        "7FD365A7C2DDECBBF03009F34339FA02AF333133",
                        // VeriSign Class 3 Public Primary Certification Authority - G5
                        "39A55D933676616E73A761DFA16A7E59CDE66FAD" // Symantec Class 3 Secure Server CA - G4
                    });
                return twitterOptions;
            }
        }

        public static FacebookAuthenticationOptions Facebook
        {
            get
            {
                var fbOptions = new FacebookAuthenticationOptions();
                fbOptions.AppId = "724611770933030";
                fbOptions.AppSecret = "add164fc6e08456f9085b1830a9d9b8d";
                fbOptions.Scope.Add("email");
                fbOptions.Scope.Add("public_profile");
                fbOptions.Scope.Add("user_birthday");
                //http://graph.facebook.com/{{userId}}/picture - чоб картиночку получить
                fbOptions.Provider = new FacebookAuthenticationProvider()
                {
                    OnAuthenticated = async context =>
                    {
                        //Get the access token from FB and store it in the database and
                        //use FacebookC# SDK to get more information about the user
                        context.Identity.AddClaim(new Claim("FacebookAccessToken",
                            context.AccessToken));
                        context.Identity.AddClaim(new Claim("Person", context.User.ToString()));
                        context.Identity.AddClaim(new Claim("Properties", context.Properties.ToString()));
                        context.Identity.AddClaim(new Claim("Identity", context.Identity.ToString()));
                    }
                };
                return fbOptions;
            }
        }

        public static VkAuthenticationOptions Vkontakte
        {
            get
            {
                var vkOptions = new VkAuthenticationOptions();
                vkOptions.AppId = "4486804";
                vkOptions.AppSecret = "kkR9vGQ8Kg9DEx5FDzMl";
                vkOptions.Scope = "email, fields";
                vkOptions.Provider = new VkAuthenticationProvider()
                {
                    OnAuthenticated = async context =>
                    {
                        context.Identity.AddClaim(new Claim("VkontakteAccessToken", context.AccessToken));
                        context.Identity.AddClaim(new Claim("Properties", context.Properties.ToString()));
                        context.Identity.AddClaim(new Claim("First_Name", context.Name));
                        context.Identity.AddClaim(new Claim("Last_Name", context.LastName));
                        context.Identity.AddClaim(new Claim("Identity", context.Identity.Claims.ToString()));
                        context.Identity.AddClaim(new Claim("UserName", context.UserName));
                    }
                };
                return vkOptions;
            }
        }
        /// <summary>
        /// Настройки для локалхоста
        /// </summary>
        public static GooglePlusAuthenticationOptions LocalhostGooglePlus
        {
            get
            {
                var localhostOptions = settingGooglePlus("464457771285-jl59updf05c6bb2kr864bep3ulrjig5g.apps.googleusercontent.com", "H1o0iaz5yBdd7ubhVFhtPZ5g"); 
                return   localhostOptions;
            }
        }
        /// <summary>
        /// Настройки для тестового сервера деплоя 192.168.2.165
        /// </summary>
        public static GooglePlusAuthenticationOptions DeployGooglePlus
        {
            get
            {
                var deployOptions = settingGooglePlus("464457771285-qcqbl9blecimh49v05ikrogs11m5ro9l.apps.googleusercontent.com", "iyyqRcrcFl-tYkWxdzurFin2");
                return deployOptions;
            }
        }
        private static GooglePlusAuthenticationOptions settingGooglePlus(string clientId, string clientSecret)
        {
            var googleOptions = new GooglePlusAuthenticationOptions();
            googleOptions.ClientId = clientId;
            googleOptions.ClientSecret = clientSecret;
            googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.login");
            googleOptions.Scope.Add("https://www.googleapis.com/auth/userinfo.profile");
            googleOptions.Scope.Add("https://www.googleapis.com/auth/userinfo.email");
            googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.me");
            googleOptions.Provider = new GooglePlusAuthenticationProvider()
            {
                OnAuthenticated = async context =>
                {
                    context.Identity.AddClaim(new Claim("GoogleAccessToken", context.AccessToken));
                    context.Identity.AddClaim(new Claim("picture", context.User.GetValue("picture").ToString()));
                    context.Identity.AddClaim(new Claim("User", context.User.ToString()));
                    context.Identity.AddClaim(new Claim("Properties", context.Properties.ToString()));
                    context.Identity.AddClaim(new Claim("Person", context.Person.ToString()));
                }
            };
            return googleOptions;
        }
    }
}