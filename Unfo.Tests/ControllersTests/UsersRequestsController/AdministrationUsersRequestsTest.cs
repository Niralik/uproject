﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unfo.Areas.Cabinet.Controllers;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.ViewModelRepositories;

namespace UnfoTests.ControllersTests.UsersRequestsController
{
    [TestClass]
    public class AdministrationUsersRequestsTest
    {
        [TestMethod]
        public void FiltrateUsersRequestsTest()
        {
            var controller = new UserRequestsController();
            var usersRequestsForAdministration = controller.FiltrateUsersRequests(new UsersRequestsFilterParams());
            Task.WaitAll(usersRequestsForAdministration);
            var t = usersRequestsForAdministration.Result;
        }
    }
}
