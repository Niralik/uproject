﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace UnfoTests
{
    public static class Helpers
    {
        public static void ValidateModel(this Controller controller, object model)
        {


            var validationContext = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(model, validationContext, validationResults);
            foreach (var validationResult in validationResults)
            {
                foreach (var name in validationResult.MemberNames)
                {
                    controller.ModelState.AddModelError(name, validationResult.ErrorMessage);
                }

            }
        }
    }
}
