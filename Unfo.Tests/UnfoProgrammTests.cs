﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unfo.Areas.Cabinet;
using System.Web.Mvc;
using Unfo.Models;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.CommonViewModels;

namespace UnfoTests
{
    [TestClass]
    public class UnfoProgrammTests
    {
        [TestMethod]
        public void AddReport()
        {
            var c = new Unfo.Areas.Cabinet.Controllers.EventManagement.ProgrammController();

            var vm = new ViewModelRepository();




            var report = new ReportVM()
            {
                Name = "Test",
                EventId = Guid.Parse("8ab4664b-b64a-e411-840e-1caff7081cd3"),
                DateRange = new DateTimeRangeVM()
                {
                    StartDate = new DateTimeVM(DateTime.UtcNow.AddDays(-1)),
                    EndDate = new DateTimeVM(DateTime.UtcNow.AddDays(1)),
                },
                Description = "Test",
                Section = new SelectSection()
                {
                    Id = Guid.Parse("91c9d264-b64a-e411-840e-1caff7081cd3")
                },
                Hall = new SelectHall()
                {
                    Id = Guid.Parse("7be24978-b64a-e411-840e-1caff7081cd3")
                },
                Direction = new SelectDirections()
                {
                    Id = Guid.Parse("d5cb7082-b64a-e411-840e-1caff7081cd3")
                },
                Speakers = new SelectSpeakers()
                {
                    SpeakerItems = new List<SpeakerItem>()
                    {
                        new SpeakerItem()
                        {
                            ID = Guid.NewGuid(),
                            SpeakerId = Guid.Parse("1db9fcd1-b64a-e411-840e-1caff7081cd3")
                        }
                    }
                }




            };

            c.ValidateModel(report);

            var result = c.Add(report);


            Assert.IsTrue(true);
        }

        [TestMethod]
        public void EditReport()
        {
            var vm = new ViewModelRepository();

            var report = vm.Programm.Get<ReportVM, Report>(Guid.Parse("e3158e59-bc4a-e411-840e-1caff7081cd3"));

            report.Name = "OPOPOPOPOPOPO";

            vm.Programm.AddOrUpdate(report);

            Assert.IsTrue(true);
        }
    }
}
