﻿using System;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unfo.Core.Extensions;

namespace UnfoTests.AttributeTests
{
    [TestClass]
    public class DbIdAttributeTests
    {
        [TestMethod]
        public void GetDbIdTests()
        {
           var roleDbId = Role.Admin.GetDbId();
           Assert.IsNotNull(roleDbId);
           Assert.IsFalse(roleDbId == Guid.Empty);
        }
    }
}
