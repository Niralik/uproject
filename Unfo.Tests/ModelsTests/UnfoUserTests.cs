﻿using DataModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Unfo.Core.Helpers;

namespace UnfoTests.ModelsTests
{
    [TestClass]
    public class UnfoUserTests
    {
        [TestInitialize]
        public void CreatingUser()
        {
            TestingUser = new UnfoUser().Initialize();
            Assert.IsNotNull(TestingUser);
        }

        private UnfoUser TestingUser { get; set; }

        [TestMethod]
        public void PrivacyPoliticsOfCreatedUser()
        {
            Assert.IsTrue(TestingUser.Id != Guid.Empty);
            TestingUser.PrivacyPolitics.ForEach((pp) =>
            {
                Assert.IsTrue(pp.IsPrivate);
                Assert.IsTrue(pp.ID != Guid.Empty);
                Assert.IsNotNull(pp.UnfoUser);
            });
            var isAllPoliticsUnique = TestingUser.PrivacyPolitics.Distinct().Count() == TestingUser.PrivacyPolitics.Count();
            Assert.IsTrue(isAllPoliticsUnique);

            var numOfPolitics = EnumHelper<TypePrivacyPolitic>.GetValues().Count;
            Assert.IsTrue(numOfPolitics == TestingUser.PrivacyPolitics.Count);
        }

        [TestCleanup]
        public void CleanUpping()
        {
            TestingUser = null;
        }
    }
}