﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Unfo.Models.ViewModelRepositories;

namespace UnfoTests.BusinessLogiсTests
{
    [TestClass]
    public class UnfoUsersBusinesLogicTests
    {
        public IRepositoryFabric Fabric { get; set; } 
        public List<UnfoUser> TestUnfoUsers { get; set; }
        public UEvent TestEvent { get; set; }
        public UnfoUserRepository UsersBusinesLogic { get; set; }
        [TestInitialize]
        public void Initialize()
        {
            Fabric = new RepositoryFabric(new UnfoDbContext());
            UsersBusinesLogic = new UnfoUserRepository(Fabric);
            TestUnfoUsers = Fabric.Table<UnfoUser>().All().ToList();
            TestEvent = Fabric.Table<UEvent>().GetQuery().OrderBy(@event=> @event.Name).Skip(3).First();
        }
        [TestMethod]
        public void AddToEventModerators()
        {
            var list20UsersIds = TestUnfoUsers.Take(20).Select(user => user.Id).ToList();
            var task = UsersBusinesLogic.AddToEventModerators(TestEvent.ID, list20UsersIds.ToArray());
            Task.WaitAll(task);

        }
    }
}
