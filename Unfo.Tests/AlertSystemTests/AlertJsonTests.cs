﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unfo.Core;

namespace UnfoTests.AlertSystemTests
{
    [TestClass]
    public class AlertJsonTests
    {
        #region Success Warning Error Attention Tests

        [TestMethod]
        public void AlertSuccessOneMessage()
        {
            var alertJson = AlertJson.Success("One message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertOneMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Success);
        }

        [TestMethod]
        public void AlertSuccessManyMessage()
        {
            var alertJson = AlertJson.Success("One message", "Two message", "Therd message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Success);
        }

        [TestMethod]
        public void AlertErrorManyMessage()
        {
            var alertJson = AlertJson.Error("One message", "Two message", "Therd message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Error);
        }

        [TestMethod]
        public void AlertErrorOneMessage()
        {
            var alertJson = AlertJson.Error("One message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertOneMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Error);
        }

        [TestMethod]
        public void AlertAttentionManyMessage()
        {
            var alertJson = AlertJson.Attention("One message", "Two message", "Therd message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Info);
        }

        [TestMethod]
        public void AlertAttentionOneMessage()
        {
            var alertJson = AlertJson.Attention("One message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertOneMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Info);
        }

        [TestMethod]
        public void AlertWarningManyMessage()
        {
            var alertJson = AlertJson.Warning("One message", "Two message", "Therd message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Warning);
        }

        [TestMethod]
        public void AlertWarninOneMessage()
        {
            var alertJson = AlertJson.Warning("One message");
            Assert.IsNotNull(alertJson);
            var alertSection = AlertOneMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Warning);
        }

        #endregion Success Warning Error Attention Tests

        #region Other errors overloads

        [TestMethod]
        public void AlertIdentityResultError()
        {
            var identityResult = new IdentityResult("lala", "lala", "lala");
            var alertJson = AlertJson.Error(identityResult);
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Error);
        }

        [TestMethod]
        public void AlertModelStateError()
        {
            var modelStateDict = new ModelStateDictionary();
            var modelState = new ModelState();
            modelState.Errors.Add(new ModelError("OneErrorFirstProperty"));
            modelState.Errors.Add(new ModelError("TwoErrorFirstProperty"));
            modelState.Errors.Add(new ModelError("ThreeErrorFirstProperty"));
            modelStateDict.Add("test", modelState);
            var alertJson = AlertJson.Error(modelStateDict.Values);
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Error);
        }
        [TestMethod]
        public void AlertWithObjectAndCodeError()
        {
            var someobject = new { SomeTest = "Test" };
            var alertJson = AlertJson.Error(someobject, 22);
            Assert.IsNotNull(alertJson);
            Assert.IsTrue(alertJson.Data is AlertsWrapper);
            var alertWrapper = alertJson.Data as AlertsWrapper;
            Assert.IsTrue(alertWrapper.AlertSections.Count == 1);
            Assert.IsNull(alertWrapper.AlertSections.First().Message);
            Assert.IsNull(alertWrapper.AlertSections.First().Messages);  
            Assert.IsTrue(alertWrapper.Code == 22);
            Assert.AreEqual(alertWrapper.RelatedData, someobject);
        }
        [TestMethod]
        public void AlertIdentityResultSuccess()
        {
            var alertJson = AlertJson.Error(IdentityResult.Success);
            Assert.IsNotNull(alertJson);
            var alertSection = AlertOneMessage(alertJson.Data, true);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Success);
        }

        #endregion Other errors overloads

        #region Compile Test

        [TestMethod]
        public void CompilingOneAlertSectionMessage()
        {
            var alertJson = AlertJson.Compile(new AlertSection(TypeOfAlert.Success, "lala", "lala", "lala"));
            Assert.IsNotNull(alertJson);
            var alertSection = AlertManyMessage(alertJson.Data);
            Assert.IsTrue(alertSection.TypeAlert == TypeOfAlert.Success);
        }

        [TestMethod]
        public void CompilingManyAlertSectionMessage()
        {
            var alertJson = AlertJson.Compile(new AlertSection(TypeOfAlert.Success, "lala", "lala", "lala"), new AlertSection(TypeOfAlert.Error, "lalala"));
            Assert.IsNotNull(alertJson);
            Assert.IsTrue(alertJson.Data is AlertsWrapper);
            var alertWrapper = alertJson.Data as AlertsWrapper;

            Assert.IsTrue(alertWrapper.Successed == false);

            Assert.IsNotNull(alertWrapper.AlertSections);
            Assert.IsTrue(alertWrapper.AlertSections.Count == 2);

            var alertSectionOne = alertWrapper.AlertSections.First();
            Assert.IsTrue(string.IsNullOrWhiteSpace(alertSectionOne.Message));
            Assert.IsTrue(alertSectionOne.Messages.Count() == 3);
            Assert.IsNotNull(alertSectionOne.Messages.First());
            Assert.IsNotNull(alertSectionOne.Messages.ElementAt(1));
            Assert.IsNotNull(alertSectionOne.Messages.ElementAt(2));
            Assert.IsTrue(alertSectionOne.TypeAlert == TypeOfAlert.Success);

            var alertSectionTwo = alertWrapper.AlertSections.ElementAt(1);
            Assert.IsNotNull(alertSectionTwo.Message);
            Assert.IsNull(alertSectionTwo.Messages);
            Assert.IsTrue(alertSectionTwo.TypeAlert == TypeOfAlert.Error);
        }

        [TestMethod]
        public void CompilingWithObjectAndCode()
        {
            var someobject = new { SomeTest = "Test" };
            var alertJson = AlertJson.Compile(TypeOfAlert.Success, someobject, 2, "Lalala");
            Assert.IsNotNull(alertJson);
            Assert.IsTrue(alertJson.Data is AlertsWrapper);
            var alertWrapper = alertJson.Data as AlertsWrapper;
            Assert.IsTrue(alertWrapper.Code == 2);
            Assert.AreEqual(alertWrapper.RelatedData, someobject);
        }

        [TestMethod]
        public void CompilingWithObjectAndCodeAndAlertSection()
        {
            var someobject = new { SomeTest = "Test" };
            var alertJson = AlertJson.Compile(someobject, 2, new AlertSection(TypeOfAlert.Success, "lala"));
            Assert.IsNotNull(alertJson);
            Assert.IsTrue(alertJson.Data is AlertsWrapper);
            var alertWrapper = alertJson.Data as AlertsWrapper;
            Assert.IsTrue(alertWrapper.Code == 2);
            Assert.AreEqual(alertWrapper.RelatedData, someobject);
        }

        #endregion Compile Test

        [TestMethod]
        public AlertSection AlertOneMessage(object jsonData, bool isMessageNull = false)
        {
            Assert.IsTrue(jsonData is AlertsWrapper);
            var alertWrapper = jsonData as AlertsWrapper;
            Assert.IsTrue(alertWrapper.Code == 0);
            Assert.IsNull(alertWrapper.RelatedData);
            Assert.IsNotNull(alertWrapper.AlertSections);
            Assert.IsTrue(alertWrapper.AlertSections.Count == 1);
            var alertSection = alertWrapper.AlertSections.First();
            if (isMessageNull)
            {
                Assert.IsNull(alertSection.Message);
            }
            else
            {
                Assert.IsNotNull(alertSection.Message);
            }
            Assert.IsNull(alertSection.Messages);
            return alertSection;
        }

        [TestMethod]
        public AlertSection AlertManyMessage(object jsonData)
        {
            Assert.IsTrue(jsonData is AlertsWrapper);
            var alertWrapper = jsonData as AlertsWrapper;
            Assert.IsTrue(alertWrapper.Code == 0);
            Assert.IsNull(alertWrapper.RelatedData);
            Assert.IsNotNull(alertWrapper.AlertSections);
            Assert.IsTrue(alertWrapper.AlertSections.Count == 1);
            var alertSection = alertWrapper.AlertSections.First();
            Assert.IsTrue(string.IsNullOrWhiteSpace(alertSection.Message));
            Assert.IsTrue(alertSection.Messages.Count() == 3);
            Assert.IsFalse(string.IsNullOrWhiteSpace(alertSection.Messages.First()));
            Assert.IsFalse(string.IsNullOrWhiteSpace(alertSection.Messages.ElementAt(1)));
            Assert.IsFalse(string.IsNullOrWhiteSpace(alertSection.Messages.ElementAt(2)));
            return alertSection;
        }
    }
}