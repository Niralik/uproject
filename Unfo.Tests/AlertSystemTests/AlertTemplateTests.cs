﻿
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Security.Policy;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.WebPages;
using System.Web.WebSockets;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Web.Infrastructure;
using Moq;
using NUnit.Framework;
using Unfo;
using Unfo.Core;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace UnfoTests.AlertSystemTests
{
    [TestClass]
    public class AlertTemplateTests
    {
        [TestInitialize]
        public void InitializeEnviroment()
        {
            var pathToAlertTemplate = GetPathToAlertTemplate();
            Assert.IsNotNull(pathToAlertTemplate);
            AlertTemplate.AlertTemplatePath = pathToAlertTemplate;
        }

        [TestMethod]
        public void HtmlTemplateFromModelState()
        {
            var modelStateDict = new ModelStateDictionary();
            var modelState = new ModelState();
            modelState.Errors.Add(new ModelError("MessageFirst"));
            modelState.Errors.Add(new ModelError("MessageSecond"));
            modelState.Errors.Add(new ModelError("MessageThird"));
            modelStateDict.Add("test", modelState);
            var alert = new AlertTemplate(modelStateDict.Values);
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-danger\" role=\"alert\">" +
                             "<button type=\"button\" class=\"close alert-close\">" +
                             "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                             "<h4>Внимание! Обнаружены ошибки!</h4>" +
                             "<ul class=\"alert-ul\">" +
                             "<li>MessageFirst</li>" +
                             "<li>MessageSecond</li>" +
                             "<li>MessageThird</li></ul></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ","");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateFromIdentityResultManyError()
        {
            var identityResult = new IdentityResult("MessageFirst", "MessageSecond", "MessageThird");
            var alert = new AlertTemplate(identityResult);
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-danger\" role=\"alert\">" +
                             "<button type=\"button\" class=\"close alert-close\">" +
                             "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                             "<h4>Внимание! Обнаружены ошибки!</h4>" +
                             "<ul class=\"alert-ul\">" +
                             "<li>MessageFirst</li>" +
                             "<li>MessageSecond</li>" +
                             "<li>MessageThird</li></ul></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateFromIdentityResultOneError()
        {
            var identityResult = new IdentityResult("FirstError");
            var alert = new AlertTemplate(identityResult);
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-danger\" role=\"alert\">" +
                             "<button type=\"button\" class=\"close alert-close\">" +
                             "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                             "<h4>Внимание! Обнаружена ошибка!</h4>" +
                             "<span>FirstError</span></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateFromIdentityResultWithOutError()
        {
            var identityResult = new IdentityResult();
            var alert = new AlertTemplate(identityResult);
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Html == string.Empty);
        }
        [TestMethod]
        public void HtmlTemplateSuccessOneMsg()
        {
            var alert = new AlertTemplate(TypeOfAlert.Success, "Message");
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-success\" role=\"alert\">" +
                             "<button type=\"button\" class=\"close alert-close\">" +
                             "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                             "<h4>Поздравляем вас. Операция завершена успешно!</h4>" +
                             "<span>Message</span></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateSuccessManyMsg()
        {
            var alert = new AlertTemplate(TypeOfAlert.Success, "MessageFirst", "MessageSecond", "MessageThird");
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-success\" role=\"alert\">" +
                         "<button type=\"button\" class=\"close alert-close\">" +
                         "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                         "<h4>Поздравляем вас. Операция завершена успешно!</h4>" +
                         "<ul class=\"alert-ul\">" +
                         "<li>MessageFirst</li>" +
                         "<li>MessageSecond</li>" +
                         "<li>MessageThird</li></ul></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateErrorOneMsg()
        {
            var alert = new AlertTemplate(TypeOfAlert.Error, "Message");
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-danger\" role=\"alert\">" +
                             "<button type=\"button\" class=\"close alert-close\">" +
                             "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                             "<h4>Внимание! Обнаружена ошибка!</h4>" +
                             "<span>Message</span></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        [TestMethod]
        public void HtmlTemplateErrorManyMsg()
        {
            var alert = new AlertTemplate(TypeOfAlert.Error, "MessageFirst", "MessageSecond", "MessageThird");
            Assert.IsNotNull(alert);
            var assertHtml = ("<div class=\"alert active alert-danger\" role=\"alert\">" +
                         "<button type=\"button\" class=\"close alert-close\">" +
                         "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
                         "<h4>Внимание! Обнаружены ошибки!</h4>" +
                         "<ul class=\"alert-ul\">" +
                         "<li>MessageFirst</li>" +
                         "<li>MessageSecond</li>" +
                         "<li>MessageThird</li></ul></div>").Replace(" ", "");
            var alertHtml = alert.Html.Replace(" ", "");
            Assert.IsTrue(assertHtml == alertHtml);
        }
        
        public string GetPathToAlertTemplate()
        {
            //Наркоманский метод - пытаемся найти путь реальный до шаблона - отработан так же момент когда тест запускается внутри доммена приложения Нкранча
            string pathToAlertTemplate = null;
            var currentBaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            const string rootPathToAlertTemplate = "\\Scripts\\Unfo\\CustomScripts\\AlertEngine\\alert-block.handlebars";
            var directoryInfo = Directory.GetParent(currentBaseDirectory).Parent;
            DirectoryInfo unfoProjectDirectory = null;
            if (directoryInfo != null)
            {
                unfoProjectDirectory = directoryInfo.Parent;
            }
            if (unfoProjectDirectory != null)
            {
                var unfoDirectoryStandartTest = unfoProjectDirectory.GetDirectories("Unfo").FirstOrDefault();
                if (unfoDirectoryStandartTest != null) pathToAlertTemplate = unfoDirectoryStandartTest.FullName + rootPathToAlertTemplate;
            }
            if (pathToAlertTemplate == null)
            {
                var rootDisc = Path.GetPathRoot(currentBaseDirectory);
                pathToAlertTemplate = rootDisc + currentBaseDirectory.Split(new[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Skip(8).Take(4).Aggregate((a, b) => a + "\\" + b) + "\\Unfo" + rootPathToAlertTemplate;
            }
            return pathToAlertTemplate;
        }

        [TestCleanup]
        public void End()
        {
            AlertTemplate.AlertTemplatePath = null;
        }
    }
}