﻿

SET IDENTITY_INSERT [dbo].[ServiceSections] ON
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (1, N'Программа', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (2, N'Расписание участника', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (3, N'Участники', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (4, N'Спикеры', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (5, N'Партнеры', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (6, N'Организаторы', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (7, N'Объявления', 0)
INSERT INTO [dbo].[ServiceSections] (ID, Name, IsDelete) VALUES (8, N'Голосование', 0)
SET IDENTITY_INSERT [dbo].[ServiceSections] OFF



SET IDENTITY_INSERT [dbo].[Countries] ON
INSERT INTO [dbo].[Countries] (Id, Name, IsDelete) VALUES (1, N'[{"k":"ru", "v":"Россия"},{"k":"en", "v":"Russia"}]', 0)
INSERT INTO [dbo].[Countries] (Id, Name, IsDelete) VALUES (2, N'[{"k":"ru", "v":"Германия"},{"k":"en", "v":"Germany"}]', 0)
INSERT INTO [dbo].[Countries] (Id, Name, IsDelete) VALUES (3, N'[{"k":"ru", "v":"Украина"},{"k":"en", "v":"Ukraine"}]', 0)
SET IDENTITY_INSERT [dbo].[Countries] OFF