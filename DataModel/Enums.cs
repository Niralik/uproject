﻿using System;
using DataModel.Core.Attributes;

namespace DataModel
{
    public enum Role
    {
        /// <summary>
        /// Бог
        /// </summary>
        [RoleId]
        Admin,
        /// <summary>
        /// Обычный зарегестированный смертный
        /// </summary>
        [RoleId]
        User,
        /// <summary>
        /// Пользователь который обладает способностью создавать мероприятия
        /// </summary>
        [RoleId]
        Organizer,
        /// <summary>
        /// Роль говорящая что на одном из каких либо событий у пользователя есть роль модератора
        /// </summary>
        [RoleId]
        Moderator,
        /// <summary>
        /// Псевдо роль - активна в пределах контекста текущего события 
        /// и если на данном событие пользователь имеет права модерирования
        /// </summary>
        
        ModeratorOfCurrentEvent
    }
}
