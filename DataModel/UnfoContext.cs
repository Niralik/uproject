﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DataModel
{

    public class UnfoDbContext : IdentityDbContext<UnfoUser, UnfoRole, Guid, UnfoUserLogin, UnfoUserRole, UnfoUserClaim>
    {
        //public DbSet<User> Users { get; set; }
        public DbSet<UEvent> Events { get; set; }

        public DbSet<Report> Reports { get; set; }
        public DbSet<Speaker> Speakers { get; set; }
        //public DbSet<SubReport> SubReports { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Hall> Halls { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Direction> Directions { get; set; }
        public DbSet<SpeakerForReport> SpeakerForReports { get; set; }
        //public DbSet<DirectionForReport> DirectionForReport { get; set; }
        public DbSet<Payer> Organizators { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<SubscriptionToEvent> SubscriptionToEvents { get; set; }
        //public DbSet<Venue> Venues { get; set; }
        public DbSet<ServiceSection> ServiceSections { get; set; }
        public DbSet<ServiceSectionInEvent> ServiceSectionInEvents { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<VoteQuestion> VoteQuestions { get; set; }
        public DbSet<VoteResult> VoteResults { get; set; }
        public DbSet<EventModerator> EventModerators { get; set; }
        public DbSet<PrivacyPolitic> PrivacyPolitics { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<UnfoUserRole> UserRole { get; set; }
        public DbSet<EventOrganization> EventOrganizations { get; set; }
        public DbSet<QuestionsForSpeaker> QuestionsForSpeaker { get; set; }

        public DbSet<UploadImage> UploadImages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public UnfoDbContext()
            : base("UnfoContext")
        {
        }

        static UnfoDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer(new UnfoDbInitializer());

        }
        public static UnfoDbContext Create()
        {
            return new UnfoDbContext();
        }
    }

    // This is useful if you do not want to tear down the database each time you run the application.
    // public class ApplicationDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    // This example shows you how to create a new database if the Model changes
    internal class UnfoDbInitializer: DropCreateDatabaseIfModelChanges<UnfoDbContext>
    {
        private const int MaxCountItems = 30;
        private const int MinCountItems = 10;
        private const int CountUsers = 100;

        protected override void Seed(UnfoDbContext context)
        {
            InitializeRoles(context);
            InitializeAdmin(context);
            GenerateTestUsers(context);
            GenerateTestEvents(context);
            LinkUsersWithEvents(context);
            AddUsersRequest(context);
            GenerateTestReports(context);
            base.Seed(context);
        }
        internal static void GenerateTestEvents(UnfoDbContext db)
        {
            for (int i = 0; i < MaxCountItems; i++)
            {
                var random = new Random();
               
                var seedTestEvent = new UEvent();
                seedTestEvent.Name = "[{k:\"ru\", v:\"Сиид Тест Евент Нейм " + i + "\"},{k:\"en\", v:\"Seed test event name" + i + "\"}]";
                seedTestEvent.TranslitName = "Seed-Test-Event-Name-"+i;
                seedTestEvent.Color = "red";
                if (i%2 == 0)
                {
                    seedTestEvent.PromoCode = "123456";
                    seedTestEvent.IsPrivate = true;
                }
                seedTestEvent.Description = "[{k:\"ru\", v:\"Русский Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, at, exercitationem, et laborum placeat a sunt error corporis optio nam obcaecati recusandae ullam porro ipsa voluptas autem hic magni aspernatur deleniti ipsum quo praesentium quod adipisci repellendus facilis quam voluptate! Quo iste rerum at possimus iure deleniti impedit facere magnam. Quasi, ipsam minima dolor delectus in. Fugit, eligendi, cum, est ea quo sequi porro repudiandae facere architecto praesentium ipsa suscipit corrupti beatae dignissimos quaerat quisquam vero. Sunt, eum, quisquam, culpa alias sed excepturi tenetur voluptatum tempora quaerat error amet similique molestiae veritatis iusto hic modi reprehenderit rerum dolores quasi vitae eos fugit quod dolorem. Quae laboriosam est aliquid expedita minima nam accusamus nemo distinctio eveniet ipsa. Vitae, doloribus, facilis, iste, itaque tenetur labore ab incidunt sit porro non saepe mollitia. Et, magni, assumenda maiores eligendi enim eius excepturi error id a iste odit sapiente iure ipsa quasi nulla porro repudiandae.\"},{k:\"en\", v:\"English Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, at, exercitationem, et laborum placeat a sunt error corporis optio nam obcaecati recusandae ullam porro ipsa voluptas autem hic magni aspernatur deleniti ipsum quo praesentium quod adipisci repellendus facilis quam voluptate! Quo iste rerum at possimus iure deleniti impedit facere magnam. Quasi, ipsam minima dolor delectus in. Fugit, eligendi, cum, est ea quo sequi porro repudiandae facere architecto praesentium ipsa suscipit corrupti beatae dignissimos quaerat quisquam vero. Sunt, eum, quisquam, culpa alias sed excepturi tenetur voluptatum tempora quaerat error amet similique molestiae veritatis iusto hic modi reprehenderit rerum dolores quasi vitae eos fugit quod dolorem. Quae laboriosam est aliquid expedita minima nam accusamus nemo distinctio eveniet ipsa. Vitae, doloribus, facilis, iste, itaque tenetur labore ab incidunt sit porro non saepe mollitia. Et, magni, assumenda maiores eligendi enim eius excepturi error id a iste odit sapiente iure ipsa quasi nulla porro repudiandae.\"}]";
                seedTestEvent.OwnerId = db.Users.Local.Where(user => user.OwnedEvents == null || user.OwnedEvents.Count == 0).ElementAt(random.Next(MaxCountItems)).Id;
                seedTestEvent.FinishDate = new DateTime(2014, 12, 31);
                seedTestEvent.ID = Guid.NewGuid();
                seedTestEvent.StartDate = new DateTime(2014, 8, 14);
                seedTestEvent.SupportLanguages = "ru,en";
                db.Events.Add(seedTestEvent);
            }
        }

        internal static void GenerateTestReports(UnfoDbContext db)
        {
            foreach (var @event in db.Events.Local)
            {
                var counter = 0;
               
                var random = new Random();
                for (int i = 0; i < MinCountItems; i++)
                {
                    counter++;
                    var hall = new Hall()
                    {
                        Name = "[{k:\"ru\", v:\"Зал " + i + "\"},{k:\"en\", v:\"Hall" + i + "\"}]",
                        EventID = @event.ID,
                        ID = Guid.NewGuid()
                    };
                    db.Halls.Add(hall);


                    var section = new Section()
                    {
                        Name = "[{k:\"ru\", v:\"Секция " + i + "\"},{k:\"en\", v:\"Section" + i + "\"}]",
                        EventID = @event.ID,
                        ID = Guid.NewGuid()
                    };
                    db.Sections.Add(section);


                    var direction = new Direction()
                    {
                        Name = "[{k:\"ru\", v:\"Направление " + i + "\"},{k:\"en\", v:\"Direction" + i + "\"}]",
                        EventID = @event.ID,
                        ID = Guid.NewGuid()
                    };
                    db.Directions.Add(direction);

                    var speaker = new Speaker()
                    {
                        Name = "[{k:\"ru\", v:\"Докладчик " + i + "\"},{k:\"en\", v:\"Speaker" + i + "\"}]",
                        Email = "test@test" + i + ".ru",
                        EventID = @event.ID,
                        ID = Guid.NewGuid(),

                    };
                    db.Speakers.Add(speaker);
                }

                counter = 0;
                for (int i = 0; i < MinCountItems; i++)
                {
                    counter++;
                    var report = new Report();
                    report.ID = Guid.NewGuid();
                    report.EventID = @event.ID;

                    report.HallID = db.Halls.Local.Where(d => !d.IsDelete && d.EventID == @event.ID).ElementAt(random.Next(MinCountItems)).ID;
                    report.SectionID = db.Sections.Local.Where(d => !d.IsDelete && d.EventID == @event.ID).ElementAt(random.Next(MinCountItems)).ID;
                    report.DirectionID = db.Directions.Local.Where(d => !d.IsDelete && d.EventID == @event.ID).ElementAt(random.Next(MinCountItems)).ID;
                    report.Name = "[{k:\"ru\", v:\"Доклад " + counter + "\"},{k:\"en\", v:\"Report" + counter + "\"}]";
                    report.ShortDescription = "[{k:\"ru\", v:\"Краткое описание " + counter +
                                              "\"},{k:\"en\", v:\"Short Descr" + counter + "\"}]";
                    report.FullDescription = "[{k:\"ru\", v:\"Полное описание " + counter +
                                             "\"},{k:\"en\", v:\"Full Descr" + counter + "\"}]";
                    report.StartDateTime = new DateTime(2014, 12, 10);
                    report.FinishDateTime = new DateTime(2014, 12, 31);
                    report.UpdateDate = new DateTime();

                    db.Reports.Add(report);

                    var speakerForReport = new SpeakerForReport();
                    speakerForReport.CreateDate = new DateTime(2014, 12, 10);
                    speakerForReport.SpeakerID = db.Speakers.Local.Where(d=> d.EventID == @event.ID).ElementAt(random.Next(MinCountItems)).ID;
                    speakerForReport.ReportID = report.ID;
                    speakerForReport.EventID = @event.ID;
                    speakerForReport.ReportName = "[{k:\"ru\", v:\"Поддоклад " + counter +
                                                  "\"},{k:\"en\", v:\"Поддоклад" + counter + "\"}]";

                    db.SpeakerForReports.Add(speakerForReport);

                }
            }
        }

        internal static void InitializeRoles(UnfoDbContext db)
        {
            var organizer = new UnfoRole(Role.Organizer);
            var justUser = new UnfoRole(Role.User);
            var admin = new UnfoRole(Role.Admin);
            var moderator = new UnfoRole(Role.Moderator);
            db.Roles.Add(organizer);
            db.Roles.Add(justUser);
            db.Roles.Add(admin);
            db.Roles.Add(moderator);
        }

        internal static void InitializeAdmin(UnfoDbContext db)
        {
            var userManager = new UserManager<UnfoUser, Guid>(new UserStore<UnfoUser, UnfoRole, Guid, UnfoUserLogin, UnfoUserRole, UnfoUserClaim>(db));
            const string name = "admin@example.com";
            const string firstName = "Админко";
            const string fatherName = "Повелитель";
            const string lastName = "Всемогущий";
            const string phone = "89329999999";
            const string placeOfJob = "Smart Code";
            const string aboutYourself = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            const string socials = "http://vk.com/adminSmartCode";
            const string Position = "Великий админ селиконовой долины";

            var user = new UnfoUser
                {
                    UserName = name,
                    Email = name,
                    FirstName = firstName,
                    FatherName = fatherName,
                    SurName = lastName,
                    PhoneNumber = phone,
                    PlaceOfJob = placeOfJob,
                    AboutYourselfInformation = aboutYourself,
                    VkontakteLink = socials,
                    GoogleLink = socials,
                    FacebookLink = socials,
                    TwitterLink = socials,
                    PositionAtJob = Position
                }.Initialize();

            var passwordHash = userManager.PasswordHasher.HashPassword("123456");
            user.PasswordHash = passwordHash;
            user.SecurityStamp = Guid.NewGuid().ToString();
            db.Users.Add(user);
            
            var userRole = new UnfoUserRole();
            userRole.UserId = user.Id;
            userRole.RoleId = db.Roles.Local.Single(role => role.Name == Role.Admin.ToString()).Id;
            db.UserRole.Add(userRole);

        }

        internal static void GenerateTestUsers(UnfoDbContext db)
        {
            var userManager = new UserManager<UnfoUser, Guid>(new UserStore<UnfoUser, UnfoRole, Guid, UnfoUserLogin, UnfoUserRole, UnfoUserClaim>(db));
      
            const string firstName =  "Тестер";
            const string fatherName = "Болванкович";
            const string lastName = "Тестеров";
            const string phone = "89329999999";

            const string placeOfJob = "Smart Code";
            const string aboutYourself = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            const string socials =    "http://vk.com/tester";
            const string Position =   "Великий тестер среди тестеров";


            for (int i = 0; i < CountUsers; i++)
            {
                var name = String.Format("tester{0}@tester.com", i);
                var user = new UnfoUser
                {
                    UserName = name,
                    Email = name,
                    FirstName = firstName,
                    FatherName = fatherName,
                    SurName = lastName,
                    PhoneNumber = phone,
                    PlaceOfJob = placeOfJob,
                    AboutYourselfInformation = aboutYourself,
                    VkontakteLink = socials,
                    GoogleLink = socials,
                    FacebookLink = socials,
                    TwitterLink = socials,
                    PositionAtJob = Position
                }.Initialize();
                if (i % 2 == 0 || i % 3 == 0)
                {
                    foreach (var politic in user.PrivacyPolitics)
                    {
                        politic.IsPrivate = false;
                    }


                }
                var passwordHash = userManager.PasswordHasher.HashPassword("123456");
                user.PasswordHash = passwordHash;
                user.SecurityStamp = Guid.NewGuid().ToString();
                db.Users.Add(user);

                var userRole = new UnfoUserRole();
                userRole.UserId = user.Id;

                if (i % 4 == 0)
                {
                    userRole.RoleId = db.Roles.Local.Single(role => role.Name == Role.Admin.ToString()).Id;
                }

                if (i % 2 == 0 && userRole.RoleId == Guid.Empty)
                {
                    userRole.RoleId = db.Roles.Local.Single(role => role.Name == Role.Organizer.ToString()).Id;

                } 
               
                if (userRole.RoleId == Guid.Empty)
                {
                    userRole.RoleId = db.Roles.Local.Single(role => role.Name == Role.User.ToString()).Id;
                }
                db.UserRole.Add(userRole);
                

            }
           
        }
        /// <summary>
        /// Добавляем подписчиков и назначаем модераторов
        /// </summary>
        /// <param name="db"></param>
        internal static void LinkUsersWithEvents(UnfoDbContext db)
        {
            var random = new Random();
            var modertorRoleId = db.Roles.Local.Single(role => role.Name == Role.Moderator.ToString()).Id;
            var users = db.Users.Local.Take(random.Next(25, 65)).ToList();
            foreach (var @event in db.Events.Local)
            {
                int countModerators = random.Next(3, 7);
                int countSubscribers = random.Next(10, 25);

                if(@event.Moderators == null ) @event.Moderators= new Collection<EventModerator>();

                for (int i = 0; i < countModerators; i++)
                {
                    var usersWithoutModeratedEvents = users.Where(u => u.ModeratedEvents == null || u.ModeratedEvents.Count <= 1).ToList();
                    if (!usersWithoutModeratedEvents.Any()) continue;
                    var user = usersWithoutModeratedEvents.ElementAt(random.Next(usersWithoutModeratedEvents.Count));
                    if (@event.Moderators.Any(moderators => moderators.UserID == user.Id)) continue;
                    var eventModeratorItem = new EventModerator();
                    eventModeratorItem.ID = Guid.NewGuid();
                    eventModeratorItem.UserID = user.Id;
                    eventModeratorItem.EventID = @event.ID;
                    @event.Moderators.Add(eventModeratorItem);

                    var userModerator = new UnfoUserRole {RoleId = modertorRoleId, UserId = user.Id};
                    if (
                        !db.UserRole.Local.Any(
                            userModer => userModer.RoleId == modertorRoleId && userModer.UserId == user.Id))
                    {
                        db.UserRole.Add(userModerator);
                    }

                    if (user.ModeratedEvents == null) user.ModeratedEvents = new Collection<EventModerator>();
                    user.ModeratedEvents.Add(eventModeratorItem);
                }


                for (int i = 0; i < countSubscribers; i++)
                {

                    var subscriptions = new SubscriptionToEvent();
                    subscriptions.ID = Guid.NewGuid();
                    subscriptions.EventID = @event.ID;
                    var user = db.Users.Local.ElementAt(i);
                    subscriptions.UserID = user.Id;
                    db.SubscriptionToEvents.Add(subscriptions);
                }
                
            }
        }
        /// <summary>
        /// Добавляем подписчиков и назначаем модераторов
        /// </summary>
        /// <param name="db"></param>
        internal static void AddUsersRequest(UnfoDbContext db)
        {
            var random = new Random();
            foreach (var user in db.Users.Local.Take(random.Next(30,55)))
            {
                var request = new Request()
                {
                    AuthorID = user.Id,
                    CreateRequestDate = DateTime.Now,
                    ID = Guid.NewGuid(),
                    RequestState = RequestState.InProgress,
                    RequestType = TypeOfRequest.GetOrganizer,
                    RequestDescription =
                        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, at, exercitationem, et laborum placeat a sunt error corporis optio nam obcaecati recusandae ullam porro ipsa voluptas autem hic magni aspernatur deleniti ipsum quo praesentium quod adipisci repellendus facilis quam voluptate! Quo iste rerum at possimus iure deleniti impedit facere magnam. Quasi, ipsam minima dolor delectus in. Fugit, eligendi, cum, est ea quo sequi porro repudiandae facere architecto praesentium ipsa suscipit corrupti beatae dignissimos quaerat quisquam vero. Sunt, eum, quisquam, culpa alias sed excepturi tenetur voluptatum tempora quaerat error amet similique molestiae veritatis iusto hic modi reprehenderit rerum dolores quasi vitae eos fugit quod dolorem. Quae laboriosam est aliquid expedita minima nam accusamus nemo distinctio eveniet ipsa. Vitae, doloribus, facilis, iste, itaque tenetur labore ab incidunt sit porro non saepe mollitia. Et, magni, assumenda maiores eligendi enim eius excepturi error id a iste odit sapiente iure ipsa quasi nulla porro repudiandae.",
                };
                db.Requests.Add(request);
            }
        }
    }

  
}