﻿using System.Threading.Tasks;
using DataModel.ModelInterfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DataModel
{
    public class GenericRepository<TEntity> : IDisposable where TEntity : class
    {
        public UnfoDbContext DbContext;
        public DbSet<TEntity> DbSet;

        public GenericRepository(UnfoDbContext dbContext)
        {
            this.DbContext = dbContext;
            this.DbSet = dbContext.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> All()
        {
            return Get();
        }
        public virtual async Task<IEnumerable<TEntity>> AllAsync()
        {
            return await GetAsync();
        }
        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return GetQuery(filter, orderBy, includeProperties).ToList();
        }
        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await GetQuery(filter, orderBy, includeProperties).ToListAsync();
        }

        public IQueryable<TEntity> GetQuery(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (includeProperties == null) return orderBy != null ? orderBy(query) : query;

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? orderBy(query) : query;
        }

        public virtual IEnumerable<TVm> GetVM<TVm>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties) where TVm : IViewModelBuilder<TEntity>, new()
        {
            var obtainedItems = Get(filter, orderBy, includeProperties);
            return ConstructViewModels<TVm>(obtainedItems);     
        }
        public virtual async Task<IEnumerable<TVm>> GetVMAsync<TVm>(
           Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           params Expression<Func<TEntity, object>>[] includeProperties) where TVm : IViewModelBuilder<TEntity>, new()
        {
            var obtainedItems = await GetAsync(filter, orderBy, includeProperties);
            return ConstructViewModels<TVm>(obtainedItems);
        }
        private static IEnumerable<TVm> ConstructViewModels<TVm>(IEnumerable<TEntity> obtainedItems) where TVm : IViewModelBuilder<TEntity>, new()
        {
            var r = new List<TVm>();
            foreach (var item in obtainedItems)
            {
                var vm = new TVm();
                vm.Fill(item);
                r.Add(vm);
            }
            return r;
        }

        public virtual TVm GetById<TVm>(object id) where TVm : IViewModelBuilder<TEntity>, new()
        {
            var item = DbSet.Find(id);
            var result = new TVm();
            result.Fill(item);
            return result;
        }

        public virtual TEntity GetById(object id)
        {
            return DbSet.Find(id);
        }
        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await DbSet.FindAsync(id);
        }
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(IModelBuilder<TEntity> entityToDelete)
        {
            var entity = entityToDelete.GetModel();
            Delete(entity);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (DbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }
        public virtual void Delete(params TEntity[] entitiesToDelete)
        {
            foreach (var entity in entitiesToDelete)
            {
                Delete(entity);
            }
        }
        public virtual void Update(IModelBuilder<TEntity> entityToUpdate)
        {
            var entity = entityToUpdate.GetModel();
            Update(entity);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            DbContext.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public virtual void Update(List<TEntity> entitiesToUpdate)
        {
           entitiesToUpdate.ForEach(Update);
        }

        public virtual void Insert(IModelBuilder<TEntity> entity)
        {
            Insert(entity.GetModel());
        }

        public virtual void Insert(TEntity entity)
        {
            if (entity is IBaseModel && ((IBaseModel)entity).ID == default(Guid))
            {
                ((IBaseModel)entity).ID = Guid.NewGuid();
            }
            DbSet.Add(entity);
      
        }

        public virtual void Insert(params TEntity[] entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public virtual void Save()
        {
            DbContext.SaveChanges();
        }
        public virtual async Task SaveAsync()
        {
           await DbContext.SaveChangesAsync();
        }
        public void Dispose()
        {
            if (DbContext != null)
                DbContext.Dispose();
        }
    }
}