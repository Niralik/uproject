﻿using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OrangeJetpack.Localization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

// Описание классов модели

namespace DataModel
{
    public enum ContentType
    {
        Picture = 1,
        Video = 2,
        Presentation = 3,
        Certificate = 4
    }

    public enum TypeOfPayment
    {
        Card = 1,
        Cash = 2
    }

    public class Model : IBaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public bool IsDelete { get; set; }
    }

    public class BaseModel : Model, IBaseWithNameModel, ILocalizable
    {
        private string _name;

        [Localized]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    public class EventBaseModel : BaseModel, IBaseForEventModel, ICloneable
    {
        public Guid EventID { get; set; }

        [ForeignKey("EventID")]
        public virtual UEvent Event { get; set; }

        public object Clone()
        {
            var t = EnumResources.TypePrivacyPolitic_PlaceOfJob;
            return this.MemberwiseClone();
        }
    }

    #region Identity inheritors
    public class UnfoUserRole : IdentityUserRole<Guid>
    {

        [ForeignKey("UserId")]
        public virtual UnfoUser User { get; set; }

        [ForeignKey("RoleId")]
        public virtual UnfoRole Role { get; set; }
    }

    public class UnfoUserClaim : IdentityUserClaim<Guid>
    {
    }

    public class UnfoUserLogin : IdentityUserLogin<Guid>
    {
    }
    public class UnfoRole : IdentityRole<Guid, UnfoUserRole>
    {
        public UnfoRole(Role role)
        {
            Name = role.ToString();
            Id = Guid.NewGuid();
        }

        public UnfoRole()
        {
        }
    }
    #endregion Identity inheritors

    public enum TypePrivacyPolitic
    {
        [Display(ResourceType = typeof(EnumResources), Name = "TypePrivacyPolitic_Phone")]
        Phone,
        [Display(ResourceType = typeof(EnumResources), Name = "TypePrivacyPolitic_Email")]
        Email,
        [Display(ResourceType = typeof(EnumResources), Name = "TypePrivacyPolitic_PlaceOfJob")]
        PlaceOfJob
    }

    public class PrivacyPolitic : Model
    {
        public TypePrivacyPolitic TypePrivacyPolitic { get; set; }
        public bool IsPrivate { get; set; }
        public Guid UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual UnfoUser UnfoUser { get; set; }
        public PrivacyPolitic(UnfoUser user, TypePrivacyPolitic typePrivacyPolitic)
        {
            TypePrivacyPolitic = typePrivacyPolitic;
            UnfoUser = user;
            IsPrivate = true;
            ID = Guid.NewGuid();
        }
        public PrivacyPolitic()
        {
        }
    }

    public enum TypeOfRequest
    {
        GetOrganizer
    }

    public enum RequestState
    {
        NoState,
        Completed,
        Aborted,
        InProgress
    }
    /// <summary>
    ///  Класс хранящий запросы пользователя
    /// </summary>
    public class Request : Model
    {
        public RequestState RequestState { get; set; }
        public TypeOfRequest RequestType { get; set; }
        /// <summary>
        /// Пользовательское описание запроса
        /// </summary>
        public string RequestDescription { get; set; }
        /// <summary>
        /// Ответ администрации на запрос
        /// </summary>
        public string ResponseDescription { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime CreateRequestDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime UpdateRequestDate { get; set; }
        public Guid AuthorID { get; set; }
        [ForeignKey("AuthorID")]
        public virtual UnfoUser Author { get; set; }
        public Guid? AdminID { get; set; }
        [ForeignKey("AdminID")]
        public virtual UnfoUser Admin { get; set; }
    }

    public class UnfoUser : IdentityUser<Guid, UnfoUserLogin, UnfoUserRole, UnfoUserClaim>, IUnfoUser
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string FatherName { get; set; }

        /// <summary>
        /// О себе
        /// </summary>
        public string AboutYourselfInformation { get; set; }

        /// <summary>
        /// Место работы
        /// </summary>
        public string PlaceOfJob { get; set; }

        /// <summary>
        /// Занимаемая должность
        /// </summary>
        public string PositionAtJob { get; set; }

        public Guid PhotoId { get; set; }

        public string Location { get; set; }

        public string FacebookLink { get; set; }

        public string VkontakteLink { get; set; }

        public string TwitterLink { get; set; }

        public string GoogleLink { get; set; }
        public string WebSite { get; set; }
        public string SkypeId { get; set; }
        public bool IsDelete { get; set; }

        /// <summary>
        /// Дата последнего входа
        /// </summary>
        [Column(TypeName = "datetime2")]
        public DateTime LastSignInDate { get; set; }
        /// <summary>
        /// Дата регистрации
        /// </summary>
        [Column(TypeName = "datetime2")]
        public DateTime SignUpDate { get; set; }
        /// <summary>
        /// Настройки политик приватности
        /// </summary>
        public virtual ICollection<PrivacyPolitic> PrivacyPolitics { get; set; }
        /// <summary>
        /// Запросы пользователя
        /// </summary>
        public virtual ICollection<Request> Requests { get; set; }
        /// <summary>
        /// События на которых данный пользователь модератор
        /// </summary>
        public virtual ICollection<EventModerator> ModeratedEvents { get; set; }
        /// <summary>
        /// События на которые данный пользователь подписан
        /// </summary>
        public virtual ICollection<SubscriptionToEvent> EventsSubscriptions { get; set; }
        /// <summary>
        /// События создателем которых выступал данный пользователь
        /// </summary>
        public virtual ICollection<UEvent> OwnedEvents { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UnfoUser, Guid> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public UnfoUser()
        {
        }

        public UnfoUser Initialize()
        {
            if (Id != default(Guid)) return this;
            Id = Guid.NewGuid();
            SignUpDate = DateTime.Now;
            InitializePrivacyPolitics();
            return this;
        }
        private void InitializePrivacyPolitics()
        {
            PrivacyPolitics = new Collection<PrivacyPolitic>();
            foreach (TypePrivacyPolitic privacyPolitic in Enum.GetValues(typeof(TypePrivacyPolitic)))
            {
                PrivacyPolitics.Add(new PrivacyPolitic(this, privacyPolitic));
            }
        }

    }

    /// <summary>
    /// Глобальное событие типа "ИнфоТех"
    /// </summary>
    public class UEvent : BaseModel
    {
        public string Description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime FinishDate { get; set; }

        public string PromoCode { get; set; }

        public bool IsPrivate { get; set; }

        public string Color { get; set; }

        public string HashTag { get; set; }

        public string WebSite { get; set; }

        public string TranslitName { get; set; }

        public virtual ICollection<PayerForEvent> Payers { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<Speaker> Speakers { get; set; }

        public virtual ICollection<Direction> Directions { get; set; }

        public virtual ICollection<Hall> Halls { get; set; }

        public virtual ICollection<Section> Sections { get; set; }
        /// <summary>
        /// Модераторы данного события
        /// </summary>
        public virtual ICollection<EventModerator> Moderators { get; set; }
        /// <summary>
        /// Подписчики данного события
        /// </summary>
        public virtual ICollection<SubscriptionToEvent> Subscribers { get; set; }
        /// <summary>
        /// Места встреч проведения конкретных докладов
        /// </summary>
        //public virtual ICollection<Venue> Venues { get; set; }

        public virtual ICollection<ServiceSectionInEvent> ServiceSections { get; set; }

        /// <summary>
        /// Ссылки на приложения (если заказано брендированное приложение)
        /// </summary>
        public string ApplicationLinkIOS { get; set; }

        public string ApplicationLinkAndroid { get; set; }

        /// <summary>
        ///Сертификат для IOS PUSH (для брендированного приложения)
        /// </summary>
        public Guid? CertificateId { get; set; }

        /// <summary>
        ///  Пароль для сертификата
        /// </summary>
        public string CertificatePassword { get; set; }

        /// <summary>
        /// ключ для PUSH для Android
        /// </summary>
        public string GooglePushAPIKey { get; set; }

        #region дополнительные услуги

        /// <summary>
        /// /брендирование
        /// </summary>
        public bool IsBranding { get; set; }

        /// <summary>
        /// Сопровождение
        /// </summary>
        public bool IsSupport { get; set; }

        /// <summary>
        /// локализация
        /// </summary>
        public bool IsLocalization { get; set; }

        /// <summary>
        /// Поддерживаемые Языки, перечисляются через запятую вида "ru,en ..."
        /// </summary>
        public string SupportLanguages { get; set; }

        #endregion дополнительные услуги

        public virtual ICollection<Vote> Votings { get; set; }

        public virtual ICollection<VoteQuestion> VoteQuestions { get; set; }

        public virtual ICollection<Report> Reports { get; set; }

        public int TarifId { get; set; }

        public Guid? LocationId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public Guid? OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public virtual UnfoUser Owner { get; set; }
    }
    /// <summary>
    /// Подписка пользователя на мероприятие
    /// </summary>

    public class SubscriptionToEvent : Model
    {
        public Guid EventID { get; set; }
        /// <summary>
        /// Мероприятие на которое подписан пользователь
        /// </summary>
        [ForeignKey("EventID")]
        public virtual UEvent Event { get; set; }

        public Guid UserID { get; set; }
        /// <summary>
        /// Пользователь который подписан на мероприятие
        /// </summary>
        [ForeignKey("UserID")]
        public virtual UnfoUser UnfoUser { get; set; }
    }


    /// <summary>
    /// Расписание пользователя, связь с докладами
    /// </summary>
    public class SubscriptionToReport : Model
    {
        public Guid UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual UnfoUser UnfoUser { get; set; }

        public Guid ReportID { get; set; }
        [ForeignKey("ReportID")]
        public virtual Report Report { get; set; }
    }


    /// <summary>
    /// Модератор определенного события
    /// </summary>
    public class EventModerator : Model
    {
        public Guid EventID { get; set; }
        /// <summary>
        /// Модерируемое событие
        /// </summary>
        [ForeignKey("EventID")]
        public virtual UEvent Event { get; set; }

        public Guid UserID { get; set; }
        /// <summary>
        /// Модератор данного события
        /// </summary>
        [ForeignKey("UserID")]
        public virtual UnfoUser UnfoUser { get; set; }
        /// <summary>
        /// Дата делегирования пользователю возможности модерировать событие
        /// </summary>
        [Column(TypeName = "datetime2")]
        public DateTime DateAtachToModerators { get; set; }
        /// <summary>
        /// Дата удаления роли модератора у пользователя
        /// </summary>
        [Column(TypeName = "datetime2")]
        public DateTime DateDetachFromModerators { get; set; }
        /// <summary>
        /// Флаг отмечающий что у данного пользователя изъяты права модерации данного события
        /// </summary>
        public bool IsDelete { get; set; }

        public void DetachFromModerators()
        {
            IsDelete = true;
            DateDetachFromModerators = DateTime.Now;
        }
    }

    public class Report : EventBaseModel
    {
        [Localized]
        public string ShortDescription { get; set; }

        [Localized]
        public string FullDescription { get; set; }

        public bool IsPublished { get; set; }

        public bool WithSubReport { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDateTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime FinishDateTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime UpdateDate { get; set; }

        public bool IsPrivateEvent { get; set; }

        public Guid? BannerID { get; set; }

        [ForeignKey("HallID")]
        public virtual Hall Hall { get; set; }

        public Guid? HallID { get; set; }

        [ForeignKey("SectionID")]
        public virtual Section Section { get; set; }

        public Guid? SectionID { get; set; }


        [ForeignKey("DirectionID")]
        public virtual Direction Direction { get; set; }

        public Guid? DirectionID { get; set; }


        //public virtual ICollection<DirectionForReport> DirectionsForReport { get; set; }

        public virtual ICollection<SpeakerForReport> SpeakersForReport { get; set; }

        //public virtual ICollection<Speaker> Speakers { get; set; }
    }

    public class ReportRating : Model
    {

        public Guid ReportID { get; set; }

        public Guid UserID { get; set; }

        public double RatingPoints { get; set; }

        public string CommentText { get; set; }

        [ForeignKey("ReportID")]
        public virtual Report Report { get; set; }

        [ForeignKey("UserID")]
        public virtual UnfoUser User { get; set; }

    }

    public class QuestionsForSpeaker : Model
    {
        public Guid SpeakerID { get; set; }

        public Guid UserID { get; set; }

        public string Text { get; set; }

        [ForeignKey("SpeakerID")]
        public virtual Speaker Speaker { get; set; }

        [ForeignKey("UserID")]
        public virtual UnfoUser User { get; set; }

    }



    public class Speaker : EventBaseModel
    {
        public Guid? PhotoID { get; set; }

        public Guid? UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual UnfoUser User { get; set; }

        public string Email { get; set; }

        //[Localized]
        //public string Fio { get; set; }

        [Localized]
        public string Company { get; set; }

        [Localized]
        public string Specialization { get; set; }

        [Localized]
        public string City { get; set; }

        public string Phone { get; set; }

        public string VkURL { get; set; }

        public string TwitterURL { get; set; }

        public string FacebookURL { get; set; }

        [Localized]
        public string AdditionalInformation { get; set; }

        public virtual ICollection<SpeakerForReport> Reports { get; set; }
    }

    /// <summary>
    /// Поддоклады
    /// </summary>
    //public class SubReport : EventBaseModel
    //{
    //    public virtual ICollection<Speaker> Speakers { get; set; }
    //}

    public class Location : EventBaseModel
    {
        public int CountryId { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        [Localized]
        public string CityName { get; set; }
        [Localized]
        public string StreetName { get; set; }
        [Localized]
        public string BuildingNumber { get; set; }

        public double Lat { get; set; }

        public double Lon { get; set; }

        public string KladrCityId { get; set; }

        public string KladrStreetId { get; set; }
    }

    public class Hall : EventBaseModel
    {
    }

    public class Section : EventBaseModel
    {
    }

    //public class Venue : EventModelBaseModel
    //{
    //}

    /// <summary>
    /// Направление
    /// </summary>
    public class Direction : EventBaseModel
    {
        //public virtual ICollection<DirectionForReport> Reports { get; set; }
    }

    /// <summary>
    /// Информация для раздела организаторы (html)
    /// </summary>
    public class EventOrganization : EventBaseModel
    {

        /// <summary>
        /// Html с информацией об организаторах
        /// </summary>
        public string Content { get; set; }
    }

    /// <summary>
    /// Связь докладчиков и докладов
    /// </summary>
    public class SpeakerForReport : Model, ILocalizable
    {
        public Guid ReportID { get; set; }

        [ForeignKey("ReportID")]
        public virtual Report Report { get; set; }

        public Guid EventID { get; set; }

        [ForeignKey("EventID")]
        public virtual UEvent Event { get; set; }

        [Localized]
        public string ReportName { get; set; }

        public Guid SpeakerID { get; set; }

        [ForeignKey("SpeakerID")]
        public virtual Speaker Speaker { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StartTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? FinishTime { get; set; }
    }

    /// <summary>
    /// Связь направлений и докладов
    /// </summary>
    //public class DirectionForReport : Model
    //{
    //    public Guid ReportID { get; set; }

    //    [ForeignKey("ReportID")]
    //    public virtual Report Report { get; set; }
    //    public Guid EventID { get; set; }

    //    [ForeignKey("EventID")]
    //    public virtual UEvent Event { get; set; }

    //    public Guid DirectionID { get; set; }

    //    [ForeignKey("DirectionID")]
    //    public virtual Direction Direction { get; set; }
    //}

    public class PayerForEvent : Model
    {

        public Guid PayerID { get; set; }

        [ForeignKey("PayerID")]
        public virtual Payer Payer { get; set; }

        public Guid EventID { get; set; }

        [ForeignKey("EventID")]
        public virtual UEvent @Event { get; set; }
    }

    public class Payer : BaseModel
    {
        public string Desription { get; set; }

        public string Korr { get; set; }

        public string BankName { get; set; }

        public string Inn { get; set; }

        /// <summary>
        /// Расчетный счет
        /// </summary>
        public string PaymentAccount { get; set; }

        public string Bik { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        /// <summary>
        /// Тарифный план
        /// </summary>
        public string RatePlan { get; set; }

        /// <summary>
        /// Тип оплаты
        /// </summary>
        public TypeOfPayment TypeOfPayment { get; set; }

        public virtual ICollection<PayerForEvent> Events { get; set; }
    }

    public class Sponsor : EventBaseModel
    {
        public Guid? ImageID { get; set; }

        [ForeignKey("ImageID")]
        public virtual UploadImage Image { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string WebSite { get; set; }

        /// <summary>
        /// Позиция в списке, чем больше число - тем выше в списке
        /// </summary>
        public int PositionInList { get; set; }

        public string Desription { get; set; }
    }

    public class Document : BaseModel
    {

        public string Extentions { get; set; }

        public ContentType Type { get; set; }

    }

    public class UploadImage : Model
    {
        public int SourceWidth { get; set; }

        public int SourceHeight { get; set; }

        public int PointX { get; set; }

        public int PointY { get; set; }

        public int SelectedWidth { get; set; }

        public int SelectedHeight { get; set; }
    }



   
    /// <summary>
    /// Разделы мобильного приложения
    /// </summary>
    public class ServiceSection : ILocalizable, IIntBaseModel
    {
        public int ID { get; set; }

        [Localized]
        public string Name { get; set; }
        
        public bool IsDelete { get; set; }
    }

    /// <summary>
    /// Активные разделы в м.приложении для мероприятия
    /// </summary>
    public class ServiceSectionInEvent : Model
    {
        public int ServiceSectionId { get; set; }

        [ForeignKey("ServiceSectionId")]
        public virtual ServiceSection ServiceSection { get; set; }

        public Guid EventId { get; set; }

        [ForeignKey("EventId")]
        public virtual UEvent Event { get; set; }
    }

    public class Country : ILocalizable, IIntBaseModel
    {
        [Key]
        public int ID { get; set; }

        [Localized]
        public string Name { get; set; }

        public bool IsDelete { get; set; }
    }

    #region Голосование

    /// <summary>
    /// Тип голосования, голосование или опрос
    /// </summary>
    public enum VoteTypes
    {
        Vote = 1,
        Quiz = 2
    }

    public class Vote : EventBaseModel
    {
        /// <summary>
        /// тип голосования опрос(2) или голосование(1)
        /// </summary>
        public VoteTypes Type { get; set; }

        public bool IsOnline { get; set; }

        public bool IsFinished { get; set; }

        public DateTime? DateStart { get; set; }

        public DateTime? DateEnd { get; set; }

        public virtual ICollection<VoteQuestion> Questions { get; set; }

        [Localized]
        public string Description { get; set; }
    }

    public class VoteQuestion : BaseModel
    {
        public Guid VoteID { get; set; }

        [ForeignKey("VoteID")]
        public virtual Vote Vote { get; set; }

        [Localized]
        public string Description { get; set; }
    }

    public class VoteResult : BaseModel
    {
        public Guid QuestionID { get; set; }

        [ForeignKey("QuestionID")]
        public virtual VoteQuestion Question { get; set; }

        public Guid? VoteId { get; set; }

        [ForeignKey("VoteId")]
        public virtual Vote Vote { get; set; }

        public Guid UserID { get; set; }

        public string Answer { get; set; }

        public DateTime DateVote { get; set; }

        [Localized]
        public string Description { get; set; }
    }

    #endregion Голосование
}