﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataModel.Core.Attributes
{
    public class RoleIdAttribute : Attribute
    {
        public static Dictionary<Role, Guid> RolesIds { get; set; }

        static RoleIdAttribute()
        {
            var fabric = new RepositoryFabric(new UnfoDbContext());
            RolesIds = new Dictionary<Role, Guid>();
            var roles = fabric.Table<UnfoRole>().Get().ToList();
            roles.ForEach(role => RolesIds.Add((Role)Enum.Parse(typeof(Role), role.Name), role.Id));
            fabric.Dispose();
        }
    }
}