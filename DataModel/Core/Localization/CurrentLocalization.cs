﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DataModel.Core.Localization
{
    public static class CurrentLocalization
    {
        public const string CookieName = "current_culture";

        public const string DefaultLang = "ru";
        /// <summary>
        /// Текущая выбранная языковая культура интерфейса - возвращает в формате TwoLetterISOLanguageName
        /// </summary>
        public static string UI
        {
            get { return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName; }
        }
        /// <summary>
        /// Текущая выбранная языковая культура в режиме редактирования - возвращает в формате TwoLetterISOLanguageName
        /// </summary>
        public static string Edit
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Request.Cookies[CookieName] == null) return DefaultLang;
                return HttpContext.Current.Request.Cookies[CookieName].Value;
            }
        }
    }
}
