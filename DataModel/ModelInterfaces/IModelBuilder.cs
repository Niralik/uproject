﻿namespace DataModel.ModelInterfaces
{
    public interface IModelBuilder<TEntity> where TEntity : class
    {
        /// <summary>
        /// Метод возвращает новый объект какой либо бизнес модели заполненый данынми из текущей
        /// модели представления
        /// </summary>
        /// <returns></returns>
        TEntity GetModel();
        /// <summary>
        /// Обновляет заданный объект бизнес модели данными из текущей модели представления
        /// </summary>
        /// <param name="dbEntity">Бизнес модель которую необоходимо обновить</param>
        /// <returns></returns>
        TEntity UpdateModel(TEntity dbEntity);
    }

    public interface IViewModelBuilder<in TEntity> where TEntity : class
    {
        /// <summary>
        /// Заполняет предложенную ViewModel данными из базы данных
        /// </summary>
        /// <param name="dbEntity">бизнес модель</param>
        void Fill(TEntity dbEntity);
    }
}