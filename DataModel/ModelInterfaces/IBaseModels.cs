﻿using System;

namespace DataModel.ModelInterfaces
{
    public interface IIsDelete
{
        bool IsDelete { get; set; }
}
    public interface IBaseModel:IIsDelete
    {
        Guid ID { get; set; }
        
    }
    public interface IIntBaseModel : IIsDelete
{
        int ID { get; set; }
}

    public interface IBaseWithNameModel: IBaseModel
    {
        string Name { get; set; }
    }

    public interface IBaseForEventModel: IBaseWithNameModel
    {
        Guid EventID { get; set; }

        UEvent Event { get; set; }
    }
}