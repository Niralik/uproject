using Microsoft.AspNet.Identity;
using System;

namespace DataModel.ModelInterfaces
{
    public interface IShortUser : IUser<Guid>
    {
        /// <summary>
        /// �������
        /// </summary>
        string SurName { get; set; }

        /// <summary>
        /// ���
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// ��������
        /// </summary>
        string FatherName { get; set; }

        string Location { get; set; }

        string Email { get; set; }

        string PhoneNumber { get; set; }

        Guid PhotoId { get; set; }

    }

    public interface IUnfoUser : IShortUser
    {
        /// <summary>
        /// � ����
        /// </summary>
        string AboutYourselfInformation { get; set; }

        /// <summary>
        /// ����� ������
        /// </summary>
        string PlaceOfJob { get; set; }

        /// <summary>
        /// ���������� ���������
        /// </summary>
        string PositionAtJob { get; set; }

        string FacebookLink { get; set; }

        string VkontakteLink { get; set; }

        string TwitterLink { get; set; }

        string GoogleLink { get; set; }
        string WebSite { get; set; }
        string SkypeId { get; set; }

        bool IsDelete { get; set; }
         DateTime LastSignInDate { get; set; }
         DateTime SignUpDate { get; set; }
    }
}