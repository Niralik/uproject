﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.ModelInterfaces;

namespace DataModel
{
    public interface IRepositoryFabric : IDisposable
    {
        RepositoryFabric And<TEntity>() where TEntity : class;

        UnfoDbContext NewContext();

        GenericRepository<TEntity> Table<TEntity>() where TEntity : class;
    }

    public class RepositoryFabric : IRepositoryFabric
    {
        private readonly UnfoDbContext _dbContext;

        private readonly List<object> _repositories;

        public RepositoryFabric()
            : this(new UnfoDbContext())
        {
            _repositories = new List<object>();
        }

        public RepositoryFabric(UnfoDbContext dbContext)
        {
            _repositories = new List<object>();

            _dbContext = dbContext;
        }

        public RepositoryFabric And<TEntity>() where TEntity : class
        {
            if (_repositories.All(d => typeof(GenericRepository<TEntity>) != d.GetType()))
            {
                var t = new GenericRepository<TEntity>(_dbContext);
                _repositories.Add(t);
            }
            return this;
        }

        public UnfoDbContext NewContext()
        {
            return new UnfoDbContext();
        }

        public GenericRepository<TEntity> Table<TEntity>() where TEntity : class
        {
            var r =
                (GenericRepository<TEntity>)
                    _repositories.FirstOrDefault(d => typeof(GenericRepository<TEntity>) == d.GetType());
            if (r == null)
            {
                var t = new GenericRepository<TEntity>(_dbContext);
                _repositories.Add(t);
            }
            return
                (GenericRepository<TEntity>)
                    _repositories.FirstOrDefault(d => typeof(GenericRepository<TEntity>) == d.GetType());
        }

        public void Dispose()
        {
            if (_repositories != null)
                foreach (var repository in _repositories)
                {
                    ((IDisposable)repository).Dispose();
                }

            if (_dbContext != null)
                _dbContext.Dispose();
        }
    }
}