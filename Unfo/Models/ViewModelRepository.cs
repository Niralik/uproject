﻿
﻿using DataModel;
using System;
﻿using Unfo.Models.UnfoBusinessLogic;
﻿using Unfo.Models.ViewModelRepositories;
﻿using SettingsEventRepository = Unfo.Models.ViewModelRepositories.SettingsEventRepository;

namespace Unfo.Models
{
    public class ViewModelRepository : IDisposable
    {
        private static ViewModelRepository instance { get; set; }

        public ViewModelRepository()
        {
            Fabric = new RepositoryFabric();
            InitializationVMRepositories();
        }

        public ViewModelRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
            InitializationVMRepositories();
        }

        public IRepositoryFabric Fabric { get; private set; }

        public DictionariesRepository Dictionaries { get; private set; }

        public EventRepository Events { get; private set; }

        public UnfoUserRepository UnfoUsers { get; private set; }

        public UsersRequestsLogic UsersRequests { get; private set; }

        public SettingsEventRepository SettingsEvent { get; private set; }


        public ProgrammRepository Programm { get; private set; }

        public VotingRepository Votings { get; private set; }

        public ResourcesRepository Resources { get; private set; }

        public MyAccountVMRepository MyAccount { get; private set; }

        public ReportRepository Reports { get; private set; }

        /// <summary>
        /// Singleton
        /// </summary>
        public static ViewModelRepository Instance
        {
            get { return instance ?? (instance = new ViewModelRepository()); }
        }

        private void InitializationVMRepositories()
        {
            Dictionaries = new DictionariesRepository();
            Events = new EventRepository();
            UnfoUsers = new UnfoUserRepository();
            SettingsEvent = new SettingsEventRepository();
            Resources = new ResourcesRepository();
            MyAccount = new MyAccountVMRepository();
            Votings = new VotingRepository();
            Programm = new ProgrammRepository();
            Reports = new ReportRepository();
            UsersRequests = new UsersRequestsLogic();
            instance = this;
        }

        public void Dispose()
        {
            if (Fabric != null)
            {
                Fabric.Dispose();
            }
            Dictionaries = null;
            Events = null;
            UnfoUsers = null;
            SettingsEvent = null;
            Resources = null;
            MyAccount = null;
            Votings = null;
            Programm = null;
            Reports = null;
            UsersRequests = null;
            instance = null;
        }
    }
}