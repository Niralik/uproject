﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Globalization;
using System.Web.Mvc;
using Unfo.App_LocalResources;

namespace Unfo.Models.ViewModels.CommonViewModels
{
    public class DateVM
    {
        public static string DateFormat
        {
            get { return App_LocalResources.Resources.DateFormat; }
        }
        public static string DateFormatJS
        {
            get { return App_LocalResources.Resources.DateFormatJS; }
        }

        public DateVM()
        {

        }

        public DateVM(DateTime date)
        {
            if (date != new DateTime())
            {
                DateString = date.ToString(Format);
                Date = date;
            }
        }
        public DateVM(DateTime? date)
        {
            if (date.HasValue && date != new DateTime())
            {

                DateString = date.Value.ToString(Format);
                Date = date.Value;
            }
        }
        public virtual string Format
        {
            get { return DateVM.DateFormat; }
        }

        public string DateString
        {
            get;
            set;
        }

        public virtual bool HasTime { get { return false; } }

        public virtual DateTime Date { get; set; }
        //get
        //{
        //    DateTime result;

        //    if (DateTime.TryParseExact(DateString,
        //           Format,
        //           CultureInfo.InvariantCulture,
        //           DateTimeStyles.None,
        //           out result))
        //    {
        //        return result;
        //    }
        //    return default(DateTime);
        //}
    }

    public class DateRangeVM
    {

        [Display(ResourceType = typeof(Resources), Name = "EventVM_StartDate_Дата_начала")]
        public DateVM StartDate { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_FinishDate_Дата_окончания")]
        public DateVM EndDate { get; set; }
    }

    public class DateTimeRangeVM
    {

        [Display(ResourceType = typeof(Resources), Name = "EventVM_StartDate_Дата_начала")]
        public DateTimeVM StartDate { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_FinishDate_Дата_окончания")]
        public DateTimeVM EndDate { get; set; }
    }

    public class DateTimeVM : DateVM
    {
        public DateTimeVM()
        {

        }

        public static new string DateFormat
        {
            get { return App_LocalResources.Resources.DateTimeFormat; }
        }
        public static new string DateFormatJS
        {
            get { return App_LocalResources.Resources.DateTimeFormatJS; }
        }


        public DateTimeVM(DateTime date)
        {
            if (date != new DateTime())
                DateString = date.ToString(Format);
        }
        public DateTimeVM(DateTime? date)
        {
            if (date.HasValue && date != new DateTime())
                DateString = date.Value.ToString(Format);
        }

        public override string Format
        {
            get { return DateTimeVM.DateFormat; }
        }

        public override bool HasTime
        {
            get { return true; }
        }
        public override DateTime Date
        {
            get
            {
                DateTime result;

                if (DateTime.TryParseExact(DateString,
                       Format,
                       CultureInfo.CreateSpecificCulture("ru-RU"),
                       DateTimeStyles.None,
                       out result))
                {
                    return result;
                }
                return default(DateTime);
            }
        }
    }


    //public sealed class DateTimeVm
    //{
    //    //[RegularExpression(@"^[0-9]{2}.[0-9]{2}.[0-9]{4}$", ErrorMessageResourceType = typeof (System.Resources), ErrorMessageResourceName = "DateView_Date_У_даты_не_верный_формат")]
    //    public string DateString { get; set; }

    //    public string DateFormat { get; set; }

    //    public DateTime Date { get; set; }

    //    public DateTimeVm()
    //    {
    //    }
    //    public DateTimeVm(DateTime date, bool onlyDate = true)
    //    {
    //        DateString = onlyDate ? date.ToString("dd.MM.yyyy") : date.ToString("dd.MM.yyyy HH:mm");
    //        Date = date;
    //    }

    //    public DateTimeVm(string date)
    //    {
    //        DateString = date;
    //        Date = GetDate();
    //    }

    //    public DateTime GetDate()
    //    {
    //        DateTime result;
    //        if (DateTime.TryParse(DateString, out result))
    //        {
    //            return result;
    //        }
    //        return default(DateTime);
    //    }

    //    public String GetPrettyDate()
    //    {
    //        return Date.ToString("dd mmmm");
    //    }
    //}

    public class EditableTextVM
    {
        //var sanitizer = new HtmlSanitizer();
        //return sanitizer.Sanitize(_text);

        [AllowHtml, DataType(DataType.Html)]
        public string Text { get; set; }

        public EditableTextVM(string text)
        {
            Text = text ?? "";
        }

        public EditableTextVM()
        {
        }
    }

    public class HtmlEditorVM
    {

        //var sanitizer = new HtmlSanitizer();
        //return sanitizer.Sanitize(_text);

        [AllowHtml, DataType(DataType.Html)]
        public string Text { get; set; }

        public HtmlEditorVM(string text)
        {
            Text = text ?? "";
        }

        public HtmlEditorVM()
        {
        }
    }

    public class TextVM
    {
        //var sanitizer = new HtmlSanitizer();
        //return sanitizer.Sanitize(_text);

        public string Text { get; set; }

        public TextVM(string text)
        {
            Text = text ?? "";
        }

        public TextVM()
        {
        }
    }

    public enum CropForm
    {
        Square, Any
    }

    public class ImageCropperVM : BaseVM
    {
        public CropForm TypeCrop { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public decimal Ratio
        {
            get
            {
                if (TypeCrop == CropForm.Square) return 1;
                return 0;
            }
        }

        public int PointX { get; set; }

        public int PointY { get; set; }

    }

    public class UploadImageVM
    {
        public Guid ID { get; set; }

        public Size SourceSize { get; set; }

        public Size NormalSize { get; set; }
    }
}