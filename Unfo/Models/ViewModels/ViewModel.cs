﻿using DataModel;
using DataModel.ModelInterfaces;
using System;

namespace Unfo.Models.ViewModels
{
    //Для тестов
    public class ViewModel : IViewModelBuilder<UnfoUser>, IModelBuilder<UnfoUser>
    {
        public string FullName { get; set; }

        public void Fill(UnfoUser dbUser)
        {
            FullName = dbUser.UserName;
        }

        public UnfoUser GetModel()
        {
            throw new NotImplementedException();
        }

        public UnfoUser UpdateModel(UnfoUser modelForUpdate)
        {
            throw new NotImplementedException();
        }
    }


}