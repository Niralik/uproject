﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DataModel;
using DataModel.ModelInterfaces;
using System;
using Microsoft.AspNet.Identity;
using Unfo.App_LocalResources;
using Unfo.Areas.Cabinet.Controllers;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Models.ViewModels
{

    public class ManageExternalLoginsVM
    {
        public IList<Enum> ExistedLinkedLogins { get; set; }
        public ManageExternalLoginsVM(IEnumerable<UserLoginInfo> linkedAccounts)
        {
            ExistedLinkedLogins = new List<Enum>();
            parseLinkedAccountLoginsToEnums(linkedAccounts);
        }

        private void parseLinkedAccountLoginsToEnums(IEnumerable<UserLoginInfo> linkedAccounts)
        {
            foreach (var linkedAccount in linkedAccounts)
            {
                ExternalLogins enumExternalLogin;
                var isSuccessParseExternalLogin = Enum.TryParse(linkedAccount.LoginProvider, true, out enumExternalLogin);
                if (isSuccessParseExternalLogin)
                {
                    ExistedLinkedLogins.Add(enumExternalLogin);
                }
            }
        }
    }
    public class ManagePasswordVM
    {
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_ConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources),
            ErrorMessageResourceName = "Account_ConfirmPassword_Alert")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_StringLength", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_Password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "MyAccount_ChangePassword_OldPassword")]
        public string OldPassword { get; set; }
    }

  

}