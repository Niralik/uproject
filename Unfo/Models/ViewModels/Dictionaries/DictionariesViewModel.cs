﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using DataModel;
using DataModel.ModelInterfaces;
using System;
using OrangeJetpack.Localization;
using Unfo.App_LocalResources;
using Unfo.Core.CustomValidations;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.CommonViewModels;


namespace Unfo.Models.ViewModels.Dictionaries
{



    /// <summary>
    /// Места
    /// </summary>
    public class LocationVM :
        LocalizeBaseVM<LocationVM, Location>,
        IModelBuilder<Location>
    {
        public LocationVM() { }

        public LocationVM(Guid eventId)
            : base(eventId)
        {
            CountryId = 0;
        }
        [Display(ResourceType = typeof(Resources), Name = "c_City")]
        public string CityName { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Street")]
        public string StreetName { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Building")]
        public string BuildingNumber { get; set; }

        public string KladrCityId { get; set; }

        public string KladrStreetId { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Country")]
        public int CountryId { get; set; }

        public LocationVM(Location data)
        {
            Fill(data);
        }

        public override void Fill(string language, Location model = null, Guid? eventId = null)
        {
            base.Fill(language, model, eventId);

            if (model != null)
            {
                try
                {
                    CityName = Model.Localize(Language, d => d.CityName).CityName;
                    StreetName = Model.Localize(Language, d => d.StreetName).StreetName;
                    BuildingNumber = Model.Localize(Language, d => d.BuildingNumber).BuildingNumber;
                }
                catch (Exception)
                {
                    CityName = model.CityName;

                    StreetName = model.StreetName;
                    BuildingNumber = model.BuildingNumber;
                }

                ID = model.ID;
                Lat = model.Lat;
                Lon = model.Lon;
                CountryId = model.CountryId;
                KladrCityId = model.KladrCityId;
                KladrStreetId = model.KladrStreetId;
            }

        }





        public Location GetModel()
        {
            var lc1 = new List<LocalizedContent>();
            var lc2 = new List<LocalizedContent>();
            var lc3 = new List<LocalizedContent>();
            if (Forms != null && Forms.Any())
            {
                var vm = Forms.First();
                CountryId = vm.CountryId;
                ID = vm.ID;
                Lat = vm.Lat;
                Lon = vm.Lon;
            }
            foreach (var vm in Forms)
            {
                lc1.Add(new LocalizedContent(vm.Language, vm.CityName));
                lc2.Add(new LocalizedContent(vm.Language, vm.StreetName));
                lc3.Add(new LocalizedContent(vm.Language, vm.BuildingNumber));
            }

            return new Location()
            {
                CityName = LocalizedContent.Serialize(lc1),
                StreetName = LocalizedContent.Serialize(lc2),
                BuildingNumber = LocalizedContent.Serialize(lc3),
                EventID = EventId,
                ID = ID,
                Lat = Lat,
                Lon = Lon,
                KladrCityId = KladrCityId,
                KladrStreetId = KladrStreetId,
                CountryId = CountryId

            };
        }

        public Location UpdateModel(Location modelForUpdate)
        {

            modelForUpdate.BuildingNumber = Localization(modelForUpdate.BuildingNumber, BuildingNumber);
            modelForUpdate.CityName = Localization(modelForUpdate.CityName, CityName);
            modelForUpdate.StreetName = Localization(modelForUpdate.StreetName, StreetName);
            return modelForUpdate;
        }
    }


    public class HallVM :
        LocalizeBaseVM<HallVM, Hall>,
        IModelBuilder<Hall>
    {
        [Display(ResourceType = typeof(Resources), Name = "c_Title")]
        public string FullName { get; set; }



        public HallVM()
        {
        }

        public HallVM(Guid eventId)
            : base(eventId)
        {

        }

        public HallVM(Hall data)
        {
            Fill(data);
            //CreateLocalizeForm(data);
        }


        public override void Fill(string language, Hall model = null, Guid? eventId = null)
        {
            base.Fill(language, model, eventId);

            try
            {
                FullName = Model.Localize(Language, d => d.Name).Name;
            }
            catch (Exception)
            {

            }

        }


        public Hall GetModel()
        {

            var lc = new List<LocalizedContent>();
            foreach (var sectionVm in Forms)
            {
                ID = sectionVm.ID;
                lc.Add(new LocalizedContent(sectionVm.Language, sectionVm.FullName));
            }

            return new Hall()
            {
                Name = LocalizedContent.Serialize(lc),
                EventID = EventId,
                ID = ID

            };
        }

        public Hall UpdateModel(Hall modelForUpdate)
        {

            modelForUpdate.Name = Localization(modelForUpdate.Name, FullName);

            return modelForUpdate;
        }


    }


    public class SpeakerVM :
       LocalizeBaseVM<SpeakerVM, Speaker>,
       IModelBuilder<Speaker>
    {

        public Guid? UserId { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Email")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Fio")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Company")]
        public string Company { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Specialization")]
        public string Specialization { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Location")]
        public string City { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_PhoneNumber")]
        public string Phone { get; set; }

        public string VkURL { get; set; }

        public string TwitterURL { get; set; }

        public string FacebookURL { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Additional_info")]
        public string AdditionalInformation { get; set; }



        public SpeakerVM()
        {
        }

        public SpeakerVM(Guid eventId)
            : base(eventId)
        {

        }

        public SpeakerVM(Speaker data)
        {
            Fill(data);
            //CreateLocalizeForm(data);
        }


        public override void Fill(string language, Speaker model = null, Guid? eventId = null)
        {
            base.Fill(language, model, eventId);

            if (Model == null) return;
            try
            {
                Name = Model.Localize(Language, d => d.Name).Name;
                AdditionalInformation = Model.Localize(Language, d => d.AdditionalInformation).AdditionalInformation;
                Company = Model.Localize(Language, d => d.Company).Company;
                Specialization = Model.Localize(Language, d => d.Specialization).Specialization;
                City = Model.Localize(Language, d => d.City).City;
                //Email = Model.Localize(Language, d => d.Email).Email;
            }
            catch (Exception)
            {
                if (Model != null)
                {
                    Name = Model.Name;
                    AdditionalInformation = Model.AdditionalInformation;
                    Company = Model.Company;
                    Specialization = Model.Specialization;
                    City = Model.City;
                    //Email = Model.Email;
                }

            }

            if (Model != null)
            {
                VkURL = Model.VkURL;
                TwitterURL = Model.TwitterURL;
                FacebookURL = Model.FacebookURL;
                Phone = Model.Phone;
                Email = Model.Email;
                UserId = Model.UserID;
            }

        }


        public Speaker GetModel()
        {
            var name = new List<LocalizedContent>();
            //var mail = new List<LocalizedContent>();

            var spec = new List<LocalizedContent>();
            var company = new List<LocalizedContent>();
            var city = new List<LocalizedContent>();
            foreach (var sectionVm in Forms)
            {
                ID = sectionVm.ID;
                Email = sectionVm.Email;
                name.Add(new LocalizedContent(sectionVm.Language, sectionVm.Name));
                //mail.Add(new LocalizedContent(sectionVm.Language, sectionVm.Email));
                spec.Add(new LocalizedContent(sectionVm.Language, sectionVm.Specialization));
                company.Add(new LocalizedContent(sectionVm.Language, sectionVm.Company));
                city.Add(new LocalizedContent(sectionVm.Language, sectionVm.City));
            }

            return new Speaker()
            {
                Name = LocalizedContent.Serialize(name),
                Email = Email,
                Specialization = LocalizedContent.Serialize(spec),
                Company = LocalizedContent.Serialize(company),
                City = LocalizedContent.Serialize(city),
                EventID = EventId,
                ID = ID

            };
        }

        public Speaker UpdateModel(Speaker modelForUpdate)
        {

            //modelForUpdate.Name = Localization(modelForUpdate.Name, FullName);
            return modelForUpdate;
        }


    }

    public class SectionVM :
        LocalizeBaseVM<SectionVM, Section>,
        IModelBuilder<Section>
    {

        public SectionVM()
        {

        }


        public SectionVM(Guid eventId)
            : base(eventId)
        {

        }

        public SectionVM(Section data)
        {
            this.Fill(string.Empty, data);

        }


        public override void Fill(string language, Section model = null, Guid? eventId = null)
        {
            base.Fill(language, model, eventId);

            try
            {
                FullName = Model.Localize(Language, d => d.Name).Name;
            }
            catch (Exception)
            {

            }
        }


        [Required(ErrorMessage = "Укажите название")]
        [Display(ResourceType = typeof(Resources), Name = "c_Title")]
        public string FullName { get; set; }



        public Section GetModel()
        {
            var lc = new List<LocalizedContent>();
            foreach (var vm in Forms)
            {
                ID = vm.ID;
                lc.Add(new LocalizedContent(vm.Language, vm.FullName));
            }

            return new Section()
            {
                Name = LocalizedContent.Serialize(lc),
                EventID = EventId,
                ID = ID

            };
        }

        public Section UpdateModel(Section modelForUpdate)
        {

            modelForUpdate.Name = Localization(modelForUpdate.Name, FullName);

            return modelForUpdate;
        }


    }

    public class DirectionVM :
        LocalizeBaseVM<DirectionVM, Direction>,
        IModelBuilder<Direction>
    {
        public DirectionVM() { }

        public DirectionVM(Direction data)
        {
            Fill(string.Empty, data);

        }

        public DirectionVM(Guid eventId) : base(eventId) { }


        [Display(ResourceType = typeof(Resources), Name = "c_Title")]
        public string FullName { get; set; }


        public override void Fill(string language, Direction model = null, Guid? eventId = null)
        {

            base.Fill(language, model, eventId);

            try
            {
                FullName = model.Localize(Language, d => d.Name).Name;
            }
            catch (Exception)
            {


            }
        }


        public Direction GetModel()
        {

            var lc = new List<LocalizedContent>();
            foreach (var sectionVm in Forms)
            {
                ID = sectionVm.ID;
                lc.Add(new LocalizedContent(sectionVm.Language, sectionVm.FullName));
            }

            return new Direction
            {
                Name = LocalizedContent.Serialize(lc),
                EventID = EventId,
                ID = ID

            };
        }

        public Direction UpdateModel(Direction modelForUpdate)
        {

            modelForUpdate.Name = Localization(modelForUpdate.Name, FullName);
            return modelForUpdate;
        }


    }

    public class SponsorVM : BaseEventVM, IModelBuilder<Sponsor>, IViewModelBuilder<Sponsor>
    {
        public SponsorVM()
        {
            SizePreview = 200;
            InitImageCropper(CropForm.Square, SizePreview);
            
        }

        public SponsorVM(Sponsor data, CropForm typeCrop, int sizePreview)
        {
            SizePreview = sizePreview;
            InitImageCropper(typeCrop, sizePreview);
            Fill(data);
            
        }

        public SponsorVM(Sponsor data)
        {
            SizePreview = 200;
            InitImageCropper(CropForm.Square, SizePreview);
            Fill(data);

        }

        void InitImageCropper(CropForm typeCrop, int sizePreview)
        {
            Image = new ImageCropperVM();
            Image.TypeCrop = CropForm.Square;
            Image.Width = sizePreview;
        }


        public ImageCropperVM Image { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string WebSite { get; set; }

        public string About { get; set; }

        public int SizePreview { get; set; }

        public Sponsor GetModel()
        {
            return new Sponsor()
            {
                ID = ID,
                EventID = EventID,
                Name = GetLocalizeField(Name),
                Desription = GetLocalizeField(About),
                Phone = Phone
            };
        }

        public Sponsor UpdateModel(Sponsor modelForUpdate)
        {
            modelForUpdate.Desription = Localization(modelForUpdate.Desription, About);
            modelForUpdate.Name = Localization(modelForUpdate.Name, Name);
            //modelForUpdate.Phone = Localization(modelForUpdate.Phone, Phone);
            //modelForUpdate.WebSite = Localization(modelForUpdate.Phone, Phone);

            return modelForUpdate;
        }

        public void Fill(Sponsor dbEntity)
        {
            if (dbEntity != null)
            {
                Name = GetLocalizeField(dbEntity.Name);
                About = GetLocalizeField(dbEntity.Desription);
                EventID = dbEntity.EventID;
                ID = dbEntity.ID;
                Phone = dbEntity.Phone;
                WebSite = dbEntity.WebSite;

            }
        }
    }


    public class PromoOrganizatorsVM : BaseEventVM, IModelBuilder<EventOrganization>
    {
        public PromoOrganizatorsVM() { }

        public PromoOrganizatorsVM(EventOrganization model)
        {
            if (model != null)
            {
                EventID = model.EventID;
                ID = model.ID;
                Content = new HtmlEditorVM(GetLocalizeField(model.Content));
            }

        }

        public HtmlEditorVM Content { get; set; }

        public EventOrganization GetModel()
        {
            return new EventOrganization()
            {
                ID = ID == Guid.Empty ? Guid.NewGuid() : ID,
                EventID = EventID,
                Content = GetLocalizeField(Content.Text),
            };
        }

        public EventOrganization UpdateModel(EventOrganization modelForUpdate)
        {
            modelForUpdate.Content = Localization(modelForUpdate.Content, Content.Text);
            return modelForUpdate;
        }
    }

}