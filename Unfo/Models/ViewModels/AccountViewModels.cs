﻿using System.Collections.ObjectModel;
using DataModel;
using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using Unfo.App_LocalResources;

namespace Unfo.Models.ViewModels
{
    public enum TypeOfExternalSignInProvider
    {
        Twitter,
        GooglePlus,
        Vkontakte,
        Facebook
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class ExternalSignInConfirmationVM : UnfoUserAccountInfoBaseVM
    {
        public ExternalSignInConfirmationVM()
        {
        }

        /// <summary>
        /// конструктор заполняющий данные на основе данных пришедших из соц сетей
        /// </summary>
        /// <param name="externalLoginInfo"></param>
        public ExternalSignInConfirmationVM(ExternalLoginInfo externalLoginInfo)
        {
            TypeOfExternalSignInProvider = (TypeOfExternalSignInProvider)Enum.Parse(typeof(TypeOfExternalSignInProvider), externalLoginInfo.Login.LoginProvider);
            var externalClaims = externalLoginInfo.ExternalIdentity.Claims;
            switch (TypeOfExternalSignInProvider)
            {
                case TypeOfExternalSignInProvider.Twitter:
                    //Твиттер не дает мейла - не дает фамилии и имени - только логин страницы - добавить если понадобится создание ссылок на профили
                    break;

                case TypeOfExternalSignInProvider.GooglePlus:
                    var googlePlusClaims = JsonConvert.DeserializeObject(externalClaims.First(c => c.Type == "User").Value, typeof(GooglePlusClaims));
                    FillViewModelByGooglePlusData((GooglePlusClaims)googlePlusClaims);
                    break;

                case TypeOfExternalSignInProvider.Vkontakte:
                    FillViewModelByVkontakteData(new VkontakteClaims(externalClaims));
                    break;

                case TypeOfExternalSignInProvider.Facebook:
                    var facebookClaims = JsonConvert.DeserializeObject(externalClaims.First(c => c.Type == "Person").Value, typeof(FacebookClaims));
                    FillViewModelByFacebookData((FacebookClaims)facebookClaims);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public TypeOfExternalSignInProvider TypeOfExternalSignInProvider { get; set; }

        //private void FillViewModelTwitterData(TwitterClaims twitterClaims)
        //{
        //}
        private void FillViewModelByFacebookData(FacebookClaims facebookClaims)
        {
            Email = facebookClaims.email;
            FirstName = facebookClaims.first_name;
            SurName = facebookClaims.last_name;
        }

        private void FillViewModelByGooglePlusData(GooglePlusClaims googlePlusClaims)
        {
            Email = googlePlusClaims.email;
            FirstName = googlePlusClaims.given_name;
            SurName = googlePlusClaims.family_name;
        }

        private void FillViewModelByVkontakteData(VkontakteClaims vkontakteClaims)
        {
            Email = vkontakteClaims.Email;
            FirstName = vkontakteClaims.FirstName;
            SurName = vkontakteClaims.LastName;
        }

        //    public TwitterClaims()
        //    {
        //    }
        //}
        internal class FacebookClaims
        {
            public FacebookClaims()
            {
            }

            public string email { get; set; }

            public string first_name { get; set; }

            public string id { get; set; }

            public string last_name { get; set; }
        }

        internal class GooglePlusClaims
        {
            public GooglePlusClaims()
            {
            }

            public string email { get; set; }

            public string family_name { get; set; }

            public string given_name { get; set; }

            public string sub { get; set; }
        }

        //internal class TwitterClaims
        //{
        //    public int sub { get; set; }
        //    public string given_name { get; set; }
        //    public string family_name { get; set; }
        //    public string email { get; set; }
        internal class VkontakteClaims
        {
            public VkontakteClaims()
            {
            }

            public VkontakteClaims(IEnumerable<Claim> externalClaims)
            {
                FirstName = externalClaims.First(c => c.Type == "First_Name").Value;
                LastName = externalClaims.First(c => c.Type == "Last_Name").Value;
                Email = externalClaims.First(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value;
                Id = externalClaims.First(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            }

            public string Email { get; set; }

            public string FirstName { get; set; }

            public string Id { get; set; }

            public string LastName { get; set; }
        }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof(Resources), Name = "c_Email")]
        public string Email { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Resources), Name = "c_Email")]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string Code { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_ConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Account_ConfirmPassword_Alert")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof(Resources), Name = "c_Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_StringLength", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_Password")]
        public string Password { get; set; }
    }

    public class SendCodeViewModel
    {
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }

        public string ReturnUrl { get; set; }

        public string SelectedProvider { get; set; }
    }

    public class SignInVM
    {
        [Required]
        [Display(ResourceType = typeof(Resources), Name = "c_Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class SignUpVM : UnfoUserAccountInfoBaseVM
    {
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_ConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Account_ConfirmPassword_Alert")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_StringLength", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = "Account_Password")]
        public string Password { get; set; }
    }

    public class UnfoUserAccountInfoBaseVM : IShortUser, IModelBuilder<UnfoUser>
    {
        public string Location { get; set; }

        [Required]
        [EmailAddress]
        [Remote("ValidateEmailOfUserInSystem", "Account", ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Account_Validate_Alert_IsExistingEmail")]
        public string Email { get; set; }

        public string FatherName { get; set; }

        [Required]
        public string FirstName { get; set; }

        public Guid Id { get; private set; }

        public Guid PhotoId { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public string SurName { get; set; }

        public string UserName { get; set; }

        public UnfoUser GetModel()
        {
            var user = new UnfoUser().Initialize();
            user.UserName = Email;
            user.Email = Email;
            user.SurName = SurName;
            user.FirstName = FirstName;
            user.FatherName = FatherName;
            user.Location = Location;
            user.PhoneNumber = PhoneNumber;
            user.PhotoId = PhotoId;
            return user;
        }

        public UnfoUser UpdateModel(UnfoUser modelForUpdate)
        {
            throw new NotImplementedException();
        }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        public string Provider { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public string ReturnUrl { get; set; }
    }
}