﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Scripting.Utils;

namespace Unfo.Models.ViewModels
{
    /// <summary>
    /// Модель для представления администрирования пользовательских запросов
    /// </summary>
    public class UserRequestAdminVM : IViewModelBuilder<Request>
    {
        public Guid ID { get; private set; }
        public RequestState RequestState { get; private set; }
        public TypeOfRequest TypeRequest { get; private set; }
        public string LastRequestDescription { get; private set; }
        public DateTime LastRequestCreateDate { get; private set; }
        public ShortUserVM Author { get; private set; }
        /// <summary>
        /// Личность что подтвердила запрос пользователя либо отвергла
        /// </summary>
        public ShortUserVM Admin { get; private set; }
        public List<UserRequestsHistoryVM> History { get; private set; }
        public  UserRequestAdminVM(IEnumerable<Request> requestsOfOneAuthor)
        {
            var orderedRequests = requestsOfOneAuthor.OrderByDescending(request => request.CreateRequestDate);
            Fill(orderedRequests.First());
            if (orderedRequests.All(request=> request.ID != ID)) throw new ArgumentException("в листе должны быть запросы только одного пользователя");
            FillHistory(orderedRequests);
        }

        private void FillHistory(IEnumerable<Request> orderedRequests)
        {
            History = orderedRequests.Select(request => new UserRequestsHistoryVM(request)).ToList();
        }

        public void Fill(Request dbEntity)
        {
            ID = dbEntity.ID;
            RequestState = dbEntity.RequestState;
            TypeRequest = dbEntity.RequestType;
            LastRequestDescription = dbEntity.RequestDescription;
            LastRequestCreateDate = dbEntity.CreateRequestDate;
            Author = new ShortUserVM(dbEntity.Author);
            Admin = new ShortUserVM(dbEntity.Admin);
        }

        
    }

    public class UserRequestsHistoryVM
    {
        public DateTime RequestDescriptionDate { get; private set; }
        public string RequestDescriptin { get; private set; }
        public DateTime ResponseDescriptionDate { get; private set; }
        public string ResponseDescriptin { get; private set; }

        public UserRequestsHistoryVM(Request request)
        {
            RequestDescriptionDate = request.CreateRequestDate;
            RequestDescriptin = request.RequestDescription;
            ResponseDescriptionDate = request.UpdateRequestDate;
            ResponseDescriptin = request.ResponseDescription;
        }

    }
    /// <summary>
    /// Представление
    /// </summary>
    public class UserRequestVm : IViewModelBuilder<Request>, IModelBuilder<Request>
    {
        public Guid Id { get; set; }
        public RequestState RequestState { get; set; }
        public TypeOfRequest RequestType { get; set; }
        /// <summary>
        /// Пользовательское описание запроса
        /// </summary>
        public string RequestDescription { get; set; }
        /// <summary>
        /// Ответ администрации на запрос
        /// </summary>
        public string ResponseDescription { get; set; }
        public DateTime CreateRequestDate { get; set; }
        public DateTime UpdateRequestDate { get; set; }
        public ShortUserVM Author { get; set; }
        public ShortUserVM Admin { get; set; }
        public UserRequestVm()
        {

        }
        public UserRequestVm(Request dbEntity)
        {
            Fill(dbEntity);
        }
        public void Fill(Request dbEntity)
        {
            Id = dbEntity.ID;
            RequestDescription = dbEntity.RequestDescription;
            ResponseDescription = dbEntity.ResponseDescription;
            RequestState = dbEntity.RequestState;
            RequestType = dbEntity.RequestType;
            CreateRequestDate = dbEntity.CreateRequestDate;
            UpdateRequestDate = dbEntity.UpdateRequestDate;
            if (dbEntity.Author != null) Author = new ShortUserVM(dbEntity.Author);
            if (dbEntity.Admin != null) Admin = new ShortUserVM(dbEntity.Admin);
        }

        public void SetAuthor(Guid userId)
        {
            if (Author == null)
            {
                Author = new ShortUserVM { Id = userId };
            }
        }
        public void SetAdmin(Guid userId)
        {
            if (Admin == null)
            {
                Admin = new ShortUserVM { Id = userId };
            }
        }
        public Request GetModel()
        {
            return UpdateModel(new Request());
        }

        public Request UpdateModel(Request dbEntity)
        {
            dbEntity.ID = Id;
            dbEntity.CreateRequestDate = CreateRequestDate == default(DateTime) ? DateTime.Now : CreateRequestDate;
            dbEntity.UpdateRequestDate = DateTime.Now;
            dbEntity.RequestState = RequestState;
            dbEntity.RequestType = RequestType;
            dbEntity.RequestDescription = RequestDescription;
            dbEntity.ResponseDescription = ResponseDescription;
            dbEntity.AuthorID = Author.Id;
            if (Admin != null) dbEntity.AdminID = Admin.Id;
            return dbEntity;
        }


    }
}