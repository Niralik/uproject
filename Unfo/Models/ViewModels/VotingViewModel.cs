﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using CsQuery.Engine.PseudoClassSelectors;
using DataModel;
using DataModel.ModelInterfaces;
using OrangeJetpack.Localization;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Models.ViewModels
{

    public class BaseVM
    {
        public Guid ID { get; set; }

        public BaseVM()
        {

        }

        public BaseVM(Guid id)
        {
            ID = id;
        }

        public string Localization(string fieldData, string val)
        {
            return Core.Helpers.LocalizeHelper.Localization(CurrentLocalization.Edit, fieldData, val);
        }

        public string GetLocalizeField(string field)
        {
            return Core.Helpers.LocalizeHelper.GetLocalizeField(CurrentLocalization.Edit, field);
        }
    }
    public class BaseEventVM : BaseVM
    {
        public Guid EventID { get; set; }

        public BaseEventVM()
            : base()
        {

        }

        public BaseEventVM(Guid eventID)
        {
            EventID = eventID;
        }
    }

    public class ShortVoteViewModel : BaseEventVM, IModelBuilder<Vote>
    {
        public ShortVoteViewModel(Vote vote, string lang)
        {
            Fill(vote, lang);
        }

        public ShortVoteViewModel()
        {

        }

        public ShortVoteViewModel(Guid eventId)
            : base(eventId)
        {
        }

        public void Fill(Vote vote, string lang)
        {
            try
            {
                Name = vote.Localize(lang, d => d.Name).Name;
                Description = vote.Localize(lang, d => d.Description).Description;
            }
            catch (Exception)
            {
                Name = vote.Name;
                Description = vote.Description;
            }
            ID = vote.ID;
            EventID = vote.EventID;
            IsFinished = vote.IsFinished;
            IsOnline = vote.IsOnline;
            IsOpros = vote.Type == VoteTypes.Quiz;
        }



        public Vote GetModel()
        {
            var lcName = new List<LocalizedContent>();
            var lcDescription = new List<LocalizedContent>();

            var langs = CurrentEvent.GetAccessCultures();
            foreach (var lang in langs)
            {
                if (lang.Key == CurrentLocalization.Edit)
                {
                    lcName.Add(new LocalizedContent(CurrentLocalization.Edit, Name));
                    lcDescription.Add(new LocalizedContent(CurrentLocalization.Edit, Description));
                }
                else
                {
                    lcName.Add(new LocalizedContent(lang.Key, ""));
                    lcDescription.Add(new LocalizedContent(lang.Key, ""));
                }
            }


            return new Vote()
            {
                Name = LocalizedContent.Serialize(lcName),
                Description = LocalizedContent.Serialize(lcDescription),
                EventID = EventID,
                IsOnline = IsOnline,
                IsFinished = IsFinished,
                Type = IsOpros ? VoteTypes.Quiz : VoteTypes.Vote,
                ID = ID,
            };
        }



        public Vote UpdateModel(Vote data)
        {
            var nameFields = LocalizeField(LocalizedContent.Deserialize(data.Name).ToList(), Name);
            var descriptionFields = LocalizeField(LocalizedContent.Deserialize(data.Description).ToList(), Description);

            data.Name = LocalizedContent.Serialize(nameFields);
            data.Description = LocalizedContent.Serialize(descriptionFields);
            data.EventID = EventID;
            data.IsOnline = IsOnline;
            data.IsFinished = IsFinished;
            data.Type = IsOpros ? VoteTypes.Quiz : VoteTypes.Vote;
            data.ID = ID;

            return data;
        }

        private IEnumerable<LocalizedContent> LocalizeField(ICollection<LocalizedContent> fields, string name)
        {
            bool insert = false;
            foreach (var item in fields)
            {
                if (item.Key == CurrentLocalization.Edit)
                {
                    item.Value = Name;
                    insert = true;
                }
            }
            if (!insert)
                fields.Add(new LocalizedContent(CurrentLocalization.Edit, Name));

            return fields;
        }

        [Display(ResourceType = typeof(Resources), Name = "Voting_Название_голосования")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "Voting_Текст_вопроса")]
        public string Description { get; set; }

        public bool IsOnline { get; set; }

        public bool IsFinished { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "Voting_Опрос")]
        public bool IsOpros { get; set; }

    }



    public class VoteViewModel : ShortVoteViewModel
    {
        public VoteViewModel(Vote vote, string lang)
        {
            Fill(vote, lang);
        }

        public VoteViewModel()
        {

        }

        public VoteViewModel(Guid eventId)
            : base(eventId)
        {
        }

        public void Fill(Vote vote, string lang)
        {
            base.Fill(vote, lang);
            if (vote.Questions != null) Questions = vote.Questions.ToList().Select(d => new VoteQuestionViewModel(d, lang)).ToList();
        }

        public int? Sum { get; set; }

        public List<VoteQuestionViewModel> Questions { get; set; }


        public IList<string> Answers { get; set; }


        public Vote GetModel()
        {
            return base.GetModel();
        }

    }

    public class VoteQuestionViewModel : BaseVM, IModelBuilder<VoteQuestion>
    {
        private Guid VoteId { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }

        public VoteQuestionViewModel(VoteQuestion voteQuestion, string lang)
        {
            Fill(voteQuestion, lang);
        }

        public VoteQuestionViewModel()
        {
            ID = new Guid();
        }



        public VoteQuestionViewModel(Guid eventId)
            : base(eventId)
        {

        }

        public void Fill(VoteQuestion voteQuestion, string language)
        {
            if (voteQuestion == null) return;
            ID = voteQuestion.ID;
            VoteId = voteQuestion.VoteID;

            try
            {
                Name = voteQuestion.Localize(language, d => d.Name).Name;

            }
            catch (Exception)
            {
                Name = voteQuestion.Name;
            }
        }


        public VoteQuestion GetModel()
        {
            ID = ID;

            var langs = CurrentEvent.GetAccessCultures();
            var lcName = langs.Select(lang => lang.Key == CurrentLocalization.Edit ? new LocalizedContent(CurrentLocalization.Edit, Name) : new LocalizedContent(lang.Key, "")).ToList();


            return new VoteQuestion()
            {
                Name = LocalizedContent.Serialize(lcName),
                ID = ID,
                Description = LocalizedContent.Serialize(lcName),
            };
        }

        public VoteQuestion UpdateModel(VoteQuestion data)
        {
            var fileds = LocalizedContent.Deserialize(data.Name).ToList();
            bool insert = false;
            foreach (var item in fileds)
            {
                if (item.Key == CurrentLocalization.Edit)
                {
                    item.Value = Name;
                    insert = true;
                }
            }
            if (!insert)
                fileds.Add(new LocalizedContent(CurrentLocalization.Edit, Name));


            data.Name = LocalizedContent.Serialize(fileds);
            data.ID = ID;

            return data;
        }

        //public VoteQuestion UpdateModel(VoteQuestion modelForUpdate)
        //{
        //    modelForUpdate.Name = Localization(modelForUpdate.Name, Name);
        //    modelForUpdate.Description = Localization(modelForUpdate.Description, Name);

        //    return modelForUpdate;
        //}
    }
}