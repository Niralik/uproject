﻿using DataModel;
using DataModel.ModelInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNet.Identity;
using Unfo.App_LocalResources;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Core.GlobalClasses;

namespace Unfo.Models.ViewModels
{
    public class SimpleUserVM : IUser<Guid>, IViewModelBuilder<UnfoUser>, IModelBuilder<UnfoUser>
    {
        public Guid Id { get;  set; }
        public string UserName { get; set; }

        public SimpleUserVM()
        {
            
        }

        public SimpleUserVM(UnfoUser dbUser)
        {
            Fill(dbUser);
        }
        public void Fill(UnfoUser dbEntity)
        {
            Id = dbEntity.Id;
            UserName = dbEntity.UserName;
        }

        public UnfoUser GetModel()
        {
            return UpdateModel(new UnfoUser());
        }

        public UnfoUser UpdateModel(UnfoUser dbEntity)
        {
            UserName = dbEntity.UserName;
            Id = dbEntity.Id;
            return dbEntity;
        }
    }
    public class ShortUserVM :SimpleUserVM, IShortUser, IViewModelBuilder<UnfoUser>, IModelBuilder<UnfoUser>
    {
        private string _phoneNumber;
        private string _email;

        public virtual ICollection<Role> Roles { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Surname")]
        public virtual string SurName { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_FirstName")]
        public virtual string FirstName { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_FatherName")]
        public virtual string FatherName { get; set; }

        public virtual string Location { get; set; }

        public virtual string Fio { get { return this.ToString(); } }

        [Display(ResourceType = typeof(Resources), Name = "UnfoUserVM_Photo")]
        public virtual Guid PhotoId { get; set; }

        private List<UserPrivacyPoliticVM> PrivacyPolitics { get; set; }

        public ShortUserVM()
        {
        }

        public ShortUserVM(UnfoUser dbUser)
        {
            if (dbUser == null) return;
            Fill(dbUser);
        }

        public new void Fill(UnfoUser dbUser)
        {
            base.Fill(dbUser);
            SurName = dbUser.SurName;
            FirstName = dbUser.FirstName;
            FatherName = dbUser.FatherName;
            Location = dbUser.Location;
            Email = dbUser.Email;
            PhoneNumber = dbUser.PhoneNumber;
            PhotoId = dbUser.PhotoId;
            Roles = SetRoles(dbUser);
            PrivacyPolitics = dbUser.PrivacyPolitics.Select(politic => new UserPrivacyPoliticVM(politic)).ToList();
        }

        private static ICollection<Role> SetRoles(UnfoUser dbUser)
        {
            var userRoles = new List<Role>();
            var castedDbUserRoles =
                dbUser.Roles.SelectMany(
                    userRole =>
                        Enum.GetValues(typeof(Role))
                            .Cast<Role>()
                            .Where(role => userRole.Role.Name == role.ToString()));
            userRoles.AddRange(castedDbUserRoles);
            return userRoles;
        }

        public new UnfoUser GetModel()
        {
            return UpdateModel(new UnfoUser());
        }

        public new UnfoUser UpdateModel(UnfoUser modelForUpdate)
        {
            modelForUpdate.SurName = SurName;
            modelForUpdate.FirstName = FirstName;
            modelForUpdate.FatherName = FatherName;
            modelForUpdate.Location = Location;
            modelForUpdate.PhotoId = PhotoId;
            modelForUpdate.PhoneNumber = _phoneNumber;
            return modelForUpdate;
        }

        #region Свойства с логикой скрытия на основе Политик приватности

        [Display(ResourceType = typeof(Resources), Name = "c_EmailAdress")]
        public virtual string Email
        {
            get
            {
                if (_email == null) return "";
                return IsPrivateField(TypePrivacyPolitic.Email) ? null : _email;
            }
            set { _email = value; }
        }

        [Display(ResourceType = typeof(Resources), Name = "c_PhoneNumber")]
        public virtual string PhoneNumber
        {
            get
            {
                if (_phoneNumber == null) return "";
                return IsPrivateField(TypePrivacyPolitic.Phone) ? null : _phoneNumber;
            }
            set { _phoneNumber = value; }
        }

        #endregion Свойства с логикой скрытия на основе Политик приватности

        /// <summary>
        /// Метод выводит статус приватности поля исходя из названия политики, роли текущего пользователя и
        /// зависимости от случая если текущий пользователь владелец предоставленных данных
        /// </summary>
        /// <param name="privacyPolitic"></param>
        /// <returns></returns>
        protected bool IsPrivateField(TypePrivacyPolitic privacyPolitic)
        {
            if (CurrentUser.Get.IsInRole(Role.Admin, Role.Organizer) || CurrentUser.Get.Id == Id) return false;
            return PrivacyPolitics.Single(politic => politic.TypePrivacyPolitic == privacyPolitic).IsPrivate;
        }

        /// <summary>
        /// Возвращает Фамилию и Имя - при установленном флаге возвращает полное ФИО
        /// </summary>
        /// <param name="isFullFio"></param>
        /// <returns></returns>
        public  string ToString(bool isFullFio = false)
        {
            var fio = string.Format("{0} {1} ", SurName, FirstName);
            if (isFullFio) fio += FatherName;
            return fio;
        }

        /// <summary>
        /// Проверка юзера есть у него одна из перечисленых ролей или нет
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public bool HasRole(params Role[] roles)
        {
            return Roles.Any(roles.Contains);
        }

        /// <summary>
        /// Проверка юзера содержит ли он все перечисленные роли
        /// </summary>
        /// <param name="roles"></param>
        public bool HasRoles(params Role[] roles)
        {
            throw new NotImplementedException();
        }
    }

    public class UnfoUserVM : ShortUserVM, IUnfoUser, IViewModelBuilder<UnfoUser>, IModelBuilder<UnfoUser>
    {
        private string _placeOfJob;

        public UnfoUserVM()
        {
        }

        public UnfoUserVM(UnfoUser dbUser)
        {
            Fill(dbUser);
        }

        [Display(ResourceType = typeof(Resources), Name = "UnfoUserVM_AboutYourself")]
        public virtual string AboutYourselfInformation { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "UnfoUserVM_PositionAtJob")]
        public virtual string PositionAtJob { get; set; }

        [DisplayName("Facebook")]
        public virtual string FacebookLink { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "UnfoUserVM_Vkontakte")]
        public virtual string VkontakteLink { get; set; }

        [DisplayName("Twitter")]
        public virtual string TwitterLink { get; set; }

        [DisplayName("Google")]
        public virtual string GoogleLink { get; set; }

        public string WebSite { get; set; }

        public string SkypeId { get; set; }

        public DateTime LastSignInDate { get; set; }

        public DateTime SignUpDate { get; set; }

        public bool IsDelete { get; set; }

        #region Свойства с логикой скрытия на основе Политик приватности

        [Display(ResourceType = typeof(Resources), Name = "UnfoUserVM_PlaceOfJob")]
        public virtual string PlaceOfJob
        {
            get
            {
                if (_placeOfJob == null) return "";
                return IsPrivateField(TypePrivacyPolitic.PlaceOfJob) ? null : _placeOfJob;
            }
            set { _placeOfJob = value; }
        }

        #endregion Свойства с логикой скрытия на основе Политик приватности

        public new void Fill(UnfoUser dbUser)
        {
            base.Fill(dbUser);
            AboutYourselfInformation = dbUser.AboutYourselfInformation;
            PositionAtJob = dbUser.PositionAtJob;
            PlaceOfJob = dbUser.PlaceOfJob;
            WebSite = dbUser.WebSite;
            SkypeId = dbUser.SkypeId;
            FacebookLink = dbUser.FacebookLink;
            TwitterLink = dbUser.TwitterLink;
            GoogleLink = dbUser.GoogleLink;
            VkontakteLink = dbUser.VkontakteLink;
            SignUpDate = dbUser.SignUpDate;
            LastSignInDate = dbUser.LastSignInDate;
        }

        public new UnfoUser UpdateModel(UnfoUser modelForUpdate)
        {
            modelForUpdate = base.UpdateModel(modelForUpdate);
            modelForUpdate.AboutYourselfInformation = AboutYourselfInformation;
            modelForUpdate.PlaceOfJob = _placeOfJob;
            modelForUpdate.PositionAtJob = PositionAtJob;
            modelForUpdate.SkypeId = SkypeId;

            //формируем красивые ссылки чтоб хтмл тег <a> адекватно работал
            modelForUpdate.FacebookLink = FacebookLink == null ? null : new UriBuilder(FacebookLink).ToStringWithHttps();
            modelForUpdate.VkontakteLink = VkontakteLink == null ? null : new UriBuilder(VkontakteLink).ToStringWithHttps();
            modelForUpdate.TwitterLink = TwitterLink == null ? null : new UriBuilder(TwitterLink).ToStringWithHttps();
            modelForUpdate.GoogleLink = GoogleLink == null ? null : new UriBuilder(GoogleLink).ToStringWithHttps();
            modelForUpdate.WebSite = WebSite == null ? null : new UriBuilder(WebSite).ToStringWithoutPorts();
            modelForUpdate.IsDelete = IsDelete;
            return modelForUpdate;
        }
    }

    public class UpdateUnfoUserVM : UnfoUserVM
    {
        [MaxLength(2000, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string AboutYourselfInformation
        {
            get { return base.AboutYourselfInformation; }
            set { base.AboutYourselfInformation = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string PositionAtJob
        {
            get { return base.PositionAtJob; }
            set { base.PositionAtJob = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string PlaceOfJob
        {
            get { return base.PlaceOfJob; }
            set { base.PlaceOfJob = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string FacebookLink
        {
            get { return base.FacebookLink; }
            set { base.FacebookLink = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string VkontakteLink
        {
            get { return base.VkontakteLink; }
            set { base.VkontakteLink = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string TwitterLink
        {
            get { return base.TwitterLink; }
            set { base.TwitterLink = value; }
        }

        [MaxLength(200, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string GoogleLink
        {
            get { return base.GoogleLink; }
            set { base.GoogleLink = value; }
        }

        [MaxLength(140, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string SurName
        {
            get { return base.SurName; }
            set { base.SurName = value; }
        }

        [MaxLength(140, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string FirstName
        {
            get { return base.FirstName; }
            set { base.FirstName = value; }
        }

        [MaxLength(140, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        public override string FatherName
        {
            get { return base.FatherName; }
            set { base.FatherName = value; }
        }

        public override string Location
        {
            get { return base.Location; }
            set { base.Location = value; }
        }

        //[Phone(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_Phone")]
        [MaxLength(20, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MaxLength")]
        [MinLength(6, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "Validate_Alert_MinLength")]
        public override string PhoneNumber
        {
            get { return base.PhoneNumber; }
            set { base.PhoneNumber = value; }
        }

        public override Guid PhotoId
        {
            get { return base.PhotoId; }
            set { base.PhotoId = value; }
        }
    }

    public class UserPrivacyPoliticsVM
    {
        public Guid UserID { get; set; }

        public List<UserPrivacyPoliticVM> UserPrivacyPolitics { get; set; }

        public UserPrivacyPoliticsVM(UnfoUser unfoUser)
        {
            UserID = unfoUser.Id;
            UserPrivacyPolitics = unfoUser.PrivacyPolitics.Select(pp => new UserPrivacyPoliticVM(pp)).ToList();
        }

        public UserPrivacyPoliticsVM()
        {
        }
    }

    public class UserPrivacyPoliticVM : IViewModelBuilder<PrivacyPolitic>, IModelBuilder<PrivacyPolitic>
    {
        [Required]
        public Guid ID { get; set; }

        [Required]
        public Guid UserID { get; set; }

        [Required]
        public TypePrivacyPolitic TypePrivacyPolitic { get; set; }

        [Required]
        public bool IsPrivate { get { return !IsPublic; } }

        public bool IsPublic { get; set; }

        public UserPrivacyPoliticVM(PrivacyPolitic pp)
        {
            Fill(pp);
        }

        public UserPrivacyPoliticVM()
        {
        }

        public void Fill(PrivacyPolitic dbEntity)
        {
            this.ID = dbEntity.ID;
            UserID = dbEntity.UserID;
            TypePrivacyPolitic = dbEntity.TypePrivacyPolitic;
            IsPublic = !dbEntity.IsPrivate;
        }

        /// <summary>
        /// Все политики у пользователей инициализируются на этапе создания -
        /// нет обходимости получить модель из модели представления
        /// </summary>
        /// <returns></returns>
        public PrivacyPolitic GetModel()
        {
            throw new NotImplementedException();
        }

        public PrivacyPolitic UpdateModel(PrivacyPolitic modelForUpdate)
        {
            modelForUpdate.ID = ID;
            modelForUpdate.IsPrivate = IsPrivate;
            modelForUpdate.TypePrivacyPolitic = TypePrivacyPolitic;
            modelForUpdate.UserID = UserID;
            return modelForUpdate;
        }
    }

    public class EventModeratorVm : IModelBuilder<EventModerator>
    {
        public Guid EventId { get; set; }

        public Guid UserId { get; set; }

        public DateTime DateAtachToModerators { get; set; }

        public DateTime DateDetachFromModerators { get; set; }

        public EventModeratorVm(Guid moderatedEventId, Guid newModeratorUserId)
        {
            Initialize(moderatedEventId, newModeratorUserId);
        }

        private void Initialize(Guid eventId, Guid userId)
        {
            EventId = eventId;
            UserId = userId;
            DateAtachToModerators = DateTime.Now;
        }

        public EventModerator GetModel()
        {
            return UpdateModel(new EventModerator());
        }

        public EventModerator UpdateModel(EventModerator modelForUpdate)
        {
            modelForUpdate.EventID = EventId;
            modelForUpdate.DateAtachToModerators = DateAtachToModerators;
            modelForUpdate.UserID = UserId;
            modelForUpdate.DateDetachFromModerators = DateDetachFromModerators;
            return modelForUpdate;
        }
    }
}