﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DataModel;
using DataModel.ModelInterfaces;
using OrangeJetpack.Localization;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Models.ViewModels
{
    public class SelectHall
    {
        public Guid Id { get; set; }
    }

    public class SelectSection
    {
        public Guid Id { get; set; }
    }

    public class SelectSpeakers
    {
        public List<SpeakerItem> SpeakerItems { get; set; }
    }

    public class SpeakerItem : BaseVM, IModelBuilder<SpeakerForReport>
    {

        public Guid SpeakerId { get; set; }

        public string SpeakerName { get; set; }

        public string ReportName { get; set; }

        public Guid EventId { get; set; }

        public DateTimeRangeVM DateRange { get; set; }

        public SpeakerForReport GetModel()
        {
            var model = new SpeakerForReport()
            {
                ID = ID,
                SpeakerID = SpeakerId,
                CreateDate = DateTime.UtcNow,
                EventID = EventId,
                ReportName = GetLocalizeField(ReportName)


            };
            if (DateRange != null)
            {
                if (DateRange.StartDate != null)
                {
                    model.StartTime = DateRange.StartDate.Date;
                    model.FinishTime = DateRange.EndDate.Date;
                }
            }
            return model;
        }

        public SpeakerForReport UpdateModel(SpeakerForReport modelForUpdate)
        {
            return modelForUpdate;
        }
    }

    public class SelectDirections
    {
        public Guid? Id { get; set; }
    }

    public class EditDirections
    {
        public List<KeyValuePair<Guid, string>> DirectionItems { get; set; }
    }

    public class ReportBaseVM : BaseVM, IViewModelBuilder<Report>
    {
        public Guid EventId { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Title")]
        public string Name { get; set; }


        [Display(ResourceType = typeof(Resources), Name = "Report_IsPublish")]
        public bool IsPublished { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "Report_IsPrivate")]
        public bool IsPrivate { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "Report_Period")]
        public DateTimeRangeVM DateRange { get; set; }


        public ReportBaseVM()
        {

        }

        public ReportBaseVM(Report model)
        {
            Fill(model);
        }



        public void Fill(Report dbEntity)
        {
            ID = dbEntity.ID;
            Name = dbEntity.Localize(CurrentLocalization.Edit, d => d.Name).Name;
            IsPublished = dbEntity.IsPublished;
            IsPrivate = dbEntity.IsPrivateEvent;
            EventId = dbEntity.EventID;

            DateRange = new DateTimeRangeVM()
            {
                EndDate = new DateTimeVM(dbEntity.FinishDateTime),
                StartDate = new DateTimeVM(dbEntity.StartDateTime)
            };

        }
    }


    public class ReportVM : ReportBaseVM, IModelBuilder<Report>, IViewModelBuilder<Report>
    {
        public ReportVM()
        {

        }

        public ReportVM(Report model)
        {
            Fill(model);
        }

        public new void Fill(Report dbEntity)
        {
            base.Fill(dbEntity);
            WithSubReports = dbEntity.WithSubReport;
            if (dbEntity.HallID.HasValue)
            {
                Hall = new SelectHall()
                {
                    Id = dbEntity.HallID.Value
                };
            }
            if (dbEntity.SectionID.HasValue)
            {
                Section = new SelectSection()
                {
                    Id = dbEntity.SectionID.Value
                };
            }

            Direction = new SelectDirections()
            {
                Id = dbEntity.DirectionID
            };

            Description = dbEntity.Localize(CurrentLocalization.Edit, d => d.FullDescription).FullDescription;
            Speakers = new SelectSpeakers();

            Speakers.SpeakerItems = dbEntity.SpeakersForReport.Select(d => new SpeakerItem()
            {
                ID = d.ID,
                EventId = d.EventID,
                SpeakerName = d.Speaker.Localize(CurrentLocalization.Edit, s => s.Name).Name,
                DateRange = new DateTimeRangeVM()
                {
                    StartDate = new DateTimeVM(d.StartTime),
                    EndDate = new DateTimeVM(d.FinishTime),

                },
                ReportName = d.Localize(CurrentLocalization.Edit, o => o.ReportName).ReportName,
                SpeakerId = d.SpeakerID

            }).ToList();

        }
        public bool WithSubReports { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "t_Hall")]
        public SelectHall Hall { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "t_Direction")]
        public SelectDirections Direction { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "t_Section")]
        public SelectSection Section { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_Description")]
        public string Description { get; set; }

         [Display(ResourceType = typeof(Resources), Name = "t_Speakers")]
        public SelectSpeakers Speakers { get; set; }

        public Report GetModel()
        {
            var name = new List<LocalizedContent>();
            var desctiption = new List<LocalizedContent>();



            var cultures = CurrentEvent.GetAccessCultures();
            foreach (var culture in cultures)
            {
                name.Add(new LocalizedContent(culture.Key, Name));
                if (Description != null)
                    desctiption.Add(new LocalizedContent(culture.Key, Description));
            }

            var model = new Report()
            {
                ID = ID,
                EventID = EventId,
                UpdateDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow,
                Name = Name,
                HallID = Hall.Id,
                FullDescription = Description,
                SectionID = Section.Id,
                StartDateTime = DateRange.StartDate.Date,
                FinishDateTime = DateRange.EndDate.Date,
                DirectionID = Direction.Id,
                WithSubReport = WithSubReports

                //Directions = new Collection<Direction>(Directions.Ids.Select(d=>new Direction(){ID = d}).ToList())

            };


            SelectSpeakers speakers = null;
            if (Speakers != null && Speakers.SpeakerItems != null)
            {
                foreach (var speakerItem in Speakers.SpeakerItems)
                {
                    speakerItem.EventId = EventId;
                }
                model.SpeakersForReport = Speakers != null && Speakers.SpeakerItems != null
                    ? Speakers.SpeakerItems.Select(d => d.GetModel()).ToList()
                    : null;
            }
            return model;
        }

        public Report UpdateModel(Report modelForUpdate)
        {

            var model = GetModel();
            modelForUpdate.Name = Localization(modelForUpdate.Name, Name);
            modelForUpdate.FullDescription = Localization(modelForUpdate.FullDescription, Description);
            modelForUpdate.FullDescription = model.FullDescription;
            modelForUpdate.HallID = model.HallID;

            modelForUpdate.StartDateTime = model.StartDateTime;
            modelForUpdate.FinishDateTime = model.FinishDateTime;

            modelForUpdate.SectionID = model.SectionID;
            modelForUpdate.DirectionID = model.DirectionID;
            modelForUpdate.IsPrivateEvent = model.IsPrivateEvent;
            modelForUpdate.IsPublished = model.IsPublished;
            modelForUpdate.SpeakersForReport = model.SpeakersForReport;
            modelForUpdate.UpdateDate = DateTime.UtcNow;
            modelForUpdate.WithSubReport = model.WithSubReport;
            modelForUpdate.SpeakersForReport = model.SpeakersForReport;
            return modelForUpdate;

        }
    }


    public class OrderFilterVM
    {

        public string Date { get; set; }
        public List<SelectListItem> Dates { get; set; }

        public Guid Section { get; set; }
        public List<SelectListItem> Sections { get; set; }

        public Guid Direction { get; set; }
        public List<SelectListItem> Directions { get; set; }

        public Guid Hall { get; set; }
        public List<SelectListItem> Halls { get; set; }

    }


}