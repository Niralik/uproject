﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;

namespace Unfo.Models.ViewModels.Interfaces
{
    public interface IImageRepository
    {
        UploadImage SaveImage(Guid imageId, string fileName, int sourceWidth, int sourceHeight);

        void UpdateImage(Guid imageId, int selectedWidth, int selectedHeight, int selectedX, int selectedY);
    }
}