﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Helpers;
using DataModel;
using OrangeJetpack.Localization;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Models.ViewModels
{

    public interface ILocalizeFormVM<TViewModel, TModel>
        where TViewModel : class, new()
        where TModel : class, ILocalizable, new()
    {
        IList<TViewModel> Forms { get; }

        LocalizeForm<TViewModel> CreateLocalizeForm(TModel u);

        void SetLocalizeForms(IList<TViewModel> forms);
        LocalizeForm<TViewModel> CreateLocalizeForm();

    }


    public class LocalizeForm<TViewModel> where TViewModel : class, new()
    {
        public IList<TViewModel> Forms { get; set; }

        public LocalizeForm()
        {
        }

        public LocalizeForm(IList<TViewModel> forms)
        {
            Forms = forms;
        }

        public Type GetFormType()
        {
            return typeof(TViewModel);
        }
    }




    public class BaseEventVM<TModel>:BaseVM where TModel : EventBaseModel, new()
    {
        private string _language;
        public string Language
        {
            set { _language = value; }
            get
            {
                if (!string.IsNullOrWhiteSpace(_language)) return _language;
                return CurrentLocalization.Edit;
            }
        }

        public TModel Model { get; private set; }

        public Guid EventId { get; set; }

        

        public string Localization(string fieldData, string val)
        {
            return Core.Helpers.LocalizeHelper.Localization(Language, fieldData, val);
        }

        public BaseEventVM() { }

        public BaseEventVM(TModel model)
        {
            Fill(string.Empty, model);
        }

        public BaseEventVM(string language)
        {
            Fill(language);
        }


        public BaseEventVM(string language, Guid eventId)
        {
            Fill(language, null, eventId);
        }

        public virtual void Fill(string language, TModel model = null, Guid? eventId = null)
        {
            Language = language;
            Model = model;
            if (model != null)
            {
                ID = model.ID;
                EventId = model.EventID;
            }
            if (eventId.HasValue)
            {
                EventId = eventId.Value;
            }


        }

        public void Fill(TModel model = null, Guid? eventId = null)
        {

            this.Fill(string.Empty, model, eventId);

        }



    }

    public class LocalizeBaseVM<TViewModel, TModel> :
        BaseEventVM<TModel>, ILocalizeFormVM<TViewModel, TModel>

        where TViewModel : BaseEventVM<TModel>, new()
        where TModel : EventBaseModel, new()
    {


        public IList<TViewModel> Forms { get; private set; }



        public LocalizeBaseVM() { }



        public LocalizeBaseVM(string language)
            : base(language)
        {

        }

        public LocalizeBaseVM(Guid eventId)
        {
            Fill(string.Empty, null, eventId);
        }

        public LocalizeBaseVM(string language, Guid eventId)
            : base(language, eventId)
        {

        }

        public LocalizeForm<TViewModel> CreateLocalizeForm(TModel u)
        {
            //Fill(u);

            Forms = new List<TViewModel>();
            foreach (var accessCulture in CurrentEvent.GetAccessCultures())
            {
                var tm = new TViewModel();
                if (u == null)
                    tm.Fill(accessCulture.Key, null);
                else
                    tm.Fill(accessCulture.Key, (TModel)u.Clone());

                Forms.Add(tm);
            }
            return new LocalizeForm<TViewModel>(Forms);
        }

        public void SetLocalizeForms(IList<TViewModel> forms)
        {
            Forms = forms;
        }

        public LocalizeForm<TViewModel> CreateLocalizeForm()
        {
            Forms = new List<TViewModel>();
            foreach (var accessCulture in CurrentEvent.GetAccessCultures())
            {
                var tm = new TViewModel();
                if (Model == null)
                    tm.Fill(accessCulture.Key, null);
                else
                    tm.Fill(accessCulture.Key, (TModel)Model.Clone());

                Forms.Add(tm);
            }
            return new LocalizeForm<TViewModel>(Forms);
        }
    }
}