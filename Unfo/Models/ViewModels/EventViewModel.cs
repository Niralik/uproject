﻿using System.ComponentModel;
using DataModel;
using DataModel.ModelInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using OrangeJetpack.Localization;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Core.Helpers;
using Unfo.Core.ValidationAttributes;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Models.ViewModels
{

    public class EventVM : BaseVM, IViewModelBuilder<UEvent>, IModelBuilder<UEvent>
    {
        #region Поля
        //public virtual Guid ID { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_Name_Название")]
        public virtual string Name { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_Name_Описание")]
        public virtual string Description { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_DatePeriod")]
        public virtual DateRangeVM DatePeriod { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_PromoCode_Промокод")]
        public virtual string PromoCode { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_IsPrivate_Закрытое_мероприятие")]
        public virtual string TranslitName { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_IsPrivate_Закрытое_мероприятие")]
        public virtual bool IsPrivate { get; set; }

         [Display(ResourceType = typeof(Resources), Name = "EventVM_Directions")]
        public EditDirections Directions { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "EventVM_EventPlace")]
        public Guid? EventPlaceId { get; set; }

        public virtual bool IsDelete { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "c_HashCode")]
        public virtual string HashTag { get; set; }

        public virtual string WebSite { get; set; }

        public int TariffId { get; set; }
        public LocationVM Location { get; set; }

        public List<Guid> Moderators { get; set; }

        public bool IsFavoriteForCurrentUser { get; set; }
        #endregion Поля

        public EventVM()
        {
        }

        public EventVM(UEvent data)
        {
            Fill(data);
        }

        public virtual void Fill(UEvent data)
        {
            ID = data.ID;
            Name = GetLocalizeField(data.Name);
            TranslitName = data.TranslitName;
            Description = GetLocalizeField(data.Description);
            DatePeriod = new DateRangeVM();
            DatePeriod.StartDate = new DateVM(data.StartDate);
            DatePeriod.EndDate = new DateVM(data.FinishDate);
            PromoCode = data.PromoCode;
            IsPrivate = data.IsPrivate;
            IsDelete = data.IsDelete;
            TariffId = data.TarifId;
            HashTag = data.HashTag;
            EventPlaceId = data.LocationId;
            WebSite = data.WebSite;
            Directions = new EditDirections();
            Directions.DirectionItems = data.Directions.Where(d => !d.IsDelete).Localize(CurrentLocalization.Edit, d => d.Name)
                .Select(d => new KeyValuePair<Guid, string>(d.ID, d.Name)).ToList();
            Location = new LocationVM(data.ID);
            Location.Language = CurrentLocalization.UI;
            Location.Fill(data.Location);

            Moderators = data.Moderators.Select(m => m.UserID).ToList();

            IsFavoriteForCurrentUser = data.Subscribers.Any(subscriber => subscriber.UserID == CurrentUser.Get.Id);
        }

        public virtual UEvent GetModel()
        {


            var e = new UEvent
            {
                ID = ID != Guid.Empty ? ID : Guid.NewGuid(),
                Name = Name,
                Description = Description,
                StartDate = DatePeriod.StartDate.Date,
                FinishDate = DatePeriod.EndDate.Date,
                PromoCode = PromoCode,
                //TranslitName = Transliteration.Front(Name).ToLower(),
                IsPrivate = IsPrivate,
                IsDelete = IsDelete,
                TarifId = TariffId,
                WebSite = WebSite
            };
            return e;
        }


        public UEvent UpdateModel(UEvent modelForUpdate)
        {
            modelForUpdate.LocationId = EventPlaceId;
            modelForUpdate.Name = Localization(modelForUpdate.Name, Name);
            modelForUpdate.FinishDate = DatePeriod.EndDate.Date;
            modelForUpdate.StartDate = DatePeriod.StartDate.Date;
            modelForUpdate.HashTag = HashTag;
            modelForUpdate.IsPrivate = IsPrivate;
            modelForUpdate.WebSite = WebSite;
            modelForUpdate.PromoCode = PromoCode;
            modelForUpdate.Description = Localization(modelForUpdate.Description, Description);

            return modelForUpdate;
        }
    }

    public class CreateEventVM
    {

        [Required(ErrorMessage = "Выберите тариф")]
        public int TariffId { get; set; }


        [Required(ErrorMessage = "Укажите название мероприятия")]
        public string Name { get; set; }

        public bool IsBranding { get; set; }

        public bool IsLocalize { get; set; }

        public bool HasAdministrator { get; set; }
    }

    public class UpdateEventVM : EventVM
    {
        //[EventNameValidation(ErrorMessage = "Уже есть событие с таким названием")]
        public override string Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }




    }

    public class InfoEventVM : EventVM, IModelBuilder<UEvent>
    {
    

        public IEnumerable<ReportVM> Reports { get; set; }

        public override void Fill(UEvent data)
        {
            base.Fill(data);
         


            Reports = data.Reports.Select(d => new ReportVM(d));
        }


    }


    public class UploadFile
    {
        public Guid FileId { get; set; }
    }

    public enum Tariff
    {
        [Display(ResourceType = typeof(Resources), Name = "Tariff1")]
        Tariff1,
        [Display(ResourceType = typeof(Resources), Name = "Tariff2")]
        Tariff2,
        [Display(ResourceType = typeof(Resources), Name = "Tariff3")]
        Tariff3
    }

    public class SettingsVM
    {
        public SettingsVM()
        {

        }

        public SettingsVM(UEvent data)
        {
            EventId = data.ID;
            ServiceSectionsIds = data.ServiceSections.Select(d => d.ServiceSectionId).ToList();
            if (!string.IsNullOrWhiteSpace(data.SupportLanguages))
                SupportLanguages = data.SupportLanguages.Split(',').Select(d => d.Trim()).ToList();
            else
            {
                SupportLanguages = CurrentEvent.GetAccessCultures().Select(d => d.Key).ToList();
            }
            IOSAppLink = data.ApplicationLinkIOS;
            AndroidAppLink = data.ApplicationLinkAndroid;
            Branding = data.IsBranding;
            Localization = data.IsLocalization;
            if (data.CertificateId.HasValue)
            {
                CertificateId = data.CertificateId;
                CertificatePassword = data.CertificatePassword;
            }
            GoogleAppKey = data.GooglePushAPIKey;
            Support = data.IsSupport;
            TarifId = (Tariff)data.TarifId;
        }
        public Guid EventId { get; set; }

        [Display(ResourceType = typeof(Resources), Name = "SettingsVM_Разделы_Приложения")]
        public List<int> ServiceSectionsIds { get; set; }

        public List<string> SupportLanguages { get; set; }

        public string IOSAppLink { get; set; }

        public string AndroidAppLink { get; set; }

        public string CertificatePassword { get; set; }

        public Guid? CertificateId { get; set; }

        public string GoogleAppKey { get; set; }

        public bool Support { get; set; }

        public bool Branding { get; set; }

        public bool Localization { get; set; }

        public Tariff TarifId { get; set; }
    }
}