﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataModel;
using DataModel.ModelInterfaces;
using Owin.Security.Providers.ArcGISOnline.Provider;
using Unfo.Core;
using Unfo.Core.Helpers;
using Unfo.Core.Wrappers;
using Unfo.Models.ViewModels;

namespace Unfo.Models.UnfoBusinessLogic
{
    public class EventRepository : BaseViewModelRepository
    {
        public EventRepository()
            : base()
        {
        }

        public EventRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }

        public List<EventVM> GetEvents()
        {
            var events = Fabric.Table<UEvent>().DbSet.ToList().Select(d => new EventVM(d)).ToList();
            return events;
        }

        public TViewModel GetEvent<TViewModel>(Guid? id)
            where TViewModel : IViewModelBuilder<UEvent>, new()
        {
            if (!id.HasValue) return new TViewModel();
            var @event = Fabric.Table<UEvent>().Get(d => d.ID == id.Value && !d.IsDelete, d => d.OrderBy(r => r.StartDate), d => d.Reports,d=>d.Directions).SingleOrDefault();
            var vm = new TViewModel();
            vm.Fill(@event);
            return vm;
        }

        public UEvent GetByRouteName(string urlName)
        {
            if (String.IsNullOrWhiteSpace(urlName)) return null;

            var @event = Fabric.Table<UEvent>().Get(d => d.TranslitName == urlName, null, d=>d.Payers).FirstOrDefault();
            return @event;
        }


        public bool CheckIfEventNameExists(string name, Guid eventID)
        {
            var translitName = Transliteration.Front(name).ToLower();
            var @event = eventID != Guid.Empty ?
                Fabric.Table<UEvent>().DbSet.FirstOrDefault(d => d.TranslitName == translitName && d.ID != eventID) 
                :
                Fabric.Table<UEvent>().DbSet.FirstOrDefault(d => d.TranslitName == translitName);

            return @event == null;
        }


        public string Create(CreateEventVM data, Guid? ownerId)
        {
            var id = Guid.NewGuid();
            var name = GetTranslitName(data.Name);
            Fabric.Table<UEvent>().Insert(new UEvent()
            {
                ID = id,
                Name = data.Name,
                TarifId =  data.TariffId,
                IsLocalization = data.IsLocalize,
                IsBranding = data.IsBranding,
                IsSupport =  data.HasAdministrator,
                OwnerId = ownerId,
                SupportLanguages = "ru",
                TranslitName = name
            });
            Fabric.Table<UEvent>().Save();
            return name;
        }

        string GetTranslitName(string eventName)
        {
            var ename = Transliteration.Front(eventName).ToLower();
            var hasName = Fabric.Table<UEvent>().Get(d => d.TranslitName == ename).Any();
            var rnd = new Random();
            while (hasName)
            {
                ename = string.Format("{0}-{1}", ename, rnd.Next(1,1000000)).ToLower();
                hasName = Fabric.Table<UEvent>().Get(d => d.TranslitName == ename).Any();
            }

            return ename;

        }

        public void Update(EventVM data)
        {
            
            var rep = Fabric.Table<UEvent>();
            var @event = rep.GetById(data.ID);
            @event = data.UpdateModel(@event);

            Fabric.Table<UEvent>().Update(@event);
            Fabric.Table<UEvent>().Save();
        }

        public void DeleteEvent(Guid id)
        {
            Fabric.Table<UEvent>().Delete(id);
            Fabric.Table<UEvent>().Save();
        }

        public async Task<UnfoResult> CheckIsExistEvent(Guid eventId)
        {
            var isExistEvent = await Fabric.Table<UEvent>().GetQuery().AnyAsync(uEvent => uEvent.ID == eventId && !uEvent.IsDelete);
            return isExistEvent ? UnfoResult.Success : UnfoResult.Failed();
        }

        public async Task<UnfoResult> ToFavorite(Guid userId, Guid eventId)
        {
            try
            {
                var subscriptionToEvent = Fabric.Table<SubscriptionToEvent>();
                var subcsription = new SubscriptionToEvent() { EventID = eventId, UserID = userId };
                subscriptionToEvent.Insert(subcsription);
                await subscriptionToEvent.SaveAsync();
                return UnfoResult.Success;
            }
            catch (Exception)
            {
                return UnfoResult.Failed();
            }
        }
        public async Task<UnfoResult> UnFavorite(Guid userId, Guid eventId)
        {
            try
            {
                var subscriptionToEvent = Fabric.Table<SubscriptionToEvent>();
                var dbSubcsription = await subscriptionToEvent.GetQuery().SingleAsync(subcsription=> subcsription.EventID == eventId && subcsription.UserID == userId);
                subscriptionToEvent.Delete(dbSubcsription);
                await subscriptionToEvent.SaveAsync();
                return UnfoResult.Success;
            }
            catch (Exception)
            {
                return UnfoResult.Failed();
            }
        }
        public async Task<UnfoResult> CheckIsFavoriteForUser(Guid userId, Guid eventId)
        {
            var isExistSubscription = await Fabric.Table<SubscriptionToEvent>().GetQuery().AnyAsync(dbSubscription => dbSubscription.EventID == eventId && dbSubscription.UserID == userId && !dbSubscription.IsDelete);
            if (isExistSubscription) return UnfoResult.Success;
            return UnfoResult.Failed();
        }

        
    }
}