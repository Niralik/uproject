﻿using DataModel;
using Microsoft.AspNet.Identity.Owin;
using NinjaNye.SearchExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Unfo.Core;
using Unfo.Core.Attributes;
using Unfo.Core.Filters;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Models.ViewModelRepositories
{
    public class UsersVmRepository : BaseViewModelRepository
    {
        public UsersVmRepository()
        {
        }

        public UsersVmRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }


    }

    public class UsersFilter: BaseUnfoFilter<UnfoUser>
    {
     
        public new UsersFilterParams FilterParams { get { return (UsersFilterParams)base.FilterParams; } }

        public UsersFilter(IBaseFilterParams filterParams)
            : base(filterParams)
        {
            
        }

        #region Фильтрация запроса

        protected override void FiltrateQuery()
        {
            var initialUsersQuery = CurrentDbContext.Users.Where(user => !user.IsDelete);

            var filteredBySearchQuery = FiltrateBySearchQuery(initialUsersQuery);
            var filteredByEventId = FiltrateByEventId(filteredBySearchQuery);
            var filteteredByRolesQuery = FiltrateByRoles(filteredByEventId);

            FilteredQuery = filteteredByRolesQuery;
        }
        private IQueryable<UnfoUser> FiltrateByEventId(IQueryable<UnfoUser> incommingUsersQuery)
        {
            //Если в параметрах поисках указан модератор - то выходим из метода - так как модераторы не присуствуют в подписках
            if (FilterParams.EventId == default(Guid) || FilterParams.FilterRole == FilterRole.Moderator) return incommingUsersQuery;

            var filteredQueryByEventId = incommingUsersQuery.Where(user => user.EventsSubscriptions.Any(eventsSubs => eventsSubs.EventID == FilterParams.EventId));
            return filteredQueryByEventId;
        }
        protected override IQueryable<UnfoUser> SearchByCriteria(IQueryable<UnfoUser> incommingQuery, string[] searchTerms)
        {
            return searchTerms.Aggregate(incommingQuery, (users, term) => users.Search(SearchCriteria.Users).Containing(term));
        }
   
        private IQueryable<UnfoUser> FiltrateByRoles(IQueryable<UnfoUser> incommingUsersQuery)
        {
            if (FilterParams.FilterRole == FilterRole.None) return incommingUsersQuery;

            IQueryable<UnfoUser> filteredQueryByRoles;
            //Модератор это псевдо роль - поиск юзеров производится другой логикой
            if (FilterParams.FilterRole == FilterRole.Moderator)
            {
                var moderators = CurrentDbContext.Users.Where(user => user.ModeratedEvents.Count > 0);
                //Вытаскиваем только модераторов определенного события
                if (FilterParams.EventId != default(Guid)) moderators = moderators.Where(user => user.ModeratedEvents.Any(modEvents => modEvents.EventID == FilterParams.EventId && !modEvents.IsDelete));

                filteredQueryByRoles = incommingUsersQuery.Join(moderators, user => user.Id, moderator => moderator.Id, (user, moderator) => user);
            }
            else
            {
                var usersIdsFilteredByRole = CurrentDbContext.Roles.Where(role => role.Name == FilterParams.FilterRole.ToString()).SelectMany(role => role.Users);
                //объединям входящий запрос с запросом юзеров отфлитрованных по роли - в итоге получаем только тех пользователей которые имеют определенную роль
                //в уже параметрезированном запросе
                filteredQueryByRoles = incommingUsersQuery.Join(usersIdsFilteredByRole, user => user.Id, userRole => userRole.UserId, (user, userRole) => user);
            }

            return filteredQueryByRoles;
        }
        #endregion Фильтрация запроса

        protected override void SortQuery()
        {
            IQueryable<UnfoUser> sortedUsersQuery;
            switch (FilterParams.TypeSorting)
            {
                case TypeUsersSorting.None:
                    return;

                case TypeUsersSorting.SignedUpAsc:
                    sortedUsersQuery = FilteredQuery.OrderBy(user => user.SignUpDate);
                    break;

                case TypeUsersSorting.SignedUpDsc:
                    sortedUsersQuery = FilteredQuery.OrderByDescending(user => user.SignUpDate);
                    break;

                case TypeUsersSorting.LastSignInAsc:
                    sortedUsersQuery = FilteredQuery.OrderBy(user => user.LastSignInDate);
                    break;

                case TypeUsersSorting.LastSignInDsc:
                    sortedUsersQuery = FilteredQuery.OrderByDescending(user => user.LastSignInDate);
                    break;

                case TypeUsersSorting.NameAsc:
                    sortedUsersQuery = FilteredQuery.OrderBy(user => user.SurName);
                    break;

                case TypeUsersSorting.NameDsc:
                    sortedUsersQuery = FilteredQuery.OrderByDescending(user => user.SurName);
                    break;

                case TypeUsersSorting.Location:
                    throw new ArgumentOutOfRangeException(MethodBase.GetCurrentMethod().Name);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(MethodBase.GetCurrentMethod().Name);
            }
            FilteredQuery = sortedUsersQuery;
        }

    }



    public class UsersFilterParams : BaseUnfoFilterParams<UsersFilterParams>, IPageableParams<UsersFilterParams>
    {

        private TypeUsersSorting _typeSorting;
      
        public Guid EventId { get; set; }

        public FilterRole FilterRole { get; set; }

        public TypeUsersSorting TypeSorting
        {
            get { return _typeSorting == TypeUsersSorting.None ? TypeUsersSorting.NameAsc : _typeSorting; }
            set { _typeSorting = value; }
        }

        public UsersFilterParams()
        {

        }

        public UsersFilterParams SetSort(TypeUsersSortingVm sorting)
        {
            var clone = Clone();
            switch (sorting)
            {
                case TypeUsersSortingVm.SignedUp:
                    clone.TypeSorting = TypeSorting == TypeUsersSorting.SignedUpAsc ? TypeUsersSorting.SignedUpDsc : TypeUsersSorting.SignedUpAsc;
                    break;
                case TypeUsersSortingVm.Name:
                    clone.TypeSorting = TypeSorting == TypeUsersSorting.NameAsc ? TypeUsersSorting.NameDsc : TypeUsersSorting.NameAsc;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("sorting");
            }
            return clone;
        }
     
        public UsersFilterParams SetFilterRole(FilterRole role)
        {
            var clone = Clone();
            clone.FilterRole = role;
            return clone;
        }

        public UsersFilterParams SetEventId(Guid eventId)
        {
            var clone = Clone();
            clone.EventId = eventId;
            return clone;
        }
        public UsersFilterParams SetCurrentPage(int numCurrentPage)
        {
            CurrentPage = numCurrentPage;
            return this;
        }
    }

    /// <summary>
    /// Список Ролей и Псевдой ролей используемых в системе -
    /// псевдороли - это разрешение на доступ к какой либо части UI но не роль в своем понимании
    /// например Модератор - это ПсевдоРоль
    /// </summary>
    public enum FilterRole
    {
        None,
        Admin,
        Organizer,
        Moderator,
        User
    }
    public enum TypeUsersSortingVm
{
        SignedUp,
        Name
}
    public enum TypeUsersSorting
    {
        None,
        SignedUpAsc,
        SignedUpDsc,
        LastSignInAsc,
        LastSignInDsc,
        NameAsc,
        NameDsc,
        Location
    }

}