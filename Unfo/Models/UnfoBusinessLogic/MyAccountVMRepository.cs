﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DataModel;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Unfo.App_LocalResources;
using Unfo.Areas.Cabinet.Controllers;
using Unfo.Core;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Core.ValidationAttributes;
using Unfo.Core.Wrappers;
using Unfo.Models.ViewModels;

namespace Unfo.Models.ViewModelRepositories
{

    
    public class MyAccountVMRepository : BaseViewModelRepository
    {
        public MyAccountVMRepository()
            : base()
        {
        }
        public MyAccountVMRepository(IRepositoryFabric repFabric) : base(repFabric)
        {
        }
        public async Task<JsonResult> AddPassword(ManagePasswordVM managePasswordVm)
        {
            var resultOfAdd = await UserManager.AddPasswordAsync(CurrentUser.Id, managePasswordVm.Password);
            if (resultOfAdd.Succeeded)
            {
                var resultOfUpdate = await SignInManager.UpdateCookieForCurrentUserAsync();
                if (resultOfUpdate.Succeeded) return AlertJson.Success(Resources.MyAccount_AddPassword_Alert_Success);         
            }
            return AlertJson.Error(Resources.Validate_Alert_IternalServerError);
        }

        public async Task<JsonResult> ChangePassword(ManagePasswordVM managePasswordVm)
        {
            var resultOfChanging = await UserManager.ChangePasswordAsync(CurrentUser.Id, managePasswordVm.OldPassword, managePasswordVm.Password);
            if (resultOfChanging.Succeeded)
            {
                return AlertJson.Success(Resources.MyAccount_ChangePassword_Alert_Success);
            }
            return AlertJson.Error(Resources.MyAccount_ChangePassword_Alert_OldPasswordError);
        }

        public async Task<UserRequestVm> GetUserRequest(Guid UserId)
        {
            //вытаскиваем самый последний запрос пользователя по дате или ничего если от этого пользователя нет запросов
            var dbUserRequest = await Fabric.Table<Request>().GetQuery().Where(request => request.AuthorID == UserId && request.RequestType == TypeOfRequest.GetOrganizer && !request.IsDelete)
                                                                        .OrderByDescending(request => request.CreateRequestDate).FirstOrDefaultAsync();
            return dbUserRequest != null ? new UserRequestVm(dbUserRequest) : new UserRequestVm();
        }


        public async Task<UnfoResult> CreateNewUserRequest(UserRequestVm userRequestVm)
        {
            try
            {
                var userRequestRepository = Fabric.Table<Request>();
                userRequestRepository.Insert(userRequestVm.GetModel());
                await userRequestRepository.SaveAsync();
                return UnfoResult.Success;
            }
            catch (Exception)
            {
                return UnfoResult.Failed();
            }
            
        }
    }

    public enum ExternalLogins
    {
        [CssClass("fa-twitter")]
        Twitter,
        [CssClass("fa-vk")]
        Vkontakte,
        [CssClass("fa-google")]
        GooglePlus,
        [CssClass("fa-facebook")]
        Facebook
    }
    public enum AccountStatusMessage
    {
        [Display(ResourceType = typeof(Resources), Name = "Account_SignIn_LockoutError_Alert")]
        ToLinkExternalLoginSuccess,
        Error,
        UserWithExternalLoginExistError
    }
}