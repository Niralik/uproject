﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using DataModel.ModelInterfaces;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Models.ViewModelRepositories
{
    public class VotingRepository:BaseViewModelRepository
    {

        public VotingRepository()
            : base()
        {
        }

        public VotingRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }

        #region Голосование

        public VoteViewModel GetVote(Guid id)
        {
            var vote = Fabric.Table<Vote>().DbSet.SingleOrDefault(d=>d.ID == id);
            return new VoteViewModel(vote, CurrentLocalization.Edit);
        }


        public IEnumerable<ShortVoteViewModel> GetVotes(Guid eventId)
        {
            var data = Fabric.Table<Vote>().DbSet.Where(d => !d.IsDelete && d.EventID == eventId)
                .ToList().Select(d => new ShortVoteViewModel(d, CurrentLocalization.Edit)).ToList();

            //foreach (var vote in data)
            //{
            //    var res = GetVoteWithResults(CurrentEvent.ID, vote.ID);
            //    vote.Sum = res.Sum;
            //    //if (vote.sum > 0)
            //    //    vote.isFinished = true;
            //}

            return data;
        }


        public List<VoteQuestionViewModel> GetQuestions(Guid id)
        {
            var data = Fabric.Table<VoteQuestion>().DbSet.Where(d => d.VoteID == id).ToList().Select(d => new VoteQuestionViewModel(d, CurrentLocalization.Edit)).ToList();
            return data;
        }

        //public void EditVote(LocalizeForm<VoteViewModel> data)
        //{

        //    var v = new Vote();
        //    if (data.Forms != Guid.Empty)
        //    {
        //        v = Fabric.Get<Vote>().GetById(data.Id);
        //    }

        //    v.Name = data.Name;
        //    v.Description = data.Description;
              
        //    v.ID = data.Id;
        //    v.IsOnline = data.IsOnline;
        //    v.Type = data.IsOpros ? VoteTypes.Quiz : VoteTypes.Vote; 
        //    v.EventID = CurrentEvent.ID;

        //    Fabric.Get<Vote>().Insert(v);
        //    Fabric.Get<Vote>().Save();

        //    if (data.IsOpros) return;

        //    if (data.Questions == null || data.Questions.Count <= 0) return;

        //    foreach (var item in data.Questions.Where(item => item.Name != ""))
        //    {
        //        UpdateQuestion(item, v.ID);
        //    }
        //}


        public void UpdateQuestion(VoteQuestionViewModel data, Guid eventID)
        {
            var q = new VoteQuestion();
            if (data.ID != Guid.Empty)
            {
                q = Fabric.Table<VoteQuestion>().GetById(data.ID);
            }

            q.VoteID = eventID;
            if (data.ID != Guid.Empty)
            {
                Fabric.Table<VoteQuestion>().Update(q);
            }
            else
            {
                Fabric.Table<VoteQuestion>().Insert(q);
            }
            Fabric.Table<VoteQuestion>().Save();
        }

        public void DeleteVote(Guid id)
        {
            var data = Fabric.Table<Vote>().GetById(id);
            if (data != null)
                data.IsDelete = true;

            Fabric.Table<Vote>().Update(data);
            Fabric.Table<Vote>().Save();
        }



        public void ToogleVote(Guid id)
        {
            VoteViewModel data;
            var vote = Fabric.Table<Vote>().GetById(id);
            if (vote != null && vote.ID != Guid.Empty)
            {
                if (vote.IsOnline == false)
                {
                    vote.IsOnline = true;
                }
                else
                {
                    vote.IsFinished = vote.IsFinished == false;
                }
                Fabric.Table<Vote>().Update(vote);
            }
            Fabric.Table<Vote>().Save();
        }



        public void Vote(Guid question, Guid userID)
        {
            var rep = Fabric.Table<VoteResult>();
            rep.Insert(new VoteResult()
            {
                DateVote = DateTime.Now,
                UserID = CurrentUser.Id,
                QuestionID = question,
            });

            rep.Save();
        }

        public void VoteWithAnswer(Guid voteID, string answer)
        {
            Fabric.Table<VoteResult>().Insert(new VoteResult()
            {
                DateVote = DateTime.Now,
                UserID = CurrentUser.Id, 
                VoteId = voteID,
                Answer = answer
            });
            Fabric.Table<VoteResult>().Save();
        }


        public bool HasVote(Guid voteID, Guid userID)
        {
            var voteResult =  Fabric.Table<VoteResult>().DbSet.SingleOrDefault(d => d.UserID == userID && (d.Question.VoteID == voteID || d.VoteId == voteID));
            return voteResult != null;
        }


        public VoteViewModel GetVoteWithResults(Guid eventId, Guid id)
        {
            var result = GetVote(id);


            if (!result.IsOpros)
            {
                var totalCount = 0;
                foreach (var voteQuestion in result.Questions)
                {
                    var sum = Fabric.Table<VoteResult>().DbSet.Count(d => d.QuestionID == voteQuestion.ID);
                    totalCount += sum;
                    voteQuestion.Result = sum;
                }
                result.Sum = totalCount;
            }
            else
            {
                result.Answers = new List<string>();
                var data = Fabric.Table<VoteResult>().DbSet.Where(d => d.VoteId == id).ToList();
                foreach (var voteAnswer in data)
                {
                    result.Answers.Add(voteAnswer.Answer);
                }
                result.Sum = result.Answers.Count;
            }
            result.Sum = result.Sum;
            return result;
        }


        public ShortVoteViewModel GetVoteByQuestion(Guid id, string lang)
        {
            var q = Fabric.Table<VoteQuestion>().GetById(id);
            if (q == null || q.ID == Guid.Empty) return new ShortVoteViewModel();

            var data = new ShortVoteViewModel(q.Vote, lang);
           
            return data;
        }


        public bool HasVotes(Guid eventId, Guid id)
        {
            var results = GetVoteWithResults(eventId, id);
            return results.Sum > 0;
        }

        #endregion





        public LocalizeForm<TViewModel> Get<TViewModel, TModel>(Guid eventId, Guid id)
            where TModel : EventBaseModel, new()
            where TViewModel : LocalizeBaseVM<TViewModel, TModel>, new()
        {
            var model =
                Fabric.Table<TModel>()
                    .Get(d => d.ID == id && d.EventID == eventId, null, null).SingleOrDefault();

            var se = new TViewModel();
            return se.CreateLocalizeForm(model); ;
        }


        public Guid AddOrUpdate(VoteViewModel data)
        {
            //base.AddOrUpdate<Vote>(data);

            var rep = Fabric.Table<Vote>();
            var model = data.GetModel();
            if (model.ID == Guid.Empty)
            {
                rep.Insert(model);
                rep.Save();
            }
            else
            {
                var report = rep.GetById(model.ID);
                if (report.EventID == CurrentEvent.ID)
                {
                    report = data.UpdateModel(report);
                    rep.Update(report);
                    rep.Save();
                }
            }

            if (data.Questions != null)
            {
                foreach (var question in data.Questions)
                {
                    UpdateQuestions(question, model.ID);
                }
            }

            return model.ID;
        }


        public void UpdateQuestions(VoteQuestionViewModel data, Guid voteID)
        {
            var rep = Fabric.Table<VoteQuestion>();
            var model = data.GetModel();
            model.VoteID = voteID;
            if (model.ID == Guid.Empty)
            {
                rep.Insert(model);
                rep.Save();
            }
            else
            {
                var question = rep.GetById(data.ID);
                question = data.UpdateModel(question);
                rep.Update(question);
                rep.Save();
            }
        }
    }
}