﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataModel;
using NinjaNye.SearchExtensions;
using System.Linq;
using Unfo.Core.Filters;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Models.UnfoBusinessLogic.Events
{
    public class EventsFilter : BaseUnfoFilter<UEvent>
    {
        public new EventsFilterParams FilterParams { get { return (EventsFilterParams)base.FilterParams; } }

        public EventsFilter(IBaseFilterParams filterParams)
            : base(filterParams)
        {
        }

        protected override void SortQuery()
        {
            FilteredQuery = FilteredQuery.OrderByDescending(uEvent => uEvent.StartDate);
        }

        #region Фильтрация запроса

        protected override void FiltrateQuery()
        {
            var initialQuery = CurrentDbContext.Events.Where(request => !request.IsDelete);
            var filteredBySearchQuery = FiltrateBySearchQuery(initialQuery);
            var filteredByTime = FiltrateByTime(filteredBySearchQuery);
            var filteredByQuery = FiltrateByQuery(filteredByTime);
            var filteredByRole = FiltrateByRole(filteredByQuery);

            FilteredQuery = filteredByRole;
        }

        private IQueryable<UEvent> FiltrateByTime(IQueryable<UEvent> incommingQuery)
        {
            if (FilterParams.Time == FilterTime.None) return incommingQuery;
            IQueryable<UEvent> filteredByTimeQuery = null;
            switch (FilterParams.Time)
            {
                case FilterTime.Active:
                    filteredByTimeQuery = incommingQuery.Where(uEvent => uEvent.StartDate <= DateTime.Now && uEvent.FinishDate >= DateTime.Now);
                    break;
                case FilterTime.Past:
                    filteredByTimeQuery = incommingQuery.Where(uEvent => uEvent.FinishDate < DateTime.Now);
                    break;
                case FilterTime.Future:
                    var start = DateTime.Now.Date.AddDays(1);
                    filteredByTimeQuery = incommingQuery.Where(uEvent => DbFunctions.TruncateTime(uEvent.StartDate) >= start);
                    break;
            }
            return filteredByTimeQuery;
        }

        private IQueryable<UEvent> FiltrateByQuery(IQueryable<UEvent> incommingQuery)
        {
            if (FilterParams.Query == EventsFilterTypeQuery.None) return incommingQuery;
            IQueryable<UEvent> filteredByQuery = null;
            switch (FilterParams.Query)
            {
                case EventsFilterTypeQuery.Favorite:
                    var currentUserSubscriptions = CurrentDbContext.SubscriptionToEvents.Where(subscription=> subscription.UserID == CurrentUser.Get.Id);
                    filteredByQuery = incommingQuery.Join(currentUserSubscriptions, uEvent=> uEvent.ID, subsription=> subsription.EventID, (uEvent, subscription)=> uEvent);
                    break;

            }
            return filteredByQuery;
        }

        private IQueryable<UEvent> FiltrateByRole(IQueryable<UEvent> incommingQuery)
        {
            if (FilterParams.Role == FilterRole.None) return incommingQuery;
            IQueryable<UEvent> filteredByRole = null;
            switch (FilterParams.Role)
            {
                case FilterRole.Moderator:
                    var currentUserModerate = CurrentDbContext.EventModerators.Where(eventModerator => eventModerator.UserID == CurrentUser.Get.Id);
                    filteredByRole = incommingQuery.Join(currentUserModerate, uEvent=> uEvent.ID, eventModerate=> eventModerate.EventID, (uEvent, eventModerate)=> uEvent);
                    break;
                case FilterRole.User:
                    filteredByRole = incommingQuery.Take(0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return filteredByRole;
        }

        protected override IQueryable<UEvent> SearchByCriteria(IQueryable<UEvent> incommingQuery, string[] searchTerms)
        {
            var filteredQueryBySearchCriteria = searchTerms.Aggregate(incommingQuery, (users, term) => users.Search(SearchCriteria.Events).Containing(term));
            return filteredQueryBySearchCriteria;
        }

        #endregion Фильтрация запроса
    }

    public class EventsFilterParams : BaseUnfoFilterParams<EventsFilterParams>, IPageableParams<EventsFilterParams>
    {
        public FilterRole Role { get; set; }
        public EventsFilterTypeQuery Query { get; set; }
        public FilterTime Time { get; set; }
        public EventsFilterParams()
        {
        }

        /// <summary>
        /// Метод устанавливает номер страницы и возвращает измененный объект
        /// </summary>
        public EventsFilterParams SetCurrentPage(int numCurrentPage)
        {
            CurrentPage = numCurrentPage;
            return this;
        }

        public EventsFilterParams SetTime(FilterTime newTime)
        {
            var clone = Clone();
            clone.Time = newTime;
            return clone;
        }
        public EventsFilterParams SetRole(FilterRole newFilterRole)
        {
            var clone = Clone();
            clone.Role = newFilterRole;
            return clone;
        }
        public EventsFilterParams SetQuery(EventsFilterTypeQuery query)
        {
            var clone = Clone();
            clone.Query = query;
            return clone;
        }
        public Task<ActionResult> GetAction()
        {
            var action = MVC.Cabinet.Events.All(this);
            if (Role == FilterRole.Moderator)
            {
                action = MVC.Cabinet.Events.MyModerate(this);
            }
            if (Query == EventsFilterTypeQuery.Favorite)
            {
                action = MVC.Cabinet.Events.Favorite(this);
            }
            if (Time == FilterTime.Active)
            {
                action = MVC.Cabinet.Events.Active(this);
            }
            return action;
        }
    }

    public enum EventsFilterTypeQuery
    {
        None,
        Favorite
    }

    public enum FilterTime
    {
        None,
        Active,
        Past,
        Future
    }
}