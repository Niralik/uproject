﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using DataModel.ModelInterfaces;
using Unfo.Core;
using Unfo.Core.Helpers;
using Unfo.Models.ViewModels;

namespace Unfo.Models.UnfoBusinessLogic.Events
{
    public class EventRepository : BaseViewModelRepository
    {
        public EventRepository()
            : base()
        {
        }

        public EventRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }

        public List<EventVM> GetEvents()
        {
            var events = Fabric.Table<UEvent>().DbSet.ToList().Select(d => new EventVM(d)).ToList();
            return events;
        }

        public TViewModel GetEvent<TViewModel>(Guid? id)
            where TViewModel : IViewModelBuilder<UEvent>, new()
        {
            if (!id.HasValue) return new TViewModel();
            var @event = Fabric.Table<UEvent>().Get(d => d.ID == id.Value && !d.IsDelete, d => d.OrderBy(r => r.StartDate), d => d.Reports, d => d.Directions).SingleOrDefault();
            var vm = new TViewModel();
            vm.Fill(@event);
            return vm;
        }

        public UEvent GetByRouteName(string urlName)
        {
            if (String.IsNullOrWhiteSpace(urlName)) return null;

            var @event = Fabric.Table<UEvent>().Get(d => d.TranslitName == urlName, null, d => d.Payers).FirstOrDefault();
            return @event;
        }


        public bool CheckIfEventNameExists(string name, Guid eventID)
        {
            var translitName = Transliteration.Front(name).ToLower();
            var @event = eventID != Guid.Empty ?
                Fabric.Table<UEvent>().DbSet.FirstOrDefault(d => d.TranslitName == translitName && d.ID != eventID)
                :
                Fabric.Table<UEvent>().DbSet.FirstOrDefault(d => d.TranslitName == translitName);

            return @event == null;
        }


        public string Create(CreateEventVM data, Guid? ownerId)
        {
            var id = Guid.NewGuid();
            var name = GetTranslitName(data.Name);
            Fabric.Table<UEvent>().Insert(new UEvent()
            {
                ID = id,
                Name = data.Name,
                TarifId = data.TariffId,
                IsLocalization = data.IsLocalize,
                IsBranding = data.IsBranding,
                IsSupport = data.HasAdministrator,
                OwnerId = ownerId,
                SupportLanguages = "ru",
                TranslitName = name
            });
            Fabric.Table<UEvent>().Save();
            return name;
        }

        string GetTranslitName(string eventName)
        {
            var ename = Transliteration.Front(eventName).ToLower();
            var hasName = Fabric.Table<UEvent>().Get(d => d.TranslitName == ename).Any();
            var rnd = new Random();
            while (hasName)
            {
                ename = string.Format("{0}-{1}", ename, rnd.Next(1, 1000000)).ToLower();
                hasName = Fabric.Table<UEvent>().Get(d => d.TranslitName == ename).Any();
            }

            return ename;

        }

        public void Update(EventVM data)
        {

            var rep = Fabric.Table<UEvent>();
            var @event = rep.GetById(data.ID);
            @event = data.UpdateModel(@event);

            Fabric.Table<UEvent>().Update(@event);
            Fabric.Table<UEvent>().Save();
        }

        public void DeleteEvent(Guid id)
        {
            Fabric.Table<UEvent>().Delete(id);
            Fabric.Table<UEvent>().Save();
        }
    }


}