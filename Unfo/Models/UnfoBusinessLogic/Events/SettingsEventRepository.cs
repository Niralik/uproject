﻿using System;
using System.Linq;
using System.Web;
using DataModel;
using Unfo.Core;
using Unfo.Models.ViewModels;

namespace Unfo.Models.UnfoBusinessLogic
{
    public class SettingsEventRepository : BaseViewModelRepository
    {
        public SettingsEventRepository()
            : base()
        {
        }
        public SettingsEventRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }

        private UEvent GetEvent(Guid eventId)
        {
            var uevent = Fabric.Table<UEvent>().Get(d => d.ID == eventId && !d.IsDelete, null, d => d.ServiceSections).FirstOrDefault();
            if (uevent == null) throw new HttpException(404, "Not found event");
            return uevent;
        }

        public SettingsVM GetSettings(Guid eventId)
        {
            var uevent = GetEvent(eventId);

            return new SettingsVM(uevent);
        }

        public void UpdateSettings(SettingsVM data)
        {
            var uevent = GetEvent(data.EventId);
            var sections = uevent.ServiceSections.ToList();
            var servicesections = Fabric.Table<ServiceSectionInEvent>();
            foreach (var item in sections)
            {
                servicesections.Delete(item);
            }
            servicesections.Save();

            if (data.ServiceSectionsIds != null)
            {
                //var sectionsList = Fabric.Table<ServiceSection>().Get(d => data.ServiceSectionsIds.Contains(d.ID));

                foreach (var item in data.ServiceSectionsIds)
                {
                    servicesections.Insert(new ServiceSectionInEvent()
                    {
                        EventId = data.EventId,
                        ID = Guid.NewGuid(),
                        ServiceSectionId = item
                    });
                }
            }
            

            uevent.SupportLanguages = data.SupportLanguages.Aggregate((i, j) => i + "," + j);
            uevent.ApplicationLinkAndroid = data.AndroidAppLink;
            uevent.ApplicationLinkIOS = data.IOSAppLink;
            uevent.IsLocalization = data.Localization;
            uevent.CertificatePassword = data.CertificatePassword;
            uevent.IsBranding = data.Branding;
            uevent.IsSupport = data.Support;
            uevent.TarifId = (int) data.TarifId;
            uevent.CertificateId = data.CertificateId;
            uevent.CertificatePassword = data.CertificatePassword;

            Fabric.Table<UEvent>().Update(uevent);
            Fabric.Table<UEvent>().Save();
        }
    }
}