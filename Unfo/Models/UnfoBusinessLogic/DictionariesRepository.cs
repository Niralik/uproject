﻿using DataModel;
using DataModel.ModelInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.Dictionaries;

namespace Unfo.Models.ViewModelRepositories
{
    public class DictionariesRepository : BaseViewModelRepository
    {
        public DictionariesRepository()
            : base()
        {
        }

        public DictionariesRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }




        #region Места
        public IEnumerable<LocationVM> GetLocations(Guid eventId)
        {
            var venues = Fabric.Table<Location>().Get(d => !d.IsDelete && d.EventID == eventId).ToList().Select(d => new LocationVM(d)).ToList();
            return venues;
        }

        public void DeleteLocation(Guid locationId,Guid eventId) 
        {
            var table = Fabric.Table<Location>();
            var loc = table.GetById(locationId);
            if (loc != null && loc.EventID == CurrentEvent.ID)
            {
                loc.IsDelete = true;

                table.Update(loc);


                var eventTable = Fabric.Table<UEvent>();
                var @event = eventTable.GetById(CurrentEvent.ID);
                @event.LocationId = null;
                eventTable.Update(@event);

                eventTable.Save();
                table.Save();
            }
        }

        #endregion Места

        #region Directions
        public IEnumerable<DirectionVM> GetDirections(Guid eventId)
        {
            return Fabric.Table<Direction>().Get(d => d.EventID == eventId && !d.IsDelete, null, null).Select(d => new DirectionVM(d));
        }

        #endregion Directions

        #region Halls

        public IEnumerable<HallVM> GetHalls(Guid eventId)
        {
            return Fabric.Table<Hall>().Get(d => d.EventID == eventId && !d.IsDelete, null, null).Select(d => new HallVM(d));
        }

        public void Delete<TModel>(Guid id, Guid eventId) where TModel : EventBaseModel
        {
            var rep = Fabric.Table<TModel>();
            var sec = rep.Get(d => d.ID == id && d.EventID == eventId).SingleOrDefault();
            if (sec != null)
            {
                sec.IsDelete = true;
                rep.Update(sec);
                rep.Save();
            }
        }
        #endregion

        #region Speackers

        public IEnumerable<SpeakerVM> GetSpeackers(Guid eventId)
        {
            return Fabric.Table<Speaker>().Get(d => d.EventID == eventId && !d.IsDelete, null, null).Select(d => new SpeakerVM(d)).ToList();
        }
        #endregion

        #region Sections

        public IEnumerable<SectionVM> GetSections(Guid eventId)
        {
            return Fabric.Table<Section>().Get(d => d.EventID == eventId && !d.IsDelete, null, null).Select(d => new SectionVM(d)).ToList();
        }

        public LocalizeForm<TViewModel> Get<TViewModel, TModel>(Guid eventId, Guid id)
            where TModel : EventBaseModel, new()
            where TViewModel : LocalizeBaseVM<TViewModel, TModel>, new()
        {
            var model =
                Fabric.Table<TModel>()
                    .Get(d => d.ID == id && d.EventID == eventId, null, null).SingleOrDefault();

            var se = new TViewModel();
            return se.CreateLocalizeForm(model); ;
        }

        public void AddOrUpdate<TModel>(IModelBuilder<TModel> item) where TModel : EventBaseModel, new()
        {
            var rep = Fabric.Table<TModel>();
            var model = item.GetModel();
            if (model.ID == default(Guid))
            {
                model.ID = Guid.NewGuid();
                rep.Insert(model);
            }
            else
            {
                //var sec = ;
                if (rep.DbSet.Any(d => d.ID == model.ID && d.EventID == model.EventID))
                {
                    //sec = item.UpdateModel(sec);
                    rep.Update(model);
                }
            }

            rep.Save();
        }

        #endregion Sections

        #region Organizations

        public PromoOrganizatorsVM GetOrganization(Guid eventId)
        {
            return
                new PromoOrganizatorsVM(
                    Fabric.Table<EventOrganization>()
                        .Get(d => d.EventID == eventId && !d.IsDelete, null, null)
                        .SingleOrDefault());
        }

        public void UpdateOrganizators(PromoOrganizatorsVM data)
        {

            var org = Fabric.Table<EventOrganization>()
                .Get(d => d.EventID == data.EventID , null, null)
                .SingleOrDefault();

            if (org != null)
            {
                org = data.UpdateModel(org);
                Fabric.Table<EventOrganization>().Update(org);
            }
            else
            {
                Fabric.Table<EventOrganization>().Insert(data.GetModel());
            }

            Fabric.Table<EventOrganization>().Save();

        }

        


        #endregion

        #region Sponsors

        public IEnumerable<SponsorVM> GetSponsors(Guid eventId)
        {
            var sponsor = Fabric.Table<Sponsor>().Get(d => !d.IsDelete && d.EventID == eventId).ToList().Select(d => new SponsorVM(d)).ToList();
            return sponsor;
        }



        public void UpdateSponsors(SponsorVM data)
        {

            var org = Fabric.Table<Sponsor>()
                .Get(d => d.EventID == data.EventID, null, null)
                .SingleOrDefault();

            if (org != null)
            {
                org = data.UpdateModel(org);
                Fabric.Table<Sponsor>().Update(org);
            }
            else
            {
                Fabric.Table<Sponsor>().Insert(data.GetModel());
            }

            Fabric.Table<Sponsor>().Save();

        }

        #endregion

    }
}