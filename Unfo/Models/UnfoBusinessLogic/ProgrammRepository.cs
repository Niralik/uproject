﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using DataModel.ModelInterfaces;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;

namespace Unfo.Models.ViewModelRepositories
{
    public class ProgrammFilter
    {
        public DateTime? Date { get; set; }

        public Guid? SectionId { get; set; }

        public Guid? HallId { get; set; }

        public Guid? DirectionId { get; set; }
    }
    public class ProgrammRepository : BaseViewModelRepository
    {

        public ProgrammRepository()
            : base()
        {
        }
        public ProgrammRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }

        public ReportVM Get(Guid id)
        {
            var result = new ReportVM();

            var list = Fabric.Table<Report>().Get(d => d.ID == id && !d.IsDelete, null, d => d.SpeakersForReport).SingleOrDefault();
            result.Fill(list);
            return result;

        }

        public void AddOrUpdate(IModelBuilder<Report> data)
        {
            var model = data.GetModel();

            if (model.ID == (default(Guid)))
            {
                model.ID = Guid.NewGuid();
                Fabric.Table<Report>().Insert(model);
            }
            else
            {


                var speakersRep = Fabric.Table<SpeakerForReport>();
                var speakersList = speakersRep.Get(d => d.EventID == model.EventID && d.ReportID == model.ID);
                foreach (var item in speakersList)
                {
                    speakersRep.Delete(item);
                }
                speakersRep.Save();


                var repReports = Fabric.Table<Report>();
                var updatemodel = repReports.Get(d => d.EventID == model.EventID && d.ID == model.ID).SingleOrDefault();
                if (updatemodel != null)
                {
                    updatemodel = data.UpdateModel(updatemodel);
                    Fabric.Table<Report>().Update(updatemodel);
                }



            }
            Fabric.Table<Report>().Save();

            //var rep = Fabric.Table<DirectionForReport>();
            //if (model.DirectionsForReport != null)
            //    foreach (var item in model.DirectionsForReport)
            //    {
            //        item.ReportID = model.ID;
            //        rep.Insert(item);
            //    }

            //rep.Save();



            //var sprep = Fabric.Get<SpeakerForReport>();
            //if (model.SpeakersForReport != null)
            //    foreach (var item in model.SpeakersForReport)
            //    {
            //        item.ReportID = model.ID;
            //        sprep.Insert(item);
            //    }

            //sprep.Save();


        }

        public override void AddOrUpdate<TModel>(IModelBuilder<TModel> data)
        {
            base.AddOrUpdate(data);
        }
    }
}