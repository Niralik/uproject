﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataModel;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin.Security.Providers.ArcGISOnline.Provider;
using Unfo.App_LocalResources;
using Unfo.Areas.Cabinet.Controllers;
using Unfo.Core;
using Unfo.Core.Extensions;
using Unfo.Core.Wrappers;
using Unfo.Models.ViewModels;

namespace Unfo.Models.ViewModelRepositories
{
    public class UnfoUserRepository : BaseViewModelRepository
    {
        public UnfoUserRepository()
            : base()
        {
        }

        public UnfoUserRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }

        /// <summary>
        /// Возвращает true если email такой уже существует
        /// </summary>
        /// <param name="email">введенный пользователем email</param>
        /// <returns></returns>
        public async Task<bool> CheckIsExistingEmailAsync(string email)
        {
            return await Fabric.Table<UnfoUser>().DbSet.AnyAsync(u => u.Email == email && !u.IsDelete);
        }

        public async Task<Guid> GetUserIdByProviderKey(string loginProviderKey)
        {
            var userId = await Fabric.Table<UnfoUserLogin>().DbSet.Where(l => l.ProviderKey == loginProviderKey).Select(l => l.UserId).FirstOrDefaultAsync();
            return userId;
        }

        public async Task<UnfoResult> ChangeUserPrivacyPolitic(UnfoUser updatingUser, UserPrivacyPoliticVM privacyPoliticWithNewStateVm)
        {
            try
            {
                var updatingPrivacyPolitic = updatingUser.PrivacyPolitics.Single(pp => pp.ID == privacyPoliticWithNewStateVm.ID);
                privacyPoliticWithNewStateVm.UpdateModel(updatingPrivacyPolitic);
                var userUpdateResult = await UserManager.UpdateAsync(updatingUser);
                if (userUpdateResult.Succeeded) return UnfoResult.Success;
                return UnfoResult.Failed(Resources.Validate_Alert_IternalServerError);
            }
            catch (Exception)
            {
                return UnfoResult.Failed(Resources.Validate_Alert_IternalServerError);
            }
        }

        public async Task RemoveFromEventModerators(Guid eventId, List<Guid> usersIds)
        {
                var eventModeratorsRepository = Fabric.Table<EventModerator>();
                var dbEventModeratorsForRemoving = await eventModeratorsRepository.GetQuery().Where(eventModerator => eventModerator.EventID == eventId && usersIds.Contains(eventModerator.UserID) && !eventModerator.IsDelete).ToListAsync();
                if (!dbEventModeratorsForRemoving.Any()) return;
                dbEventModeratorsForRemoving.ForEach(eventModerator=> eventModerator.DetachFromModerators());
                eventModeratorsRepository.Update(dbEventModeratorsForRemoving);
                await eventModeratorsRepository.SaveAsync();
        }
        public async Task ActualizeUsersModeratorsRoles(List<Guid> usersIds)
        {
         
            var eventModeratorsRepository = Fabric.Table<EventModerator>();
            var existEventModerators = await eventModeratorsRepository.GetQuery().Where(eventModerator=> usersIds.Contains(eventModerator.UserID) && !eventModerator.IsDelete).ToListAsync();
            var usersWithoutModeratorsRightsIds = usersIds.Where(userId => existEventModerators.All(eventModerator => eventModerator.UserID != userId)).ToArray();
            if (!usersWithoutModeratorsRightsIds.Any()) return;
            await RemoveFromRole(Role.Moderator, usersWithoutModeratorsRightsIds);
            await eventModeratorsRepository.SaveAsync();
        }
        public async Task<UnfoUserVM> GetOwnerOfEvent(Guid eventId)
        {
            var ownerOfEvent = await Fabric.Table<UEvent>().GetQuery().Where(@event => @event.ID == eventId).Select(@event => @event.Owner).SingleAsync();
            return new UnfoUserVM(ownerOfEvent);
        }
        public async Task<List<Guid>> ClarifyUsersIdsFromOrganizer(UnfoUserVM ownerOfEvent, List<Guid> usersIds)
        {
            var isOwnerOrganizer = ownerOfEvent.HasRole(Role.Organizer);
            if (isOwnerOrganizer && usersIds.Contains(ownerOfEvent.Id))
            {
                usersIds.Remove(ownerOfEvent.Id);
            }
            return usersIds;
        }

        public async Task<UnfoResult> CheckIsRemovingOrganizer(UnfoUserVM ownerOfEvent, Guid userId)
        {
            var isOwnerOrganizer = ownerOfEvent.HasRole(Role.Organizer);
            if (isOwnerOrganizer && ownerOfEvent.Id == userId) return UnfoResult.Failed();
            return UnfoResult.Success;
        }

        public async Task<UnfoResult> AddToEventModerators(Guid eventId, params Guid[] usersIds)
        {
            try
            {
                var eventModeratorsRepository = Fabric.Table<EventModerator>();
                var listAddingEventModerators = usersIds.Select(userId => new EventModeratorVm(eventId, userId)).ToList();  
                listAddingEventModerators.ForEach(eventModeratorVm => eventModeratorsRepository.Insert(eventModeratorVm.GetModel()));
                await AddToRole(Role.Moderator, usersIds.ToArray());
                await eventModeratorsRepository.SaveAsync();
                return UnfoResult.Success;
            }
            catch (Exception)
            {
                return UnfoResult.Failed(Resources.Validate_Alert_IternalServerError);
            }
        }

        public async Task AddToRole(Role addingRole, params Guid[] usersIds)
        {
            var someRole = await RoleManager.Roles.Where(role => role.Name == addingRole.ToString()).SingleAsync();
            var clearedFromExistingUsersRoles = await ClearFromExistingUsersRoles(addingRole, usersIds.ToList());
            var usersSomeRoles = clearedFromExistingUsersRoles.Select(userId => new UnfoUserRole() { RoleId = someRole.Id, UserId = userId }).ToArray();
            Fabric.Table<UnfoUserRole>().Insert(usersSomeRoles);
        }

        private async Task<List<Guid>> ClearFromExistingUsersRoles(Role checkedRole, List<Guid> usersIds)
        {
            var checkedRoleId = checkedRole.GetDbId();
            var existingUsersIdsRoles = await Fabric.Table<UnfoUserRole>().GetQuery().Where(usersRoles => usersRoles.RoleId == checkedRoleId && usersIds.Contains(usersRoles.UserId)).Select(usersRoles => usersRoles.UserId).ToArrayAsync();
            usersIds.RemoveAll(existingUsersIdsRoles.Contains);
            return usersIds;
        }
        public async Task RemoveFromRole(Role removingRole, params Guid[] usersIds)
        {
            var userRoleRepository = Fabric.Table<UnfoUserRole>();
            var someRole = await RoleManager.Roles.Where(role => role.Name == removingRole.ToString()).SingleAsync();
            var removingUsersRoles = await userRoleRepository.GetQuery().Where(userRole=>usersIds.Contains(userRole.UserId) && userRole.RoleId == someRole.Id).ToArrayAsync();
            userRoleRepository.Delete(removingUsersRoles);
        }
        /// <summary>
        /// Проверяем есть ли в модераторах проверяемого события уже такие пользователи - если есть удаляем их из списка
        /// </summary>
        /// <returns></returns>
        public async Task<List<Guid>> ClearFromExistingEventModerators(Guid eventId, List<Guid> usersIds)
        {
            var eventModeratorsRepository = Fabric.Table<EventModerator>();
            var existingUsersIds = await eventModeratorsRepository.GetQuery().Where(eventModerator => usersIds.Any(userId => eventModerator.UserID == userId) && eventModerator.EventID == eventId && !eventModerator.IsDelete)
                                                                               .Select(eventModerator=>eventModerator.UserID).ToListAsync();
            usersIds.RemoveAll(existingUsersIds.Contains);
            return usersIds;
        }


        
    }
}