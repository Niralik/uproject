﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using OrangeJetpack.Localization;
using Unfo.Core;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.Interfaces;

namespace Unfo.Models.ViewModelRepositories
{

    public class ResourcesRepository : BaseViewModelRepository, IImageRepository
    {
        public ResourcesRepository()
            : base()
        {
        }
        public ResourcesRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }

        public IEnumerable<SelectListItem> GetServiceSections(int[] selectedIds = null)
        {
            return Fabric.Table<ServiceSection>().Get(d => !d.IsDelete).Select(d => new SelectListItem()
            {
                Text = d.Name,
                Value = d.ID.ToString(),
                Selected = selectedIds != null && selectedIds.Contains(d.ID)
            });
        }

        public void SaveDocument(Guid documentId, string documentName, ContentType type)
        {
            Fabric.Table<Document>().Insert(new Document()
            {
                ID = documentId,
                Name = Path.GetFileName(documentName),
                Extentions = Path.GetExtension(documentName),
                Type = type
            });

            Fabric.Table<Document>().Save();
        }

        public Document GetDocument(Guid id)
        {
            return Fabric.Table<Document>().GetById(id);
        }

        public IEnumerable<KeyValuePair<string, string>> GetAccessCultures()
        {
            var culture = ConfigurationManager.AppSettings["LocalizationLanguages"];
            if (string.IsNullOrWhiteSpace(culture))
                return new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>( "ru",new CultureInfo("ru").DisplayName)
                   
                };
            culture = culture.Trim().ToLower();
            var cultures = culture.Split(',').Select(d => d.Trim()).ToList();

            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in cultures)
            {
                result.Add(new KeyValuePair<string, string>(item, new CultureInfo(item).DisplayName));
            }
            return result;
        }

        public IEnumerable<KeyValuePair<string, string>> GetAccessCultures(Guid? eventId)
        {
            var culture = string.Empty;
            if (eventId != null)
            {
                var uevent = Fabric.Table<UEvent>().GetById(eventId.Value);
                if (uevent != null)
                    culture = uevent.SupportLanguages;
            }
            else
            {
                culture = ConfigurationManager.AppSettings["LocalizationLanguages"];
            }

            if (string.IsNullOrWhiteSpace(culture))
                return new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>( "ru",new CultureInfo("ru").DisplayName)
                };
            culture = culture.Trim().ToLower();
            var cultures = culture.Split(',').Select(d => d.Trim()).ToList();

            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in cultures)
            {
                result.Add(new KeyValuePair<string, string>(item, new CultureInfo(item).DisplayName));
            }
            return result;
        }

        public IEnumerable<SelectListItem> GetAccessCulturesForLocalization(List<string> selectedLang = null)
        {
            var loc = GetAccessCultures();

            var result = new List<SelectListItem>();
            foreach (var item in loc)
            {
                result.Add(new SelectListItem() { Text = item.Value, Value = item.Key, Selected = selectedLang != null && (selectedLang.Contains(item.Key)) });
            }

            return result;
        }

        public IEnumerable<SelectListItem> GetCountries()
        {
            return Fabric.Table<Country>().Get(d => !d.IsDelete).ToList().Select(d => new SelectListItem()
            {
                Text = d.Localize(CurrentLocalization.Edit, k => k.Name).Name,
                Value = d.ID.ToString()
            });
        }

        public void SaveImage(UploadImage image)
        {
            Fabric.Table<UploadImage>().Insert(image);
            Fabric.Table<UploadImage>().Save();
        }

        public UploadImage SaveImage(Guid imageId, string fileName, int sourceWidth, int sourceHeight)
        {
            var image = new UploadImage()
            {
                ID = imageId,
                SourceHeight = sourceHeight,
                SourceWidth = sourceWidth

            };
            Fabric.Table<UploadImage>().Insert(image);
            Fabric.Table<UploadImage>().Save();

            return image;
        }

        public void UpdateImage(Guid imageId, int selectedWidth, int selectedHeight, int selectedX, int selectedY)
        {
            var image = Fabric.Table<UploadImage>().GetById(imageId);
            if (image == null) return;
            image.SelectedHeight = selectedHeight;
            image.SelectedWidth = selectedWidth;
            image.PointX = selectedX;
            image.PointY = selectedY;


            Fabric.Table<UploadImage>().Save();
        }
    }
}