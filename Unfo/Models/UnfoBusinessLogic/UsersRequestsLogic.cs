﻿using System.Linq;
using DataModel;
using NinjaNye.SearchExtensions;
using Unfo.Core;
using Unfo.Core.Filters;

namespace Unfo.Models.UnfoBusinessLogic
{
    public class UsersRequestsLogic : BaseViewModelRepository
    {
        public UsersRequestsLogic()
        {
        }

        public UsersRequestsLogic(IRepositoryFabric repFabric)
            : base(repFabric)
        {
        }


    }
   

    public  class UsersRequestsFilter: BaseUnfoFilter<Request>
    {
        public new UsersRequestsFilterParams FilterParams { get { return (UsersRequestsFilterParams)base.FilterParams; } }

        public UsersRequestsFilter(IBaseFilterParams filterParams): base(filterParams)
        {
            
        }

        protected override void SortQuery()
        {
            FilteredQuery = FilteredQuery.OrderByDescending(request=>request.RequestState).ThenByDescending(request => request.CreateRequestDate);
        }

     

        #region Фильтрация запроса
        protected override void FiltrateQuery()
        {
            var initialUsersRequestQuery = CurrentDbContext.Requests.Where(request => !request.IsDelete);

            var filteredBySearchQuery = FiltrateBySearchQuery(initialUsersRequestQuery);
            var filteredByTypeRequest = FiltrateByTypeRequest(filteredBySearchQuery);

            FilteredQuery = filteredByTypeRequest;
        }

        private IQueryable<Request> FiltrateByTypeRequest(IQueryable<Request> incommingQuery)
        {
            if (FilterParams.RequestState == FilterRequestState.None) return incommingQuery;
            var filteredQuery = incommingQuery.Where(request=> request.RequestState.ToString() == FilterParams.RequestState.ToString());
            return filteredQuery;
        }

        protected override IQueryable<Request> SearchByCriteria(IQueryable<Request> incommingQuery, string[] searchTerms)
        {
            var usersSearchedQuery = searchTerms.Aggregate(CurrentDbContext.Users.Where(user => !user.IsDelete), (users, term) => users.Search(SearchCriteria.Users).Containing(term));
            var filteredQueryBySearchCriteria = incommingQuery.Join(usersSearchedQuery, request => request.AuthorID, user => user.Id, (request, user) => request);
            return filteredQueryBySearchCriteria;
        }
    
        #endregion Фильтрация запроса
    }

   
    

    public class UsersRequestsFilterParams : BaseUnfoFilterParams<UsersRequestsFilterParams>, IPageableParams<UsersRequestsFilterParams>
    {
        public FilterRequestState RequestState { get; set; }
      
        public UsersRequestsFilterParams()
        {

        }

        public UsersRequestsFilterParams SetRequestState(FilterRequestState filterRequestState)
        {
            var clone = Clone();
            clone.RequestState = filterRequestState;
            return clone;
        }
        /// <summary>
        /// Метод устанавливает номер страницы и возвращает измененный объект
        /// </summary>
        public UsersRequestsFilterParams SetCurrentPage(int numCurrentPage)
        {
            CurrentPage = numCurrentPage;
            return this;
        }

       
    }

    public enum FilterRequestState
    {
        None,
        Completed,
        Aborted,
        InProgress
    }
    
}