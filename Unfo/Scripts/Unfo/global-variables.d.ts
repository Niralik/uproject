﻿declare var g_cabinetHomeIndexUrl: string;
declare var g_urlReferrer: any;
declare enum TypeOfAlert {
    Info = 0,
    Warning = 1,
    Success = 2,
    Error = 3,
}
declare var TypeOfAlertCss: {
    Info: string;
    Warning: string;
    Success: string;
    Error: string;
};
declare var optsOfBigRoundCenteredSpinner: {
    lines: number;
    length: number;
    width: number;
    radius: number;
    corners: number;
    rotate: number;
    direction: number;
    speed: number;
    trail: number;
    shadow: boolean;
    hwaccel: boolean;
    className: string;
    zIndex: number;
    top: string;
    left: string;
};
declare var optsOfSmallRoundRightSpinner: {
    lines: number;
    length: number;
    width: number;
    radius: number;
    corners: number;
    rotate: number;
    direction: number;
    color: string;
    speed: number;
    trail: number;
    shadow: boolean;
    hwaccel: boolean;
    className: string;
    zIndex: number;
    top: string;
    left: string;
};
declare var optsOfBigPurpleRoundCenteredSpinner: any;
declare var optsOfBigGreenRoundCenteredSpinner: any;
declare var optsOfSmallGreenRoundRightSpinner: any;
