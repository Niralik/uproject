﻿
$(function () {
    var imageSize;
    var imageWidth,
        prefix = $('.crop-image').attr('data-prefix');

    var jcrop_api;


    var ratio = $('.crop-image').attr('data-ratio');
    var croptype = $('.crop-image').attr('data-type');
    imageWidth = parseInt($('.crop-image').attr('data-width'));
    

    $('.image-uploader input').fileupload({
        dataType: 'json',
        add: function (e, data) {
            $('.image-uploader .progress').removeClass('hide');

            if (data.autoUpload || (data.autoUpload !== false &&
            $(this).fileupload('option', 'autoUpload'))) {
                data.process().done(function () {
                    data.submit();
                });
            }
            
        },
        done: function (e, data) {
            var option = {};
            $('.cropImageModal .crop-image img, .cropImageModal .image-preview img').attr('src', getImageUrl(data.result.Response.ID, 'normal'));

            $('.image_params input[name=ID]').val(data.result.Response.ID);
            if (croptype == 0) {
                var normalWidth = parseInt(data.result.Response.NormalSize.Width) / 2;
                var normalHeight = parseInt(data.result.Response.NormalSize.Height) / 2;
                $('.cropImageModal .crop-image img').attr('width', normalWidth);
                $('.cropImageModal .crop-image img').attr('height', normalHeight);

                //option.boxWidth = data.result.Response.NormalSize.Width / 2;
                //option.boxHeight = data.result.Response.NormalSize.Height / 2;
                imageSize = [normalWidth, normalHeight];

                option = {
                    setSelect:[0,0, imageWidth, imageWidth],
                    boxWidth: normalWidth,
                    boxHeight: normalHeight,
                    onChange: showPreview,
                    onSelect: showPreview,
                    aspectRatio: ratio
                };
                //option.trueSize = [data.result.Response.NormalSize.Width, data.result.Response.NormalSize.Height];
            } else {
                option = {

                    //aspectRatio: ratio
                    onChange: setParams,
                    onSelect: setParams
                };
                option.trueSize = [data.result.Response.SourceSize.Width, data.result.Response.SourceSize.Height];
            }
            console.log(option);


            $('.cropImageModal .crop-image img').Jcrop(option, function () {
                jcrop_api = this;
            });

            $('.cropImageModal').modal('show');
            $('.image-uploader .progress').addClass('hide');
            $('.image-uploader .bar').css(
                'width', '0'
            );

        },
        progressall: function (e, data) {

            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.image-uploader .bar').css(
                'width',
                progress + '%'
            );
        }
    });



    $('.cropImageBtn').click(function () {
        $.post('/Cabinet/CommonResources/CropImage', getParams(), function (data) {
            jcrop_api.destroy();
            $('.cropImageModal .crop-image img, .cropImageModal .image-preview img').attr('src', '');
            if (data.Result == 1) {
                $('.image-container .image-result').attr('src', getImageUrl(data.Response, 'normal'));
                
                $('.image-container').removeClass('hide');
                

            } else {

            }
            $('.cropImageModal').modal('hide');
        });
    });


    var showPreview = function (coords) {
        var rx = imageWidth / coords.w;
        var ry = imageWidth / coords.h;

        $('.image-preview img').css({
            width: Math.round(rx * imageSize[0]) + 'px',
            height: Math.round(ry * imageSize[1]) + 'px',
            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
            marginTop: '-' + Math.round(ry * coords.y) + 'px'
        });

        setParams(coords);
    };

    var setParams = function (coords) {

        if (croptype == 0) {
            $('.image_params input[name=Width]').val(parseInt(coords.w));
            $('.image_params input[name=Height]').val(parseInt(coords.w));
        } else {
            $('.image_params input[name=Width]').val(parseInt(coords.w));
            $('.image_params input[name=Height]').val(parseInt(coords.h));

        }
        $('.image_params input[name=PointX]').val(parseInt(coords.x));
        $('.image_params input[name=PointY]').val(parseInt(coords.y));

    };


    var getParams = function () {
        var obj = {};
        obj.TypeCrop = $('.image_params input[name=TypeCrop]').val();
        obj.PointX = $('.image_params input[name=PointX]').val();
        obj.PointY = $('.image_params input[name=PointY]').val();
        obj.Width = $('.image_params input[name=Width]').val();
        obj.Height = $('.image_params input[name=Height]').val();
        obj.ID = $('.image_params input[name=ID]').val();
        console.log(obj);
        return obj;

    };
    $('.closeModal').click(function() {
        $('.cropImageModal').modal('hide');
        //$('.image-container').removeClass('hide');
        jcrop_api.release();

    });
});

