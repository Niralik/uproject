﻿class Scrollbars {

    private _leftMenuBody = $('.menu-body');

    constructor() {
        if (this._leftMenuBody.length == 0) return;
        this._calculateMenuBodyHeight();
        this._applyPerfectScrollbar();
        this._resizeWindowUpdatePerfectScrollbar();
        
    }

    private _calculateMenuBodyHeight() {
        var menuHeight = $('.main-sidebar').height();
        var menuHeaderHeight = $('.main-sidebar .current-user').height();
        var menuFooterHeight = $('.main-sidebar .bottom-menu').height();
        var menuBodyMargin = parseInt(this._leftMenuBody.css('margin-top').split('px')[0]) + parseInt(this._leftMenuBody.css('margin-bottom').split('px')[0]);
        var heightMenuBody = menuHeight - menuHeaderHeight - menuFooterHeight - menuBodyMargin;

        this._leftMenuBody.innerHeight(heightMenuBody);
    }
    private _applyPerfectScrollbar() {
        this._leftMenuBody.perfectScrollbar({
            includePadding: true,
            maxScrollbarLength: 500
    });
    }
    private _resizeWindowUpdatePerfectScrollbar() {
        var oldWindowHeight = $(window).height();
        $(window).resize((e: JQueryEventObject) => {
            if (oldWindowHeight != $(window).height()) {
                this._calculateMenuBodyHeight();
                this._leftMenuBody.perfectScrollbar('update');
            }
        });
    }
}

$(() => {
    var scrollbars = new Scrollbars();
})