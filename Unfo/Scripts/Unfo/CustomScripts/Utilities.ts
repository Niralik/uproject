﻿class Utilites {
    public static Declination(a, b, c, quantity) {
    /// <summary>Метод возвражает слово нужного склонения на основе значения контрольной цифры</summary>
    /// <param name="a" type="string">единственное число (именительный падеж)</param>
    /// <param name="b" type="string">множественное число (именительный падеж)</param>
    /// <param name="c" type="string"> множественное число (родительный падеж)</param>
    /// <param name="quantity " type="int">количество</param>
    var words = [c, a, b];
    var index = quantity % 100;
    if (index >= 11 && index <= 14) { index = 0; }
    else { index = (index %= 10) < 5 ? (index > 2 ? 2 : index) : 0; }
    return (words[index]);
    }
    public static HasWhiteSpaceOrNull(s) {
    //регулярка проверяет если есть только пробелы то тру
    return /^\s+$/.test(s) || s.length === 0;
    }
    public static IsNotHasWhiteSpaceOrNull(s) {
        
       return !Utilites.HasWhiteSpaceOrNull(s);
    }
} 