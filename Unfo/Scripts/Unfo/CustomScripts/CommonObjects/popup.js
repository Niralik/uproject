﻿function Popup() {
    var _this = this;

    this.initialize = function(urlToController, templateName) {
        this.UrlToController = urlToController;
        this.PopupTemplate = Handlebars.templates[templateName];
    };

    this.show = function (data) {
        if (this.PopupTemplate) {
            var filledHtmlOfTemplate = this.PopupTemplate(data);
            showModal(filledHtmlOfTemplate);
        } else {
            throw new Error("Не найден popup шаблон");
        }
    };

    function showModal(htmlBlock) {
        $(".modal-backdrop").remove();
        var popupWrapper = $("#PopupWrapper");
        popupWrapper.empty();
        popupWrapper.html(htmlBlock);
        var popup = $(".modal", popupWrapper);
        popup.modal('show');
    }
}