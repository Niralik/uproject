﻿
function ErrorHandler() {
    /// <summary>
    /// Функция создающая объект представляющий из себя общую логику выполняющуюся на каждой
    /// странице сайта
    /// </summary>
    var self = this;

    this.initialize = function () {
        globalHandlingOfErrors();
    };

    function globalHandlingOfErrors() {
        /// <summary>
        /// Инициализация обработчиков аджакс ошибок и ошибок страницы
        /// </summary>
        handlingAjaxErrors();
        handlingWindowErrors();
    }
    function handlingWindowErrors() {
        /// <summary>
        /// Метод обрабатывающий ошибки страницы
        /// </summary>
        $(window).error(function (e) {
            debugger;
            addMessageToErrorWrapper(e.originalEvent.message);
        });
    }
    function handlingAjaxErrors() {
        /// <summary>
        /// Метод обрабатывающий аджакс ошибки
        /// </summary>
        $(document).ajaxError(function (event, request, settings) {
            addMessageToErrorWrapper(settings.url + ' ' + request.status + ' ' + request.statusText);
        });
    }
    function addMessageToErrorWrapper(errorMessage) {
        /// <summary>
        /// Метод отображает в правом верхнем углу ошибки пойманные на этапе сборки страницы
        /// </summary>
        /// <param name="errorMessage" type="string">Сообщение об ошибке</param>
        var $javaErrorWrapper = $('#JavaScriptErrorWrapper');
        if ($javaErrorWrapper) {
            $javaErrorWrapper.append(errorMessage + '<br/>');
            $javaErrorWrapper.show();
        }
    }
}