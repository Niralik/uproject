﻿(function ($) {
     $.fn.serialize = function (options) {
         return $.param(this.serializeArray(options));
     };

     $.fn.serializeArray = function (options) {
         var o = $.extend({
         checkboxesAsBools: false
     }, options || {});

     var rselectTextarea = /select|textarea/i;
     var rinput = /text|hidden|password|search/i;

     return this.map(function () {
         return this.elements ? $.makeArray(this.elements) : this;
     })
     .filter(function () {
         return this.name && !this.disabled &&
             (this.checked
             || (o.checkboxesAsBools && this.type === 'checkbox')
             || rselectTextarea.test(this.nodeName)
             || rinput.test(this.type));
         })
         .map(function (i, elem) {
             var val = $(this).val();
             return val == null ?
             null :
             $.isArray(val) ?
             $.map(val, function (val, i) {
                 return { name: elem.name, value: val };
             }) :
             {
                 name: elem.name,
                 value: (o.checkboxesAsBools && this.type === 'checkbox') ? //moar ternaries!
                        (this.checked ? 'true' : 'false') :
                        val
             };
         }).get();
     };
})(jQuery);

Function.prototype.inheritsFrom = function (parentClassOrObject) {
    if (parentClassOrObject.constructor == Function) {
        //Normal Inheritance
        this.prototype = new parentClassOrObject;
        this.prototype.constructor = this;
        this.prototype.parent = parentClassOrObject.prototype;
    } else {
        //Pure Virtual Inheritance
        this.prototype = parentClassOrObject;
        this.prototype.constructor = this;
        this.prototype.parent = parentClassOrObject;
    }
    return this;
};

function isArray(myArray) {
    return myArray.constructor.toString().indexOf("Array") > -1;
}
function hasWhiteSpaceOrNull(s) {
    //регулярка проверяет если есть только пробелы то тру
    return /^\s+$/.test(s) || s.length === 0;
}

function straightPostRequest(path, parameters) {
    var form = $("<form></form>", {
                 "method": "post",
                 "action": path
    }).appendTo("body");;
    $.each(parameters, function(key, value) {
        var field = $('<input></input>', {
            "type": "hidden",
            "name": key,
            "value": value
        });
        form.append(field);
    });
    form.submit();
}

function toBool (string) {
    if (string && typeof string == "string") {
        switch (string.toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(string);
        }
    }
    return false;
};

Array.prototype.last = function() {
    return this[this.length - 1];
};
