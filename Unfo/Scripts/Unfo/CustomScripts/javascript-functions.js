﻿function declination(a, b, c, quantity) {
    /// <summary>Метод возвражает слово нужного склонения из значения контрольной цифры</summary>
    /// <param name="a" type="string">множественное число (родительный падеж)</param>
    /// <param name="b" type="string">единственное число (именительный падеж)</param>
    /// <param name="c" type="string">множественное число (именительный падеж)</param>
    /// <param name="quantity " type="int">количество</param>
    var words = [a, b, c];
    var index = quantity % 100;
    if (index >= 11 && index <= 14) {
        index = 0;
    } else {
        index = (index %= 10) < 5 ? (index > 2 ? 2 : index) : 0;
    }
    return (words[index]);
}
;
//# sourceMappingURL=javascript-functions.js.map
