﻿//создание селектора ':transparent'
$.extend($.expr[':'], {
    transparent: (elem, i, attr) => ($(elem).css("opacity") === "0")
}); 
$.extend($.expr[':'], {
    opaque: (elem, i, attr) => ($(elem).css("opacity") === "1")
}); 

function parseBool(value:string) {
    if (value && typeof value == "string") {
        switch (value.toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }
    return false;
};