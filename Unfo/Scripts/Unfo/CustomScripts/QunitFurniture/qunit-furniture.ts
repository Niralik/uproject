﻿class QUnitFurniture {

/**
     * Статичный метод который добавляет всю оснастку для визуацлизации модуля тестирования на каждой страничке
     */
    static Intialize() {

        var $qUnitTestsContainer: JQuery = $('.qunit-container'),
            $miniQunitButton: JQuery = $('.mini-qunit-button'),
            $textOfMiniButton: JQuery = $miniQunitButton.find('span');

        var isFailedDataName = 'is-failed',
            isShowedDataName = 'is-showed';

        $miniQunitButton.click(()=> {
            var isNotFailedUnitTest: boolean = !$qUnitTestsContainer.data(isFailedDataName);

            var isShowedUnitTests: boolean = $qUnitTestsContainer.data(isShowedDataName);
            if (isShowedUnitTests) {
                _hideTestsContainer();
                if (isNotFailedUnitTest) {
                    $textOfMiniButton.html('Show Tests');
                }
            } else {
                _showTestsContainer();
                if (isNotFailedUnitTest) {
                    $textOfMiniButton.html('Hide Tests');
                }
            }
        });

        QUnit.done((details: DoneCallbackObject)=> {
            if (details.failed > 0) {
                $qUnitTestsContainer.data(isFailedDataName, true);
                $miniQunitButton.addClass('failed');
                $textOfMiniButton.html(details.failed.toString());
            } else {
                $qUnitTestsContainer.data(isFailedDataName, false);
                $miniQunitButton.removeClass('failed');
            }
            console.log("Total: ", details.total, " Failed: ", details.failed, " Passed: ", details.passed, " Runtime: ", details.runtime);
        });

        function _showTestsContainer() {
            $qUnitTestsContainer.removeClass('bounceOutUp').addClass('bounceInDown');
            $qUnitTestsContainer.data(isShowedDataName, true);
            $qUnitTestsContainer.show();
        }

        function _hideTestsContainer() {
            $qUnitTestsContainer.removeClass('bounceInDown').addClass('bounceOutUp');
            $qUnitTestsContainer.data(isShowedDataName, false);
            setTimeout(()=> {
                $qUnitTestsContainer.hide();
            }, 1000);

        };

    }

}