﻿/**
* Интерфейс описывающий Теги в шаблоне хендлбар
*/
interface IAlertTemplateData {
    AlertCss: string;
    AlertHeader: string;
    AlertMessages: string[];
    AlertMessage: string;
}
/**
* Интерфейс описывающий ответ с сервера аналогичен классу AlertWrapper на сервере
*/
interface IServerResponse {
    Successed: boolean;
    Code?: number;
    RelatedData?: any;
    AlertSections: IAlertSection[];
}
/**
* Интерфейс описывает одну секцию представляющию опвещение определенного типа
*/
interface IAlertSection {
    TypeAlert: TypeOfAlert;
    Messages?: string[];
    Message: string;
}
declare enum TypeAlertDisplay {
    SlideDownView = 0,
    PopupView = 1,
    StandartView = 2,
}
/**
* Интерфейс описывает глобальные настройки инициализации для всего объекта Системы Оповещения
*/
interface IAlertOptions {
    TypeAlertDisplay?: TypeAlertDisplay;
    IsAutoHide?: boolean;
    HideDelay?: number;
    IsErrorAutoHide?: boolean;
}
/**
* Интерфейс описывает данные необходимые для вывода конкретного оповещения через систему Оповещения при слудующем вызове метода Show
*/
interface IAlertData extends IAlertSection {
    NumOfInvalid?: number;
}
/**
* Интерфейс описывает настройки одного следующего вызова метода Show - после чего настройки очищаются
*/
interface IOneCallOptions extends IAlertOptions {
}
declare class AlertEngine {
    private _alertTemplate;
    private _defaultOptions;
    private _options;
    private _oneCallOptions;
    private _currentOptions;
    private _alertsData;
    private _$generatedAlerts;
    public $AlertWrapper: JQuery;
    constructor(alertOptions?: IAlertOptions, alertWrapperSelector?: string);
    public Post(alertData: IAlertData, oneCallOptions?: IOneCallOptions): void;
    private _generateAlert();
    public ShowAll(alertSections: IAlertSection[], oneCallOptions?: IOneCallOptions): void;
    public Show(): void;
    public Error(errorMessage: string): void;
    private _showByPopupMessenger();
    private _showByUsualWay();
    private _setCurrentOptions();
    private _addToPageAlert($alertTag);
    private _displayByOptions($alertTag);
    private _setAutohide($alertTage);
    private _hidePromise;
    private _hideOne($alertTag);
    private _hideByOptions($alertTag);
    public HideAll(): void;
    private _ClearMessages();
    private _generateAlertTemplateData();
    private _generateAlertMessagesList(currentAlertData);
    private _generateAlertMessage(currentAlertData);
    private _trySetStyleToDisplayNames(alertMessages);
    private _trySetStyleToDisplayName(alertMessage);
    private _getAlertCss(currentAlertData);
    private _getAlertHeaderByTypeAlert(currentAlertData);
    static SetupEnviroment(): void;
}
