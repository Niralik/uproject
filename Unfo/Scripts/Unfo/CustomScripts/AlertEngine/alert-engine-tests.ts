﻿class AlertEngingeTests {
    static Initialize() {
        var idTestContainer = "testAlertEngineContainer",
            idTestContainerSelector = "#" + idTestContainer;
        QUnit.begin(()=> {
            $("<div></div>", {
                "class": "alert-wrapper-test",
                "id": idTestContainer,
                "style": "display:none"
            }).appendTo("body");
        });
        QUnit.test("Alert engine template test", assert => {
            var template = Handlebars.templates['alert-block'];
            assert.ok(template != undefined, "Template exist");
        });

        QUnit.done(()=> {
            $(idTestContainerSelector).remove();
        });
    }
}