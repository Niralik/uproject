﻿var AlertEngingeTests = (function () {
    function AlertEngingeTests() {
    }
    AlertEngingeTests.Initialize = function () {
        var idTestContainer = "testAlertEngineContainer", idTestContainerSelector = "#" + idTestContainer;
        QUnit.begin(function () {
            $("<div></div>", {
                "class": "alert-wrapper-test",
                "id": idTestContainer,
                "style": "display:none"
            }).appendTo("body");
        });
        QUnit.test("Alert engine template test", function (assert) {
            var template = Handlebars.templates['alert-block'];
            assert.ok(template != undefined, "Template exist");
        });

        QUnit.done(function () {
            $(idTestContainerSelector).remove();
        });
    };
    return AlertEngingeTests;
})();
//# sourceMappingURL=alert-engine-tests.js.map
