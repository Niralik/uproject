﻿/**
* Интерфейс описывающий Теги в шаблоне хендлбар
*/
interface IAlertTemplateData {
    AlertCss: string;
    AlertHeader: string;
    AlertMessages: string[];
    AlertMessage: string;
}
/**
* Интерфейс описывающий ответ с сервера аналогичен классу AlertWrapper на сервере
*/
interface IServerResponse {
    Successed: boolean;
    Code?: number;
    RelatedData?: any;
    AlertSections: IAlertSection[];
}
/**
* Интерфейс описывает одну секцию представляющию опвещение определенного типа
*/
interface IAlertSection {
    TypeAlert: TypeOfAlert;
    Messages?: string[];
    Message: string;
}
enum TypeAlertDisplay {
    SlideDownView,
    PopupView,
    StandartView
}
/**
* Интерфейс описывает глобальные настройки инициализации для всего объекта Системы Оповещения
*/
interface IAlertOptions {
    TypeAlertDisplay?: TypeAlertDisplay;
    IsAutoHide?: boolean;
    HideDelay?: number;
    IsErrorAutoHide?: boolean;
}
/**
* Интерфейс описывает данные необходимые для вывода конкретного оповещения через систему Оповещения при слудующем вызове метода Show
*/
interface IAlertData extends IAlertSection {
    NumOfInvalid?: number;
}
/**
* Интерфейс описывает настройки одного следующего вызова метода Show - после чего настройки очищаются
*/
interface IOneCallOptions extends IAlertOptions {
}

class AlertEngine {
    /// <summary>
    /// Класс аккамулирующий логику показа оповещений о результатах каких либо операций
    /// </summary>
    private _alertTemplate: HandlebarsTemplateDelegate = Handlebars.templates['alert-block'];
    private _defaultOptions: IAlertOptions = {
        TypeAlertDisplay: TypeAlertDisplay.StandartView,
        IsAutoHide: false,
        HideDelay: 10000,
        IsErrorAutoHide: false
    }
    private _options: IAlertOptions;
    private _oneCallOptions: IOneCallOptions;
    private _currentOptions: IAlertOptions;
    private _alertsData: IAlertData[] = [];
    private _$generatedAlerts: JQuery[] = [];
    public $AlertWrapper: JQuery;

    constructor(alertOptions?: IAlertOptions, alertWrapperSelector?: string) {
        /// <summary>
        /// Создаем экземпляр Алерт движка закрепленного за контайнером в котором будут отображаться
        /// все оповещения
        /// </summary>
        /// <param name="alertWrapperSelector" type="string">
        /// Селектор указывающий на контейнер будующий оповещений
        /// </param>
        /// <param name="alertOptions" type="IAlertOptions">
        /// Общие настройки для всего экземпляра Алерта
        /// </param>
        this.$AlertWrapper = $(alertWrapperSelector);
        this._options = $.extend({}, this._defaultOptions, alertOptions);  
        this._setCurrentOptions();

    }

    public Post(alertData: IAlertData, oneCallOptions?: IOneCallOptions) {
        /// <summary>
        /// Инициализация объекта алерт движка новыми исходнымии данными
        /// </summary>
        /// <param name="displayedAlert" type="IAlertData">
        /// Данные для отображения оповещения
        /// </param>
        /// <param name="oneCallOptions?" type="IOneCallOptions">
        /// Настройки алерт системы для следующего одного вызова метода Show
        /// </param>
        this._oneCallOptions = $.extend({}, this._oneCallOptions, oneCallOptions);
        this._alertsData.push(alertData);
        this._setCurrentOptions();
        this._generateAlert();
    }
    private _generateAlert() {
        if (this._alertTemplate && this._currentOptions.TypeAlertDisplay != TypeAlertDisplay.PopupView) {
            var generatedAlertHtml = this._alertTemplate(this._generateAlertTemplateData());
            this._$generatedAlerts.push($(generatedAlertHtml));
        }
    }

    public ShowAll(alertSections: IAlertSection[], oneCallOptions?: IOneCallOptions) {
        /// <summary>
        /// Метод инициализириует  и показывает оповещения пришедшие с сервера и спрятанные в массив секций
        /// </summary>
        this._alertsData = [];
        if (this.$AlertWrapper && alertSections) {
            $.each(alertSections, (index, alertSection: IAlertSection) => {
                this.Post({
                    TypeAlert: alertSection.TypeAlert,
                    Messages: alertSection.Messages,
                    Message: alertSection.Message
                }, oneCallOptions);
            });
            this.Show();
        }
    }

    public Show() {
        /// <summary>
        /// Показывает сгенерированное на основе проиницилизрованных данных оповещение в контенейре
        /// указаном в конструкторе
        /// </summary>
        if (this._currentOptions.TypeAlertDisplay == TypeAlertDisplay.PopupView) {
            this._showByPopupMessenger();
        } else {
            this._showByUsualWay();
        }
        this._oneCallOptions = null;
    }
    public Error(errorMessage: string) {
        this.Post({ Message: errorMessage, TypeAlert: TypeOfAlert.Error });
        this.Show();
    }
    private _showByPopupMessenger(): void {
        if (!this._alertsData) return;

        $.each(this._alertsData, (index, alertData: IAlertData) => {
            Messenger().post({
                message: alertData.Message,
                type: TypeOfAlert[alertData.TypeAlert].toLowerCase(),
                showCloseButton: true
            });
        });
    }

    private _showByUsualWay(): void {
        if (!this._$generatedAlerts) return;
        $.each(this._$generatedAlerts, (index, $generatedAlert) => {
            if (this._hidePromise) {
                this._hidePromise.then(() => {
                    this._addToPageAlert($generatedAlert);
                    this._hidePromise = null;
                });
            } else {
                this._addToPageAlert($generatedAlert);
            }
            this._setAutohide($generatedAlert);
        });
    }
    private _setCurrentOptions(): void {
        if (this._oneCallOptions && Object.keys(this._oneCallOptions).length) {
            this._currentOptions = this._oneCallOptions;
        } else {
            this._currentOptions = this._options;
        }
    }
    private _addToPageAlert($alertTag: JQuery) {
        $alertTag.appendTo(this.$AlertWrapper);
        this._displayByOptions($alertTag);
    }
    private _displayByOptions($alertTag: JQuery) {
        switch (this._currentOptions.TypeAlertDisplay) {
            case TypeAlertDisplay.SlideDownView:
                $alertTag.slideDown(400);
                break;
            case TypeAlertDisplay.StandartView:
                $alertTag.addClass('active');
                break;
            default:
        }
    }
    private _setAutohide($alertTage: JQuery): void {
        if (this._currentOptions.IsAutoHide) {
            if ($alertTage.is('.alert-danger') && !this._currentOptions.IsErrorAutoHide) return;
            setTimeout(() => {
                this._hideOne($alertTage);
            }, this._currentOptions.HideDelay);
        }
    }
    private _hidePromise: JQueryPromise<{}>;
    private _hideOne($alertTag: JQuery): void {
        var deffered = $.Deferred();
        this._hideByOptions($alertTag);
        setTimeout(() => {
            $alertTag.remove();
            deffered.resolve();
        }, 450);
        this._hidePromise = deffered.promise();
    }
    private _hideByOptions($alertTag: JQuery): void {
        switch (this._currentOptions.TypeAlertDisplay) {
            case TypeAlertDisplay.SlideDownView:
                $alertTag.slideUp(400);
                break;
            case TypeAlertDisplay.StandartView:
                $alertTag.removeClass('active');
                break;
            default:
        }
    }

    public HideAll(): void {
        var standartAlerts = $('.alert').filter((index, alert: HTMLElement) => { return !$(alert).is('.messenger-message'); });
        $.each(standartAlerts, (index, alert) => {
            this._hideOne($(alert));
        });
        this._$generatedAlerts = [];
        this._ClearMessages();
    }

    private _ClearMessages() {
        this._alertsData = [];
    }

    private _generateAlertTemplateData(): IAlertTemplateData {
        /// <summary>
        /// Метод генерирует данные для шаблона -  исходя из того было проинизлированно одно сообщение или массив
        /// генерируется разное представление из за ограничений хендбара темплейтов используется два свойства характириующих
        /// одну и ту же смысловую единицу, для массива сообщений представление в виде листа - для одного просто вывод текста
        /// </summary>
        //Так как данные для шаблона генерятся после каждой инициализации Данных последним элементом в массиве данных текущего объекта будет
        //необходимый нам для создания данных для шаблона
        var currentAlertData = this._alertsData[this._alertsData.length - 1];
        var alertData: IAlertTemplateData = {
            AlertCss: this._getAlertCss(currentAlertData),
            AlertHeader: this._getAlertHeaderByTypeAlert(currentAlertData),
            AlertMessages: this._generateAlertMessagesList(currentAlertData),
            AlertMessage: this._generateAlertMessage(currentAlertData)
        };
        return alertData;
    }
    private _generateAlertMessagesList(currentAlertData: IAlertData): string[] {
        var styledAlertMessages = this._trySetStyleToDisplayNames(currentAlertData.Messages);
        return styledAlertMessages;
    }
    private _generateAlertMessage(currentAlertData: IAlertData): string {
        var styledAlertMessage = this._trySetStyleToDisplayName(currentAlertData.Message);
        return styledAlertMessage;
    }
    private _trySetStyleToDisplayNames(alertMessages: string[]): string[] {
        if (alertMessages) {
            var styledMessages = [];
            alertMessages.forEach((alertMessage) => {
                var styledMessage = this._trySetStyleToDisplayName(alertMessage);
                styledMessages.push(styledMessage);
            });
            return styledMessages;
        }
        return null;
    }
    private _trySetStyleToDisplayName(alertMessage: string): string {
        /// <summary>
        /// Если с сервера приходят сообщения с названиями полей где произошел сбой
        /// название полей выделяются жирным
        /// </summary>
        if (alertMessage) {
            //TODO Разморозить когда Handlebars.net допилит экранирование тегов
            //var splittedMessage = alertMessage.split(':');
            //if (splittedMessage.length > 1) {
            //    var displayName = splittedMessage[0],
            //        styledDisplayName = '<b>' + displayName + '</b>';
            //    splittedMessage[0] = styledDisplayName;
            //    return splittedMessage.join(':');
            //}
            return alertMessage;
        }
        return null;
    }
    private _getAlertCss(currentAlertData: IAlertData): string {
        var alertCss = '';
        switch (currentAlertData.TypeAlert) {
            case TypeOfAlert.Warning:
                alertCss = TypeOfAlertCss.Warning;
                break;
            case TypeOfAlert.Error:
                alertCss = TypeOfAlertCss.Error;
                break;
            case TypeOfAlert.Success:
                alertCss = TypeOfAlertCss.Success;
                break;
            case TypeOfAlert.Info:
                alertCss = TypeOfAlertCss.Info;
                break;
            default:
                alertCss = TypeOfAlertCss.Error;
                break;
        }
        if (this._currentOptions.TypeAlertDisplay == TypeAlertDisplay.SlideDownView) {
            alertCss += ' page-alert';
        }
        return alertCss;
    }
    private _getAlertHeaderByTypeAlert(currentAlertData: IAlertData) {
        switch (currentAlertData.TypeAlert) {
            case TypeOfAlert.Warning: return 'Предупреждение!';
            case TypeOfAlert.Success: return 'Операция завершена успешно!';
            case TypeOfAlert.Info: return 'Внимание!';
            case TypeOfAlert.Error:
                //в том случае если вдруг проинициализирован конструкто с количество невалидных полей -
                //выводим заголовок во множественном чесле даже если принято одно сообщение оповещения
                if (currentAlertData.Messages || currentAlertData.NumOfInvalid > 1) {
                    return 'Обнаружены ошибки!';
                }
                return 'Обнаружена ошибка!';
        }
    }

    public static SetupEnviroment() {
        $(document).on('click', '.alert .close', (e: Event) => {
            e.preventDefault();
            var $closeBtn = $(e.currentTarget),
                $alert = $closeBtn.closest('.alert');

            if ($alert.is('.page-alert')) {
                $alert.slideUp(400);
            } else {
                $alert.removeClass('active');
            }
            setTimeout(() => {
                $alert.remove();
            }, 400);
        });
    }
}