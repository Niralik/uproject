﻿

var TypeAlertDisplay;
(function (TypeAlertDisplay) {
    TypeAlertDisplay[TypeAlertDisplay["SlideDownView"] = 0] = "SlideDownView";
    TypeAlertDisplay[TypeAlertDisplay["PopupView"] = 1] = "PopupView";
    TypeAlertDisplay[TypeAlertDisplay["StandartView"] = 2] = "StandartView";
})(TypeAlertDisplay || (TypeAlertDisplay = {}));




var AlertEngine = (function () {
    function AlertEngine(alertOptions, alertWrapperSelector) {
        /// <summary>
        /// Класс аккамулирующий логику показа оповещений о результатах каких либо операций
        /// </summary>
        this._alertTemplate = Handlebars.templates['alert-block'];
        this._defaultOptions = {
            TypeAlertDisplay: 2 /* StandartView */,
            IsAutoHide: false,
            HideDelay: 10000,
            IsErrorAutoHide: false
        };
        this._alertsData = [];
        this._$generatedAlerts = [];
        /// <summary>
        /// Создаем экземпляр Алерт движка закрепленного за контайнером в котором будут отображаться
        /// все оповещения
        /// </summary>
        /// <param name="alertWrapperSelector" type="string">
        /// Селектор указывающий на контейнер будующий оповещений
        /// </param>
        /// <param name="alertOptions" type="IAlertOptions">
        /// Общие настройки для всего экземпляра Алерта
        /// </param>
        this.$AlertWrapper = $(alertWrapperSelector);
        this._options = $.extend({}, this._defaultOptions, alertOptions);
        this._setCurrentOptions();
    }
    AlertEngine.prototype.Post = function (alertData, oneCallOptions) {
        /// <summary>
        /// Инициализация объекта алерт движка новыми исходнымии данными
        /// </summary>
        /// <param name="displayedAlert" type="IAlertData">
        /// Данные для отображения оповещения
        /// </param>
        /// <param name="oneCallOptions?" type="IOneCallOptions">
        /// Настройки алерт системы для следующего одного вызова метода Show
        /// </param>
        this._oneCallOptions = $.extend({}, this._oneCallOptions, oneCallOptions);
        this._alertsData.push(alertData);
        this._setCurrentOptions();
        this._generateAlert();
    };
    AlertEngine.prototype._generateAlert = function () {
        if (this._alertTemplate && this._currentOptions.TypeAlertDisplay != 1 /* PopupView */) {
            var generatedAlertHtml = this._alertTemplate(this._generateAlertTemplateData());
            this._$generatedAlerts.push($(generatedAlertHtml));
        }
    };

    AlertEngine.prototype.ShowAll = function (alertSections, oneCallOptions) {
        var _this = this;
        /// <summary>
        /// Метод инициализириует  и показывает оповещения пришедшие с сервера и спрятанные в массив секций
        /// </summary>
        this._alertsData = [];
        if (this.$AlertWrapper && alertSections) {
            $.each(alertSections, function (index, alertSection) {
                _this.Post({
                    TypeAlert: alertSection.TypeAlert,
                    Messages: alertSection.Messages,
                    Message: alertSection.Message
                }, oneCallOptions);
            });
            this.Show();
        }
    };

    AlertEngine.prototype.Show = function () {
        /// <summary>
        /// Показывает сгенерированное на основе проиницилизрованных данных оповещение в контенейре
        /// указаном в конструкторе
        /// </summary>
        if (this._currentOptions.TypeAlertDisplay == 1 /* PopupView */) {
            this._showByPopupMessenger();
        } else {
            this._showByUsualWay();
        }
        this._oneCallOptions = null;
    };
    AlertEngine.prototype.Error = function (errorMessage) {
        this.Post({ Message: errorMessage, TypeAlert: 3 /* Error */ });
        this.Show();
    };
    AlertEngine.prototype._showByPopupMessenger = function () {
        if (!this._alertsData)
            return;

        $.each(this._alertsData, function (index, alertData) {
            Messenger().post({
                message: alertData.Message,
                type: TypeOfAlert[alertData.TypeAlert].toLowerCase(),
                showCloseButton: true
            });
        });
    };

    AlertEngine.prototype._showByUsualWay = function () {
        var _this = this;
        if (!this._$generatedAlerts)
            return;
        $.each(this._$generatedAlerts, function (index, $generatedAlert) {
            if (_this._hidePromise) {
                _this._hidePromise.then(function () {
                    _this._addToPageAlert($generatedAlert);
                    _this._hidePromise = null;
                });
            } else {
                _this._addToPageAlert($generatedAlert);
            }
            _this._setAutohide($generatedAlert);
        });
    };
    AlertEngine.prototype._setCurrentOptions = function () {
        if (this._oneCallOptions && Object.keys(this._oneCallOptions).length) {
            this._currentOptions = this._oneCallOptions;
        } else {
            this._currentOptions = this._options;
        }
    };
    AlertEngine.prototype._addToPageAlert = function ($alertTag) {
        $alertTag.appendTo(this.$AlertWrapper);
        this._displayByOptions($alertTag);
    };
    AlertEngine.prototype._displayByOptions = function ($alertTag) {
        switch (this._currentOptions.TypeAlertDisplay) {
            case 0 /* SlideDownView */:
                $alertTag.slideDown(400);
                break;
            case 2 /* StandartView */:
                $alertTag.addClass('active');
                break;
            default:
        }
    };
    AlertEngine.prototype._setAutohide = function ($alertTage) {
        var _this = this;
        if (this._currentOptions.IsAutoHide) {
            if ($alertTage.is('.alert-danger') && !this._currentOptions.IsErrorAutoHide)
                return;
            setTimeout(function () {
                _this._hideOne($alertTage);
            }, this._currentOptions.HideDelay);
        }
    };

    AlertEngine.prototype._hideOne = function ($alertTag) {
        var deffered = $.Deferred();
        this._hideByOptions($alertTag);
        setTimeout(function () {
            $alertTag.remove();
            deffered.resolve();
        }, 450);
        this._hidePromise = deffered.promise();
    };
    AlertEngine.prototype._hideByOptions = function ($alertTag) {
        switch (this._currentOptions.TypeAlertDisplay) {
            case 0 /* SlideDownView */:
                $alertTag.slideUp(400);
                break;
            case 2 /* StandartView */:
                $alertTag.removeClass('active');
                break;
            default:
        }
    };

    AlertEngine.prototype.HideAll = function () {
        var _this = this;
        var standartAlerts = $('.alert').filter(function (index, alert) {
            return !$(alert).is('.messenger-message');
        });
        $.each(standartAlerts, function (index, alert) {
            _this._hideOne($(alert));
        });
        this._$generatedAlerts = [];
        this._ClearMessages();
    };

    AlertEngine.prototype._ClearMessages = function () {
        this._alertsData = [];
    };

    AlertEngine.prototype._generateAlertTemplateData = function () {
        /// <summary>
        /// Метод генерирует данные для шаблона -  исходя из того было проинизлированно одно сообщение или массив
        /// генерируется разное представление из за ограничений хендбара темплейтов используется два свойства характириующих
        /// одну и ту же смысловую единицу, для массива сообщений представление в виде листа - для одного просто вывод текста
        /// </summary>
        //Так как данные для шаблона генерятся после каждой инициализации Данных последним элементом в массиве данных текущего объекта будет
        //необходимый нам для создания данных для шаблона
        var currentAlertData = this._alertsData[this._alertsData.length - 1];
        var alertData = {
            AlertCss: this._getAlertCss(currentAlertData),
            AlertHeader: this._getAlertHeaderByTypeAlert(currentAlertData),
            AlertMessages: this._generateAlertMessagesList(currentAlertData),
            AlertMessage: this._generateAlertMessage(currentAlertData)
        };
        return alertData;
    };
    AlertEngine.prototype._generateAlertMessagesList = function (currentAlertData) {
        var styledAlertMessages = this._trySetStyleToDisplayNames(currentAlertData.Messages);
        return styledAlertMessages;
    };
    AlertEngine.prototype._generateAlertMessage = function (currentAlertData) {
        var styledAlertMessage = this._trySetStyleToDisplayName(currentAlertData.Message);
        return styledAlertMessage;
    };
    AlertEngine.prototype._trySetStyleToDisplayNames = function (alertMessages) {
        var _this = this;
        if (alertMessages) {
            var styledMessages = [];
            alertMessages.forEach(function (alertMessage) {
                var styledMessage = _this._trySetStyleToDisplayName(alertMessage);
                styledMessages.push(styledMessage);
            });
            return styledMessages;
        }
        return null;
    };
    AlertEngine.prototype._trySetStyleToDisplayName = function (alertMessage) {
        /// <summary>
        /// Если с сервера приходят сообщения с названиями полей где произошел сбой
        /// название полей выделяются жирным
        /// </summary>
        if (alertMessage) {
            //TODO Разморозить когда Handlebars.net допилит экранирование тегов
            //var splittedMessage = alertMessage.split(':');
            //if (splittedMessage.length > 1) {
            //    var displayName = splittedMessage[0],
            //        styledDisplayName = '<b>' + displayName + '</b>';
            //    splittedMessage[0] = styledDisplayName;
            //    return splittedMessage.join(':');
            //}
            return alertMessage;
        }
        return null;
    };
    AlertEngine.prototype._getAlertCss = function (currentAlertData) {
        var alertCss = '';
        switch (currentAlertData.TypeAlert) {
            case 1 /* Warning */:
                alertCss = TypeOfAlertCss.Warning;
                break;
            case 3 /* Error */:
                alertCss = TypeOfAlertCss.Error;
                break;
            case 2 /* Success */:
                alertCss = TypeOfAlertCss.Success;
                break;
            case 0 /* Info */:
                alertCss = TypeOfAlertCss.Info;
                break;
            default:
                alertCss = TypeOfAlertCss.Error;
                break;
        }
        if (this._currentOptions.TypeAlertDisplay == 0 /* SlideDownView */) {
            alertCss += ' page-alert';
        }
        return alertCss;
    };
    AlertEngine.prototype._getAlertHeaderByTypeAlert = function (currentAlertData) {
        switch (currentAlertData.TypeAlert) {
            case 1 /* Warning */:
                return 'Предупреждение!';
            case 2 /* Success */:
                return 'Операция завершена успешно!';
            case 0 /* Info */:
                return 'Внимание!';
            case 3 /* Error */:
                //в том случае если вдруг проинициализирован конструкто с количество невалидных полей -
                //выводим заголовок во множественном чесле даже если принято одно сообщение оповещения
                if (currentAlertData.Messages || currentAlertData.NumOfInvalid > 1) {
                    return 'Обнаружены ошибки!';
                }
                return 'Обнаружена ошибка!';
        }
    };

    AlertEngine.SetupEnviroment = function () {
        $(document).on('click', '.alert .close', function (e) {
            e.preventDefault();
            var $closeBtn = $(e.currentTarget), $alert = $closeBtn.closest('.alert');

            if ($alert.is('.page-alert')) {
                $alert.slideUp(400);
            } else {
                $alert.removeClass('active');
            }
            setTimeout(function () {
                $alert.remove();
            }, 400);
        });
    };
    return AlertEngine;
})();
//# sourceMappingURL=alert-engine.js.map
