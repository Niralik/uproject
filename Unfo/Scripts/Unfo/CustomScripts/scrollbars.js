﻿var Scrollbars = (function () {
    function Scrollbars() {
        this._leftMenuBody = $('.menu-body');
        if (this._leftMenuBody.length == 0)
            return;
        this._calculateMenuBodyHeight();
        this._applyPerfectScrollbar();
        this._resizeWindowUpdatePerfectScrollbar();
    }
    Scrollbars.prototype._calculateMenuBodyHeight = function () {
        var menuHeight = $('.main-sidebar').height();
        var menuHeaderHeight = $('.main-sidebar .current-user').height();
        var menuFooterHeight = $('.main-sidebar .bottom-menu').height();
        var menuBodyMargin = parseInt(this._leftMenuBody.css('margin-top').split('px')[0]) + parseInt(this._leftMenuBody.css('margin-bottom').split('px')[0]);
        var heightMenuBody = menuHeight - menuHeaderHeight - menuFooterHeight - menuBodyMargin;

        this._leftMenuBody.innerHeight(heightMenuBody);
    };
    Scrollbars.prototype._applyPerfectScrollbar = function () {
        this._leftMenuBody.perfectScrollbar({
            includePadding: true,
            maxScrollbarLength: 500
        });
    };
    Scrollbars.prototype._resizeWindowUpdatePerfectScrollbar = function () {
        var _this = this;
        var oldWindowHeight = $(window).height();
        $(window).resize(function (e) {
            if (oldWindowHeight != $(window).height()) {
                _this._calculateMenuBodyHeight();
                _this._leftMenuBody.perfectScrollbar('update');
            }
        });
    };
    return Scrollbars;
})();

$(function () {
    var scrollbars = new Scrollbars();
});
//# sourceMappingURL=scrollbars.js.map
