﻿var g_cabinetHomeIndexUrl = "/Cabinet/Home/index";
var g_urlReferrer = $('#urlReferrer').val();

///<var>Тип оповещения</var>
///Порядок должен совпадать с перечеслением на сервере
enum TypeOfAlert {
    Info,
    Warning,
    Success,
    Error
};
///<var>Css классы представляющие тип оповещения</var>
var TypeOfAlertCss = {
    Info:"alert-info",
    Warning: 'alert-warning',
    Success: 'alert-success',
    Error:'alert-danger'
};

var optsOfBigRoundCenteredSpinner =
{
    lines: 9, // The number of lines to draw
    length: 0, // The length of each line
    width: 17, // The line thickness
    radius: 21, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 66, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    speed: 1, // Rounds per second
    trail: 42, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '48.2%',
    left: '50.1%'
    };
var optsOfSmallRoundRightSpinner = {
    lines: 9, // The number of lines to draw
    length: 0, // The length of each line
    width: 11, // The line thickness
    radius: 13, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 66, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: 'rgb(90, 203, 90)', // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 42, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '50.2%',
    left: '92%'
};
var optsOfBigPurpleRoundCenteredSpinner = $.extend({}, optsOfBigRoundCenteredSpinner, { color: '#59688d' });
var optsOfBigGreenRoundCenteredSpinner = $.extend({}, optsOfBigRoundCenteredSpinner, { color: '#76b852' });
var optsOfSmallGreenRoundRightSpinner = $.extend({}, optsOfSmallRoundRightSpinner, { color: '#76b852' });

