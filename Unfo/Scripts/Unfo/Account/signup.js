﻿function SignUp() {
    var self = this,
        $signUpForm = $('#signUpForm'),
        signUpIdealFrom,
        urlToSignUpController = $signUpForm.attr('action'),
        spinner,
        $loadingSpinner,
        $submitBtn = $signUpForm.find('.submit-container .btn'),
        $inputsOfSignUpForm = $signUpForm.find('input:not([type=checkbox]):not([type=submit])'),
        alert = new AlertEngine({ IsAutoHide: true, IsErrorAutoHide: false }, '.alert-wrapper:first'),
        isExternalLoginSignUp = toBool($signUpForm.data('is-external-login-sign-up')),
        isStandartSignUp = !isExternalLoginSignUp,
        $interactiveIcon = $('.fa.interactive-icon');

    this.Inititialize = function () {
        var signUpRules = {
            'Email': 'required email ajax',
            'FirstName': 'required name',
            'SurName': 'required name',
            'FatherName': 'name',
            'PhoneNumber': 'min:6 max:20'
        };
        if (isStandartSignUp) {
            signUpRules['Password'] = 'required weakpass';
            signUpRules['ConfirmPassword'] = 'required equalto:Password';
        }

        $signUpForm.idealforms({
            rules: signUpRules,
            adaptiveWidth: 500,
            onValidate: function (item, ruleName, valid) {
                if ($(item).is('#Email')) hideLinkIcon();

                if (ruleName === "ajax" && valid) {
                    toggleDisabledOfSubmitBtnsByInvalidsField(this);
                }
                if (ruleName === "ajax" && !valid) {
                    var email = $(item).val();
                    $interactiveIcon.removeClass('fa-link fa-info-circle');
                    handleExistingPasswordCase(email);
                }
            },
            onSubmit: function (invalid, e, f) {
                e.preventDefault();
                if (invalid > 0) {
                    var errorMessage = 'Пожалуйста, внесите исправления и попробуйте снова';
                    alert.Post({ TypeAlert: TypeOfAlert.Error, Message: errorMessage, NumOfInvalid: invalid });
                    alert.Show();
                } else {
                    sendDataOfFormByAjaxToServer();
                }
            }
        });
        signUpIdealFrom = $signUpForm.data('idealforms');

        

        initializeSubmitToggle();
        initializeInteractiveIconSettings();
    };
    function initializeInteractiveIconSettings() {
        $interactiveIcon.one('click', function () {
            if ($(this).is('.fa-link') && isExternalLoginSignUp) {
                showSuggestLinkModalWindow();
            } 
        });
        $interactiveIcon.tooltip({
            container: 'body'
        });
        $interactiveIcon.on('shown.bs.tooltip', function() {
            setTimeout(function() {
                $interactiveIcon.tooltip('hide');
            },3000);
        });
    }
    function initializeSubmitToggle()
    {
        toggleDisabledOfSubmitBtnsByInvalidsField();
        $inputsOfSignUpForm.on('change keyup click', function () {
            toggleDisabledOfSubmitBtnsByInvalidsField();
        });
    }

    function _showSpinner($hiddingBtn, options) {
        var $socialWrapper = $hiddingBtn.parent();
        spinner = new Spinner(options);
        spinner.spin($socialWrapper);
        $loadingSpinner = $('.spinner', $socialWrapper);
        //Как бы утверждаем чтоб анимация затухания скрываемой социальной кнопки
        //произошла только тогда когда отработает анимация спиннера
        $loadingSpinner.promise().then(function () {
            $hiddingBtn.fadeOut(350, function () {
                $loadingSpinner.fadeIn(350);
            });
        });
    }

    function _hideSpinner($showingBtn) {
        $loadingSpinner.fadeOut(350, function () {
            $showingBtn.fadeIn(350);
            spinner.stop();
        });
    }

    function toggleDisabledOfSubmitBtnsByInvalidsField(idealForm) {
        /// <summary>Изменение режима доступности кнопки сабмита - если есть невалидные поля кнопка заблочена</summary>
        var invalidFields = 0;
        if (idealForm) {
            invalidFields = idealForm.GetInvalidFields();
        } else {
            invalidFields = signUpIdealFrom.GetInvalidFields();
        }

        if (invalidFields.length > 0) {
            $submitBtn.attr("disabled", "disabled");
        } else {
            $submitBtn.removeAttr("disabled");
        }
    };

    function sendDataOfFormByAjaxToServer() {
        $.ajax({
            type: "POST",
            url: urlToSignUpController,
            dataType: "JSON",
            beforeSend: function (jqXHR, settings) {
                var signUpData = $signUpForm.serialize();
                if (signUpData.length > 0) {
                    settings.data = settings.data + "&" + signUpData;
                }
                _showSpinner($submitBtn, optsOfBigGreenRoundCenteredSpinner);
                alert.HideAll();
            },
            success: function (response) {
                if (response.Successed) {
                    if (g_urlReferrer) {
                        window.location.replace(g_urlReferrer);
                    } else {
                        window.location.replace(g_cabinetHomeIndexUrl);
                    }
                } else {
                    alert.ShowAll(response.AlertSections);
                    _hideSpinner($submitBtn);
                }
            }
        });
    };

    //#region Обработка случая существования емейла в системе
    function handleExistingPasswordCase(email) {
        signUpIdealFrom.HideErrorHint($('#Email'));
        checkHasThisUserPassword(email).then(function (checkResponse) {
            if (checkResponse.Successed && isExternalLoginSignUp) {
                $interactiveIcon.addClass('fa-link');
                showLinkIcon();
            } else {
                $interactiveIcon.addClass('fa-info-circle');
                getUserExternalLogins(email).then(function(getResponse) {
                    if (getResponse.Successed) {
                        changeTooltip(getResponse.ExternalLogins);
                        showLinkIcon();
                    }
                });
            }
            
        });
    }

    function checkHasThisUserPassword(email) {
        var url = $('#urlToCheckHasPasswordAction').val();
        return $.ajax({
            type: "GET",
            url: url,
            data: { email: email },
            dataType: "json",
        });
    }

    function getUserExternalLogins(email) {
        var url = $('#urlToGetUserExternalLoginsAction').val();
        return $.ajax({
            type: "Get",
            url: url,
            data: { email: email },
            dataType: "json",
        });
    }

    //#region Обработка ситуации когда у запрешивомой учетной записи есть пароль
    var showIconPromise;
    function showLinkIcon() {
        //Если показ линк иконки уже запущен то не допускаем до кода ниже
        if (showIconPromise) return;

        var deffered = $.Deferred();

        if (hideIconPromise) {
            hideIconPromise.then(function () {
                animateShow(deffered);
                hideIconPromise = null;
            });
        } else {
            animateShow(deffered);
        }
        showIconPromise = deffered.promise();

        function animateShow(animateDeffered) {
            $interactiveIcon.show().animate(
           {
               opacity: 1,
               right: "8px"
           }, {
               duration: 200,
               complete: function () {
                   $interactiveIcon.tooltip('show');
                   animateDeffered.resolve();
               }
           }
       );
        }
    }

    var hideIconPromise;
    function hideLinkIcon() {
        //если анимация скрытия уже запущена - то не пускаем никого ниже
        if (hideIconPromise) return;

        var deffered = $.Deferred();

        if (showIconPromise) {
            showIconPromise.then(function () {
                animateHide(deffered);
                showIconPromise = null;
            });
        } else {
            animateHide(deffered);
        }
        hideIconPromise = deffered.promise();
        function animateHide(amimateDeffered) {
            $interactiveIcon.animate(
            {
                opacity: 0,
                right: "0px"
            }, {
                duration: 200,
                complete: function () {
                    $interactiveIcon.hide();
                    $interactiveIcon.tooltip('hide');
                    amimateDeffered.resolve();
                }
            });
        }
    }

    var modalAlert;
    function showSuggestLinkModalWindow() {
        var email = $('#Email').val();
        var suggestLinkModalTemplate = Handlebars.templates['suggest-link-modal-window'];
        var $suggestLinkModal = $(suggestLinkModalTemplate({ Email: email }));
        $suggestLinkModal.appendTo('#signup');
        modalAlert = new AlertEngine({ IsAutoHide: true }, '.modal .alert-wrapper');
        var $linkExternalProviderForm = $('form', $suggestLinkModal);
        $linkExternalProviderForm.idealforms({
            adaptiveWidth: 900,
            iconHtml: '',
            onSubmit: function (invalid, e, f) {
                e.preventDefault();
                if (invalid == 0) {
                    _signInWithLinkToExternalLogin($linkExternalProviderForm);
                }
            }
        });
        $suggestLinkModal.modal({
            backdrop: 'static'
        });
        attachEventsToSuggestLinkModal($suggestLinkModal);
    };

    function attachEventsToSuggestLinkModal($modal) {
        $modal.on('hidden.bs.modal', function (e) {
            $(this).remove();
            $interactiveIcon.one('click', function () {
                showSuggestLinkModalWindow();
            });
            if (requiestSignInWithLink) {
                requiestSignInWithLink.abort();
            }
        });
        $modal.on('shown.bs.modal', function () {
            signUpIdealFrom.HideErrorHint($('#Email'));
        });
    };

    var requiestSignInWithLink;
    function _signInWithLinkToExternalLogin($linkExternalProviderForm) {
        var urlToAction = $linkExternalProviderForm.attr('action');
        requiestSignInWithLink = $.ajax({
            type: "POST",
            url: urlToAction,
            dataType: "json",
            beforeSend: function (jqXHR, settings) {
                var signInWithLinkData = $linkExternalProviderForm.serialize();
                if (signInWithLinkData.length > 0) {
                    settings.data = settings.data + "&" + signInWithLinkData;
                }
                _showSpinner($linkExternalProviderForm.find('.btn'), optsOfSmallGreenRoundRightSpinner);
                modalAlert.HideAll();
            },
            success: function (response) {
                if (response.Successed) {
                    if (g_urlReferrer) {
                        window.location.replace(g_urlReferrer);
                    } else {
                        window.location.replace(g_cabinetHomeIndexUrl);
                    }
                } else {
                    modalAlert.ShowAll(response.AlertSections);
                    _hideSpinner($linkExternalProviderForm.find('.btn'));
                }
            }
        });
    }
//#endregion
    
    //#region Обработка ситуации когда у запрешивомой учетной записи нет пароля
    function changeTooltip(externalLogins) {
        
        var tooltipTemplate = Handlebars.templates['external-logins-tooltip'];
        var tooltipHtml = tooltipTemplate({ ExternalLogins: externalLogins });
        $interactiveIcon.attr('data-original-title', tooltipHtml);
    }
    //#endregion

    //#endregion
}

$(function () {
    var signUp = new SignUp();
    signUp.Inititialize();
});