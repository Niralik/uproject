﻿function SignIn() {
    var self = this,
        $standartSignInForm = $('#signInForm'),
        $ExternalSignInForm = $('#externalLoginForm'),
        standartSignInIdealForm,
        urlToSignInController,
        urlToExternalSignInController,
        $spinnerOfLoad,
        $btnsOfStandartSignIn,
        $btnsOfExternalSignIn,
        $inputsOfSignInForm = $standartSignInForm.find('input:not([type=checkbox]):not([type=submit])'),
        alert = new AlertEngine( { IsAutoHide: true },'.alert-wrapper');

    var TypeOfSignIn =
    {
        StandartSignIn: 0,
        ExternalSignIn: 1
    };
    this.initialize = function () {
        urlToSignInController = $standartSignInForm.attr("action");
        urlToExternalSignInController = $ExternalSignInForm.attr("action");

        $standartSignInForm.idealforms({
            rules: {
                'Email': 'required email',
                'Password': 'required'
            },
            adaptiveWidth: 300,
            onValidate: function (item, e, valid) {
              
            },
            onSubmit: function (invalid, e, f) {
                e.preventDefault();
            }
        });
        standartSignInIdealForm = Idealforms.GetInstance($standartSignInForm);
        var $signInFormBtnsWrapper = $standartSignInForm.find('.actions:first');
        initilizeSpinner($signInFormBtnsWrapper, TypeOfSignIn.StandartSignIn);

        $("#signInButton").click(function (e) {
            e.preventDefault();
            standartSignIn();
        });

        $('.social-icon').click(function () {
            var $socialButton = $(this),
                $socialButtonWrapper = $socialButton.parent();

            initilizeSpinner($socialButtonWrapper, TypeOfSignIn.ExternalSignIn);
            externalSignIn($socialButton);
        });

        toggleDisabledModeOfSubmitButtonsByInvalidsField();
        $inputsOfSignInForm.on('change keyup click blur', function () {
            toggleDisabledModeOfSubmitButtonsByInvalidsField();
        });
    };

    function initilizeSpinner($containerOfspinner, typeOfSignIn) {
        var optionsOfLoadingSpinner,
            spinnerOfLoad;

        optionsOfLoadingSpinner = {
            lines: 9, // The number of lines to draw
            length: 0, // The length of each line
            width: 15, // The line thickness
            radius: 18, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 66, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#59688d', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 42, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '48.2%',
            left: '50.1%'
        };
        spinnerOfLoad = new Spinner(optionsOfLoadingSpinner).spin();
        $containerOfspinner.append(spinnerOfLoad.el);
        $spinnerOfLoad = $containerOfspinner.find('.spinner');
        if (typeOfSignIn == TypeOfSignIn.ExternalSignIn) {
            $btnsOfExternalSignIn = $containerOfspinner.find('.social-icon');
        } else {
            $btnsOfStandartSignIn = $containerOfspinner.find('.btn');
        }
    };

    function showSpinnerOfLoad($hiddingBtns) {
        /// <summary>Плавно показывает анимированную иконку процесса загрузки и скрывает кнопки, для Логина</summary>
        $spinnerOfLoad.promise().then(function () {
            $hiddingBtns.fadeOut(350, function () {
                $spinnerOfLoad.fadeIn(350);
            });
        });
    };
    function hideSpinnerOfLoad($showingBtns) {
        /// <summary>Плавно скрывает анимированную иконку процесса загрузки и показывает кнопки, для Логина</summary>
        $showingBtns.promise().then(function () {
            $spinnerOfLoad.fadeOut(350, function () {
                $showingBtns.fadeIn(350);
            });
        });
    };

    function toggleDisabledModeOfSubmitButtonsByInvalidsField() {
        /// <summary>Изменение режима доступности кнопки сабмита - если есть невалидные поля кнопка заблочена</summary>
        var idealForm = $standartSignInForm.data('idealforms');
        var invalidFields = idealForm.GetInvalidFields();

        if (invalidFields.length > 0) {
            $btnsOfStandartSignIn.attr("disabled", "disabled");
        } else {
            $btnsOfStandartSignIn.removeAttr("disabled");
        }
    }

    function standartSignIn() {
        alert.HideAll();
        var idealForm = $standartSignInForm.data('idealforms'),
            invalidFields = idealForm.GetInvalidFields();

        if (invalidFields.length > 0) {
            var errorMessage = 'Пожалуйста, внесите исправления и попробуйте снова';     
            alert.Post({TypeAlert: TypeOfAlert.Error, Messages: errorMessage, NumOfInvalid: invalidFields.length});
            alert.Show();
        } else {

            $.ajax({
                type: "POST",
                url: urlToSignInController,
                dataType: "JSON",
                beforeSend: function (jqXHR, settings) {
                    var signInData = $standartSignInForm.serialize({ checkboxesAsBools: true });
                    if (signInData) {
                        settings.data = settings.data + "&" + signInData;
                    }
                    showSpinnerOfLoad($btnsOfStandartSignIn);
                },
                success: function (response) {
                    if (response.Successed) {
                        if (g_urlReferrer) {
                            window.location.replace(g_urlReferrer);
                        } else {
                            window.location.replace(g_cabinetHomeIndexUrl);
                        }
                    } else {
                        alert.ShowAll(response.AlertSections);
                        hideSpinnerOfLoad($btnsOfStandartSignIn);
                    }
                },
                error: function () {
                    alert.Post({ TypeAlert: TypeOfAlert.Error, Message: Idealforms.LocalizedErrors.internalServerError});
                    alert.Show();
                    hideSpinnerOfLoad($btnsOfStandartSignIn);
                }
            });
        }
    }

    function externalSignIn($socialButton) {
        var provider = $socialButton.data('value');
        var $providerInput = $ExternalSignInForm.find('#provider');
        $providerInput.val(provider);
        showSpinnerOfLoad($btnsOfExternalSignIn);
        $ExternalSignInForm.submit();
    }
}

var signIn;
$(function () {
    signIn = new SignIn();
    signIn.initialize();
});