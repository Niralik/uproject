﻿
$(function() {
    var opts = {
        lines: 11, // The number of lines to draw
        length: 0, // The length of each line
        width: 11, // The line thickness
        radius: 19, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 54, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 28, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9 // The z-index (defaults to 2000000000)


    };


    var spinner = new Spinner(opts).spin();
    $(document).on('click', '#addLocation, .editLocation', function() {
        var self = this;

        $('#modalForEdit').modal('show');

        $('#spinner').append(spinner.el);
        $('#spinner .spinner').show();
        $.get('/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/EditLocation', { id: $(self).attr('data-id') }, function(data) {

            $('#modalForEdit .modal-content').html(data);
            $.validator.unobtrusive.parse('#modalForEdit');
        });
    });

    if ($('#choosePlanModal')[0]) {
        $('#choosePlanModal').modal('show');
    }
    $(document).on('click', '.addDirection', function() {
        getModal('EditDirection');
    });

    $('#selectTariff').click(function (e) {
        e.preventDefault();
        $('#choosePlanModal').modal('hide');
    })
    var $plans = $(".plans .plan");
    $plans.click(function () {
        $plans.removeClass("selected");
        $(this).addClass("selected");
        $('#TariffId').val($(this).attr('data-tariff-id'));
        $('#selectTariff').removeClass('disabled');
    });

    var $step_triggers = $("[data-step]");
    var $step_panels = $(".step-panel");
    var $tabs = $(".steps .step");

    $step_triggers.click(function (e) {
        e.preventDefault();
        var go_to_step = $(this).data("step");

        $step_panels.removeClass("active");
        $step_panels.eq(go_to_step).addClass("active");

        $tabs.removeClass("active");
        $tabs.eq(go_to_step).addClass("active");

        if (go_to_step === 1) {
            $("#billing-form input:text:eq(0)").focus();
        }
    });

    $('#IsPrivate').change(function (e) {
        if ($(this).prop('checked')) {
            $('.promocode').removeClass('hide');
        } else {
            $('.promocode').addClass('hide');
        }
    });
    $(document).on('click', '.removeDirection', function (e) {
        var self = this;
        $.post('/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/DeleteDirection', { id: $(this).attr('data-id') }, function (data) {
            $(self).closest('.tag').remove();
        });
    });
});
function afterModalSubmit(type) {

    if (type == 'direction') {
        $.get('/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/Directions?typeView=1', function (data) {
   
            $('.direction-block').html(data);

        });
    } else {
        $.get('/Cabinet/Event/' + $('#eventName').val() + '/Resources/GetList?type=' + type, function (data) {

            var t = $('.select-' + type);
            if (t.length > 1) {
                $.each(t, function () {
                    insertToSelect(data.Response, 'Выберите', this);
                });
            }
            else
                insertToSelect(data.Response, 'Выберите', t);

        });

    }




}