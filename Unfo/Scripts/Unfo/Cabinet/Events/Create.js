﻿
$(function () {
    $.validator.par
    if ($('#choosePlanModal')[0]) {
        $('#choosePlanModal').modal('show');
    }

    $('#selectTariff').click(function (e) {
        e.preventDefault();
        $('#choosePlanModal').modal('hide');
    })
    var $plans = $(".plans .plan");
    $plans.click(function () {
        $plans.removeClass("selected");
        $(this).addClass("selected");
        $('#TariffId').val($(this).attr('data-tariff-id'));
        $('#createEventBtn').removeClass('disabled');
    });


    var $step_triggers = $("[data-step]");
    var $step_panels = $(".step-panel");
    var $tabs = $(".steps .step");

    $step_triggers.click(function (e) {
        e.preventDefault();
        var go_to_step = $(this).data("step");

        $step_panels.removeClass("active");
        $step_panels.eq(go_to_step).addClass("active");

        $tabs.removeClass("active");
        $tabs.eq(go_to_step).addClass("active");

        if (go_to_step === 1) {
            $("#event-form input:text:eq(0)").focus();
        }
    });

    $('#event-form').submit(function (e) {
        if ($(this).valid()) {
            $.post($(this).attr('action'), getFormInputs('#event-form'),function (data) {
                
                if (data.Result == 1) {
                    window.location.href = '/Cabinet/Event/' + data.Response + '/Edit';
                }
            },'JSON');
        }
        e.preventDefault();
    });

    $('#createEventBtn').click(function (e) {

        $('#event-form').submit();


        e.preventDefault();
    });
});
