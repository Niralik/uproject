﻿declare class Events {
    private _isFavoriteView;
    private _$events;
    private _$favoriteBtns;
    private _popupAlert;
    constructor();
    private _initializationReadmoreBtns();
    private _getDescsWithLargeText();
    private _initializationToFavoriteLogic();
    private _ToggleEventFavorite(event, urlToAction);
    private _favoriteStarToogleState(favoriteStar, isSuccessed);
    private _favoriteStarToProgressState(favoriteStar);
    private _favoriteStarToActiveState(favoriteStar);
    private _favoriteStarToNoneState(favoriteStar);
}
