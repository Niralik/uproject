﻿class Events {
    private _isFavoriteView = parseBool($('#isFavoriteView').val());
    private _$events = $('.event');
    private _$favoriteBtns = this._$events.find('#favoriteBtn');
    private _popupAlert: AlertEngine = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });

    constructor() {
        this._initializationReadmoreBtns();
        this._initializationToFavoriteLogic();
    }
    private _initializationReadmoreBtns() {
        var eventDescsForReadmore = this._getDescsWithLargeText();
        eventDescsForReadmore.readmore({
            maxHeight: 32,
            speed: 300,
            moreLink: '<a href="#">' + l8on.Common.MoreInfo + '</a>',
            lessLink: '<a href="#">' + l8on.Common.Hide + '</a>',
        });
    }
    private _getDescsWithLargeText(): JQuery {
        var $eventDescs = $('.event .desc');
        var $descsWithLargeTest = $eventDescs.filter((index, desc) => {
            var text = $(desc).find('span');
            return text.height() > $(desc).height();
        });
        return $descsWithLargeTest;
    }
    private _initializationToFavoriteLogic() {
        var urlToFavoriteAction = $('#urlToFavoriteAction').val();
        var urlToUnFavoriteAction = $('#urlToUnFavoriteAction').val();
        this._$favoriteBtns.click((e: JQueryEventObject) => {
            var favoriteBtn = $(e.target);
            var event = favoriteBtn.closest('.event');
            var isFavoriteEvent = parseBool(event.data('is-favorite-event'));
            if (isFavoriteEvent) {
                this._ToggleEventFavorite(event, urlToUnFavoriteAction);
            } else {
                this._ToggleEventFavorite(event, urlToFavoriteAction);
            }
        });
    }
    private _ToggleEventFavorite(event: JQuery, urlToAction: string): void {
        var eventId = event.find('#eventId').val();
        var eventName = event.find('.title a').text();
        var favoriteStar = event.find('#favoriteBtn .fa');
        $.ajax({
            type: "POST",
            url: urlToAction,
            data: { eventId: eventId, eventName: eventName },
            beforeSend: () => {
                this._favoriteStarToProgressState(favoriteStar);
            },
            success: (response: IServerResponse) => {
                if (response.Successed) {
                    this._favoriteStarToogleState(favoriteStar, true);
                } else {
                    this._favoriteStarToogleState(favoriteStar, false);
                }
                this._popupAlert.ShowAll(response.AlertSections);
            },
            error: () => {
                this._favoriteStarToogleState(favoriteStar, false);
                this._popupAlert.Error(l8on.InternalServerError);
            }
        });
    }
    private _favoriteStarToogleState(favoriteStar: JQuery, isSuccessed: boolean) {
        var container = favoriteStar.closest('a');
        var isFavoriteEvent = parseBool(container.closest('.event').data('is-favorite-event'));
        if (isFavoriteEvent) {
            if (isSuccessed) {
                this._favoriteStarToNoneState(favoriteStar);
            } else {
                this._favoriteStarToActiveState(favoriteStar);
            }
        } else {
            if (isSuccessed) {
                this._favoriteStarToActiveState(favoriteStar);
            } else {
                this._favoriteStarToNoneState(favoriteStar);
            }
        }
    }
    private _favoriteStarToProgressState(favoriteStar: JQuery) {
        var container = favoriteStar.closest('a');
        favoriteStar.addClass('fa-spin');
        container.addClass('active');
        container.attr('data-original-title', '');
        container.attr('disabled', 'disabled');
        container.tooltip('hide');
    }
    private _favoriteStarToActiveState(favoriteStar: JQuery) {
        var container = favoriteStar.closest('a');
        favoriteStar.removeClass('fa-spin');
        container.attr('data-original-title', 'Убрать из избранного');
        container.removeAttr('disabled');

    }
    private _favoriteStarToNoneState(favoriteStar: JQuery) {
        var container = favoriteStar.closest('a');
        favoriteStar.removeClass('fa-spin');
        container.removeClass('active');
        container.attr('data-original-title', 'В избранное');
        container.removeAttr('disabled');
        if (this._isFavoriteView) {
            var $eventLi = favoriteStar.closest('li');
            $eventLi.slideUp(400, () => {
                $eventLi.remove();
            });
        }
    }
}
$(() => {
    var events = new Events();
})