﻿var Events = (function () {
    function Events() {
        this._isFavoriteView = parseBool($('#isFavoriteView').val());
        this._$events = $('.event');
        this._$favoriteBtns = this._$events.find('#favoriteBtn');
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this._initializationReadmoreBtns();
        this._initializationToFavoriteLogic();
    }
    Events.prototype._initializationReadmoreBtns = function () {
        var eventDescsForReadmore = this._getDescsWithLargeText();
        eventDescsForReadmore.readmore({
            maxHeight: 32,
            speed: 300,
            moreLink: '<a href="#">' + l8on.Common.MoreInfo + '</a>',
            lessLink: '<a href="#">' + l8on.Common.Hide + '</a>'
        });
    };
    Events.prototype._getDescsWithLargeText = function () {
        var $eventDescs = $('.event .desc');
        var $descsWithLargeTest = $eventDescs.filter(function (index, desc) {
            var text = $(desc).find('span');
            return text.height() > $(desc).height();
        });
        return $descsWithLargeTest;
    };
    Events.prototype._initializationToFavoriteLogic = function () {
        var _this = this;
        var urlToFavoriteAction = $('#urlToFavoriteAction').val();
        var urlToUnFavoriteAction = $('#urlToUnFavoriteAction').val();
        this._$favoriteBtns.click(function (e) {
            var favoriteBtn = $(e.target);
            var event = favoriteBtn.closest('.event');
            var isFavoriteEvent = parseBool(event.data('is-favorite-event'));
            if (isFavoriteEvent) {
                _this._ToggleEventFavorite(event, urlToUnFavoriteAction);
            } else {
                _this._ToggleEventFavorite(event, urlToFavoriteAction);
            }
        });
    };
    Events.prototype._ToggleEventFavorite = function (event, urlToAction) {
        var _this = this;
        var eventId = event.find('#eventId').val();
        var eventName = event.find('.title a').text();
        var favoriteStar = event.find('#favoriteBtn .fa');
        $.ajax({
            type: "POST",
            url: urlToAction,
            data: { eventId: eventId, eventName: eventName },
            beforeSend: function () {
                _this._favoriteStarToProgressState(favoriteStar);
            },
            success: function (response) {
                if (response.Successed) {
                    _this._favoriteStarToogleState(favoriteStar, true);
                } else {
                    _this._favoriteStarToogleState(favoriteStar, false);
                }
                _this._popupAlert.ShowAll(response.AlertSections);
            },
            error: function () {
                _this._favoriteStarToogleState(favoriteStar, false);
                _this._popupAlert.Error(l8on.InternalServerError);
            }
        });
    };
    Events.prototype._favoriteStarToogleState = function (favoriteStar, isSuccessed) {
        var container = favoriteStar.closest('a');
        var isFavoriteEvent = parseBool(container.closest('.event').data('is-favorite-event'));
        if (isFavoriteEvent) {
            if (isSuccessed) {
                this._favoriteStarToNoneState(favoriteStar);
            } else {
                this._favoriteStarToActiveState(favoriteStar);
            }
        } else {
            if (isSuccessed) {
                this._favoriteStarToActiveState(favoriteStar);
            } else {
                this._favoriteStarToNoneState(favoriteStar);
            }
        }
    };
    Events.prototype._favoriteStarToProgressState = function (favoriteStar) {
        var container = favoriteStar.closest('a');
        favoriteStar.addClass('fa-spin');
        container.addClass('active');
        container.attr('data-original-title', '');
        container.attr('disabled', 'disabled');
        container.tooltip('hide');
    };
    Events.prototype._favoriteStarToActiveState = function (favoriteStar) {
        var container = favoriteStar.closest('a');
        favoriteStar.removeClass('fa-spin');
        container.attr('data-original-title', 'Убрать из избранного');
        container.removeAttr('disabled');
    };
    Events.prototype._favoriteStarToNoneState = function (favoriteStar) {
        var container = favoriteStar.closest('a');
        favoriteStar.removeClass('fa-spin');
        container.removeClass('active');
        container.attr('data-original-title', 'В избранное');
        container.removeAttr('disabled');
        if (this._isFavoriteView) {
            var $eventLi = favoriteStar.closest('li');
            $eventLi.slideUp(400, function () {
                $eventLi.remove();
            });
        }
    };
    return Events;
})();
$(function () {
    var events = new Events();
});
//# sourceMappingURL=Events.js.map
