﻿$(function() {
    $('#ServiceSectionsIds,#SupportLanguages').select2({
        placeholder: "Выберите разделы приложения"
    });


    $('input[data-hide-block]').change(function () {
        var block = '#'+$(this).attr('data-hide-block');
        if ($(block).hasClass('hide')) {
            $(block).removeClass('hide');
        }else
            $(block).addClass('hide');
    });
})