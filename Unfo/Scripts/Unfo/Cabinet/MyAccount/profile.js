﻿
function Profile() {

    var self = this,
        $updateProfileForm = $('#updateProfileForm'),
        urlToProfileController = $updateProfileForm.attr('action'),
        $spinnerOfLoad,
        $submitBtn = $updateProfileForm.find('input[type=submit]'),
        $inputs = $updateProfileForm.find('input:not([type=checkbox]):not([type=submit])'),
        popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView }),
        slideDownAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.SlideDownView }, '.page-alerts');

    this.Initialize = function () {
        $updateProfileForm.idealforms({
            rules: {
                'FirstName': 'required name',
                'SurName': 'required name',
                'FatherName': 'name',
                'PhoneNumber': 'min:6 max:20',
                'PlaceOfJob': 'min:1',
                'PositionAtJob': 'min:1',
                'AboutYourselfInformation': 'min:1',
                'WebSite': 'url',
                'TwitterLink': 'url',
                'GoogleLink': 'url',
                'FacebookLink': 'url',
                'VkontakteLink': 'url',
                'SkypeId': 'min:2'
            },
            adaptiveWidth: 545,
            onValidate: function (item, e, valid) {
                if (e === "ajax" && valid) {
                    toggleDisabledModeOfSubmitBtnsByInvalidsField(this);
                }
            },
            onSubmit: function (invalid, e, f) {
                e.preventDefault();
                if (invalid == 0) {

                    sendDataOfFormByAjaxToServer();
                }
            }
        });

        var $submitBtnWrapper = $submitBtn.parent();
        initializeSpinner($submitBtnWrapper);

        toggleDisabledModeOfSubmitBtnsByInvalidsField();
        $inputs.on('change keyup click', function () {
            toggleDisabledModeOfSubmitBtnsByInvalidsField();
        });
    };

    function initializeSpinner($containerOfspinner) {
        var spinnerOfLoad = new Spinner(optsOfBigGreenRoundCenteredSpinner).spin();
        $containerOfspinner.append(spinnerOfLoad.el);
        $spinnerOfLoad = $containerOfspinner.find('.spinner');
    };

    function showSpinnerOfLoad() {
        /// <summary>Плавно показывает анимированную иконку процесса загрузки и скрывает кнопку регистрации</summary>
        $submitBtn.fadeTo(350, 0, function () {
            $submitBtn.hide();
            $spinnerOfLoad.fadeTo(350, 1);
        });
    };
    function hideSpinnerOfLoad() {
        /// <summary>Плавно скрывает анимированную иконку процесса загрузки и показывает кнопку регистрации</summary>
        $spinnerOfLoad.fadeTo(350, 0, function () {
            $spinnerOfLoad.hide();
            $submitBtn.fadeTo(350, 1);
        });
    };
    function toggleDisabledModeOfSubmitBtnsByInvalidsField(idealForm) {
        /// <summary>Изменение режима доступности кнопки сабмита - если есть невалидные поля кнопка заблочена</summary>
        var invalidFields = 0;
        if (idealForm) {
            invalidFields = idealForm.GetInvalidFields();
        } else {
            idealForm = $updateProfileForm.data('idealforms');
            invalidFields = idealForm.GetInvalidFields();
        }

        if (invalidFields.length > 0) {
            $submitBtn.attr("disabled", "disabled");
        } else {
            $submitBtn.removeAttr("disabled");
        }
    };
    function sendDataOfFormByAjaxToServer() {
        $.ajax({
            type: "POST",
            url: urlToProfileController,
            dataType: "JSON",
            beforeSend: function (jqXHR, settings) {
                var signUpData = $updateProfileForm.serialize();
                if (signUpData.length > 0) {
                    settings.data = settings.data + "&" + signUpData;
                }
                showSpinnerOfLoad();
                popupAlert.HideAll();
            },
            success: function (response) {
                if (response.Successed) {
                    popupAlert.ShowAll(response.AlertSections);
                } else {
                    slideDownAlert.ShowAll(response.AlertSections);
                }
                hideSpinnerOfLoad();
            }
        });
    };
}

var Profile;
$(function () {
    Profile = new Profile();
    Profile.Initialize();
});