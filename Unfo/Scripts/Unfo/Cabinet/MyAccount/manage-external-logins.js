﻿
function ManageExternalLogins() {
    var self = this,
        $externalLoginBtns,
$externalLoginsWrappers,
        $unlinkBtns,
        spinner = new Spinner(optsOfBigPurpleRoundCenteredSpinner),
        $loadingSpinner,
        slideDownAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.SlideDownView }, '.page-alerts'),
        popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });

    function _initialization() {
        $externalLoginsWrappers = $('.social-icon-wrapper');
        $externalLoginBtns = $('.social-icon');
        $unlinkBtns = $externalLoginBtns.find('.unlink-btn');
        _addEvents();
        unfo.ClearAddressBar();
    }
    function _addEvents() {
        $externalLoginBtns.on('click', function () {
            var $externalLoginBtn = $(this);

            var isLinkedSocialProfile = toBool($externalLoginBtn.data('is-linked'));
            if (isLinkedSocialProfile) return false;

            _toLinkThisExternalLoginToAccount($externalLoginBtn);
        });
        $unlinkBtns.on('click', function (e) {
            var unlinkBtn = $(this),
                $externalLoginBtn = unlinkBtn.parent();
            _removeExternalLogin($externalLoginBtn);
            return false;
        });
        _unlinkBtnShowEvents();
    }

    var urlOfToLinkExternalLoginAction = $('#urlOfToLinkExternalLoginAction').val();
    function _toLinkThisExternalLoginToAccount($externalLoginBtn) {
        var dataToServer = _constructDataForToLinkExternalLogin($externalLoginBtn);
        if (dataToServer) {
            _showSpinner($externalLoginBtn);
            straightPostRequest(urlOfToLinkExternalLoginAction, dataToServer);
        }
    }
    function _constructDataForToLinkExternalLogin($externalLoginBtn) {
        var externalLoginProvider = $externalLoginBtn.data('value');
        var externalLoginArgName = $('#argOfToLinkExternalLoginAction').val();
        var dataToServer = {};
        dataToServer[externalLoginArgName] = externalLoginProvider;
        var requestToken = $('[name="__RequestVerificationToken"]').val();
        dataToServer["__RequestVerificationToken"] = requestToken;
        return dataToServer;
    }

    var urlOfRemoveExternalLoginAction = $("#urlOfRemoveExternalLoginAction").val();
    function _removeExternalLogin($socialBtn) {
        var externalLoginProvider = $socialBtn.data('value');
        var dataToServer = {},
            argNameOfExternalLogin = $("#argOfRemoveExternalLoginAction").val();
        dataToServer[argNameOfExternalLogin] = externalLoginProvider;

        $.ajax({
            type: "POST",
            url: urlOfRemoveExternalLoginAction,
            data: dataToServer,
            dataType: "json",
            beforeSend: function () {
                _showSpinner($socialBtn);
                popupAlert.HideAll();
            },
            success: function (response) {
                if (response.Successed) {
                    popupAlert.ShowAll(response.AlertSections);
                    _deactivateSocialBtn($socialBtn);
                    $socialBtn.data('is-linked', false);
                } else {
                    slideDownAlert.ShowAll(response.AlertSections);
                }
           
                _hideSpinner($socialBtn);
            }
        });
    }

    var socialColors = {
        'vkontakte': '#587a9f',
        'googleplus': '#db4531',
        'twitter': '#55acee',
        'facebook': '#3c599f'
    };
    function _generateOptionsBySocialColor($socialBtn) {
        var currentSocialName = $socialBtn.data('value').toLowerCase();
        var optionsWithNewColor = optsOfBigPurpleRoundCenteredSpinner;
        $.each(socialColors, function (socialName, socialColor) {
            if (currentSocialName === socialName) {
                optionsWithNewColor = $.extend({}, optsOfBigRoundCenteredSpinner, { color: socialColor });
            } 
        });
        return optionsWithNewColor;
    };
    function _showSpinner($hiddingSocialBtn) {
        var $socialWrapper = $hiddingSocialBtn.parent();
        spinner = new Spinner(_generateOptionsBySocialColor($hiddingSocialBtn));
        spinner.spin($socialWrapper);
        $loadingSpinner = $('.spinner', $socialWrapper);
        //Как бы утверждаем чтоб анимация затухания скрываемой социальной кнопки
        //произошла только тогда когда отработает анимация спиннера
        $loadingSpinner.promise().then(function () {
            $hiddingSocialBtn.fadeOut(350, function () {
                $loadingSpinner.fadeIn(350);
            });
        });
    }
    function _hideSpinner($showingSocialBtn) {
        $loadingSpinner.fadeOut(350, function () {
            $showingSocialBtn.fadeIn(350);
            spinner.stop();
        });
    }
    function _deactivateSocialBtn($socialBtn) {
        var $socialWrapper = $socialBtn.parent(),
            $unlinkBtn = $socialBtn.find('.unlink-btn:first-child');
        $unlinkBtn.removeClass('active');
        $socialWrapper.removeClass('active');
    }
    function _unlinkBtnShowEvents() {
        $unlinkBtns.on('mouseenter', function () {
            _unlinkFadeTo($(this), 300, 1);
        });
        $unlinkBtns.on('mouseleave', function () {
            _unlinkFadeTo($(this), 300, 0.5);
        });
        $externalLoginsWrappers.on('mouseenter', function () {
            var $childrenUnlinkBtn = $(this).find('.unlink-btn');
            _unlinkFadeTo($childrenUnlinkBtn, 300, 0.5, null, function () {
                $childrenUnlinkBtn.show();
            });
        });
        $externalLoginsWrappers.on('mouseleave', function () {
            var $childrenUnlinkBtn = $(this).find('.unlink-btn');
            _unlinkFadeTo($childrenUnlinkBtn, 300, 0, function () {
                $childrenUnlinkBtn.hide();
            });
        });
        function _unlinkFadeTo($unlinkBtn, duration, opacity, afterFade, beforeFade) {
            if ($unlinkBtn.hasClass('active')) {
                if (beforeFade) beforeFade.apply(this);
                $unlinkBtn.fadeTo(duration, opacity, afterFade);
            }
        };
    }
    _initialization();
}

var manageExternalLogins;
$(function () {
    manageExternalLogins = new ManageExternalLogins();
});