﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />

class UserPrivacyPolitics {
    private $privacyPoliticsCheckboxs: JQuery;
    private $urlToAction: string;
    private $mainContainer: JQuery;
    private requestsToServer: IRequests = {};
    private userId: string;
    private popupAlert: AlertEngine;

    constructor() {
        this.popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });
        this.$mainContainer = $('.user-privacy-politics');
        this.$privacyPoliticsCheckboxs = this.$mainContainer.find('[type="checkbox"]');
        this.$urlToAction = this.$mainContainer.data('action');
        this.userId = this.$mainContainer.find('#UserID').val();
        this._addEvents();
        this._initializeBootstrapSwitch();
    }
    private _addEvents(): void {
        this.$privacyPoliticsCheckboxs.on('switchChange.bootstrapSwitch', (e:Event, checked:boolean) => {
            var privacyPoliticCheckBox = <HTMLInputElement>e.target;
            var nameCheckBox = privacyPoliticCheckBox.name;
            this._abortPreviousRequest(nameCheckBox);
            this.requestsToServer[nameCheckBox] = this._setNewPrivacyPoliticState(privacyPoliticCheckBox);
        });
    }
    private _setNewPrivacyPoliticState(privacyPoliticCheckBox: HTMLInputElement): JQueryXHR {
        return $.ajax({
            type: 'POST',
            url: this.$urlToAction,
            data: this._generateNewPrivacyPoliticData(privacyPoliticCheckBox),
            beforeSend: ()=> {
                this.popupAlert.HideAll();
            },
            success: (response: IServerResponse) => {
                if (!response.Successed) {
                    privacyPoliticCheckBox.checked = !privacyPoliticCheckBox.checked;
                }
                this.popupAlert.ShowAll(response.AlertSections);
            },
            error: (e:any)=> {
                if (e.statusText != "abort") {
                    this.popupAlert.Post({ TypeAlert: TypeOfAlert.Error, Message: l8on.InternalServerError});
                    this.popupAlert.Show();
                }
            }            
        });
    }

    private _generateNewPrivacyPoliticData(privacyPoliticCheckBox: HTMLInputElement): Object {
        var namePrivacyPolitic = privacyPoliticCheckBox.name;
        var userPrivacyPoliticID = this.$mainContainer.find('[name="' + namePrivacyPolitic + '"][type="hidden"]').val();
        var isChecked = privacyPoliticCheckBox.checked;
        return {
            ID: userPrivacyPoliticID,
            UserId: this.userId,
            IsPublic: isChecked,
            TypePrivacyPolitic: namePrivacyPolitic
        };
    }
    private _abortPreviousRequest(nameCheckBox: string) :void{
        if (this.requestsToServer[nameCheckBox]) {
            this.requestsToServer[nameCheckBox].abort();
        }
    }
    private _initializeBootstrapSwitch(): void {
        $("[data-switch]").bootstrapSwitch({
            onText: "YES",
            offText: "NO"
        });
    }
}

$(()=> {
    var userPrivacyPolitics = new UserPrivacyPolitics();
});