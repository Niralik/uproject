﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />
var UserPrivacyPolitics = (function () {
    function UserPrivacyPolitics() {
        this.requestsToServer = {};
        this.popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this.$mainContainer = $('.user-privacy-politics');
        this.$privacyPoliticsCheckboxs = this.$mainContainer.find('[type="checkbox"]');
        this.$urlToAction = this.$mainContainer.data('action');
        this.userId = this.$mainContainer.find('#UserID').val();
        this._addEvents();
        this._initializeBootstrapSwitch();
    }
    UserPrivacyPolitics.prototype._addEvents = function () {
        var _this = this;
        this.$privacyPoliticsCheckboxs.on('switchChange.bootstrapSwitch', function (e, checked) {
            var privacyPoliticCheckBox = e.target;
            var nameCheckBox = privacyPoliticCheckBox.name;
            _this._abortPreviousRequest(nameCheckBox);
            _this.requestsToServer[nameCheckBox] = _this._setNewPrivacyPoliticState(privacyPoliticCheckBox);
        });
    };
    UserPrivacyPolitics.prototype._setNewPrivacyPoliticState = function (privacyPoliticCheckBox) {
        var _this = this;
        return $.ajax({
            type: 'POST',
            url: this.$urlToAction,
            data: this._generateNewPrivacyPoliticData(privacyPoliticCheckBox),
            beforeSend: function () {
                _this.popupAlert.HideAll();
            },
            success: function (response) {
                if (!response.Successed) {
                    privacyPoliticCheckBox.checked = !privacyPoliticCheckBox.checked;
                }
                _this.popupAlert.ShowAll(response.AlertSections);
            },
            error: function (e) {
                if (e.statusText != "abort") {
                    _this.popupAlert.Post({ TypeAlert: 3 /* Error */, Message: l8on.InternalServerError });
                    _this.popupAlert.Show();
                }
            }
        });
    };

    UserPrivacyPolitics.prototype._generateNewPrivacyPoliticData = function (privacyPoliticCheckBox) {
        var namePrivacyPolitic = privacyPoliticCheckBox.name;
        var userPrivacyPoliticID = this.$mainContainer.find('[name="' + namePrivacyPolitic + '"][type="hidden"]').val();
        var isChecked = privacyPoliticCheckBox.checked;
        return {
            ID: userPrivacyPoliticID,
            UserId: this.userId,
            IsPublic: isChecked,
            TypePrivacyPolitic: namePrivacyPolitic
        };
    };
    UserPrivacyPolitics.prototype._abortPreviousRequest = function (nameCheckBox) {
        if (this.requestsToServer[nameCheckBox]) {
            this.requestsToServer[nameCheckBox].abort();
        }
    };
    UserPrivacyPolitics.prototype._initializeBootstrapSwitch = function () {
        $("[data-switch]").bootstrapSwitch({
            onText: "YES",
            offText: "NO"
        });
    };
    return UserPrivacyPolitics;
})();

$(function () {
    var userPrivacyPolitics = new UserPrivacyPolitics();
});
//# sourceMappingURL=user-privacy-politics.js.map
