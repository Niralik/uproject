﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />
var RequestState;
(function (RequestState) {
    RequestState[RequestState["NoState"] = 0] = "NoState";
    RequestState[RequestState["Completed"] = 1] = "Completed";
    RequestState[RequestState["Aborted"] = 2] = "Aborted";
    RequestState[RequestState["InProgress"] = 3] = "InProgress";
})(RequestState || (RequestState = {}));
var RequestType;
(function (RequestType) {
    RequestType[RequestType["GetOrganizer"] = 0] = "GetOrganizer";
})(RequestType || (RequestType = {}));

var UserRequests = (function () {
    function UserRequests() {
        this._spinner = new Spinner(SpinnerOptions.SmallRoundLefterGreen);
        this._durationAnimation = 400;
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this._$requestContainer = $('.request');
        this._$btnShowRequestDescription = $('.btn-show-request', this._$requestContainer);

        this._$requestDescriptionContainer = $('.request-description-container');
        this._$requestDescription = this._$requestDescriptionContainer.find('textarea');

        this._$responseDescriptionContainer = $('.response-description-container');
        this._$responseDescription = this._$responseDescriptionContainer.children();

        this._$btnSendRequestWrapper = $('.btn-wrapper', this._$requestContainer);
        this._$btnSendRequest = $('.btn-send-request', this._$btnSendRequestWrapper);

        this._$requestStateContainer = $('.request-state');

        this._$sectionRequestOfOrganizer = $('.request-for-organizer');
        this._urlToNewRequestAction = this._$sectionRequestOfOrganizer.data('action');

        this._$btnShowResponseDescription = this._$sectionRequestOfOrganizer.find('.btn-show-response-description');

        this._requestState = parseInt($('#requestState').val());
        this._requestType = parseInt($('#requestType').val());

        this._intializeViewLogicByRequestState();
    }
    UserRequests.prototype._intializeViewLogicByRequestState = function () {
        switch (this._requestState) {
            case 0 /* NoState */:
                this._showDescriptionAndSendBtnByClickShowRequestBtn();
                this._handleRequestDescriptionTextareaEvents();
                this._sendNewRequestToServerByClickSendBtn();
                break;
            case 1 /* Completed */:
                break;
            case 2 /* Aborted */:
                this._showDescriptionAndSendBtnByClickShowRequestBtn();
                this._handleClicksOnBtnShowResponseDescription();
                this._handleRequestDescriptionTextareaEvents();
                this._sendNewRequestToServerByClickSendBtn();
                break;
            case 3 /* InProgress */:
                break;
            default:
        }
    };
    UserRequests.prototype._handleClicksOnBtnShowResponseDescription = function () {
        var _this = this;
        this._$btnShowResponseDescription.on('click', function () {
            _this._$responseDescriptionContainer.slideToggle(_this._durationAnimation);
        });
    };
    UserRequests.prototype._showDescriptionAndSendBtnByClickShowRequestBtn = function () {
        var _this = this;
        this._$btnShowRequestDescription.on('click', function () {
            _this._$btnShowRequestDescription.fadeOut(_this._durationAnimation, function () {
                _this._$requestDescriptionContainer.fadeIn(_this._durationAnimation);
                _this._$btnSendRequestWrapper.fadeIn(_this._durationAnimation);
            });
        });
    };

    UserRequests.prototype._handleRequestDescriptionTextareaEvents = function () {
        var _this = this;
        this._activateSendBtnByFullnessTextarea();
        this._$requestDescription.on('keyup', function () {
            _this._activateSendBtnByFullnessTextarea();
        });
    };
    UserRequests.prototype._activateSendBtnByFullnessTextarea = function () {
        if (Utilites.IsNotHasWhiteSpaceOrNull(this._$requestDescription.val())) {
            this._$btnSendRequest.removeClass('disabled');
        } else {
            this._$btnSendRequest.addClass('disabled');
        }
    };

    UserRequests.prototype._sendNewRequestToServerByClickSendBtn = function () {
        var _this = this;
        this._$btnSendRequest.on('click', function () {
            if (Utilites.HasWhiteSpaceOrNull(_this._$requestDescription.val()))
                return;
            $.ajax({
                type: 'POST',
                url: _this._urlToNewRequestAction,
                data: _this._assembleDataForNewRequest(),
                beforeSend: function () {
                    _this._showSpinnerHideSendBtn();
                },
                success: function (response) {
                    if (response.Successed) {
                        _this._changeRequestStateToInProgress();
                    } else {
                        _this._showSendBtnHideSpinner();
                    }
                    _this._popupAlert.ShowAll(response.AlertSections);
                },
                error: function () {
                    _this._popupAlert.Post({ TypeAlert: 3 /* Error */, Message: l8on.InternalServerError });
                }
            });
        });
    };
    UserRequests.prototype._changeRequestStateToInProgress = function () {
        var _this = this;
        this._$requestStateContainer.fadeIn(this._durationAnimation);
        var $abortLabel = this._$requestStateContainer.find('.label-danger');
        $abortLabel.fadeOut(this._durationAnimation);
        $abortLabel.promise().then(function () {
            _this._$requestStateContainer.find('.label-info').fadeIn(_this._durationAnimation);
        });
        this._$btnShowResponseDescription.fadeOut(this._durationAnimation);
        this._$responseDescriptionContainer.fadeOut(this._durationAnimation);
        this._$requestContainer.fadeOut(this._durationAnimation);
    };
    UserRequests.prototype._assembleDataForNewRequest = function () {
        var newRequestData = {
            RequestType: 0 /* GetOrganizer */,
            RequestDescription: this._$requestDescription.val()
        };
        return newRequestData;
    };

    UserRequests.prototype._showSpinnerHideSendBtn = function () {
        var _this = this;
        this._spinner.spin(this._$btnSendRequestWrapper);
        this._$processingSpinner = $('.spinner', this._$btnSendRequestWrapper);
        this._$processingSpinner.promise().then(function () {
            _this._$btnSendRequest.promise().then(function () {
                _this._$btnSendRequest.fadeTo(_this._durationAnimation, 0, function () {
                    _this._$btnSendRequest.hide();
                    _this._$processingSpinner.fadeIn(_this._durationAnimation);
                });
            });
        });
    };
    UserRequests.prototype._showSendBtnHideSpinner = function () {
        var _this = this;
        this._$processingSpinner.fadeOut(this._durationAnimation, function () {
            _this._spinner.stop();

            _this._$btnSendRequest.fadeTo(_this._durationAnimation, 1, function () {
                _this._$btnSendRequest.show();
            });
        });
    };
    return UserRequests;
})();

$(function () {
    var userRequests = new UserRequests();
});
//# sourceMappingURL=user-requests.js.map
