﻿function ManagePassword() {
    var self = this,
        spinner = new Spinner(optsOfBigGreenRoundCenteredSpinner),
        $loadingSpinner,
        popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView }),
          slideDownAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.SlideDownView }, '.page-alerts'),
        $managePasswordForm = $('#managePasswordForm'),
        idealform,
        $submitBtn = $managePasswordForm.find('[type="submit"]'),
        itsChangePassword = _checkItsChangePasswordBehavior(),
        itsAddPassword = !itsChangePassword;

    function _initialization() {
    
        $managePasswordForm.idealforms({
            rules: _constructValidationRules(),
            adaptiveWidth: 545,
            onValidate: function (item, e, valid) {

            },
            onSubmit: function (invalid, e, f) {
                if (invalid) return false;
                _sendPasswordToServer();
            }
        });
        idealform = $managePasswordForm.data('idealforms');
    }
    function _constructValidationRules() {
        var validationRules = {
            'Password': 'required weakpass',
            'ConfirmPassword': 'required equalto:Password',
        };
        if (itsChangePassword) {
            validationRules['OldPassword'] = 'required';
        }
        return validationRules;
    }
    function _checkItsChangePasswordBehavior() {
        var oldPasswordInput = $('#OldPassword')[0];
        if (oldPasswordInput) {
            return true;
        }
        return false;
    }
    function _sendPasswordToServer() {
        var urlToManagePasswordAction = $managePasswordForm.data("action");
        $.ajax({
            type: "POST",
            url: urlToManagePasswordAction,
            dataType: "JSON",
            beforeSend: function (jqXHR, settings) {
                var formData = $managePasswordForm.serialize();
                if (formData.length > 0) {
                    settings.data = settings.data + "&" + formData;
                }
                _showSpinner($submitBtn);
                popupAlert.HideAll();
            },
            error: function () {
                popupAlert.Post({ TypeAlert: TypeOfAlert.Error, Message: Idealforms.LocalizedErrors.internalServerError});
                popupAlert.Show();
                _hideSpinner($submitBtn);
            },
            success: function (response) {
              
                _hideSpinner($submitBtn);

                if (response.Successed) {
                    popupAlert.ShowAll(response.AlertSections);
                    $managePasswordForm[0].reset();
                    idealform.Reset();
                    if (itsAddPassword) {
                        setTimeout(function () {
                            window.location.replace('');
                        }, 100);
                    }
                } else {
                    slideDownAlert.ShowAll(response.AlertSections);
                }

            }
        });
    }
    function _showSpinner($hiddingSocialBtn) {
        var $socialWrapper = $hiddingSocialBtn.parent();
        spinner.spin($socialWrapper);
        $loadingSpinner = $('.spinner', $socialWrapper);
        $hiddingSocialBtn.fadeOut(350, function () {
            $loadingSpinner.fadeIn(350);
        });
    }
    function _hideSpinner($showingSocialBtn) {
        $loadingSpinner.fadeOut(350, function () {
            $showingSocialBtn.fadeIn(350);
            spinner.stop();
        });
    }

    _initialization();
}
$(function () {
    var mp = new ManagePassword();
});