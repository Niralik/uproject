﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />
declare enum RequestState {
    NoState = 0,
    Completed = 1,
    Aborted = 2,
    InProgress = 3,
}
declare enum RequestType {
    GetOrganizer = 0,
}
interface INewRequest {
    RequestType: RequestType;
    RequestDescription: string;
}
declare class UserRequests {
    private _popupAlert;
    private _spinner;
    private _$sectionRequestOfOrganizer;
    private _$btnShowRequestDescription;
    private _$requestDescriptionContainer;
    private _$requestDescription;
    private _$btnSendRequestWrapper;
    private _$btnSendRequest;
    private _$responseDescriptionContainer;
    private _$responseDescription;
    private _$requestStateContainer;
    private _$requestContainer;
    private _$btnShowResponseDescription;
    private _requestState;
    private _requestType;
    private _urlToNewRequestAction;
    private _durationAnimation;
    constructor();
    private _intializeViewLogicByRequestState();
    private _handleClicksOnBtnShowResponseDescription();
    private _showDescriptionAndSendBtnByClickShowRequestBtn();
    private _handleRequestDescriptionTextareaEvents();
    private _activateSendBtnByFullnessTextarea();
    private _sendNewRequestToServerByClickSendBtn();
    private _changeRequestStateToInProgress();
    private _assembleDataForNewRequest();
    private _$processingSpinner;
    private _showSpinnerHideSendBtn();
    private _showSendBtnHideSpinner();
}
