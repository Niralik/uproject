﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />
enum RequestState {
    NoState,
    Completed,
    Aborted,
    InProgress
}
enum RequestType {
    GetOrganizer
}
interface INewRequest {
    RequestType: RequestType;
    RequestDescription: string;
}
class UserRequests {

    private _popupAlert: AlertEngine;
    private _spinner = new Spinner(SpinnerOptions.SmallRoundLefterGreen);

    private _$sectionRequestOfOrganizer:JQuery;
    private _$btnShowRequestDescription: JQuery;

    private _$requestDescriptionContainer: JQuery;
    private _$requestDescription: JQuery;

    private _$btnSendRequestWrapper:JQuery;
    private _$btnSendRequest: JQuery;

    private _$responseDescriptionContainer: JQuery;
    private _$responseDescription: JQuery;

    private _$requestStateContainer: JQuery;
    private _$requestContainer:JQuery;

    private _$btnShowResponseDescription: JQuery;

    private _requestState: RequestState;
    private _requestType: RequestType;

    private _urlToNewRequestAction: string;

    private _durationAnimation = 400;
    
    constructor() {
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });
        this._$requestContainer = $('.request');
        this._$btnShowRequestDescription = $('.btn-show-request', this._$requestContainer);

        this._$requestDescriptionContainer = $('.request-description-container');
        this._$requestDescription = this._$requestDescriptionContainer.find('textarea');

        this._$responseDescriptionContainer = $('.response-description-container');
        this._$responseDescription = this._$responseDescriptionContainer.children();

        this._$btnSendRequestWrapper = $('.btn-wrapper', this._$requestContainer);
        this._$btnSendRequest = $('.btn-send-request', this._$btnSendRequestWrapper);

        this._$requestStateContainer = $('.request-state');

        this._$sectionRequestOfOrganizer = $('.request-for-organizer');
        this._urlToNewRequestAction = this._$sectionRequestOfOrganizer.data('action');

        this._$btnShowResponseDescription = this._$sectionRequestOfOrganizer.find('.btn-show-response-description');

        this._requestState = parseInt($('#requestState').val());
        this._requestType = parseInt($('#requestType').val());

        this._intializeViewLogicByRequestState();
    }
   private _intializeViewLogicByRequestState() {
       switch (this._requestState) {
           case RequestState.NoState:
               this._showDescriptionAndSendBtnByClickShowRequestBtn();
               this._handleRequestDescriptionTextareaEvents();
               this._sendNewRequestToServerByClickSendBtn();
               break;
           case RequestState.Completed:
               break;
           case RequestState.Aborted:
               this._showDescriptionAndSendBtnByClickShowRequestBtn();
               this._handleClicksOnBtnShowResponseDescription();
               this._handleRequestDescriptionTextareaEvents();
               this._sendNewRequestToServerByClickSendBtn();
               break;
           case RequestState.InProgress:
               break;
       default:
       }
   }
    private _handleClicksOnBtnShowResponseDescription() {
        this._$btnShowResponseDescription.on('click', () => {
          this._$responseDescriptionContainer.slideToggle(this._durationAnimation);
        });
       
    }
    private _showDescriptionAndSendBtnByClickShowRequestBtn() {
        this._$btnShowRequestDescription.on('click', () => {
            this._$btnShowRequestDescription.fadeOut(this._durationAnimation, () => {
                this._$requestDescriptionContainer.fadeIn(this._durationAnimation);
                this._$btnSendRequestWrapper.fadeIn(this._durationAnimation);
            });
        });
    }

    private _handleRequestDescriptionTextareaEvents() {
        this._activateSendBtnByFullnessTextarea();
        this._$requestDescription.on('keyup', ()=> {
            this._activateSendBtnByFullnessTextarea();
        });    
    }
    private _activateSendBtnByFullnessTextarea() {
        if (Utilites.IsNotHasWhiteSpaceOrNull(this._$requestDescription.val())) {
            this._$btnSendRequest.removeClass('disabled');
        } else {
            this._$btnSendRequest.addClass('disabled');
        }
    }
     
    private _sendNewRequestToServerByClickSendBtn() {
        this._$btnSendRequest.on('click', ()=> {
            if (Utilites.HasWhiteSpaceOrNull(this._$requestDescription.val())) return;
            $.ajax({
                type: 'POST',
                url: this._urlToNewRequestAction,
                data: this._assembleDataForNewRequest(),
                beforeSend: ()=> {
                    this._showSpinnerHideSendBtn();
                },
                success: (response: IServerResponse) => {
                    if (response.Successed) {
                        this._changeRequestStateToInProgress();
                    } else {
                        this._showSendBtnHideSpinner();
                    }
                    this._popupAlert.ShowAll(response.AlertSections);
                },
                error: () => {
                    this._popupAlert.Post({ TypeAlert: TypeOfAlert.Error, Message: l8on.InternalServerError });
                },
        });
        });
    }
    private _changeRequestStateToInProgress() {
        this._$requestStateContainer.fadeIn(this._durationAnimation);
        var $abortLabel = this._$requestStateContainer.find('.label-danger');
        $abortLabel.fadeOut(this._durationAnimation);
        $abortLabel.promise().then(() => {
            this._$requestStateContainer.find('.label-info').fadeIn(this._durationAnimation);
        });
        this._$btnShowResponseDescription.fadeOut(this._durationAnimation);
        this._$responseDescriptionContainer.fadeOut(this._durationAnimation);
        this._$requestContainer.fadeOut(this._durationAnimation);
    }
    private _assembleDataForNewRequest() {
        var newRequestData: INewRequest = {
            RequestType: RequestType.GetOrganizer,
            RequestDescription: this._$requestDescription.val()
        };
        return newRequestData;
    }
    private _$processingSpinner:JQuery;
    private _showSpinnerHideSendBtn() {
        this._spinner.spin(this._$btnSendRequestWrapper);
        this._$processingSpinner = $('.spinner', this._$btnSendRequestWrapper);
        this._$processingSpinner.promise().then(()=> {
            this._$btnSendRequest.promise().then(()=> {
                this._$btnSendRequest.fadeTo(this._durationAnimation, 0, ()=> {
                    this._$btnSendRequest.hide();
                    this._$processingSpinner.fadeIn(this._durationAnimation);
                });
            });
        });
    }
    private _showSendBtnHideSpinner() {
        this._$processingSpinner.fadeOut(this._durationAnimation, ()=> {
            this._spinner.stop();

            this._$btnSendRequest.fadeTo(this._durationAnimation, 1, ()=> {
                this._$btnSendRequest.show();
            });
        });
    }
}

$(()=> {
    var userRequests = new UserRequests();
});