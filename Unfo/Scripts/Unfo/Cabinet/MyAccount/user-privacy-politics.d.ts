﻿/// <reference path="../../../typings/bootstrap/bootstrap.d.ts" />
declare class UserPrivacyPolitics {
    private $privacyPoliticsCheckboxs;
    private $urlToAction;
    private $mainContainer;
    private requestsToServer;
    private userId;
    private popupAlert;
    constructor();
    private _addEvents();
    private _setNewPrivacyPoliticState(privacyPoliticCheckBox);
    private _generateNewPrivacyPoliticData(privacyPoliticCheckBox);
    private _abortPreviousRequest(nameCheckBox);
    private _initializeBootstrapSwitch();
}
