﻿$(function () {

    $('#addReport').click(function (e) {
        e.preventDefault();
        var queryForm = {};
        var hasParams = false;
        if ($('#programmFilter #Date').val() != '') {
            queryForm.date = $('#programmFilter #Date').val();
            hasParams = true;
        }
        if ($('#programmFilter #Section').val() != '') {
            queryForm.section = $('#programmFilter #Section').val();
            hasParams = true;
        }
        if ($('#programmFilter #Hall').val() != '') {
            queryForm.hall = $('#programmFilter #Hall').val();
            hasParams = true;
        }
        if ($('#programmFilter #Direction').val() != '') {
            queryForm.direction = $('#programmFilter #Direction').val();
            hasParams = true;
        }

        var path = $(this).attr('href');
        if (hasParams)
            path += '?' + Serialize(queryForm);

        window.location = path;

    });
});