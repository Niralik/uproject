﻿
var opts = {
    lines: 11, // The number of lines to draw
    length: 0, // The length of each line
    width: 11, // The line thickness
    radius: 19, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 54, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#000', // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 28, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9 // The z-index (defaults to 2000000000)
};
var spinner = new Spinner(opts).spin();

$(function () {
    refreshInputs();

    $('#addSpeaker').click(function () {

        $.get('/Cabinet/Event/' + $('#eventName').val() + '/Programm/SpeakerItem', { hasSubreport: $('#WithSubReports').val() }, function (data) {

            $('#speakersList').append(data);
            initDatePicker();
            refreshInputs();
        });
    });
    $('.addSection').click(function () {
        getModal('EditSection');
    });

    $('.addHall').click(function () {
        getModal('EditHall');
    });
    $(document).on('click', '.addSpeaker', function (e) {
        e.preventDefault();
        getModal('EditSpeaker');
    })


    $('.addDirection').click(function () {
        getModal('EditDirection');
    });
    $(document).on('click', '.deleteItem', function (e) {
        e.preventDefault();
        $(this).closest('.speaker-item').remove();
    });
    $('#WithSubReports').change(function () {
        refreshInputs();
    });
});


function refreshInputs() {
    if ($('#WithSubReports').is(":checked")) {
        $('.speaker-report').show();
    } else {
        $('.speaker-report').hide();
    }
}
function afterModalSubmit(type) {
    $.get('/Cabinet/Event/' + $('#eventName').val() + '/Resources/GetList?type=' + type, function (data) {

        var t = $('.select-' + type);
        if (t.length > 1) {
            $.each(t, function () {
                insertToSelect(data.Response, 'Выберите', this);
            });
        }
        else
            insertToSelect(data.Response, 'Выберите', t);
    });
}
