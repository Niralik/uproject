﻿enum FilterRequestState {
    None,
    Completed,
    Aborted,
    InProgress
}
enum TypeRequestAction {
    Approve,
    Abort
}
class UsersRequests {
    private _filterRequestState: FilterRequestState = parseInt($('#filterRequestState').val());
    private _popupAlert: AlertEngine = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });
    private _requestActionSpinners: Spinner[] = [];
    constructor() {
        this._initializationReadmoreBtns();
        this._initializationSpinners();
        this._initializeApproveRequestLogic();
        this._initializeAbortRequestLogic();
    }
    private _initializationSpinners() {
        var $usersRequestsActionsContainers = $('.request-actions');
        $.each($usersRequestsActionsContainers, (index, container: any) => {
            var spinner = new Spinner(SpinnerOptions.BigRoundCenteredGreen, $(container));
            this._requestActionSpinners.push(spinner);
        });
    }
    private _initializeApproveRequestLogic() {
        var $approveBtns = $('.users-requests .request-actions #approveBtn');
      
        $approveBtns.click((event: JQueryEventObject) => {
            var $userRequest = $(event.target).closest('.user-request');
            var approveUrl = $userRequest.find('#approveUrl').val();
            this._doAjaxApproveAbortRequestByUrl(approveUrl, $userRequest, TypeRequestAction.Approve);
        });
       
    }
    private _initializeAbortRequestLogic() {
        var $abortBtns = $('.users-requests .request-actions #abortBtn');
        $abortBtns.click((event: JQueryEventObject) => {
            var $userRequest = $(event.target).closest('.user-request');
            var abortUrl = $userRequest.find('#abortUrl').val();
            this._doAjaxApproveAbortRequestByUrl(abortUrl, $userRequest, TypeRequestAction.Abort);
        });
    }
    private _doAjaxApproveAbortRequestByUrl(urlToAction: string, $userRequest: JQuery, typeRequestAction:TypeRequestAction) {
        var $containerOfSpinner = $userRequest.find('.request-actions');
        var currentSpinner:Spinner = this._requestActionSpinners.filter((spinner:Spinner, index) => {
            return spinner.Container[0] == $containerOfSpinner[0];
        })[0];
        currentSpinner.opts = typeRequestAction == TypeRequestAction.Approve ? SpinnerOptions.BigRoundCenteredGreenForUsersRequests : SpinnerOptions.BigRoundCenteredRedForUsersRequests;
        $.ajax({
            type: 'POST',
            url: urlToAction,
            beforeSend: () => {
                currentSpinner.showSpinnerHideInsides();
            },
            success: (response: IServerResponse) => {
                if (response.Successed) {
                    this._fadeUserRequestDivByFilterRequestState($userRequest);
                    this._changeRequestStateLabel($userRequest, typeRequestAction);
                } 
                this._popupAlert.ShowAll(response.AlertSections);
                currentSpinner.hideSpinnerShowInsides();
            },
            error:() => {
                this._popupAlert.Error(l8on.InternalServerError);
            }

        });
    }
    private _changeRequestStateLabel($userRequest, typeRequestAction: TypeRequestAction) {
        var $label: JQuery = $userRequest.find('.label');
        var $actionsContainer: JQuery = $userRequest.find('.request-actions');
        $label.removeClass('label-info label-success label-danger');
        switch (typeRequestAction) {
            case TypeRequestAction.Approve:
                    $label.addClass('label-success');
                    $label.text(l8on.Cabinet.UsersRequests.Approved);
                    $actionsContainer.addClass('one-child');
                $actionsContainer.find('#approveBtn').hide();
                $actionsContainer.find('#abortBtn').removeClass('hide').show();
                break;
            case TypeRequestAction.Abort:
                $label.addClass('label-danger');
                $label.text(l8on.Cabinet.UsersRequests.Aborted);
                $actionsContainer.addClass('one-child');
                $actionsContainer.find('#abortBtn').hide();
                $actionsContainer.find('#approveBtn').removeClass('hide').show();
            break;
        }
    }
    private _fadeUserRequestDivByFilterRequestState($userRequest: JQuery) {
        if (this._filterRequestState != FilterRequestState.None) {
            this._hideUserRequest($userRequest);
        }
    }
    private _hideUserRequest($userRequest: JQuery): void {
        var $liUserRequest = $userRequest.closest('li');
        $liUserRequest.slideUp(400, () => {
            $liUserRequest.remove();
            if ($('.users-requests li').length == 0) {
                window.location.replace('');
            }
        });
    }
    private _initializationReadmoreBtns() {
        var requestDescsForReadmore = this._getDescsWithLargeText();
        requestDescsForReadmore.readmore({
            maxHeight: 32,
            speed: 300,
            moreLink: '<a href="#">' + l8on.Common.MoreInfo+'</a>',
            lessLink: '<a href="#">' + l8on.Common.Hide +'</a>',
        });
    }
    private _getDescsWithLargeText(): JQuery {
        var $userRequestDesc = $('.user-request .desc');
        var $descsWithLargeTest = $userRequestDesc.filter((index, desc) => {
            var text = $(desc).find('span');
            return text.height() > $(desc).height();
        });
        return $descsWithLargeTest;
    }

    
}
$(() => {
    var usersRequests = new UsersRequests();
})