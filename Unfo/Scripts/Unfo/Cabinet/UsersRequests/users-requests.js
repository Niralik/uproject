﻿var FilterRequestState;
(function (FilterRequestState) {
    FilterRequestState[FilterRequestState["None"] = 0] = "None";
    FilterRequestState[FilterRequestState["Completed"] = 1] = "Completed";
    FilterRequestState[FilterRequestState["Aborted"] = 2] = "Aborted";
    FilterRequestState[FilterRequestState["InProgress"] = 3] = "InProgress";
})(FilterRequestState || (FilterRequestState = {}));
var TypeRequestAction;
(function (TypeRequestAction) {
    TypeRequestAction[TypeRequestAction["Approve"] = 0] = "Approve";
    TypeRequestAction[TypeRequestAction["Abort"] = 1] = "Abort";
})(TypeRequestAction || (TypeRequestAction = {}));
var UsersRequests = (function () {
    function UsersRequests() {
        this._filterRequestState = parseInt($('#filterRequestState').val());
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this._requestActionSpinners = [];
        this._initializationReadmoreBtns();
        this._initializationSpinners();
        this._initializeApproveRequestLogic();
        this._initializeAbortRequestLogic();
    }
    UsersRequests.prototype._initializationSpinners = function () {
        var _this = this;
        var $usersRequestsActionsContainers = $('.request-actions');
        $.each($usersRequestsActionsContainers, function (index, container) {
            var spinner = new Spinner(SpinnerOptions.BigRoundCenteredGreen, $(container));
            _this._requestActionSpinners.push(spinner);
        });
    };
    UsersRequests.prototype._initializeApproveRequestLogic = function () {
        var _this = this;
        var $approveBtns = $('.users-requests .request-actions #approveBtn');

        $approveBtns.click(function (event) {
            var $userRequest = $(event.target).closest('.user-request');
            var approveUrl = $userRequest.find('#approveUrl').val();
            _this._doAjaxApproveAbortRequestByUrl(approveUrl, $userRequest, 0 /* Approve */);
        });
    };
    UsersRequests.prototype._initializeAbortRequestLogic = function () {
        var _this = this;
        var $abortBtns = $('.users-requests .request-actions #abortBtn');
        $abortBtns.click(function (event) {
            var $userRequest = $(event.target).closest('.user-request');
            var abortUrl = $userRequest.find('#abortUrl').val();
            _this._doAjaxApproveAbortRequestByUrl(abortUrl, $userRequest, 1 /* Abort */);
        });
    };
    UsersRequests.prototype._doAjaxApproveAbortRequestByUrl = function (urlToAction, $userRequest, typeRequestAction) {
        var _this = this;
        var $containerOfSpinner = $userRequest.find('.request-actions');
        var currentSpinner = this._requestActionSpinners.filter(function (spinner, index) {
            return spinner.Container[0] == $containerOfSpinner[0];
        })[0];
        currentSpinner.opts = typeRequestAction == 0 /* Approve */ ? SpinnerOptions.BigRoundCenteredGreenForUsersRequests : SpinnerOptions.BigRoundCenteredRedForUsersRequests;
        $.ajax({
            type: 'POST',
            url: urlToAction,
            beforeSend: function () {
                currentSpinner.showSpinnerHideInsides();
            },
            success: function (response) {
                if (response.Successed) {
                    _this._fadeUserRequestDivByFilterRequestState($userRequest);
                    _this._changeRequestStateLabel($userRequest, typeRequestAction);
                }
                _this._popupAlert.ShowAll(response.AlertSections);
                currentSpinner.hideSpinnerShowInsides();
            },
            error: function () {
                _this._popupAlert.Error(l8on.InternalServerError);
            }
        });
    };
    UsersRequests.prototype._changeRequestStateLabel = function ($userRequest, typeRequestAction) {
        var $label = $userRequest.find('.label');
        var $actionsContainer = $userRequest.find('.request-actions');
        $label.removeClass('label-info label-success label-danger');
        switch (typeRequestAction) {
            case 0 /* Approve */:
                $label.addClass('label-success');
                $label.text(l8on.Cabinet.UsersRequests.Approved);
                $actionsContainer.addClass('one-child');
                $actionsContainer.find('#approveBtn').hide();
                $actionsContainer.find('#abortBtn').removeClass('hide').show();
                break;
            case 1 /* Abort */:
                $label.addClass('label-danger');
                $label.text(l8on.Cabinet.UsersRequests.Aborted);
                $actionsContainer.addClass('one-child');
                $actionsContainer.find('#abortBtn').hide();
                $actionsContainer.find('#approveBtn').removeClass('hide').show();
                break;
        }
    };
    UsersRequests.prototype._fadeUserRequestDivByFilterRequestState = function ($userRequest) {
        if (this._filterRequestState != 0 /* None */) {
            this._hideUserRequest($userRequest);
        }
    };
    UsersRequests.prototype._hideUserRequest = function ($userRequest) {
        var $liUserRequest = $userRequest.closest('li');
        $liUserRequest.slideUp(400, function () {
            $liUserRequest.remove();
            if ($('.users-requests li').length == 0) {
                window.location.replace('');
            }
        });
    };
    UsersRequests.prototype._initializationReadmoreBtns = function () {
        var requestDescsForReadmore = this._getDescsWithLargeText();
        requestDescsForReadmore.readmore({
            maxHeight: 32,
            speed: 300,
            moreLink: '<a href="#">' + l8on.Common.MoreInfo + '</a>',
            lessLink: '<a href="#">' + l8on.Common.Hide + '</a>'
        });
    };
    UsersRequests.prototype._getDescsWithLargeText = function () {
        var $userRequestDesc = $('.user-request .desc');
        var $descsWithLargeTest = $userRequestDesc.filter(function (index, desc) {
            var text = $(desc).find('span');
            return text.height() > $(desc).height();
        });
        return $descsWithLargeTest;
    };
    return UsersRequests;
})();
$(function () {
    var usersRequests = new UsersRequests();
});
//# sourceMappingURL=users-requests.js.map
