﻿declare enum FilterRequestState {
    None = 0,
    Completed = 1,
    Aborted = 2,
    InProgress = 3,
}
declare enum TypeRequestAction {
    Approve = 0,
    Abort = 1,
}
declare class UsersRequests {
    private _filterRequestState;
    private _popupAlert;
    private _requestActionSpinners;
    constructor();
    private _initializationSpinners();
    private _initializeApproveRequestLogic();
    private _initializeAbortRequestLogic();
    private _doAjaxApproveAbortRequestByUrl(urlToAction, $userRequest, typeRequestAction);
    private _changeRequestStateLabel($userRequest, typeRequestAction);
    private _fadeUserRequestDivByFilterRequestState($userRequest);
    private _hideUserRequest($userRequest);
    private _initializationReadmoreBtns();
    private _getDescsWithLargeText();
}
