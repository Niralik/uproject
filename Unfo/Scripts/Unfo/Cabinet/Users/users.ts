﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
class Users {
    private _usersCheckSystem: UsersCheck;
    private _usersActions: UsersActions;
    constructor() {
        this._usersCheckSystem = new UsersCheck();
        this._usersActions = new UsersActions(this._usersCheckSystem);
    }
   
}
$(() => {
    var users = new Users();
});