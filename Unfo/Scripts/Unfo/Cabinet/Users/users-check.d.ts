﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
declare class UsersCheck {
    private checkboxClass;
    private _$usersTableContainer;
    private _$allCheckboxs;
    private _$userCheckBoxs;
    private _$headCheckBox;
    constructor();
    private _initializeICheck();
    public Refresh(): void;
    private _initializeHeadCheckBoxLogic();
    private _setCheckedClasses(checkbox, checkedStatus);
    private _handleCheckEvent();
    private _reloadPageIfUsersListEmpty();
    private _visualizationUsersActionByChecked();
    private _$usersActionDropdown;
    private _$thContents;
    private _$thNameContainer;
    private durationFadeAnimation;
    private _showUsersActions(numCheckedUsers);
    private _hideUsersActions();
    private _addSelectedUsersString(numCheckedUsers);
    private _getNumberCheckedUsers();
}
