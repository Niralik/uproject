﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
var Users = (function () {
    function Users() {
        this._usersCheckSystem = new UsersCheck();
        this._usersActions = new UsersActions(this._usersCheckSystem);
    }
    return Users;
})();
$(function () {
    var users = new Users();
});
//# sourceMappingURL=users.js.map
