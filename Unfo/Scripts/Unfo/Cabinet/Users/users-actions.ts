﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
class UsersActions {
    private _$usersActions: JQuery;
    private _currentEventId: string;
    private _usersCheckSystem: UsersCheck;
    private _popupAlert: AlertEngine;
    constructor(usersCheckSystem: UsersCheck) {
        this._usersCheckSystem = usersCheckSystem;
        this._$usersActions = $('.users-actions .dropdown-menu li a');
        this._currentEventId = $('#CurrentEventId').val();
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });
       this._handleDeleteModeratorClick();
    }
    private _handleDeleteModeratorClick() {
        var deleteFromModeratorsAction = this._$usersActions.filter('#deleteFromModerator');
        deleteFromModeratorsAction.on('click', () => {
            var checkedUsers = $('.users tbody tr.checked');
            var usersIds = checkedUsers.find('#userId').map((index, element: HTMLInputElement)=> { return element.value; });
            $.ajax({
                type: 'POST',
                url: '/Cabinet/Users/RemoveFromModerators',
                data: { 'usersIds': usersIds.toArray(), 'eventId': this._currentEventId},
                success: (response: IServerResponse) => {
                    if (response.Successed) {
                        checkedUsers.fadeOut(350, ()=> {
                            checkedUsers.remove();
                            this._usersCheckSystem.Refresh();
                        });
                    }
                   
                    this._popupAlert.ShowAll(response.AlertSections);
                }
            });
        });
    }

}