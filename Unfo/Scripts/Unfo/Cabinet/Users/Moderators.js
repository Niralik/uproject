﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
var Moderators = (function () {
    function Moderators() {
        this._typeHeadInput = $('#moderators-bloodhound .typeahead');
        this._suggestionModeratorTemplate = Handlebars.templates['suggestion-moderator'];
        this._potentialModeratorTemplate = Handlebars.templates['potential-moderator'];
        this._$addModeratorBtn = $('.add-moderators-modal #addModeratorBtn');
        this._$suggestionsContainer = $('#moderators-bloodhound .tt-dropdown-menu');
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this._$statusSearchContainer = $('#moderators-bloodhound .status-search-container');
        this._$modalFooter = $('.add-moderators-modal .modal-footer');
        this._searchingSpinner = new Spinner(SpinnerOptions.VerySmallRoundLefterBlue, this._$statusSearchContainer);
        this._addingSpinner = new Spinner(SpinnerOptions.SmallRoundLefterGreen, this._$modalFooter);
        this._$potentialModeratorsContainer = $('.potential-moderators');
        this._$potentialModeratorsList = this._$potentialModeratorsContainer.find('.well');
        this._durationAnimation = 400;
        this._moderators = this._initializeModeratorsBloodhound();
        this._moderators.initialize();
        this._initializeTypehead();
        this._handleModeratorSelected();
        this._handleAddingModerators();
    }
    Moderators.prototype._initializeModeratorsBloodhound = function () {
        var _this = this;
        var bloodhound = new Bloodhound({
            name: 'moderators',
            limit: 50,
            remote: {
                url: '/Cabinet/Users/Search?SearchQuery=%QUERY',
                ajax: {
                    beforeSend: function () {
                        _this._showSpinnerHideInsides(_this._searchingSpinner);
                    },
                    success: function () {
                    },
                    complete: function () {
                        _this._hideSpinnerShowInsides(_this._searchingSpinner);
                    }
                }
            },
            datumTokenizer: function (d) {
                var t = Bloodhound.tokenizers.obj.whitespace(d.Fio);
                return t;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
        return bloodhound;
    };
    Moderators.prototype._initializeTypehead = function () {
        var _this = this;
        var options = {
            hint: true,
            highlight: true,
            minLength: 3
        };
        var dataset = {
            name: 'moderators',
            displayKey: function (d) {
                return d.SurName + ' ' + d.FirstName;
            },
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: this._moderators.ttAdapter(),
            templates: {
                empty: '<div class="empty-message">' + l8on.Cabinet.Users.EmptyModeratorSearch + '</div>',
                suggestion: function (datum) {
                    return _this._suggestionModeratorTemplate(datum);
                }
            }
        };
        this._typeHeadInput.typeahead(options, dataset);
        this._typeHeadInput.attr('placeholder', l8on.Cabinet.Users.ModeratorSearchPlaceHolderEmptyList);
    };

    Moderators.prototype._handleModeratorSelected = function () {
        var _this = this;
        this._typeHeadInput.on('typeahead:selected', function (event, moderator, dataSetName) {
            _this._addToPotenialModeratorsList(moderator);
            _this._clearTypeHeadInput();
            _this._showPotentialModeratorsContainer();
            _this._handleRemovingUsersFromList();
            _this._actualizeAddModeratorBtn();
        });
    };
    Moderators.prototype._clearTypeHeadInput = function () {
        this._typeHeadInput.val('');
    };
    Moderators.prototype._handleRemovingUsersFromList = function () {
        var _this = this;
        var closeBtns = this._$potentialModeratorsList.find('.close');
        closeBtns.one('click', function (event) {
            var $removingUserFromList = $(event.target).closest('.potential-moderator');
            $removingUserFromList.slideUp(350, function () {
                $removingUserFromList.remove();
                _this._tryHandleEmptyUsersList();
                _this._actualizeAddModeratorBtn();
            });
        });
    };

    Moderators.prototype._actualizeAddModeratorBtn = function () {
        var numOfPotentialModerators = this._$potentialModeratorsList.find('.potential-moderator').length;
        if (numOfPotentialModerators === 0) {
            this._$addModeratorBtn.attr('disabled', 'disabled');
        } else {
            this._$addModeratorBtn.removeAttr('disabled');
        }
    };
    Moderators.prototype._tryHandleEmptyUsersList = function () {
        var numOfPotentialModerators = this._$potentialModeratorsList.find('.potential-moderator').length;
        if (numOfPotentialModerators === 0) {
            this._hidePotentialModeratorsContainer();
            this._typeHeadInput.attr('placeholder', l8on.Cabinet.Users.ModeratorSearchPlaceHolderEmptyList);
        }
    };

    Moderators.prototype._showPotentialModeratorsContainer = function () {
        if (this._$potentialModeratorsContainer.is(':hidden')) {
            this._$potentialModeratorsContainer.slideDown(350);
        }
    };
    Moderators.prototype._hidePotentialModeratorsContainer = function () {
        if (this._$potentialModeratorsContainer.is(':visible')) {
            this._$potentialModeratorsContainer.slideUp(350);
        }
    };
    Moderators.prototype._addToPotenialModeratorsList = function (moderator) {
        if (this._checkIsExistUserInList(moderator))
            return;
        var $potentialModeratorHtml = $(this._potentialModeratorTemplate(moderator));
        this._$potentialModeratorsList.append($potentialModeratorHtml);
        this._typeHeadInput.attr("placeholder", l8on.Cabinet.Users.ModeratorSearchPlaceHolderFilledList);
        $potentialModeratorHtml.slideDown(350);
    };
    Moderators.prototype._checkIsExistUserInList = function (moderator) {
        var addingUserId = moderator.Id;
        var potentialModeratorInList = this._$potentialModeratorsList.find('.potential-moderator').filter(function (index, potentialModerator) {
            var existUserId = $(potentialModerator).find("#userId").val();
            return existUserId == addingUserId;
        });
        if (potentialModeratorInList.length > 0) {
            return true;
        }
        return false;
    };

    Moderators.prototype._handleAddingModerators = function () {
        var _this = this;
        this._currentEventId = $('#CurrentEventId').val();
        this._$addModeratorBtn.on('click', function () {
            var $usersIds = _this._$potentialModeratorsList.find('.potential-moderator').find('#userId').map(function (index, element) {
                return element.value;
            });
            $.ajax({
                type: 'POST',
                url: '/Cabinet/Users/AddToModerators',
                data: { 'usersIds': $usersIds.toArray(), 'eventId': _this._currentEventId },
                success: function (response) {
                    if (response.Successed) {
                        window.location.replace('');
                    }
                    _this._popupAlert.ShowAll(response.AlertSections);
                }
            });
        });
    };

    Moderators.prototype._showSpinnerHideInsides = function (spinner) {
        var _this = this;
        if (spinner.Container.has('.spinner').length > 0)
            return;
        spinner.spin();
        var $processingSpinners = $('.spinner', spinner.Container);
        var $spinnerContainerInsides = spinner.Container.children().filter(function (index, child) {
            return !$(child).is('.spinner');
        });
        $processingSpinners.promise().then(function () {
            $spinnerContainerInsides.promise().then(function () {
                $spinnerContainerInsides.fadeOut(_this._durationAnimation, function () {
                    $processingSpinners.fadeIn(_this._durationAnimation);
                });
            });
        });
    };
    Moderators.prototype._hideSpinnerShowInsides = function (spinner) {
        var _this = this;
        var $processingSpinners = $('.spinner', spinner.Container);
        var $spinnerContainerInsides = spinner.Container.children().filter(function (index, child) {
            return !$(child).is('.spinner');
        });
        if ($spinnerContainerInsides.is(":visible") && $spinnerContainerInsides.is(':opaque'))
            return;
        $processingSpinners.fadeOut(this._durationAnimation, function () {
            spinner.stop();
            $spinnerContainerInsides.fadeIn(_this._durationAnimation);
        });
    };
    return Moderators;
})();

$(function () {
    var moderators = new Moderators();
});
//# sourceMappingURL=Moderators.js.map
