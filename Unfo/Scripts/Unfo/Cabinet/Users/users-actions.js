﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
var UsersActions = (function () {
    function UsersActions(usersCheckSystem) {
        this._usersCheckSystem = usersCheckSystem;
        this._$usersActions = $('.users-actions .dropdown-menu li a');
        this._currentEventId = $('#CurrentEventId').val();
        this._popupAlert = new AlertEngine({ TypeAlertDisplay: 1 /* PopupView */ });
        this._handleDeleteModeratorClick();
    }
    UsersActions.prototype._handleDeleteModeratorClick = function () {
        var _this = this;
        var deleteFromModeratorsAction = this._$usersActions.filter('#deleteFromModerator');
        deleteFromModeratorsAction.on('click', function () {
            var checkedUsers = $('.users tbody tr.checked');
            var usersIds = checkedUsers.find('#userId').map(function (index, element) {
                return element.value;
            });
            $.ajax({
                type: 'POST',
                url: '/Cabinet/Users/RemoveFromModerators',
                data: { 'usersIds': usersIds.toArray(), 'eventId': _this._currentEventId },
                success: function (response) {
                    if (response.Successed) {
                        checkedUsers.fadeOut(350, function () {
                            checkedUsers.remove();
                            _this._usersCheckSystem.Refresh();
                        });
                    }

                    _this._popupAlert.ShowAll(response.AlertSections);
                }
            });
        });
    };
    return UsersActions;
})();
//# sourceMappingURL=users-actions.js.map
