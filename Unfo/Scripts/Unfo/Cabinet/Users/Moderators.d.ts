﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
declare class Moderators {
    private _moderators;
    private _typeHeadInput;
    private _moderatorsTypeHeadOptions;
    private _suggestionModeratorTemplate;
    private _potentialModeratorTemplate;
    private _$addModeratorBtn;
    private _$suggestionsContainer;
    private _popupAlert;
    private _$statusSearchContainer;
    private _$modalFooter;
    private _searchingSpinner;
    private _addingSpinner;
    constructor();
    private _initializeModeratorsBloodhound();
    private _initializeTypehead();
    private _$potentialModeratorsContainer;
    private _$potentialModeratorsList;
    private _handleModeratorSelected();
    private _clearTypeHeadInput();
    private _handleRemovingUsersFromList();
    private _actualizeAddModeratorBtn();
    private _tryHandleEmptyUsersList();
    private _showPotentialModeratorsContainer();
    private _hidePotentialModeratorsContainer();
    private _addToPotenialModeratorsList(moderator);
    private _checkIsExistUserInList(moderator);
    private _currentEventId;
    private _handleAddingModerators();
    private _durationAnimation;
    private _showSpinnerHideInsides(spinner);
    private _hideSpinnerShowInsides(spinner);
}
