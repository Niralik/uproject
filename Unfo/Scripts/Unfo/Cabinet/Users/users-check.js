﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
var UsersCheck = (function () {
    function UsersCheck() {
        this.checkboxClass = "icheckbox_minimal-aero";
        this._$usersActionDropdown = $('.users-actions');
        this._$thContents = $('.th-content');
        this._$thNameContainer = this._$thContents.filter('.name-container');
        this.durationFadeAnimation = 250;
        this._$usersTableContainer = $('.users.table-container');
        this._$headCheckBox = this._$usersTableContainer.find('thead .head-checkbox input[type=checkbox]');
        this._$userCheckBoxs = this._$usersTableContainer.find('tbody .row-checkbox input[type=checkbox]');
        this._$allCheckboxs = this._$usersTableContainer.find('input[type=checkbox]');
        this._initializeICheck();
        this._initializeHeadCheckBoxLogic();
        this._handleCheckEvent();
    }
    UsersCheck.prototype._initializeICheck = function () {
        this._$allCheckboxs.iCheck({
            checkboxClass: this.checkboxClass
        });
    };
    UsersCheck.prototype.Refresh = function () {
        this._$userCheckBoxs = this._$usersTableContainer.find('tbody .row-checkbox input[type=checkbox]');
        this._visualizationUsersActionByChecked();
        this._reloadPageIfUsersListEmpty();
    };
    UsersCheck.prototype._initializeHeadCheckBoxLogic = function () {
        var _this = this;
        this._$headCheckBox.on('ifChecked || ifUnchecked', function (e) {
            var checkedStatus = e.target.checked;
            _this._$userCheckBoxs.each(function (index, checkbox) {
                _this._setCheckedClasses(checkbox, checkedStatus);
            });
        });
    };
    UsersCheck.prototype._setCheckedClasses = function (checkbox, checkedStatus) {
        checkbox.checked = checkedStatus;
        if (checkedStatus == checkbox.checked) {
            $(checkbox).closest('tr').removeClass('checked');
            $(checkbox).closest('.' + this.checkboxClass).removeClass('checked');
        }
        if (checkbox.checked) {
            $(checkbox).closest('tr').addClass('checked');
            $(checkbox).closest('.' + this.checkboxClass).addClass('checked');
        }
    };
    UsersCheck.prototype._handleCheckEvent = function () {
        var _this = this;
        this._$allCheckboxs.on('ifChecked || ifUnchecked', function (e) {
            var checkbox = e.target;
            _this._setCheckedClasses(checkbox, checkbox.checked);
            _this._visualizationUsersActionByChecked();
        });
    };
    UsersCheck.prototype._reloadPageIfUsersListEmpty = function () {
        if (this._$userCheckBoxs.length == 0) {
            window.location.replace('');
        }
    };
    UsersCheck.prototype._visualizationUsersActionByChecked = function () {
        var numCheckedUsers = this._getNumberCheckedUsers();
        if (numCheckedUsers > 0) {
            if (this._$usersActionDropdown.is(':visible')) {
                this._addSelectedUsersString(numCheckedUsers);
            } else {
                this._showUsersActions(numCheckedUsers);
            }
        } else {
            this._hideUsersActions();
        }
    };

    UsersCheck.prototype._showUsersActions = function (numCheckedUsers) {
        var _this = this;
        this._$thContents.animate({
            opacity: 0
        }, this.durationFadeAnimation, function () {
            _this._$thNameContainer.hide();
            _this._addSelectedUsersString(numCheckedUsers);
            _this._$usersActionDropdown.fadeIn(_this.durationFadeAnimation);
        });
    };
    UsersCheck.prototype._hideUsersActions = function () {
        var _this = this;
        this._$usersActionDropdown.fadeOut(this.durationFadeAnimation, function () {
            _this._$thNameContainer.show();
            _this._$thContents.animate({ opacity: 1 }, _this.durationFadeAnimation);
        });
    };
    UsersCheck.prototype._addSelectedUsersString = function (numCheckedUsers) {
        var $numUsersStringContainer = $('.users-actions #numOfUsers');
        var headerUsersString = Utilites.Declination(l8on.Common.SelectedSingular, l8on.Common.SelectedPlural, l8on.Common.SelectedPlural, numCheckedUsers);
        var numUsersString = Utilites.Declination(l8on.Common.UsersNominativeSingular, l8on.Common.UsersNominativePlural, l8on.Common.UsersGenetivePlural, numCheckedUsers);
        var numSelectedUsersString = '(' + headerUsersString + ' ' + numCheckedUsers + ' ' + numUsersString + ')';
        $numUsersStringContainer.html(numSelectedUsersString);
    };
    UsersCheck.prototype._getNumberCheckedUsers = function () {
        var countChecked = 0;
        this._$userCheckBoxs.each(function (index, checkbox) {
            if (checkbox.checked) {
                countChecked++;
            }
        });
        return countChecked;
    };
    return UsersCheck;
})();
//# sourceMappingURL=users-check.js.map
