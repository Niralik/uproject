﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />

class Moderators {
    private _moderators: Bloodhound<{}>;
    private _typeHeadInput: JQuery = $('#moderators-bloodhound .typeahead');
    private _moderatorsTypeHeadOptions: TypeaheadOptions;
    private _suggestionModeratorTemplate: HandlebarsTemplateDelegate = Handlebars.templates['suggestion-moderator'];
    private _potentialModeratorTemplate: HandlebarsTemplateDelegate = Handlebars.templates['potential-moderator']
    private _$addModeratorBtn = $('.add-moderators-modal #addModeratorBtn');
    private _$suggestionsContainer = $('#moderators-bloodhound .tt-dropdown-menu');
    private _popupAlert: AlertEngine = new AlertEngine({ TypeAlertDisplay: TypeAlertDisplay.PopupView });
    private _$statusSearchContainer: JQuery = $('#moderators-bloodhound .status-search-container');
    private _$modalFooter: JQuery = $('.add-moderators-modal .modal-footer');
    private _searchingSpinner = new Spinner(SpinnerOptions.VerySmallRoundLefterBlue, this._$statusSearchContainer);
    private _addingSpinner = new Spinner(SpinnerOptions.SmallRoundLefterGreen, this._$modalFooter);

    constructor() {
        this._moderators = this._initializeModeratorsBloodhound();
        this._moderators.initialize();
        this._initializeTypehead();
        this._handleModeratorSelected();
        this._handleAddingModerators();
    }
    private _initializeModeratorsBloodhound(): Bloodhound<{}> {
        var bloodhound = new Bloodhound({
            name: 'moderators',
            limit: 50,
            remote: {
                url: '/Cabinet/Users/Search?SearchQuery=%QUERY',
                ajax: {
                    beforeSend:()=> {
                        this._showSpinnerHideInsides(this._searchingSpinner);
                    },
                    success:() => {
                      
                    },
                    complete: () => {
                        this._hideSpinnerShowInsides(this._searchingSpinner);
                    }
                }
                
            },
            
            datumTokenizer: d=> {
                var t = Bloodhound.tokenizers.obj.whitespace(d.Fio);
                return t;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
        return bloodhound;
    }
    private _initializeTypehead(): void {
        var options = {
            hint: true,
            highlight: true,
            minLength: 3
        };
        var dataset: Twitter.Typeahead.Dataset = {
            name: 'moderators',
            displayKey: function(d) {
                return d.SurName + ' ' + d.FirstName;
            },
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: this._moderators.ttAdapter(),
            templates: {
                empty:'<div class="empty-message">'+ l8on.Cabinet.Users.EmptyModeratorSearch+ '</div>',
                suggestion: datum => this._suggestionModeratorTemplate(datum)
    }
        };
        this._typeHeadInput.typeahead(options, dataset);
        this._typeHeadInput.attr('placeholder', l8on.Cabinet.Users.ModeratorSearchPlaceHolderEmptyList);
    }
    private _$potentialModeratorsContainer = $('.potential-moderators');
   
    private _$potentialModeratorsList = this._$potentialModeratorsContainer.find('.well');
    private _handleModeratorSelected():void {
        this._typeHeadInput.on('typeahead:selected', (event, moderator, dataSetName) => {
            this._addToPotenialModeratorsList(moderator);
            this._clearTypeHeadInput();
            this._showPotentialModeratorsContainer();
            this._handleRemovingUsersFromList();
            this._actualizeAddModeratorBtn();
        });
    
    }
    private _clearTypeHeadInput() {
        this._typeHeadInput.val('');
    }
    private _handleRemovingUsersFromList() {
        var closeBtns = this._$potentialModeratorsList.find('.close');
        closeBtns.one('click', (event: Event)=> {
            var $removingUserFromList = $(event.target).closest('.potential-moderator');
            $removingUserFromList.slideUp(350, ()=> {
                $removingUserFromList.remove();
                this._tryHandleEmptyUsersList();
                this._actualizeAddModeratorBtn();
            });

        });
    }

    private _actualizeAddModeratorBtn() {
        var numOfPotentialModerators = this._$potentialModeratorsList.find('.potential-moderator').length;
        if (numOfPotentialModerators === 0) {
            this._$addModeratorBtn.attr('disabled', 'disabled');
        } else {
            this._$addModeratorBtn.removeAttr('disabled');
        }
    }
    private _tryHandleEmptyUsersList() {
        var numOfPotentialModerators = this._$potentialModeratorsList.find('.potential-moderator').length;
        if (numOfPotentialModerators === 0) {
            this._hidePotentialModeratorsContainer();
            this._typeHeadInput.attr('placeholder', l8on.Cabinet.Users.ModeratorSearchPlaceHolderEmptyList);
        }
    }

    private _showPotentialModeratorsContainer() {
        if (this._$potentialModeratorsContainer.is(':hidden')) {
            this._$potentialModeratorsContainer.slideDown(350);
        }

    }
    private _hidePotentialModeratorsContainer() {
        if (this._$potentialModeratorsContainer.is(':visible')) {
            this._$potentialModeratorsContainer.slideUp(350);
        }
    }
    private _addToPotenialModeratorsList(moderator: any) {
        if (this._checkIsExistUserInList(moderator)) return;
        var $potentialModeratorHtml = $(this._potentialModeratorTemplate(moderator));
        this._$potentialModeratorsList.append($potentialModeratorHtml);
        this._typeHeadInput.attr("placeholder", l8on.Cabinet.Users.ModeratorSearchPlaceHolderFilledList);
        $potentialModeratorHtml.slideDown(350);
    }
    private _checkIsExistUserInList(moderator):boolean {
        var addingUserId = moderator.Id;
        var potentialModeratorInList = this._$potentialModeratorsList.find('.potential-moderator').filter((index, potentialModerator: HTMLDivElement)=> {
            var existUserId = $(potentialModerator).find("#userId").val();
            return existUserId == addingUserId;
        });
        if (potentialModeratorInList.length > 0) {
            return true;
        }
        return false;
    }
    private _currentEventId: string;
    private _handleAddingModerators() {
        this._currentEventId = $('#CurrentEventId').val();
        this._$addModeratorBtn.on('click', ()=> {
            var $usersIds = this._$potentialModeratorsList.find('.potential-moderator').find('#userId').map((index, element: HTMLInputElement) => { return element.value; });
            $.ajax({
                type: 'POST',
                url: '/Cabinet/Users/AddToModerators',
                data: { 'usersIds': $usersIds.toArray(), 'eventId': this._currentEventId },
                success: (response: IServerResponse) => {
                    if (response.Successed) {
                        window.location.replace('');
                    }
                    this._popupAlert.ShowAll(response.AlertSections);
                }
            });
        });
    }
    private _durationAnimation = 400;
    private _showSpinnerHideInsides(spinner: Spinner) { 
        if (spinner.Container.has('.spinner').length >0) return;
        spinner.spin();
        var $processingSpinners = $('.spinner', spinner.Container);
        var $spinnerContainerInsides = spinner.Container.children().filter((index, child) => !$(child).is('.spinner'));
        $processingSpinners.promise().then(() => {
            $spinnerContainerInsides.promise().then(() => {
                $spinnerContainerInsides.fadeOut(this._durationAnimation, () => {
                    $processingSpinners.fadeIn(this._durationAnimation);
                });
            });
        });
    }
    private _hideSpinnerShowInsides(spinner: Spinner) {
        var $processingSpinners = $('.spinner', spinner.Container);
        var $spinnerContainerInsides = spinner.Container.children().filter((index, child) => !$(child).is('.spinner'));
        if ($spinnerContainerInsides.is(":visible") && $spinnerContainerInsides.is(':opaque')) return;
        $processingSpinners.fadeOut(this._durationAnimation, () => {
            spinner.stop();
            $spinnerContainerInsides.fadeIn(this._durationAnimation);
        });
        
    }
}

$(() => {
    var moderators = new Moderators();
});