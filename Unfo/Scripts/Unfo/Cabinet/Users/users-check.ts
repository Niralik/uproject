﻿/// <reference path="../../../typings/typeahead/typeahead.d.ts" />
class UsersCheck {
    private checkboxClass = "icheckbox_minimal-aero"
    private _$usersTableContainer: JQuery;
    private _$allCheckboxs: JQuery;
    private _$userCheckBoxs: JQuery;
    private _$headCheckBox: JQuery;
    constructor() {
        this._$usersTableContainer = $('.users.table-container');
        this._$headCheckBox = this._$usersTableContainer.find('thead .head-checkbox input[type=checkbox]');
        this._$userCheckBoxs = this._$usersTableContainer.find('tbody .row-checkbox input[type=checkbox]');
        this._$allCheckboxs = this._$usersTableContainer.find('input[type=checkbox]');
        this._initializeICheck();
        this._initializeHeadCheckBoxLogic();
        this._handleCheckEvent();
    }
    private _initializeICheck() {
        this._$allCheckboxs.iCheck({
            checkboxClass: this.checkboxClass
        });
    }
    public Refresh() {
        this._$userCheckBoxs = this._$usersTableContainer.find('tbody .row-checkbox input[type=checkbox]');
        this._visualizationUsersActionByChecked();
        this._reloadPageIfUsersListEmpty();
    }
    private _initializeHeadCheckBoxLogic() {
        this._$headCheckBox.on('ifChecked || ifUnchecked', (e:Event) => {
            var checkedStatus = (<HTMLInputElement>e.target).checked;
            this._$userCheckBoxs.each((index:number, checkbox:HTMLInputElement) => {
                this._setCheckedClasses(checkbox, checkedStatus);
            });
        });
    }
   private _setCheckedClasses(checkbox: HTMLInputElement, checkedStatus: boolean) {
       checkbox.checked = checkedStatus;
       if (checkedStatus == checkbox.checked) {
           $(checkbox).closest('tr').removeClass('checked');
           $(checkbox).closest('.' + this.checkboxClass).removeClass('checked');
       }
       if (checkbox.checked) {
           $(checkbox).closest('tr').addClass('checked');
           $(checkbox).closest('.' + this.checkboxClass).addClass('checked');
       }
   }
    private _handleCheckEvent() {
        this._$allCheckboxs.on('ifChecked || ifUnchecked', (e: Event) => {
            var checkbox = <HTMLInputElement>e.target;
            this._setCheckedClasses(checkbox, checkbox.checked);
            this._visualizationUsersActionByChecked();
        });
    }
    private _reloadPageIfUsersListEmpty() {
        if (this._$userCheckBoxs.length == 0) {
            window.location.replace('');
        }
    }
    private _visualizationUsersActionByChecked() {
        var numCheckedUsers = this._getNumberCheckedUsers();
        if (numCheckedUsers > 0) {
            if (this._$usersActionDropdown.is(':visible')) {
                this._addSelectedUsersString(numCheckedUsers);
            } else {
                this._showUsersActions(numCheckedUsers);
            }
        } else {
            this._hideUsersActions();
        }
    }

    private _$usersActionDropdown = $('.users-actions');
    private _$thContents = $('.th-content');
    private _$thNameContainer = this._$thContents.filter('.name-container');
    private durationFadeAnimation = 250;
    private _showUsersActions(numCheckedUsers: number): void {
        this._$thContents.animate({
            opacity: 0
        },this.durationFadeAnimation, ()=> {
            this._$thNameContainer.hide();
            this._addSelectedUsersString(numCheckedUsers);
            this._$usersActionDropdown.fadeIn(this.durationFadeAnimation);
        });
    }
    private _hideUsersActions():void {
        this._$usersActionDropdown.fadeOut(this.durationFadeAnimation, ()=> {
            this._$thNameContainer.show();
            this._$thContents.animate({opacity:1},this.durationFadeAnimation);
        });
    }
    private _addSelectedUsersString(numCheckedUsers: number):void {
        var $numUsersStringContainer = $('.users-actions #numOfUsers');
        var headerUsersString = Utilites.Declination(l8on.Common.SelectedSingular, l8on.Common.SelectedPlural, l8on.Common.SelectedPlural, numCheckedUsers);
        var numUsersString = Utilites.Declination(l8on.Common.UsersNominativeSingular, l8on.Common.UsersNominativePlural, l8on.Common.UsersGenetivePlural, numCheckedUsers);
        var numSelectedUsersString = '(' + headerUsersString + ' ' + numCheckedUsers + ' ' + numUsersString + ')';
        $numUsersStringContainer.html(numSelectedUsersString);
    }
    private _getNumberCheckedUsers(): number {
        var countChecked = 0;
        this._$userCheckBoxs.each((index: number, checkbox: HTMLInputElement) => {
            if (checkbox.checked) {
                countChecked++;
            }
        });
        return countChecked;
    }
}