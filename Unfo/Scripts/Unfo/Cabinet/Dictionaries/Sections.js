﻿$(function () {

    var opts = {
        lines: 11, // The number of lines to draw
        length: 0, // The length of each line
        width: 11, // The line thickness
        radius: 19, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 54, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 28, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9 // The z-index (defaults to 2000000000)


    };
    var spinner = new Spinner(opts).spin();
    $(document).on('click', '#addSections, .editSection', function () {
        var self = this;

        $('#modalForEdit').modal('show');

        $('#spinner').append(spinner.el);
        $('#spinner .spinner').show();
        $.get('/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/EditSection', { id: $(self).attr('data-id') }, function (data) {

            $('#modalForEdit .modal-content').html(data);
            $.validator.unobtrusive.parse('#modalForEdit');
        });
    });

    $(document).on('click', '.deleteSection', function () {
        var self = this;

        $.post('/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/DeleteSection', { id: $(self).attr('data-id') }, function (data) {

            $(self).closest('.section').remove();
        });

    });
});

function afterModalSubmit(type) {
    $.get(window.location.href, function (html) {
        $('#sectionsBlock').html(html);
    });
}