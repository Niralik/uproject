﻿$(document).ready(function () {

    $('.organizators').submit(function() {
        $('.html-editor-value').val($('.html-editor').code());
    })
    $('.html-editor').summernote({
        height: 400,
        codemirror: { // codemirror options
            theme: 'flatly'
        }
    });
    $('.html-editor').code($('.html-editor-value').val());
});