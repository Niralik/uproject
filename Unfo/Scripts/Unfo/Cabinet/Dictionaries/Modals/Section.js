﻿$(function () {
    $('#sectionForm').one('submit', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).valid())
            $.post($(this).attr('action'), getFormInputs('#sectionForm'), function (data) {
                if (validAjaxResponse(data)) {
                    $('#modalForEdit .modal-content').empty();
                    $('#modalForEdit').modal('hide');
                    console.log(window.location.href);

                    if (typeof afterModalSubmit == 'function') {
                        afterModalSubmit('section');
                    }

                   
                }
            });
    });
});