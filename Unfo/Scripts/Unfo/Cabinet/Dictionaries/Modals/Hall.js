﻿$(function () {
    $('#hallForm').one('submit', function (e) {
        e.preventDefault();
        if ($(this).valid())
            $.post($(this).attr('action'), getFormInputs('#hallForm'), function (data) {
                if (validAjaxResponse(data)) {
                    $('#modalForEdit .modal-content').empty();
                    $('#modalForEdit').modal('hide');
                    console.log(window.location.href);
                    if (typeof afterModalSubmit == 'function') {
                        afterModalSubmit('hall');
                    }
                }
            });
    });
});