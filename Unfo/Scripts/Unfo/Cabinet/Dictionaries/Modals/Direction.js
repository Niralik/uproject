﻿$(function () {
   
    $('#directionForm').one('submit', function(e) {
        e.preventDefault();
        if ($(this).valid())
            $.post($(this).attr('action'), getFormInputs('#directionForm'), function (data) {
                if (validAjaxResponse(data)) {
                    $('#modalForEdit .modal-content').empty();
                    $('#modalForEdit').modal('hide');
                    console.log(window.location.href);
                    if (typeof afterModalSubmit == 'function') {
                        afterModalSubmit('direction');
                    }

                }
            });
    });
});