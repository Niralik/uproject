﻿$(function () {


    $('.copy').on('input', function () {
        var self = this;
        var $items = $(this).closest('.modal-body').find(".speacker-item:not(:first) [data-copy='" + $(self).attr('data-copy') + "']");

        if ($(self).hasClass('translit')) {

            $items.val(transliterate($(self).val()));
        } else {
            $items.val($(self).val());
        }

    });
    $('#speakerForm').one('submit', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).valid())
            $.post($(this).attr('action'), getFormInputs('#speakerForm'), function (data) {
                if (validAjaxResponse(data)) {
                    $('#modalForEdit .modal-content').empty();
                    $('#modalForEdit').modal('hide');


                    if (typeof afterModalSubmit == 'function') {
                        afterModalSubmit('speaker');
                    }

                }
            });
        return false;
    });
});