﻿

var locationMap;
ymaps.ready(function () {

    initLocationMap();
});
function initLocationMap() {
    locationMap = new ymaps.Map('yamap', {
        center: [55.76, 37.64], // Москва
        zoom: 10
    });
}
var selectedCity;
var selectedStreet;
$(function () {
    
    
    $('#modalForEdit .locationItem:first .streetName, #modalForEdit .locationItem:first .cityName, #modalForEdit .locationItem:first .buildingNumber').blur(function (e) {
        e.preventDefault();

        setAddresses();
        if (isEmpty()) {

        } else {
            var myGeocoder = ymaps.geocode(locAddress());
            myGeocoder.then(
                function (res) {
                    var nearest = res.geoObjects.get(0);
                    var name = nearest.properties.get('name');
                    nearest.properties.set('iconContent', name);
                    nearest.options.set('preset', 'islands#redStretchyIcon');
                    locationMap.geoObjects.add(res.geoObjects);
                    locationMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates(), 12);
                    $('#modalForEdit .locationItem:first .Lat').val(res.geoObjects.get(0).geometry.getCoordinates()[0]);
                    $('#modalForEdit .locationItem:first .Lon').val(res.geoObjects.get(0).geometry.getCoordinates()[1]);
                },
                function (err) {
                    popupAlert('Ошибка');
                }
            );
        }


    });



    var input2 = '#modalForEdit .locationItem:first .streetName';
    var input1 = '#modalForEdit .locationItem:first .cityName';
    var input3 = '#modalForEdit .locationItem:first .buildingNumber';
    var input4 = '#modalForEdit .locationItem:first .countryId';

    var isEmpty = function () {

        return (!$(input1).val() || $(input1).val().length == 0) ||
            (!$(input2).val() || $(input2).val().length == 0) ||
            (!$(input3).val() || $(input3).val().length == 0);

    };

    var useKladr = function () {
        return ($(input4).find(":selected").val() == 1);
    };

    var locAddress = function () {
        return $(input4).find(":selected").text() + ' ' + $(input1).val() + ' ' +
            $(input2).val() + ' ' +
            $(input3).val();

    };


    
    if (useKladr()) {
        initUseKladr(self);
    } else {
        destroyKladr();
    }

   
    $(document).on('change', '.locationItem .countryId', function (e) {
        
        if (useKladr()) {
            initUseKladr();
        } else {
            destroyKladr();
        }
        $('.locationItem .countryId').val($(this).val());
    });

    locationForm
    $(document).on('submit', '#locationForm', function (e) {
 
        e.preventDefault();
        if ($(this).valid())
            $.post($(this).attr('action'), getFormInputs('#locationForm'), function (data) {
                if (validAjaxResponse(data)) {
                    $('#modalForEdit').modal('hide');
                    console.log(window.location.href);
                    $('#modalForEdit .modal-content').empty();
                    if (typeof afterModalSubmit == 'function') {
                        afterModalSubmit('location');
                    }
                }
            });
    });


    
});
function initUseKladr(self) {
    $('.locationItem:first .cityName').autocomplete({
        serviceUrl: "/Cabinet/CommonResources/Kladr",
        paramName: 'q',
        zIndex: 9999999,
        deferRequestBy: 500,
        minChars: 4,
        preventBadQueries: false,
        onSelect: function (suggestion) {
            console.log(suggestion);
            selectedCity = suggestion.data;
            $('#modalForEdit .modal-content .locationItem:first .streetName').autocomplete('setOptions', {
                params: { cityId: selectedCity ? selectedCity : null }
            });
            $(self).closest('.locationItem').find('.kladrCityId').val(suggestion.data);

            //$('#modalForEdit .modal-content  .locationItem:first  .streetName').removeAttr('disabled');

        },
        transformResult: function (response) {
            var r = $.parseJSON(response);

            return {
                suggestions: $.map(r.result, function (dataItem) {

                    return { value: dataItem.typeShort + '. ' + dataItem.name, data: dataItem.id };
                })
            };
        }
    });
    $('.locationItem:first .streetName').autocomplete({
        serviceUrl: "/Cabinet/CommonResources/Kladr",
        paramName: 'q',
        zIndex: 9999999,
        deferRequestBy: 500,
        minChars: 4,
        preventBadQueries: false,
        onSelect: function (suggestion) {
            console.log(suggestion);
            selectedStreet = suggestion.data;
            //$('#modalForEdit .modal-content .locationItem:first .buildingNumber').removeAttr('disabled');
            $(self).closest('.locationItem').find('.kladrStreetId').val(suggestion.data);

        },
        transformResult: function (response) {

            var r = $.parseJSON(response);
            console.log(r);
            return {
                suggestions: $.map(r.result, function (dataItem) {

                    return { value: dataItem.typeShort + '. ' + dataItem.name, data: dataItem.id };
                })
            };
        }
    });

  
}
function destroyKladr() {
    $('.locationItem:first .cityName').autocomplete('dispose');
    $('.locationItem:first .streetName').autocomplete('dispose');
}


function setAddresses() {

    var input2 = '.locationItem:first .streetName';
    var input1 = '.locationItem:first .cityName';
    var input3 = '.locationItem:first .buildingNumber';
    //var input4 = '#modalForEdit .locationItem:first .countryId';


    var t_input2 = '.locationItem:not(:first) .streetName';
    var t_input1 = '.locationItem:not(:first) .cityName';
    var t_input3 = '.locationItem:not(:first) .buildingNumber';
    //var t_input4 = '#modalForEdit .locationItem:not(:first) .countryId';

    $(t_input1).val(transliterate($(input1).val()));
    $(t_input2).val(transliterate($(input2).val()));
    $(t_input3).val(transliterate($(input3).val()));

}

