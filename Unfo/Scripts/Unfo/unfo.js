function Unfo() {
    /// <summary>
    /// Функция создающая объект представляющий из себя общую логику выполняющуюся на каждой
    /// странице сайта
    /// </summary>

    var self = this,
        errorHanlder = new ErrorHandler(),
        currentCulture;

    Object.defineProperty(this, "IsLocal", {
        get: function () { return _isLocal(); },
    });
    //публичные методы - доступны по всем проекту через unfo.PropertyName
    Object.defineProperty(this, "CurrentCulture", {
        get: function () { return currentCulture; },
        set: function (value) { currentCulture = value; }
    });

    function initialize() {
        setAntiForgeryTokenToAllAjaxCalls();
        initializeCurrentCulture();
        initializeTooltips();
        initializeUserTimeZone();
        if (_isLocal()) {
            QUnitFurniture.Intialize();
            AlertEngingeTests.Initialize();
        }
        AlertEngine.SetupEnviroment();
        CurrentUser.Initialize();
    };

    function setAntiForgeryTokenToAllAjaxCalls() {
        /// <summary>
        /// Метод добавляющий ко всем POST запросам значение AntiForgeryToken'a - все POST действия
        /// контроллеров должны быть помеченных соотвествующим аттрибутом
        /// </summary>
        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajaxPrefilter(function (options, originalOptions) {
            if (options.type.toUpperCase() == "POST" && (options.files == undefined || options.files.length < 1)) {
                options.data = $.param($.extend(originalOptions.data, { __RequestVerificationToken: token }));
            }
        });
    };
    function initializeCurrentCulture() {
        /// <summary>
        /// Устанавливаем текущую культуру - проверяем есть ли сейчас что то в куках - если нет -
        /// смотрим настройки культуры браузера - если культура вписывается в культуры по умолчанию
        /// то уставливаем.
        /// </summary>
        var cockieCulture = $.cookie("_culture");
        if (cockieCulture) {
            currentCulture = cockieCulture.toLowerCase();
            return;
        }
        var navigatorCulture = navigator.language || navigator.browserLanguage || navigator.systemLanguage;
        navigatorCulture = navigatorCulture.toLowerCase();
        if (navigatorCulture && (navigatorCulture === "ru" || navigatorCulture === "en")) {
            currentCulture = navigatorCulture;
        } else {
            currentCulture = "ru";
        }
        $.cookie("_culture", currentCulture, { path: '/' });
    };
    function initializeUserTimeZone() {
        try {
            var splittedDate = new Date().toTimeString().split('(');
            var timeZoneName = splittedDate[1].trim();
            var daylightName = splittedDate[2].replace('))', '');
            var timeZone = timeZoneName + ' (' + daylightName + ')';
            $.cookie("_timezone", timeZone, { path: '/' });
        } catch (e) {
            $.cookie("_timezone", '');
        }
    };
    function initializeTooltips() {
        /// <summary>
        /// У всех найденых тултипов на страничке устанавливаем контейнер боди - чтобы отображались тултипы всегда коректно
        /// </summary>
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
    }
    function _isLocal() {
        if (typeof QUnit === 'undefined') {
            return false;
        } else {
            return true;
        }
    }

    this.ClearAddressBar = function () {
        try {
            var clean_uri = location.protocol + "//" + location.host + location.pathname;
            window.history.replaceState({}, document.title, clean_uri);
        } catch (e) {
        }
    };
    initialize();
}
var unfo;

$(function () {
    unfo = new Unfo();
});