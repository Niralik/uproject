﻿/// <reference path="../../typings/jquery.cookie/jquery.cookie.d.ts" />
interface ICurrentUser {
    Id: string;
    PhotoId: string;
    FirstName: string;
    SurName: string;
    FatherName: string;
    Roles: Role[];
    HasRole(roles: Role[]): boolean;
    HasRoles(roles: Role[]): boolean;
    ToString(): string;
}
declare enum Role {
    Admin = 0,
    Organizer = 1,
    Moderator = 2,
    User = 3,
}
declare class CurrentUser {
    static Id: string;
    static AvatarId: string;
    static FirstName: string;
    static SurName: string;
    static FatherName: string;
    static IsAuthenticated: boolean;
    private static Roles;
    static Initialize(): void;
    static HasRole(checkingRole: Role): boolean;
    static HasRole(checkingRoles: Role[]): boolean;
    static HasRoles(checkingRole: Role): boolean;
    static HasRoles(checkingRoles: Role[]): boolean;
    static ToString(isFullVersion?: boolean): string;
    private static checkIsInRoles(checkingRole);
}
