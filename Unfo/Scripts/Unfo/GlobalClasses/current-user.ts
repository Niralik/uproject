﻿/// <reference path="../../typings/jquery.cookie/jquery.cookie.d.ts" />
interface ICurrentUser {
    Id: string;
    PhotoId: string;
    FirstName: string;
    SurName: string;
    FatherName: string;
    Roles: Role[];
    HasRole(roles: Role[]): boolean;
    HasRoles(roles: Role[]): boolean;
    ToString(): string;
}
enum Role {
    Admin,
    Organizer,
    Moderator,
    User
}

class CurrentUser {
    public static Id: string;
    public static AvatarId: string;
    public static FirstName: string;
    public static SurName: string;
    public static FatherName: string;
    public static IsAuthenticated: boolean;
    private static Roles: Role[];
    public static Initialize() {
        var currentUserCookie = $.cookie("_current_user");
        if (currentUserCookie) {
            var parsedCurrentUser = JSON.parse(currentUserCookie);
            CurrentUser.IsAuthenticated = true;
            CurrentUser.Id = parsedCurrentUser.Id;
            CurrentUser.AvatarId = parsedCurrentUser.PhotoId;
            CurrentUser.FirstName = parsedCurrentUser.FirstName;
            CurrentUser.SurName = parsedCurrentUser.SurName;
            CurrentUser.FatherName = parsedCurrentUser.FatherName;
            CurrentUser.Roles = parsedCurrentUser.Roles;
        } else {
            CurrentUser.IsAuthenticated = false;
        };
    }
    public static HasRole(checkingRole: Role): boolean;
    public static HasRole(checkingRoles: Role[]): boolean;
    public static HasRole(checkingRoles: any): boolean {
        var hasRole = false;
        if ($.type(checkingRoles) == "number") {
            var isInRole = CurrentUser.checkIsInRoles(checkingRoles);
            if (isInRole) hasRole = true;
        } else {
            $.each(checkingRoles, (index, checkingRole: Role) => {
                var isInRole = CurrentUser.checkIsInRoles(checkingRole);
                if (isInRole) hasRole = true;
            });
        }
        return hasRole;
    }

    public static HasRoles(checkingRole: Role): boolean;
    public static HasRoles(checkingRoles: Role[]): boolean;
    public static HasRoles(checkingRoles: any): boolean {
        var hasRoles = true;
        $.each(checkingRoles, (index, role: Role) => {
            var hasRole = CurrentUser.HasRole(role);
            if (!hasRole) hasRoles = false;
        });
        return hasRoles;
    }
    public static ToString(isFullVersion?: boolean): string {
        var fio = CurrentUser.SurName + ' ' + CurrentUser.FirstName;
        if (isFullVersion) {
            fio += ' ' + CurrentUser.FatherName;
        }
        return fio;
    }

    private static checkIsInRoles(checkingRole: Role): boolean {
        var checkingRoleName = Role[checkingRole];
        var findedRole = CurrentUser.Roles.filter((userRole) => {
            return userRole.toString() == checkingRoleName;
        });
        return findedRole.length > 0;
    }
}