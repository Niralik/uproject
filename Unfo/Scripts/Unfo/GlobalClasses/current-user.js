﻿/// <reference path="../../typings/jquery.cookie/jquery.cookie.d.ts" />
var Role;
(function (Role) {
    Role[Role["Admin"] = 0] = "Admin";
    Role[Role["Organizer"] = 1] = "Organizer";
    Role[Role["Moderator"] = 2] = "Moderator";
    Role[Role["User"] = 3] = "User";
})(Role || (Role = {}));

var CurrentUser = (function () {
    function CurrentUser() {
    }
    CurrentUser.Initialize = function () {
        var currentUserCookie = $.cookie("_current_user");
        if (currentUserCookie) {
            var parsedCurrentUser = JSON.parse(currentUserCookie);
            CurrentUser.IsAuthenticated = true;
            CurrentUser.Id = parsedCurrentUser.Id;
            CurrentUser.AvatarId = parsedCurrentUser.PhotoId;
            CurrentUser.FirstName = parsedCurrentUser.FirstName;
            CurrentUser.SurName = parsedCurrentUser.SurName;
            CurrentUser.FatherName = parsedCurrentUser.FatherName;
            CurrentUser.Roles = parsedCurrentUser.Roles;
        } else {
            CurrentUser.IsAuthenticated = false;
        }
        ;
    };

    CurrentUser.HasRole = function (checkingRoles) {
        var hasRole = false;
        if ($.type(checkingRoles) == "number") {
            var isInRole = CurrentUser.checkIsInRoles(checkingRoles);
            if (isInRole)
                hasRole = true;
        } else {
            $.each(checkingRoles, function (index, checkingRole) {
                var isInRole = CurrentUser.checkIsInRoles(checkingRole);
                if (isInRole)
                    hasRole = true;
            });
        }
        return hasRole;
    };

    CurrentUser.HasRoles = function (checkingRoles) {
        var hasRoles = true;
        $.each(checkingRoles, function (index, role) {
            var hasRole = CurrentUser.HasRole(role);
            if (!hasRole)
                hasRoles = false;
        });
        return hasRoles;
    };
    CurrentUser.ToString = function (isFullVersion) {
        var fio = CurrentUser.SurName + ' ' + CurrentUser.FirstName;
        if (isFullVersion) {
            fio += ' ' + CurrentUser.FatherName;
        }
        return fio;
    };

    CurrentUser.checkIsInRoles = function (checkingRole) {
        var checkingRoleName = Role[checkingRole];
        var findedRole = CurrentUser.Roles.filter(function (userRole) {
            return userRole.toString() == checkingRoleName;
        });
        return findedRole.length > 0;
    };
    return CurrentUser;
})();
//# sourceMappingURL=current-user.js.map
