﻿var cookie_interface_culture = "_culture"; // язык интерфейса
var cookie_content_culture = "current_culture"; // язык на котором вводится данные на форме
$(function () {

    $('.change-lang').click(function (e) {
        e.preventDefault();
        //$.removeCookie(cookie_interface_culture, { path: '/' });
        $.cookie(cookie_interface_culture, $(this).attr('data-lang'), { path: '/' });
        location.href = location.href;
    });
    $('.content-culture').click(function (e) {
        e.preventDefault();
        //$.removeCookie(cookie_content_culture, { path: '/' });
        $.cookie(cookie_content_culture, $(this).attr('data-lang'), { path: '/' });
        location.href = location.href;
    });

    initDatePicker();

    $('#createEvent').click(function (e) {
        $('#createEventModal').modal('show');
    });

    $(document).on('click', '.closeModal', function () {

        $('#modalForEdit .modal-content').empty();
        $('#modalForEdit').modal('hide');
    });

});
function initDatePicker() {
    $.each($('.datepicker'), function () {

        //var ops = {
        //    format: format,
        //    minView: hasTime ? 0 : 4,
        //    maxView: 4
        //};



        var self = this;
        var format = $(self).attr('data-date-format');
        var hasTime = $(self).find('.dateinput').hasClass('hasTime');
        var rdblock = $(self).closest('.date-range');

        var loc = 'ru';
        var minuteStepping = 10;

        if (!$(rdblock).hasClass("isInit")) {
            $(rdblock).addClass("isInit");

            if (rdblock) {

                var startdate = $(rdblock).find('.startDate .datepicker');
                var enddate = $(rdblock).find('.endDate .datepicker');

                $(startdate).datetimepicker({
                    language: loc,
                    pickTime: hasTime,
                    minuteStepping: minuteStepping

                });
                $(enddate).datetimepicker({
                    language: loc,
                    pickTime: hasTime,
                    minuteStepping: minuteStepping
                })

                $(startdate).on("dp.change", function (e) {
                    $(enddate).data("DateTimePicker").setMinDate(e.date);
                });

                $(enddate).on("dp.change", function (e) {
                    $(startdate).data("DateTimePicker").setMaxDate(e.date);
                });

            } else {
                $(self).datetimepicker({
                    language: loc,
                    pickTime: hasTime,
                    minuteStepping: minuteStepping
                });
            }
        }



    });
}
function getFormInputs(id) {
    var $inputs = $(id + ' :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });

    return values;
}
function validAjaxResponse(response) {
    if (response.result && response.result == 401) {
        window.location.href = "/Account/Signin";
        return false;
    }
    return true;
}
var translitDic = { "Ё": "YO", "Й": "I", "Ц": "TS", "У": "U", "К": "K", "Е": "E", "Н": "N", "Г": "G", "Ш": "SH", "Щ": "SCH", "З": "Z", "Х": "H", "Ъ": "'", "ё": "yo", "й": "i", "ц": "ts", "у": "u", "к": "k", "е": "e", "н": "n", "г": "g", "ш": "sh", "щ": "sch", "з": "z", "х": "h", "ъ": "'", "Ф": "F", "Ы": "I", "В": "V", "А": "a", "П": "P", "Р": "R", "О": "O", "Л": "L", "Д": "D", "Ж": "ZH", "Э": "E", "ф": "f", "ы": "i", "в": "v", "а": "a", "п": "p", "р": "r", "о": "o", "л": "l", "д": "d", "ж": "zh", "э": "e", "Я": "Ya", "Ч": "CH", "С": "S", "М": "M", "И": "I", "Т": "T", "Ь": "'", "Б": "B", "Ю": "YU", "я": "ya", "ч": "ch", "с": "s", "м": "m", "и": "i", "т": "t", "ь": "'", "б": "b", "ю": "yu" };
function transliterate(word) {
    return word.split('').map(function (char) {
        return translitDic[char] || char;
    }).join("");
}


function insertToSelect(items, label, obj) {
    if (items == undefined || items.length == 0) {
        $(obj).html('<option>' + label + '</option>');
        $(obj).attr("disabled", "disabled");
    } else {
        var str = '';
        if (label != undefined && label.length > 0) {
            str = '<option value="">' + label + '</option>';
        }
        for (var i = 0; i < items.length; i++) {
            str += '<option value="' + items[i].Value + '">' + items[i].Text + '</option>';
        }
        $(obj).html(str);

        $(obj).removeAttr("disabled");
    }
}

function Serialize(obj) {

    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");

}

function getImageUrl(imageId, type) {
    switch (type) {
        case 'thumbs':
            return '/Content/upload/img/normal/' + imageId + '.jpg';
        case 'source':
            return '/Content/upload/img/source/' + imageId + '.jpg';
        default:
            return '/Content/upload/img/normal/' + imageId + '.jpg';
    }
}
function getModal(nameView) {
    //var self = this;
    $('#modalForEdit').modal('show');
    $('#modalForEdit .modal-content').html('<div class="modal-body"><div id="spinner" style="height: 200px;"></div></div>');
    if (typeof (spinner) !== 'undefined') {
        $('#spinner').append(spinner.el);
        $('#spinner .spinner').show();
    }

    var url;
    if (nameView == 'EditSpeaker') {
        url = '/Cabinet/Event/' + $('#eventName').val() + '/Speakers/Edit';
    } else {
        url = '/Cabinet/Event/' + $('#eventName').val() + '/Dictionaries/' + nameView;
    }
    $.get(url, function (data) {

        $('#modalForEdit .modal-content').html(data);
        //$.validator.unobtrusive.parse('#modalForEdit');
    });

}
