﻿var l8on = {
    InternalServerError: "We are sorry, there was an internal server error! Reload the page, if not help, please contact to the site's administration.",
    Cabinet: {
        Users: {
            EmptyModeratorSearch: "Unable to find any moderators that match the current query",
            ModeratorSearchPlaceHolderEmptyList: "Email, name of user...",
            ModeratorSearchPlaceHolderFilledList: "You can select another user"
        },
        UsersRequests: {
            Approved: "Approved",
            Aborted: "Denied"
        }
    },
    Common: {
        SelectedSingular: 'Selected',
        SelectedPlural: 'Selected',
        UsersNominativeSingular: 'user',
        UsersNominativePlural: 'users',
        UsersGenetivePlural: 'users',
        MoreInfo: 'more',
        Hide: 'hide'
    }
}