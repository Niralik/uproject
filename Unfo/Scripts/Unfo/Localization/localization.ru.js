﻿var l8on = {
    InternalServerError: 'Просим прощения, произошла внутренняя ошибка сервера! Перезагрузите страницу, если не поможет обратитесь к администрации сайта.',
    Cabinet: {
        Users: {
            EmptyModeratorSearch: "Ни одного модератора не найдено под заданному запросу",
            ModeratorSearchPlaceHolderEmptyList: "Email, имя пользователя...",
            ModeratorSearchPlaceHolderFilledList: "Вы можете выбрать еще пользователя"
        },
        UsersRequests: {
            Approved: "Одобренно",
            Aborted: "Отказано"
        }
    },
    Common: {
        SelectedSingular: 'Выбран',
        SelectedPlural: 'Выбрано',
        UsersNominativeSingular: 'пользователь',
        UsersNominativePlural: 'пользователя',
        UsersGenetivePlural: 'пользователей',
        MoreInfo: 'подробнее',
        Hide:'cкрыть'
    }
}