﻿interface IUnfoLocalization {
    InternalServerError: string;
    Cabinet: {
        Users: {
            EmptyModeratorSearch: string;
            ModeratorSearchPlaceHolderEmptyList: string;
            ModeratorSearchPlaceHolderFilledList: string;
         
        };
        UsersRequests: {
            Approved: string;
            Aborted: string;
        };
    };
    Common: {
        SelectedSingular: string;
        SelectedPlural: string;
        UsersNominativeSingular: string;
        UsersNominativePlural: string;
        UsersGenetivePlural: string;
        MoreInfo: string;
        Hide: string;
    }
}

declare var l8on:IUnfoLocalization;