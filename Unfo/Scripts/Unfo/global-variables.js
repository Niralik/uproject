﻿var g_cabinetHomeIndexUrl = "/Cabinet/Home/index";
var g_urlReferrer = $('#urlReferrer').val();

///<var>Тип оповещения</var>
///Порядок должен совпадать с перечеслением на сервере
var TypeOfAlert;
(function (TypeOfAlert) {
    TypeOfAlert[TypeOfAlert["Info"] = 0] = "Info";
    TypeOfAlert[TypeOfAlert["Warning"] = 1] = "Warning";
    TypeOfAlert[TypeOfAlert["Success"] = 2] = "Success";
    TypeOfAlert[TypeOfAlert["Error"] = 3] = "Error";
})(TypeOfAlert || (TypeOfAlert = {}));
;

///<var>Css классы представляющие тип оповещения</var>
var TypeOfAlertCss = {
    Info: "alert-info",
    Warning: 'alert-warning',
    Success: 'alert-success',
    Error: 'alert-danger'
};

var optsOfBigRoundCenteredSpinner = {
    lines: 9,
    length: 0,
    width: 17,
    radius: 21,
    corners: 1,
    rotate: 66,
    direction: 1,
    speed: 1,
    trail: 42,
    shadow: false,
    hwaccel: true,
    className: 'spinner',
    zIndex: 2e9,
    top: '48.2%',
    left: '50.1%'
};
var optsOfSmallRoundRightSpinner = {
    lines: 9,
    length: 0,
    width: 11,
    radius: 13,
    corners: 1,
    rotate: 66,
    direction: 1,
    color: 'rgb(90, 203, 90)',
    speed: 1,
    trail: 42,
    shadow: false,
    hwaccel: true,
    className: 'spinner',
    zIndex: 2e9,
    top: '50.2%',
    left: '92%'
};
var optsOfBigPurpleRoundCenteredSpinner = $.extend({}, optsOfBigRoundCenteredSpinner, { color: '#59688d' });
var optsOfBigGreenRoundCenteredSpinner = $.extend({}, optsOfBigRoundCenteredSpinner, { color: '#76b852' });
var optsOfSmallGreenRoundRightSpinner = $.extend({}, optsOfSmallRoundRightSpinner, { color: '#76b852' });
//# sourceMappingURL=global-variables.js.map
