﻿class SpinnerOptions {
    //спинер по дефалту вот такой вот формы зеленного цвета => http://fgnass.github.io/spin.js/#?lines=9&length=0&width=17&radius=21&corners=1.0&rotate=69&trail=42&speed=1.0&direction=1&hwaccel=on
    private _spinnerDefaultOptions:ISpinnerOptions ={
        lines: 9, // The number of lines to draw
        length: 0, // The length of each line
        width: 17, // The line thickness
        radius: 21, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 66, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        speed: 1, // Rounds per second
        trail: 42, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '48.2%',
        left: '50.1%',
        color: '#76b852',
        position: 'absolute',
        opacity: 1 / 4,  
        animationDuration: 400 
    };

    private _resultSpinnerOptions:ISpinnerOptions;
    constructor(clientSpinnerOptions?: ISpinnerOptions) {

        this._resultSpinnerOptions = $.extend({}, this._spinnerDefaultOptions, clientSpinnerOptions);
    }
    public static Get(clientSpinnerOptions?: ISpinnerOptions, parentsOptions?: ISpinnerOptions): ISpinnerOptions {
        if (parentsOptions) {
            clientSpinnerOptions = $.extend({}, parentsOptions, clientSpinnerOptions);
        }
        var spinnerOpts = new SpinnerOptions(clientSpinnerOptions);
        return spinnerOpts._resultSpinnerOptions;
    }
     private static Colors = {
         Green: '#76b852',
         Purple: '#59688d',
         Steelblue: '#428bca',
         IndianRed: '#d9534f'
     }
    public static VerySmallRoundLefterBlue: ISpinnerOptions = SpinnerOptions.Get({ width: 7, radius: 8, top: '19.4px', left: '96.6%', color: SpinnerOptions.Colors.Steelblue })
    public static SmallRoundLefterGreen: ISpinnerOptions = SpinnerOptions.Get({ width: 11, radius: 13, top: '50.2%', left: '95.2%', color: SpinnerOptions.Colors.Green })
    public static SmallRoundLeftGreen: ISpinnerOptions = SpinnerOptions.Get({ left: '92%' }, SpinnerOptions.SmallRoundLefterGreen)
    public static BigRoundCenteredGreen: ISpinnerOptions = SpinnerOptions.Get({ width: 17, radius: 21, top: '48.2%', left: '50.1%' }, SpinnerOptions.SmallRoundLeftGreen)
    public static BigRoundCenteredPurple: ISpinnerOptions = SpinnerOptions.Get({ color: SpinnerOptions.Colors.Purple }, SpinnerOptions.BigRoundCenteredGreen);
    public static BigRoundCenteredGreenForUsersRequests: ISpinnerOptions = SpinnerOptions.Get({ top: '50%', left: '50%' }, SpinnerOptions.BigRoundCenteredGreen);
    public static BigRoundCenteredRedForUsersRequests: ISpinnerOptions = SpinnerOptions.Get({ top: '50%', left: '50%', color: SpinnerOptions.Colors.IndianRed }, SpinnerOptions.BigRoundCenteredGreen);
}