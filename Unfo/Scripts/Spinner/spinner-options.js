﻿var SpinnerOptions = (function () {
    function SpinnerOptions(clientSpinnerOptions) {
        //спинер по дефалту вот такой вот формы зеленного цвета => http://fgnass.github.io/spin.js/#?lines=9&length=0&width=17&radius=21&corners=1.0&rotate=69&trail=42&speed=1.0&direction=1&hwaccel=on
        this._spinnerDefaultOptions = {
            lines: 9,
            length: 0,
            width: 17,
            radius: 21,
            corners: 1,
            rotate: 66,
            direction: 1,
            speed: 1,
            trail: 42,
            shadow: false,
            hwaccel: true,
            className: 'spinner',
            zIndex: 2e9,
            top: '48.2%',
            left: '50.1%',
            color: '#76b852',
            position: 'absolute',
            opacity: 1 / 4,
            animationDuration: 400
        };
        this._resultSpinnerOptions = $.extend({}, this._spinnerDefaultOptions, clientSpinnerOptions);
    }
    SpinnerOptions.Get = function (clientSpinnerOptions, parentsOptions) {
        if (parentsOptions) {
            clientSpinnerOptions = $.extend({}, parentsOptions, clientSpinnerOptions);
        }
        var spinnerOpts = new SpinnerOptions(clientSpinnerOptions);
        return spinnerOpts._resultSpinnerOptions;
    };
    SpinnerOptions.Colors = {
        Green: '#76b852',
        Purple: '#59688d',
        Steelblue: '#428bca',
        IndianRed: '#d9534f'
    };
    SpinnerOptions.VerySmallRoundLefterBlue = SpinnerOptions.Get({ width: 7, radius: 8, top: '19.4px', left: '96.6%', color: SpinnerOptions.Colors.Steelblue });
    SpinnerOptions.SmallRoundLefterGreen = SpinnerOptions.Get({ width: 11, radius: 13, top: '50.2%', left: '95.2%', color: SpinnerOptions.Colors.Green });
    SpinnerOptions.SmallRoundLeftGreen = SpinnerOptions.Get({ left: '92%' }, SpinnerOptions.SmallRoundLefterGreen);
    SpinnerOptions.BigRoundCenteredGreen = SpinnerOptions.Get({ width: 17, radius: 21, top: '48.2%', left: '50.1%' }, SpinnerOptions.SmallRoundLeftGreen);
    SpinnerOptions.BigRoundCenteredPurple = SpinnerOptions.Get({ color: SpinnerOptions.Colors.Purple }, SpinnerOptions.BigRoundCenteredGreen);
    SpinnerOptions.BigRoundCenteredGreenForUsersRequests = SpinnerOptions.Get({ top: '50%', left: '50%' }, SpinnerOptions.BigRoundCenteredGreen);
    SpinnerOptions.BigRoundCenteredRedForUsersRequests = SpinnerOptions.Get({ top: '50%', left: '50%', color: SpinnerOptions.Colors.IndianRed }, SpinnerOptions.BigRoundCenteredGreen);
    return SpinnerOptions;
})();
//# sourceMappingURL=spinner-options.js.map
