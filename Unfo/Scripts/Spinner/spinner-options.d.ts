﻿declare class SpinnerOptions {
    private _spinnerDefaultOptions;
    private _resultSpinnerOptions;
    constructor(clientSpinnerOptions?: ISpinnerOptions);
    static Get(clientSpinnerOptions?: ISpinnerOptions, parentsOptions?: ISpinnerOptions): ISpinnerOptions;
    private static Colors;
    static VerySmallRoundLefterBlue: ISpinnerOptions;
    static SmallRoundLefterGreen: ISpinnerOptions;
    static SmallRoundLeftGreen: ISpinnerOptions;
    static BigRoundCenteredGreen: ISpinnerOptions;
    static BigRoundCenteredPurple: ISpinnerOptions;
    static BigRoundCenteredGreenForUsersRequests: ISpinnerOptions;
    static BigRoundCenteredRedForUsersRequests: ISpinnerOptions;
}
