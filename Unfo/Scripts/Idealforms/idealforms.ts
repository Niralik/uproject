﻿/// <reference path="localization/idealforms.l8on.d.ts" />
interface IIdealformOptions {
    /**CSS класс полей к которым будет применена валидация*/
    fieldClass: string;
    /**СSS класс див контейнера в котором будет хранится ошибка валидации */
    errorClass: string;
    /** Флаг отмечающий будут ли показываться иконки валидации - по умолчанию true*/
    hideValidateIcons: boolean;
    /** HTML тег иконки валидации*/
    iconHtml: string;
    iconBaseClass: string;
    iconExclamationClass: string;
    iconSuccessClass: string;
    iconValidatingClass: string;
    invalidClass: string;
    validClass: string;
    /**Флаг обозначающий что форма загрузится без валидирования существующих полей*/
    silentLoad: boolean;
    adaptiveWidth: number;
    /**Событие возникающие после валидации текущего поля */
    onValidate($currentInput: JQuery, ruleName: string, isValid: boolean): any;
    /**Событие возникающие при сабмите формы */
    onSubmit(numOfInvalid: number, event: Event): boolean;
    /**Список полей с именами правил валидации*/
    rules: IUserRules;
    errors: Object;
}
interface IUserRules {
    [inputName: string]: string;
}
interface IIdealforms {
    Initialization(): void;
    AddValidationRules(rules: any): void;
    GetInvalidFields(): JQuery;
    FocusToFirstInvalidInput(): void;
    IsValid($input?: JQuery): boolean;
    Reset($input?: JQuery): void;
    Validate($input: JQuery): boolean;
    GetValidateField($input: JQuery): JQuery;
}
interface IRequests {
    [inputName: string]: JQueryXHR;
}
//interface IErrorsLocale {
//    [ruleName: string]: string;
//}

class Idealforms implements IIdealforms {
    private _$validatingForm: JQuery;
    private _defaultOptions: IIdealformOptions = {
        fieldClass: '.field-ideal',
        errorClass: '.error',
        hideValidateIcons: true,
        iconHtml: '<i/>',
        iconBaseClass: 'fa icon',
        iconExclamationClass: 'fa-exclamation',
        iconSuccessClass: 'fa-check',
        iconValidatingClass: 'fa-circle-o-notch icon-validation',
        invalidClass: 'has-error',
        validClass: 'has-success',
        silentLoad: true,
        adaptiveWidth: 500,
        onValidate: $.noop,
        onSubmit: $.noop,
        rules: {},
        errors: {}
    }
    private _options: IIdealformOptions;
    private _$fields: JQuery = $();
    private _$inputs: JQuery = $();
    private _isFirstLaunch = true;
    //приватные переменные с названиями классов CSS
    private static _unstateFocusClass = 'input-group-focus';
    private static _errorFocusClass = 'has-error-focus';
    private static _successFocusClass = 'has-success-focus';

    constructor($targetForm: JQuery, options: IIdealformOptions) {
        this._$validatingForm = $targetForm;
        this._options = $.extend({}, this._defaultOptions, options);
        this.Initialization();
    }

    public Initialization(): void {
        this._initializeI18N();
        this._doAjaxExtension();
        this._extractValidationRulesFromMarkup();

        this.AddValidationRules(this._options.rules || {});

        //Отработка сабмита формы
        this._$validatingForm.submit((e: Event) => {
            this._validateAll();
            this.FocusToFirstInvalidInput();
            var isContinue = true;
            isContinue = this._options.onSubmit.call(this, this.GetInvalidFields().length, e);
            if (!isContinue) {
                e.preventDefault();
            }
        });
        //Ескли отключена молчаливая загрузка то будет показываться сообщения у первого невалидного поля
        if (!this._options.silentLoad) {
            // 1ms timeout to make sure error shows up
            setTimeout($.proxy(this.FocusToFirstInvalidInput, this), 1);
        }
        //изменение логики отображения идеалформ если ширина формы меньше указанной в проперти adaptiveWidth
        $(window).resize(() => { this._visualAdaptationToSize(); });
        this._visualAdaptationToSize();

        //if (this._options.hideValidateIcons == false) this._$validatingForm.addClass("without-icons");
    }

    public AddValidationRules(rules: Object) {
        /// <summary>
        /// Метод находит все валидируемые поля на основе информации из входяшего параметра,
        /// добавляет в переменную Rules настроек класса входящие правила валидации, запускает
        /// создание HTML экосистемы над каждым полем и проводит валидацию всех найденных полей.
        /// </summary>
        /// <param name="rules" type="Object">
        /// Массив правил валидации с названиями целей валидации вида: "InputName: Rule Rule Rules..."
        /// </param>
        //cобираем селекторы для поиска с помощью джеквери
        var validatedInputsSelectors = $.map(rules, (index, name) => this._nameSelector(name)).join(',');
        var $validatedInputs = this._$validatingForm.find(validatedInputsSelectors);

        $.extend(this._options.rules, rules);

        $validatedInputs.each((index, input) => {
            this._buildFieldFurniture($(input));
        });
        this._$inputs = this._$inputs.add($validatedInputs);
        this._validateAll();
        this._$fields.find(this._options.errorClass).removeClass('active');
    }

    public GetInvalidFields(): JQuery {
        /// <summary>
        /// Возвращает Jquery объект с массивом дивов с классами '.field-ideal' инпуты которых невалидные
        /// </summary>
        return this._$fields.filter(function () {
            return $(this).data(Idealforms.IsValid) === false;
        });
    }

    public FocusToFirstInvalidInput() {
        var $firstInvalidInput = this._getFirstInvalidInput();
        if ($firstInvalidInput.length) {
            this._handleError($firstInvalidInput);
            this._handleStyle($firstInvalidInput);
            $firstInvalidInput.focus();
            this._setFocusToInputGroup($firstInvalidInput);
        }
    }

    public IsValid($input?: JQuery): boolean {
        /// <summary>
        /// Метод проверяет валидна ли вся форма - либо только поле имя которого указано в параметре
        /// </summary>
        if ($input) return !this.GetInvalidFields().find($input).length;
        return !this.GetInvalidFields().length;
    }

    public Reset($input?: JQuery): void {
        /// <summary>
        /// Метод сбрасывает в изначальное состояние все поля - либо только поле имя которого указано в параметре
        /// </summary>
        var $inputs = this._$inputs;
        if ($input) $inputs = this._$inputs.filter($input);

        $inputs.filter('input:not(:checkbox, :radio)').val('');
        $inputs.filter(':checkbox, :radio').prop('checked', false);
        $inputs.filter('select').find('option').prop('selected', function () {
            return this.defaultSelected;
        });

        $inputs.change().each((index, input) => { this._toInitialState($(input)); });
    }

    public Validate($input: JQuery): boolean {
        /// <summary>
        /// Метод применяющий к полю ввода правила валидации
        /// </summary>
        return this._validate($input, true, true);
    }

    public GetValidateField($input: JQuery): JQuery {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем обеспечивающий HTML поддержку плагина
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        ///<returns type="JQuery">Jquery объект с классом .field-ideal</returns>
        return $input.closest(this._options.fieldClass);
    }

    private _validate($input: JQuery, doHandleError?: boolean, doHandleStyle?: boolean): boolean {
        /// <summary>
        /// Корневой метод плагина - координирует всю работу по валидации текущего поля на основе
        /// правил проинициализированных в клиентском коде
        /// </summary>
        /// <param name="$input" type="JQuery">Валидируемое поле ввода данных</param>
        /// <param name="doHandleError" type="bool">
        /// Флаг отражающий необходимо ли отрабатывать логику наполнения и визуализации ошибки
        /// </param>
        /// <param name="doHandleStyle" type="bool">
        /// Флаг отражающий необходимо ли применять соотвествующий стиль к валидируемому полю
        /// </param>

        var $validateField = this.GetValidateField($input),
            //название правил валидации определенные пользователем в клиентском коде
            inputName: string = $input.prop('name'),
            userRulesForField = this._options.rules[inputName].split(Idealforms.RuleSeparator),
            oldStoredValue = $validateField.data(Idealforms.Value),
            isValid = true,
            ruleName: string;

        // Don't validate input if value hasn't changed
        if ($input.is(':checkbox, :radio') && oldStoredValue == $input.val()) {
            return $validateField.data(Idealforms.IsValid);
        }
        $validateField.data(Idealforms.Value, $input.val());

        // Non-required input with empty value must pass validation
        if (!$input.val() && !this._isRequired($input)) {
            $validateField.removeData(Idealforms.IsValid);
            this._toInitialState($input);
        } else {
            $.each(userRulesForField, (index, userRule: string) => {
                //Пользовательское правило может содержать какие либо граничные аргументы, вытаскиваем их из строки,
                //где первым объектом будет является тип правила, а всё остальное аргументы переданные вместе с правилом
                var splittedUserRule = userRule.split(Idealforms.ArgSeparator);

                ruleName = splittedUserRule[0];
                var implementationOfRule = Idealforms.Rules[ruleName],
                    //предпологается что аругменты находятся сразу во 2ой ячейке массива
                    arguments = splittedUserRule.slice(1),
                    errorText: string;

                errorText = this._getErrorText($input, userRule);

                isValid = typeof implementationOfRule == "function" ?
                implementationOfRule.apply(this, [$input, $input.val()].concat(arguments)) :
                implementationOfRule.test($input.val());

                $validateField.data(Idealforms.IsValid, isValid);
                //Если данное правило это не ajax отработка и есть какие либо запросы в списке запросов,
                //то прерываем запрос
                if (ruleName != 'ajax' && Idealforms.Requests[inputName]) {
                    Idealforms.Requests[inputName].abort();
                    $validateField.removeClass("ajax");
                }

                if (doHandleError) this._handleError($input, errorText, isValid);
                if (doHandleStyle && (!this._isFirstLaunch || $input.val())) {
                    this._handleStyle($input, isValid);
                }
                if (this._isFirstLaunch) {
                    this._showValidationIcon($input);
                }
                this._options.onValidate.call(this, $input, ruleName, isValid);
                return isValid;
            });
        }
        return isValid;
    }
    private _toInitialState($input: JQuery): void {
        /// <summary>
        /// Приводим поле idealform к изначальному состоянию
        /// </summary>
        /// <param name="$input" type="JQuery">редактируемое поле</param>
        var $field = this.GetValidateField($input);
        $field.removeClass(this._options.validClass + ' ' + this._options.invalidClass);
        $field.find(this._options.errorClass).removeClass('active');
        if (this._isRequired($input)) {
            this._showValidationIcon($input);
        } else {
            this._hideValidationIcon($input);
        }
    }
    private _showValidationIcon($input: JQuery): void {
        var $field = this.GetValidateField($input),
            $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));

        if (this._options.hideValidateIcons && $field.hasClass(this._options.validClass)) {
            this._hideValidationIcon($input);
            return;
        };
        $icon.addClass('active');
        //для того чтоб анимация активации сработала как полагается ждем 300мс и применяем класс с другой анимацией переходов
        setTimeout(() => {
            $icon.addClass('ease-in-out-animation');
        }, 300);
    }
    private _hideValidationIcon($input: JQuery): void {
        var $field = this.GetValidateField($input),
            $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));
        $icon.removeClass('ease-in-out-animation');
        $icon.removeClass('active');
    }
    private _toJquerySelector(cssClasses: string): string {
        /// <summary>
        /// Метод конвертирует строку подразумевающую набор css классов вида "cssClass cssClass" в строку вида ".cssClass.cssClass" для Jquery селекторов
        /// </summary>
        /// <param name="cssClasses" type="string">строка из перечесления css классов через пробел: "cssClass cssClass"</param>
        ///<returns type="string">строка вида ".cssClass.cssClass"</returns>
        var joinedString = cssClasses.split(' ').join('.');
        var convertedString = joinedString.substring(0, joinedString.length);
        return '.' + convertedString;
    }
    private _getErrorText($input: JQuery, userRule?: string): string {
        /// <summary>
        /// Метод работает как с одним параметром, так и с двумя. Если входным параметром служит
        /// только input то вытаскивается текст ошибки для случая отработки успешно ajax запроса,
        /// вытаскивается текс ошибки предыдущего перед ajax правилом правила. Если указано вторым
        /// параметром пользовательско правило валидации для поля ввода данных, то вытаскивается
        /// ошибка привязанная к этому правилу.
        /// </summary>
        //строка UserRule содержит в себе аргументы правил - поэтому мы её разбиваем в массив
        var separatedUserRule: string[];
        var inputName = $input.prop('name');
        if (userRule) {
            separatedUserRule = userRule.split(Idealforms.ArgSeparator);
        } else {
            var userRulesOfInput = this._options.rules[inputName].split(Idealforms.RuleSeparator),
                userRuleOfInput: string = '',
                indexOfAjax = userRulesOfInput.indexOf('ajax');
            //вытаскиваем пользвательское правило которое идет перед ajax правилом
            if (indexOfAjax != -1) userRuleOfInput = userRulesOfInput[indexOfAjax - 1];

            //если вдруг такого пользовательского правила нет - берем первое правило из списка правил
            if (!userRuleOfInput) userRuleOfInput = userRulesOfInput[0];
            separatedUserRule = userRuleOfInput ? userRuleOfInput.split(Idealforms.ArgSeparator) : [];
        }
        var ruleName = separatedUserRule[0];
        var arguments = separatedUserRule.slice(1),
            errorText: string;

        errorText = Idealforms.Format.apply(null, [
            Idealforms.GetValue('LocalizedInputErrors.' + inputName + '.' + ruleName, Idealforms) ||
            Idealforms.I18N.errors[ruleName]
        ].concat(arguments));
        return errorText;
    }
    private _isRequired($input: JQuery): boolean {
        /// <summary>
        /// Метод проверяющий есть ли в правилах валидации поля ввода правило "Required" - то-есть
        /// обязательно ли данное поле для заполнения
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        if ($input.is(':checkbox, :radio, select')) return true;
        return this._options.rules[$input.prop('name')].indexOf('required') > -1;
    }
    private _setFocusToInputGroup($input: JQuery): void {
        /// <summary>
        /// Метод который добавляет различные классы Фокуса на inputGroup контейнер исходя из
        /// состояния валидации поля - применяется тогда когда input поле обернуто в div.input-group http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="$input" type="JQuery">поле фокус которого надо обработать</param>
        var $field = this.GetValidateField($input),
            $inputGroup = this._getInputGroup($input);
        if ($inputGroup.length && ($input.is(":active") || $input.is(":focus"))) {
            if ($field.hasClass(this._options.validClass)) {
                $inputGroup
                    .removeClass(Idealforms._unstateFocusClass + ' ' + Idealforms._errorFocusClass)
                    .addClass(Idealforms._successFocusClass);
            } else if ($field.hasClass(this._options.invalidClass)) {
                $inputGroup
                    .removeClass(Idealforms._unstateFocusClass + ' ' + Idealforms._successFocusClass)
                    .addClass(Idealforms._errorFocusClass);
            } else {
                $inputGroup
                    .removeClass(Idealforms._successFocusClass + ' ' + Idealforms._errorFocusClass)
                    .addClass(Idealforms._unstateFocusClass);
            }
        } else if ($inputGroup) {
            $inputGroup.removeClass(Idealforms._successFocusClass + ' ' + Idealforms._errorFocusClass + ' ' + Idealforms._unstateFocusClass);
        }
    }
    private _getInputGroup($input: JQuery): JQuery {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем
        /// обеспечивающий HTML поддержку фишечки бутстрапа http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        ///<returns type="JQuery">Jquery объект с классом .input-group</returns>
        return $input.closest('.input-group');
    }

    private _changeIconByValidationState($input: JQuery): void {
        /// <summary>
        /// Метод который меняет вид иконки исходя из состояния валидации входного поля
        /// </summary>
        /// <param name="$input" type="JQuery">поле рядом с которым расположенна иконка</param>
        var $field = this.GetValidateField($input),
            $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));
        if ($field.hasClass('ajax')) {
            $icon.removeClass(this._options.iconSuccessClass + ' ' + this._options.iconExclamationClass)
                .addClass(this._options.iconValidatingClass);
        } else {
            $icon.removeClass(this._options.iconValidatingClass);
        }
        if (this._options.hideValidateIcons) return;

        if ($field.hasClass(this._options.validClass)) {
            $icon.removeClass(this._options.iconValidatingClass + ' ' + this._options.iconExclamationClass)
                .addClass(this._options.iconSuccessClass);
        } else if ($field.hasClass(this._options.invalidClass)) {
            $icon.removeClass(this._options.iconValidatingClass + ' ' + this._options.iconSuccessClass)
                .addClass(this._options.iconExclamationClass);
        }
    }
    private _initializeI18N(): void {
        /// <summary>
        /// Локализация текущего списка текстов ошибок, на основе последней найденой ссылки на файл
        /// с локализацией
        /// </summary>
        //$.each(Idealforms.I18N, (localeName: string, localization: IIdealformLocale) => {
        //    var localizedErrors = <IErrors>localization.errors,
        //        localizedCustomInputErrors = localization.customInputErrors;

        //    delete localization.customInputErrors;
        //    delete localization.errors; 

        //    Idealforms.LocalizedErrors = localizedErrors;

        //    $.extend(Idealforms.LocalizedInputErrors, localizedCustomInputErrors);
        //});
    }
    private _doAjaxExtension(): void {
        /// <summary>
        /// Метод содержащий логику обработки Ajax правила
        /// </summary>
        $.extend(Idealforms.Rules, {
            ajax: ($input: JQuery) => {
                var $validationField = this.GetValidateField($input),
                    inputName = $input.prop('name'),
                    ajaxUrl = $input.data('val-remote-url'),
                    ajaxErrorText = Idealforms.GetValue('LocalizedInputErrors.' + inputName + '.ajaxError', Idealforms),
                    previousErrorText = this._getErrorText($input),
                    requests = Idealforms.Requests,
                    data = {};

                data[inputName] = $input.val();
                $validationField.addClass('ajax');
                if (requests[inputName]) requests[inputName].abort();
                
                requests[inputName] = $.ajax(
                    {
                        type: "POST",
                        url: ajaxUrl,
                        data: data,
                        dataType: "json",
                        success: (response) => {
                            if (response === true) {
                                $validationField.data(Idealforms.IsValid, true);
                                this._handleError($input, previousErrorText);
                            } else {
                                $validationField.data(Idealforms.IsValid, false);
                                this._handleError($input, ajaxErrorText);
                            }
                            this._handleStyle($input);
                            this._options.onValidate.call(this, $input, 'ajax', response);
                            $validationField.removeClass('ajax');

                            this._changeIconByValidationState($input);
                        }
                    });
                return true;
            }
        });
    }
    private _extractValidationRulesFromMarkup(): void {
        /// <summary>
        /// Изъятие правил валидации из HTML разметки указаной в атриббутe "data-idealforms-rules"
        /// валидируемого поля и добавление его в текущий экземпляр
        /// </summary>
        var userRules: Object = {};

        this._$validatingForm.find('input, select, textarea').each(function () {
            var currentRuleContainer = $(this);
            var fieldUserRules = currentRuleContainer.data('idealforms-rules');
            if (fieldUserRules && !userRules[this.name]) userRules[this.name] = fieldUserRules;
        });
        this.AddValidationRules(userRules);
    }
    private _validateAll(): void {
        this._$inputs.each((index, input) => {
            this._validate($(input), true, true);
        });
    }
    private _visualAdaptationToSize(): void {
        /// <summary>
        /// Адаптация визуального отображения HTML оснастки плагина на основе заданного в настройках
        /// параметра adaptiveWidth
        /// </summary>
        var formParentWidth = this._$validatingForm.parent().width(),
            formWidth = this._$validatingForm.outerWidth(),
            isAdaptive = this._options.adaptiveWidth > formWidth && formWidth == formParentWidth;
        this._$validatingForm.toggleClass('adaptive', isAdaptive);
    }
    private _buildFieldFurniture($input: JQuery): void {
        /// <summary>
        /// Метод добавляет в HTML контейнер (div.field-ideal) валидируемого поля необходимую HTML
        /// оснастку для поддержки работы плагина. Добавляет иконку валидации, необходимые классы и
        /// прикрепляет обработчики событий.
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле над которым производится оснастка</param>
        var $validateField = this.GetValidateField($input),
            $icon;

        $icon = $(this._options.iconHtml, {
            "class": this._options.iconBaseClass + ' ' + this._options.iconExclamationClass,
            click: () => {
                $input.focus();
                this._setFocusToInputGroup($input);
            }
        });
        if (!this._$fields.filter($validateField).length) {
            this._$fields = this._$fields.add($validateField);
            if (this._options.iconHtml) $validateField.append($icon);
            $validateField.addClass('idealforms-field idealforms-field-' + $input.prop('type'));
        }
        this._attachEventsHandlers($input);
    }
    private _attachEventsHandlers($input: JQuery): void {
        /// <summary>
        /// Добавление обработчиков события к валидируемому полю
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        var $field = this.GetValidateField($input);
        $input.on('change keyup', (e) => {
            if (e.which == 9 || e.which == 16) return;
            this._isFirstLaunch = false;
            this._validate($input, true, true);
            this._setFocusToInputGroup($input);
        }).focus((e) => {
                if (!this.IsValid($input)) {
                    this.ShowErrorHint($input);
                }
                this._setFocusToInputGroup($input);
            }).blur(() => {
                this.HideErrorHint($input);
                this._setFocusToInputGroup($input);
            });
    }
    public ShowErrorHint($input: JQuery): void {
        var $field = this.GetValidateField($input);
        $field.find(this._options.errorClass).addClass('active');
    }
    public HideErrorHint($input: JQuery): void {
        var $field = this.GetValidateField($input);
        $field.find(this._options.errorClass).removeClass('active');
    }
    private _handleError($input: JQuery, errorText?: string, isValid?: boolean): void {
        /// <summary>
        /// В Методе собрана логика добавления нового текста Ошибки в контейнер занимающийся
        /// отображением ошибки, и показ этого контейнера исходя из состояния входящего параметра
        /// </summary>
        /// <param name="$input" type="JQuery">
        /// Текущее валидируемое поле рядом с которым нужно отработать логику
        /// </param>
        /// <param name="errorText" type="string">Текст текущей ошибки</param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>

        isValid = isValid || this.IsValid($input);
        var $errorContainer = this.GetValidateField($input).find(this._options.errorClass);
        $errorContainer.removeClass('active');
        if (errorText) {
            $errorContainer.text(errorText);
            $errorContainer.addClass('active');
        }
        if (isValid) {
            $errorContainer.removeClass('active');
        }
    }
    private _handleStyle($input: JQuery, isValid?: boolean) {
        /// <summary>
        /// В Методе собрана логика изменения стиля валидируемого поля исходя из того валидно или не
        /// валидно данное поле.
        /// </summary>
        /// <param name="$input" type="JQuery">
        /// Текущее валидируемое поле визуализацию которого нужно изменить
        /// </param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>
        isValid = isValid || this.IsValid($input);
        this.GetValidateField($input)
            .removeClass(this._options.validClass + ' ' + this._options.invalidClass)
            .addClass(isValid ? this._options.validClass : this._options.invalidClass);

        
        this._showValidationIcon($input);
        this._changeIconByValidationState($input);
    }
    private _nameSelector(name: string): string {
        return '[name="' + name + '"]';
    }

    private _getFirstInvalidInput(): JQuery {
        return this.GetInvalidFields().first().find('input:first, textarea, select');
    }

    //#region Static functions
    static Format(str: string) {
        var args = [].slice.call(arguments, 1);
        return str.replace(/\{(\d)\}/g, function (_, match) {
            return args[+match] || '';
        }).replace(/\{\*([^*}]*)\}/g, function (_, sep) {
                return args.join(sep || ', ');
            });
    }
    static GetValue(key: string, objectOfSearch) {
        return key.split('.').reduce((a, b) => {
            return a && a[b];
        }, objectOfSearch);
    }
    static GetInstance($childOfForm: JQuery): IIdealforms {
        var $idealform = $childOfForm.closest('.idealforms');
        if ($idealform) {
            var idealform = $idealform.data('idealforms');
            return idealform;
        }
        return null;
    }
    static I18N: IIdealformI18N = IdealformI18N;
    static RuleSeparator = ' ';
    static ArgSeparator = ':';
    /** 'idealforms-valid' - константа для изъятия информация из data хранилищ тегов*/
    static IsValid = 'idealforms-valid';
    /** 'idealforms-value' - константа для изъятия информация из data хранилищ тегов*/
    static Value = 'idealforms-value';
    /** Массив хранящий исполняемые запросы на сервер - в случае если нам надо будет его отменить*/
    static Requests: IRequests = {};

    static Rules = {
        required: /.+/,
        digits: /^\d+$/,
        email: /^[^@]+@[^@]+\..{2,6}$/,
        username: /^[a-z](?=[\w.]{3,31}$)\w*\.?\w*$/i,
        name: /^[^\d \$*_]{2,}$/,
        weakpass: /^[^ \$*_]{5,}$/,
        pass: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
        strongpass: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        //phone: /^[0-9]{10}$/,
        zip: /^\d{5}$|^\d{5}-\d{4}$/,
        url: /^(?:(ftp|http|https):\/\/)?(?:[\w\-]+\.)+[a-z]{2,6}([\:\/?#].*)?$/i,
        birthdate: /^[0-9]{2}.[0-9]{2}.[0-9]{4}$/,
        snils: /^[0-9]{9}$/,
        numorder: /^[0-9]{2}$/,

        phone: (input, value) => {
            //используем точно такое же регулярное выражение как на сервере проверяющие  телефон -
            //разве что джаваскрипт не поддерживает просмотр назад в выражение поэтому регулярка изменена в обратном направлении.
            //поэтому пришлось использовать метод который реверсирует проверяемое значение
            var phoneRegex = new RegExp("^(\\d+\\s?(x|\\.txe?)\\s?)?((\\)(\\d+[\\s\\-\\.]?)?\\d+\\(|\\d+)[\\s\\-\\.]?)*(\\)([\\s\\-\\.]?\\d+)?\\d+\\+?\\((?!\\+.*)|\\d+)(\\s?\\+)?$", "i");
            var reversedValue = value.split("").reverse().join("");
            return phoneRegex.test(reversedValue);
        },
        phoneMin: (input: JQuery, value): boolean => value.length >= 8,

        number: (input: JQuery, value): boolean => !isNaN(value),

        range: (input: JQuery, value, min: number, max: number): boolean => {
            return Number(value) >= min && Number(value) <= max;
        },

        min: (input: JQuery, value, min: number): boolean => value.length >= min,

        max: (input: JQuery, value, max: number): boolean => value.length <= max,

        minoption: (input: JQuery, value, min: number): boolean => input.filter(':checked').length >= min,

        maxoption: (input: JQuery, value, max: number): boolean => input.filter(':checked').length <= max,

        minmax: (input: JQuery, value, min, max) => value.length >= min && value.length <= max,

        select: (input: JQuery, value, def) => value != def,

        extension: ($input: JQuery) => {
            var extensions = [].slice.call(arguments, 1),
                valid = false;
            var input = <HTMLInputElement> $input.get(0);
            $.each(input.files || [{ name: input.value }], (i, file) => {
                valid = $.inArray(file.name.split('.').pop().toLowerCase(), extensions) > -1;
            });
            return valid;
        },

        equalto: function (input: JQuery, value, target) {
            var $target = $('[name="' + target + '"]');
            debugger;
            var idealform: IIdealforms = this;
            if (idealform.GetInvalidFields().find($target).length) return false;

            $target.off('keyup.equalto').on('keyup.equalto', () => {
                idealform.GetValidateField(input).removeData('idealforms-value');
                idealform.Validate(input);
            });

            return input.val() == $target.val();
        },

        date: function (input: JQuery, value, format) {
            format = format || 'dd.mm.yyyy';

            var delimiter = /[^mdy]/.exec(format)[0],
                theFormat = format.split(delimiter),
                theDate = value.split(delimiter);

            function isDate(date, format) {
                var m, d, y;

                for (var i = 0, len = format.length; i < len; i++) {
                    if (/m/.test(format[i])) m = date[i];
                    if (/d/.test(format[i])) d = date[i];
                    if (/y/.test(format[i])) y = date[i];
                }

                if (!m || !d || !y) return false;

                return m > 0 && m < 13 &&
                    y && y.length == 4 &&
                    d > 0 && d <= (new Date(y, m, 0)).getDate();
            }

            return isDate(theDate, theFormat);
        }
    }
    //#endregion
};

$.fn['idealforms'] = function (options: IIdealformOptions) {
    var someForms = this;
    someForms.each(function () {
        var $someForm = $(this);
        var idealform = new Idealforms($someForm, options);
        return $.data(this, 'idealforms', idealform);
    });
    return someForms;
}

