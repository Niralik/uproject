﻿/// <reference path="localization/idealforms.l8on.d.ts" />
interface IIdealformOptions {
    /**CSS класс полей к которым будет применена валидация*/
    fieldClass: string;
    /**СSS класс див контейнера в котором будет хранится ошибка валидации */
    errorClass: string;
    /** Флаг отмечающий будут ли показываться иконки валидации - по умолчанию true*/
    hideValidateIcons: boolean;
    /** HTML тег иконки валидации*/
    iconHtml: string;
    iconBaseClass: string;
    iconExclamationClass: string;
    iconSuccessClass: string;
    iconValidatingClass: string;
    invalidClass: string;
    validClass: string;
    /**Флаг обозначающий что форма загрузится без валидирования существующих полей*/
    silentLoad: boolean;
    adaptiveWidth: number;
    /**Событие возникающие после валидации текущего поля */
    onValidate($currentInput: JQuery, ruleName: string, isValid: boolean): any;
    /**Событие возникающие при сабмите формы */
    onSubmit(numOfInvalid: number, event: Event): boolean;
    /**Список полей с именами правил валидации*/
    rules: IUserRules;
    errors: Object;
}
interface IUserRules {
    [inputName: string]: string;
}
interface IIdealforms {
    Initialization(): void;
    AddValidationRules(rules: any): void;
    GetInvalidFields(): JQuery;
    FocusToFirstInvalidInput(): void;
    IsValid($input?: JQuery): boolean;
    Reset($input?: JQuery): void;
    Validate($input: JQuery): boolean;
    GetValidateField($input: JQuery): JQuery;
}
interface IRequests {
    [inputName: string]: JQueryXHR;
}
declare class Idealforms implements IIdealforms {
    private _$validatingForm;
    private _defaultOptions;
    private _options;
    private _$fields;
    private _$inputs;
    private _isFirstLaunch;
    private static _unstateFocusClass;
    private static _errorFocusClass;
    private static _successFocusClass;
    constructor($targetForm: JQuery, options: IIdealformOptions);
    public Initialization(): void;
    public AddValidationRules(rules: Object): void;
    public GetInvalidFields(): JQuery;
    public FocusToFirstInvalidInput(): void;
    public IsValid($input?: JQuery): boolean;
    public Reset($input?: JQuery): void;
    public Validate($input: JQuery): boolean;
    public GetValidateField($input: JQuery): JQuery;
    private _validate($input, doHandleError?, doHandleStyle?);
    private _toInitialState($input);
    private _showValidationIcon($input);
    private _hideValidationIcon($input);
    private _toJquerySelector(cssClasses);
    private _getErrorText($input, userRule?);
    private _isRequired($input);
    private _setFocusToInputGroup($input);
    private _getInputGroup($input);
    private _changeIconByValidationState($input);
    private _initializeI18N();
    private _doAjaxExtension();
    private _extractValidationRulesFromMarkup();
    private _validateAll();
    private _visualAdaptationToSize();
    private _buildFieldFurniture($input);
    private _attachEventsHandlers($input);
    public ShowErrorHint($input: JQuery): void;
    public HideErrorHint($input: JQuery): void;
    private _handleError($input, errorText?, isValid?);
    private _handleStyle($input, isValid?);
    private _nameSelector(name);
    private _getFirstInvalidInput();
    static Format(str: string): string;
    static GetValue(key: string, objectOfSearch: any): string;
    static GetInstance($childOfForm: JQuery): IIdealforms;
    static I18N: IIdealformI18N;
    static RuleSeparator: string;
    static ArgSeparator: string;
    /** 'idealforms-valid' - константа для изъятия информация из data хранилищ тегов*/
    static IsValid: string;
    /** 'idealforms-value' - константа для изъятия информация из data хранилищ тегов*/
    static Value: string;
    /** Массив хранящий исполняемые запросы на сервер - в случае если нам надо будет его отменить*/
    static Requests: IRequests;
    static Rules: {
        required: RegExp;
        digits: RegExp;
        email: RegExp;
        username: RegExp;
        name: RegExp;
        weakpass: RegExp;
        pass: RegExp;
        strongpass: RegExp;
        zip: RegExp;
        url: RegExp;
        birthdate: RegExp;
        snils: RegExp;
        numorder: RegExp;
        phone: (input: any, value: any) => boolean;
        phoneMin: (input: JQuery, value: any) => boolean;
        number: (input: JQuery, value: any) => boolean;
        range: (input: JQuery, value: any, min: number, max: number) => boolean;
        min: (input: JQuery, value: any, min: number) => boolean;
        max: (input: JQuery, value: any, max: number) => boolean;
        minoption: (input: JQuery, value: any, min: number) => boolean;
        maxoption: (input: JQuery, value: any, max: number) => boolean;
        minmax: (input: JQuery, value: any, min: any, max: any) => boolean;
        select: (input: JQuery, value: any, def: any) => boolean;
        extension: ($input: JQuery) => boolean;
        equalto: (input: JQuery, value: any, target: any) => boolean;
        date: (input: JQuery, value: any, format: any) => boolean;
    };
}
