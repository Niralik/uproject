﻿/// <reference path="localization/idealforms.l8on.d.ts" />

//interface IErrorsLocale {
//    [ruleName: string]: string;
//}
var Idealforms = (function () {
    function Idealforms($targetForm, options) {
        this._defaultOptions = {
            fieldClass: '.field-ideal',
            errorClass: '.error',
            hideValidateIcons: true,
            iconHtml: '<i/>',
            iconBaseClass: 'fa icon',
            iconExclamationClass: 'fa-exclamation',
            iconSuccessClass: 'fa-check',
            iconValidatingClass: 'fa-circle-o-notch icon-validation',
            invalidClass: 'has-error',
            validClass: 'has-success',
            silentLoad: true,
            adaptiveWidth: 500,
            onValidate: $.noop,
            onSubmit: $.noop,
            rules: {},
            errors: {}
        };
        this._$fields = $();
        this._$inputs = $();
        this._isFirstLaunch = true;
        this._$validatingForm = $targetForm;
        this._options = $.extend({}, this._defaultOptions, options);
        this.Initialization();
    }
    Idealforms.prototype.Initialization = function () {
        var _this = this;
        this._initializeI18N();
        this._doAjaxExtension();
        this._extractValidationRulesFromMarkup();

        this.AddValidationRules(this._options.rules || {});

        //Отработка сабмита формы
        this._$validatingForm.submit(function (e) {
            _this._validateAll();
            _this.FocusToFirstInvalidInput();
            var isContinue = true;
            isContinue = _this._options.onSubmit.call(_this, _this.GetInvalidFields().length, e);
            if (!isContinue) {
                e.preventDefault();
            }
        });

        //Ескли отключена молчаливая загрузка то будет показываться сообщения у первого невалидного поля
        if (!this._options.silentLoad) {
            // 1ms timeout to make sure error shows up
            setTimeout($.proxy(this.FocusToFirstInvalidInput, this), 1);
        }

        //изменение логики отображения идеалформ если ширина формы меньше указанной в проперти adaptiveWidth
        $(window).resize(function () {
            _this._visualAdaptationToSize();
        });
        this._visualAdaptationToSize();
        //if (this._options.hideValidateIcons == false) this._$validatingForm.addClass("without-icons");
    };

    Idealforms.prototype.AddValidationRules = function (rules) {
        var _this = this;
        /// <summary>
        /// Метод находит все валидируемые поля на основе информации из входяшего параметра,
        /// добавляет в переменную Rules настроек класса входящие правила валидации, запускает
        /// создание HTML экосистемы над каждым полем и проводит валидацию всех найденных полей.
        /// </summary>
        /// <param name="rules" type="Object">
        /// Массив правил валидации с названиями целей валидации вида: "InputName: Rule Rule Rules..."
        /// </param>
        //cобираем селекторы для поиска с помощью джеквери
        var validatedInputsSelectors = $.map(rules, function (index, name) {
            return _this._nameSelector(name);
        }).join(',');
        var $validatedInputs = this._$validatingForm.find(validatedInputsSelectors);

        $.extend(this._options.rules, rules);

        $validatedInputs.each(function (index, input) {
            _this._buildFieldFurniture($(input));
        });
        this._$inputs = this._$inputs.add($validatedInputs);
        this._validateAll();
        this._$fields.find(this._options.errorClass).removeClass('active');
    };

    Idealforms.prototype.GetInvalidFields = function () {
        /// <summary>
        /// Возвращает Jquery объект с массивом дивов с классами '.field-ideal' инпуты которых невалидные
        /// </summary>
        return this._$fields.filter(function () {
            return $(this).data(Idealforms.IsValid) === false;
        });
    };

    Idealforms.prototype.FocusToFirstInvalidInput = function () {
        var $firstInvalidInput = this._getFirstInvalidInput();
        if ($firstInvalidInput.length) {
            this._handleError($firstInvalidInput);
            this._handleStyle($firstInvalidInput);
            $firstInvalidInput.focus();
            this._setFocusToInputGroup($firstInvalidInput);
        }
    };

    Idealforms.prototype.IsValid = function ($input) {
        /// <summary>
        /// Метод проверяет валидна ли вся форма - либо только поле имя которого указано в параметре
        /// </summary>
        if ($input)
            return !this.GetInvalidFields().find($input).length;
        return !this.GetInvalidFields().length;
    };

    Idealforms.prototype.Reset = function ($input) {
        var _this = this;
        /// <summary>
        /// Метод сбрасывает в изначальное состояние все поля - либо только поле имя которого указано в параметре
        /// </summary>
        var $inputs = this._$inputs;
        if ($input)
            $inputs = this._$inputs.filter($input);

        $inputs.filter('input:not(:checkbox, :radio)').val('');
        $inputs.filter(':checkbox, :radio').prop('checked', false);
        $inputs.filter('select').find('option').prop('selected', function () {
            return this.defaultSelected;
        });

        $inputs.change().each(function (index, input) {
            _this._toInitialState($(input));
        });
    };

    Idealforms.prototype.Validate = function ($input) {
        /// <summary>
        /// Метод применяющий к полю ввода правила валидации
        /// </summary>
        return this._validate($input, true, true);
    };

    Idealforms.prototype.GetValidateField = function ($input) {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем обеспечивающий HTML поддержку плагина
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        ///<returns type="JQuery">Jquery объект с классом .field-ideal</returns>
        return $input.closest(this._options.fieldClass);
    };

    Idealforms.prototype._validate = function ($input, doHandleError, doHandleStyle) {
        /// <summary>
        /// Корневой метод плагина - координирует всю работу по валидации текущего поля на основе
        /// правил проинициализированных в клиентском коде
        /// </summary>
        /// <param name="$input" type="JQuery">Валидируемое поле ввода данных</param>
        /// <param name="doHandleError" type="bool">
        /// Флаг отражающий необходимо ли отрабатывать логику наполнения и визуализации ошибки
        /// </param>
        /// <param name="doHandleStyle" type="bool">
        /// Флаг отражающий необходимо ли применять соотвествующий стиль к валидируемому полю
        /// </param>
        var _this = this;
        var $validateField = this.GetValidateField($input), inputName = $input.prop('name'), userRulesForField = this._options.rules[inputName].split(Idealforms.RuleSeparator), oldStoredValue = $validateField.data(Idealforms.Value), isValid = true, ruleName;

        // Don't validate input if value hasn't changed
        if ($input.is(':checkbox, :radio') && oldStoredValue == $input.val()) {
            return $validateField.data(Idealforms.IsValid);
        }
        $validateField.data(Idealforms.Value, $input.val());

        // Non-required input with empty value must pass validation
        if (!$input.val() && !this._isRequired($input)) {
            $validateField.removeData(Idealforms.IsValid);
            this._toInitialState($input);
        } else {
            $.each(userRulesForField, function (index, userRule) {
                //Пользовательское правило может содержать какие либо граничные аргументы, вытаскиваем их из строки,
                //где первым объектом будет является тип правила, а всё остальное аргументы переданные вместе с правилом
                var splittedUserRule = userRule.split(Idealforms.ArgSeparator);

                ruleName = splittedUserRule[0];
                var implementationOfRule = Idealforms.Rules[ruleName], arguments = splittedUserRule.slice(1), errorText;

                errorText = _this._getErrorText($input, userRule);

                isValid = typeof implementationOfRule == "function" ? implementationOfRule.apply(_this, [$input, $input.val()].concat(arguments)) : implementationOfRule.test($input.val());

                $validateField.data(Idealforms.IsValid, isValid);

                //Если данное правило это не ajax отработка и есть какие либо запросы в списке запросов,
                //то прерываем запрос
                if (ruleName != 'ajax' && Idealforms.Requests[inputName]) {
                    Idealforms.Requests[inputName].abort();
                    $validateField.removeClass("ajax");
                }

                if (doHandleError)
                    _this._handleError($input, errorText, isValid);
                if (doHandleStyle && (!_this._isFirstLaunch || $input.val())) {
                    _this._handleStyle($input, isValid);
                }
                if (_this._isFirstLaunch) {
                    _this._showValidationIcon($input);
                }
                _this._options.onValidate.call(_this, $input, ruleName, isValid);
                return isValid;
            });
        }
        return isValid;
    };
    Idealforms.prototype._toInitialState = function ($input) {
        /// <summary>
        /// Приводим поле idealform к изначальному состоянию
        /// </summary>
        /// <param name="$input" type="JQuery">редактируемое поле</param>
        var $field = this.GetValidateField($input);
        $field.removeClass(this._options.validClass + ' ' + this._options.invalidClass);
        $field.find(this._options.errorClass).removeClass('active');
        if (this._isRequired($input)) {
            this._showValidationIcon($input);
        } else {
            this._hideValidationIcon($input);
        }
    };
    Idealforms.prototype._showValidationIcon = function ($input) {
        var $field = this.GetValidateField($input), $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));

        if (this._options.hideValidateIcons && $field.hasClass(this._options.validClass)) {
            this._hideValidationIcon($input);
            return;
        }
        ;
        $icon.addClass('active');

        //для того чтоб анимация активации сработала как полагается ждем 300мс и применяем класс с другой анимацией переходов
        setTimeout(function () {
            $icon.addClass('ease-in-out-animation');
        }, 300);
    };
    Idealforms.prototype._hideValidationIcon = function ($input) {
        var $field = this.GetValidateField($input), $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));
        $icon.removeClass('ease-in-out-animation');
        $icon.removeClass('active');
    };
    Idealforms.prototype._toJquerySelector = function (cssClasses) {
        /// <summary>
        /// Метод конвертирует строку подразумевающую набор css классов вида "cssClass cssClass" в строку вида ".cssClass.cssClass" для Jquery селекторов
        /// </summary>
        /// <param name="cssClasses" type="string">строка из перечесления css классов через пробел: "cssClass cssClass"</param>
        ///<returns type="string">строка вида ".cssClass.cssClass"</returns>
        var joinedString = cssClasses.split(' ').join('.');
        var convertedString = joinedString.substring(0, joinedString.length);
        return '.' + convertedString;
    };
    Idealforms.prototype._getErrorText = function ($input, userRule) {
        /// <summary>
        /// Метод работает как с одним параметром, так и с двумя. Если входным параметром служит
        /// только input то вытаскивается текст ошибки для случая отработки успешно ajax запроса,
        /// вытаскивается текс ошибки предыдущего перед ajax правилом правила. Если указано вторым
        /// параметром пользовательско правило валидации для поля ввода данных, то вытаскивается
        /// ошибка привязанная к этому правилу.
        /// </summary>
        //строка UserRule содержит в себе аргументы правил - поэтому мы её разбиваем в массив
        var separatedUserRule;
        var inputName = $input.prop('name');
        if (userRule) {
            separatedUserRule = userRule.split(Idealforms.ArgSeparator);
        } else {
            var userRulesOfInput = this._options.rules[inputName].split(Idealforms.RuleSeparator), userRuleOfInput = '', indexOfAjax = userRulesOfInput.indexOf('ajax');

            //вытаскиваем пользвательское правило которое идет перед ajax правилом
            if (indexOfAjax != -1)
                userRuleOfInput = userRulesOfInput[indexOfAjax - 1];

            //если вдруг такого пользовательского правила нет - берем первое правило из списка правил
            if (!userRuleOfInput)
                userRuleOfInput = userRulesOfInput[0];
            separatedUserRule = userRuleOfInput ? userRuleOfInput.split(Idealforms.ArgSeparator) : [];
        }
        var ruleName = separatedUserRule[0];
        var arguments = separatedUserRule.slice(1), errorText;

        errorText = Idealforms.Format.apply(null, [
            Idealforms.GetValue('LocalizedInputErrors.' + inputName + '.' + ruleName, Idealforms) || Idealforms.I18N.errors[ruleName]
        ].concat(arguments));
        return errorText;
    };
    Idealforms.prototype._isRequired = function ($input) {
        /// <summary>
        /// Метод проверяющий есть ли в правилах валидации поля ввода правило "Required" - то-есть
        /// обязательно ли данное поле для заполнения
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        if ($input.is(':checkbox, :radio, select'))
            return true;
        return this._options.rules[$input.prop('name')].indexOf('required') > -1;
    };
    Idealforms.prototype._setFocusToInputGroup = function ($input) {
        /// <summary>
        /// Метод который добавляет различные классы Фокуса на inputGroup контейнер исходя из
        /// состояния валидации поля - применяется тогда когда input поле обернуто в div.input-group http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="$input" type="JQuery">поле фокус которого надо обработать</param>
        var $field = this.GetValidateField($input), $inputGroup = this._getInputGroup($input);
        if ($inputGroup.length && ($input.is(":active") || $input.is(":focus"))) {
            if ($field.hasClass(this._options.validClass)) {
                $inputGroup.removeClass(Idealforms._unstateFocusClass + ' ' + Idealforms._errorFocusClass).addClass(Idealforms._successFocusClass);
            } else if ($field.hasClass(this._options.invalidClass)) {
                $inputGroup.removeClass(Idealforms._unstateFocusClass + ' ' + Idealforms._successFocusClass).addClass(Idealforms._errorFocusClass);
            } else {
                $inputGroup.removeClass(Idealforms._successFocusClass + ' ' + Idealforms._errorFocusClass).addClass(Idealforms._unstateFocusClass);
            }
        } else if ($inputGroup) {
            $inputGroup.removeClass(Idealforms._successFocusClass + ' ' + Idealforms._errorFocusClass + ' ' + Idealforms._unstateFocusClass);
        }
    };
    Idealforms.prototype._getInputGroup = function ($input) {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем
        /// обеспечивающий HTML поддержку фишечки бутстрапа http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        ///<returns type="JQuery">Jquery объект с классом .input-group</returns>
        return $input.closest('.input-group');
    };

    Idealforms.prototype._changeIconByValidationState = function ($input) {
        /// <summary>
        /// Метод который меняет вид иконки исходя из состояния валидации входного поля
        /// </summary>
        /// <param name="$input" type="JQuery">поле рядом с которым расположенна иконка</param>
        var $field = this.GetValidateField($input), $icon = $field.find(this._toJquerySelector(this._options.iconBaseClass));
        if ($field.hasClass('ajax')) {
            $icon.removeClass(this._options.iconSuccessClass + ' ' + this._options.iconExclamationClass).addClass(this._options.iconValidatingClass);
        } else {
            $icon.removeClass(this._options.iconValidatingClass);
        }
        if (this._options.hideValidateIcons)
            return;

        if ($field.hasClass(this._options.validClass)) {
            $icon.removeClass(this._options.iconValidatingClass + ' ' + this._options.iconExclamationClass).addClass(this._options.iconSuccessClass);
        } else if ($field.hasClass(this._options.invalidClass)) {
            $icon.removeClass(this._options.iconValidatingClass + ' ' + this._options.iconSuccessClass).addClass(this._options.iconExclamationClass);
        }
    };
    Idealforms.prototype._initializeI18N = function () {
        /// <summary>
        /// Локализация текущего списка текстов ошибок, на основе последней найденой ссылки на файл
        /// с локализацией
        /// </summary>
        //$.each(Idealforms.I18N, (localeName: string, localization: IIdealformLocale) => {
        //    var localizedErrors = <IErrors>localization.errors,
        //        localizedCustomInputErrors = localization.customInputErrors;
        //    delete localization.customInputErrors;
        //    delete localization.errors;
        //    Idealforms.LocalizedErrors = localizedErrors;
        //    $.extend(Idealforms.LocalizedInputErrors, localizedCustomInputErrors);
        //});
    };
    Idealforms.prototype._doAjaxExtension = function () {
        var _this = this;
        /// <summary>
        /// Метод содержащий логику обработки Ajax правила
        /// </summary>
        $.extend(Idealforms.Rules, {
            ajax: function ($input) {
                var $validationField = _this.GetValidateField($input), inputName = $input.prop('name'), ajaxUrl = $input.data('val-remote-url'), ajaxErrorText = Idealforms.GetValue('LocalizedInputErrors.' + inputName + '.ajaxError', Idealforms), previousErrorText = _this._getErrorText($input), requests = Idealforms.Requests, data = {};

                data[inputName] = $input.val();
                $validationField.addClass('ajax');
                if (requests[inputName])
                    requests[inputName].abort();

                requests[inputName] = $.ajax({
                    type: "POST",
                    url: ajaxUrl,
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        if (response === true) {
                            $validationField.data(Idealforms.IsValid, true);
                            _this._handleError($input, previousErrorText);
                        } else {
                            $validationField.data(Idealforms.IsValid, false);
                            _this._handleError($input, ajaxErrorText);
                        }
                        _this._handleStyle($input);
                        _this._options.onValidate.call(_this, $input, 'ajax', response);
                        $validationField.removeClass('ajax');

                        _this._changeIconByValidationState($input);
                    }
                });
                return true;
            }
        });
    };
    Idealforms.prototype._extractValidationRulesFromMarkup = function () {
        /// <summary>
        /// Изъятие правил валидации из HTML разметки указаной в атриббутe "data-idealforms-rules"
        /// валидируемого поля и добавление его в текущий экземпляр
        /// </summary>
        var userRules = {};

        this._$validatingForm.find('input, select, textarea').each(function () {
            var currentRuleContainer = $(this);
            var fieldUserRules = currentRuleContainer.data('idealforms-rules');
            if (fieldUserRules && !userRules[this.name])
                userRules[this.name] = fieldUserRules;
        });
        this.AddValidationRules(userRules);
    };
    Idealforms.prototype._validateAll = function () {
        var _this = this;
        this._$inputs.each(function (index, input) {
            _this._validate($(input), true, true);
        });
    };
    Idealforms.prototype._visualAdaptationToSize = function () {
        /// <summary>
        /// Адаптация визуального отображения HTML оснастки плагина на основе заданного в настройках
        /// параметра adaptiveWidth
        /// </summary>
        var formParentWidth = this._$validatingForm.parent().width(), formWidth = this._$validatingForm.outerWidth(), isAdaptive = this._options.adaptiveWidth > formWidth && formWidth == formParentWidth;
        this._$validatingForm.toggleClass('adaptive', isAdaptive);
    };
    Idealforms.prototype._buildFieldFurniture = function ($input) {
        var _this = this;
        /// <summary>
        /// Метод добавляет в HTML контейнер (div.field-ideal) валидируемого поля необходимую HTML
        /// оснастку для поддержки работы плагина. Добавляет иконку валидации, необходимые классы и
        /// прикрепляет обработчики событий.
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле над которым производится оснастка</param>
        var $validateField = this.GetValidateField($input), $icon;

        $icon = $(this._options.iconHtml, {
            "class": this._options.iconBaseClass + ' ' + this._options.iconExclamationClass,
            click: function () {
                $input.focus();
                _this._setFocusToInputGroup($input);
            }
        });
        if (!this._$fields.filter($validateField).length) {
            this._$fields = this._$fields.add($validateField);
            if (this._options.iconHtml)
                $validateField.append($icon);
            $validateField.addClass('idealforms-field idealforms-field-' + $input.prop('type'));
        }
        this._attachEventsHandlers($input);
    };
    Idealforms.prototype._attachEventsHandlers = function ($input) {
        var _this = this;
        /// <summary>
        /// Добавление обработчиков события к валидируемому полю
        /// </summary>
        /// <param name="$input" type="JQuery">Текущее валидируемое поле</param>
        var $field = this.GetValidateField($input);
        $input.on('change keyup', function (e) {
            if (e.which == 9 || e.which == 16)
                return;
            _this._isFirstLaunch = false;
            _this._validate($input, true, true);
            _this._setFocusToInputGroup($input);
        }).focus(function (e) {
            if (!_this.IsValid($input)) {
                _this.ShowErrorHint($input);
            }
            _this._setFocusToInputGroup($input);
        }).blur(function () {
            _this.HideErrorHint($input);
            _this._setFocusToInputGroup($input);
        });
    };
    Idealforms.prototype.ShowErrorHint = function ($input) {
        var $field = this.GetValidateField($input);
        $field.find(this._options.errorClass).addClass('active');
    };
    Idealforms.prototype.HideErrorHint = function ($input) {
        var $field = this.GetValidateField($input);
        $field.find(this._options.errorClass).removeClass('active');
    };
    Idealforms.prototype._handleError = function ($input, errorText, isValid) {
        /// <summary>
        /// В Методе собрана логика добавления нового текста Ошибки в контейнер занимающийся
        /// отображением ошибки, и показ этого контейнера исходя из состояния входящего параметра
        /// </summary>
        /// <param name="$input" type="JQuery">
        /// Текущее валидируемое поле рядом с которым нужно отработать логику
        /// </param>
        /// <param name="errorText" type="string">Текст текущей ошибки</param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>
        isValid = isValid || this.IsValid($input);
        var $errorContainer = this.GetValidateField($input).find(this._options.errorClass);
        $errorContainer.removeClass('active');
        if (errorText) {
            $errorContainer.text(errorText);
            $errorContainer.addClass('active');
        }
        if (isValid) {
            $errorContainer.removeClass('active');
        }
    };
    Idealforms.prototype._handleStyle = function ($input, isValid) {
        /// <summary>
        /// В Методе собрана логика изменения стиля валидируемого поля исходя из того валидно или не
        /// валидно данное поле.
        /// </summary>
        /// <param name="$input" type="JQuery">
        /// Текущее валидируемое поле визуализацию которого нужно изменить
        /// </param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>
        isValid = isValid || this.IsValid($input);
        this.GetValidateField($input).removeClass(this._options.validClass + ' ' + this._options.invalidClass).addClass(isValid ? this._options.validClass : this._options.invalidClass);

        this._showValidationIcon($input);
        this._changeIconByValidationState($input);
    };
    Idealforms.prototype._nameSelector = function (name) {
        return '[name="' + name + '"]';
    };

    Idealforms.prototype._getFirstInvalidInput = function () {
        return this.GetInvalidFields().first().find('input:first, textarea, select');
    };

    //#region Static functions
    Idealforms.Format = function (str) {
        var args = [].slice.call(arguments, 1);
        return str.replace(/\{(\d)\}/g, function (_, match) {
            return args[+match] || '';
        }).replace(/\{\*([^*}]*)\}/g, function (_, sep) {
            return args.join(sep || ', ');
        });
    };
    Idealforms.GetValue = function (key, objectOfSearch) {
        return key.split('.').reduce(function (a, b) {
            return a && a[b];
        }, objectOfSearch);
    };
    Idealforms.GetInstance = function ($childOfForm) {
        var $idealform = $childOfForm.closest('.idealforms');
        if ($idealform) {
            var idealform = $idealform.data('idealforms');
            return idealform;
        }
        return null;
    };
    Idealforms._unstateFocusClass = 'input-group-focus';
    Idealforms._errorFocusClass = 'has-error-focus';
    Idealforms._successFocusClass = 'has-success-focus';

    Idealforms.I18N = IdealformI18N;
    Idealforms.RuleSeparator = ' ';
    Idealforms.ArgSeparator = ':';

    Idealforms.IsValid = 'idealforms-valid';

    Idealforms.Value = 'idealforms-value';

    Idealforms.Requests = {};

    Idealforms.Rules = {
        required: /.+/,
        digits: /^\d+$/,
        email: /^[^@]+@[^@]+\..{2,6}$/,
        username: /^[a-z](?=[\w.]{3,31}$)\w*\.?\w*$/i,
        name: /^[^\d \$*_]{2,}$/,
        weakpass: /^[^ \$*_]{5,}$/,
        pass: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
        strongpass: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        //phone: /^[0-9]{10}$/,
        zip: /^\d{5}$|^\d{5}-\d{4}$/,
        url: /^(?:(ftp|http|https):\/\/)?(?:[\w\-]+\.)+[a-z]{2,6}([\:\/?#].*)?$/i,
        birthdate: /^[0-9]{2}.[0-9]{2}.[0-9]{4}$/,
        snils: /^[0-9]{9}$/,
        numorder: /^[0-9]{2}$/,
        phone: function (input, value) {
            //используем точно такое же регулярное выражение как на сервере проверяющие  телефон -
            //разве что джаваскрипт не поддерживает просмотр назад в выражение поэтому регулярка изменена в обратном направлении.
            //поэтому пришлось использовать метод который реверсирует проверяемое значение
            var phoneRegex = new RegExp("^(\\d+\\s?(x|\\.txe?)\\s?)?((\\)(\\d+[\\s\\-\\.]?)?\\d+\\(|\\d+)[\\s\\-\\.]?)*(\\)([\\s\\-\\.]?\\d+)?\\d+\\+?\\((?!\\+.*)|\\d+)(\\s?\\+)?$", "i");
            var reversedValue = value.split("").reverse().join("");
            return phoneRegex.test(reversedValue);
        },
        phoneMin: function (input, value) {
            return value.length >= 8;
        },
        number: function (input, value) {
            return !isNaN(value);
        },
        range: function (input, value, min, max) {
            return Number(value) >= min && Number(value) <= max;
        },
        min: function (input, value, min) {
            return value.length >= min;
        },
        max: function (input, value, max) {
            return value.length <= max;
        },
        minoption: function (input, value, min) {
            return input.filter(':checked').length >= min;
        },
        maxoption: function (input, value, max) {
            return input.filter(':checked').length <= max;
        },
        minmax: function (input, value, min, max) {
            return value.length >= min && value.length <= max;
        },
        select: function (input, value, def) {
            return value != def;
        },
        extension: function ($input) {
            var extensions = [].slice.call(arguments, 1), valid = false;
            var input = $input.get(0);
            $.each(input.files || [{ name: input.value }], function (i, file) {
                valid = $.inArray(file.name.split('.').pop().toLowerCase(), extensions) > -1;
            });
            return valid;
        },
        equalto: function (input, value, target) {
            var $target = $('[name="' + target + '"]');
            debugger;
            var idealform = this;
            if (idealform.GetInvalidFields().find($target).length)
                return false;

            $target.off('keyup.equalto').on('keyup.equalto', function () {
                idealform.GetValidateField(input).removeData('idealforms-value');
                idealform.Validate(input);
            });

            return input.val() == $target.val();
        },
        date: function (input, value, format) {
            format = format || 'dd.mm.yyyy';

            var delimiter = /[^mdy]/.exec(format)[0], theFormat = format.split(delimiter), theDate = value.split(delimiter);

            function isDate(date, format) {
                var m, d, y;

                for (var i = 0, len = format.length; i < len; i++) {
                    if (/m/.test(format[i]))
                        m = date[i];
                    if (/d/.test(format[i]))
                        d = date[i];
                    if (/y/.test(format[i]))
                        y = date[i];
                }

                if (!m || !d || !y)
                    return false;

                return m > 0 && m < 13 && y && y.length == 4 && d > 0 && d <= (new Date(y, m, 0)).getDate();
            }

            return isDate(theDate, theFormat);
        }
    };
    return Idealforms;
})();
;

$.fn['idealforms'] = function (options) {
    var someForms = this;
    someForms.each(function () {
        var $someForm = $(this);
        var idealform = new Idealforms($someForm, options);
        return $.data(this, 'idealforms', idealform);
    });
    return someForms;
};
//# sourceMappingURL=idealforms.js.map
