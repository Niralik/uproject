﻿function IdealformsOld(targetFormOfIdealing, inputOptions) {
    var self = this,
        $TargetFormOfIdealing = $(targetFormOfIdealing),
        defaultOptions = {
            field: '.field-ideal',
            error: '.error',
            iconHtml: '<i/>',
            iconBaseClass: 'fa icon',
            iconExclamationClass: 'fa-exclamation',
            iconSuccessClass: 'fa-check',
            iconValidatingClass: 'fa-circle-o-notch icon-validation',
            invalidClass: 'has-error',
            validClass: 'has-success',
            silentLoad: true,
            adaptiveWidth: 500,
            onValidate: $.noop,
            onSubmit: $.noop,
            rules: {},
            errors: {}
        },
        options = $.extend({}, defaultOptions, inputOptions),
        $fields = $(),
        $inputs = $(),
        isFirstLaunch = true;

    //приватные переменные с названиями классов CSS
    var inputGroupSimpleFocusClass = 'input-group-focus',
        inputGroupErrorFocusClass = 'has-error-focus',
        inputGroupValidFocusClass = 'has-success-focus';

    this.initialization = function () {
        _initializeI18n();
        _ajaxExtension();
        _addValidationRulesFromMarkup();

        self.addValidationRules(options.rules || {});

        //Отработка сабмита формы
        $TargetFormOfIdealing.submit(function (e) {
            _validateAll();
            self.focusFirstInvalid();
            var isContinue = true;
            isContinue = options.onSubmit.call(self, self.getInvalid().length, e);

            if (!isContinue) {
                e.preventDefault();
            }
        });

        //Ескли отключена молчаливая загрузка то будет показываться сообщения у первого невалидного поля
        if (!options.silentLoad) {
            // 1ms timeout to make sure error shows up
            setTimeout($.proxy(self.focusFirstInvalid, this), 1);
        }
        //изменение логики отображения идеалформ если ширина формы меньше указанной в проперти
        $(window).resize(_visualAdaptationToSize);
        _visualAdaptationToSize();
    };

    function _addValidationRulesFromMarkup() {
        /// <summary>
        /// Изъятие правил валидации из HTML разметки указаной в атриббуте "data-idealforms-rules"
        /// валидируемого поля и добавление его в текущий экземпляр
        /// </summary>
        var rules = {};

        $TargetFormOfIdealing.find('input, select, textarea').each(function () {
            var currentRuleContainer = $(this);
            var rule = currentRuleContainer.data('idealforms-rules');
            if (rule && !rules[this.name]) rules[this.name] = rule;
        });

        self.addValidationRules(rules);
    };

    function _initializeI18n() {
        /// <summary>
        /// Локализация текущего списка текстов ошибок, на основе последней найденой ссылки на файл
        /// с локализацией
        /// </summary>
        $.each($.idealforms.i18n, function (locale, lang) {
            var errors = lang.errors,
                customInputErrors = lang.customInputErrors,
                options = {};

            delete lang.customInputErrors;
            delete lang.errors;

            for (var ext in lang) options[ext] = { i18n: lang[ext] };

            $.extend($.idealforms.errors, errors);
            $.extend($.idealforms.customInputErrors, customInputErrors);
            $.extend(true, self.options, options);
        });
    };

    function _buildField(input) {
        /// <summary>
        /// Метод добавляет в HTML контейнер (div.field-ideal) валидируемого поля необходимую HTML
        /// оснастку для поддержки работы плагина. Добавляет иконку валидации, необходимые классы и
        /// прикрепляет обработчики событий.
        /// </summary>
        /// <param name="input" type="object">Текущее валидируемое поле</param>

        var $field = _getField(input),
         $icon;

        $icon = $(options.iconHtml, {
            "class": options.iconBaseClass + ' ' + options.iconExclamationClass,
            click: function() {
                $(input).focus();
                _setFocusClassToInputGroupFieldByHisState(input);
            }
        });

        if (!$fields.filter($field).length) {
            $fields = $fields.add($field);
            if (options.iconHtml) $field.append($icon);
            $field.addClass('idealforms-field idealforms-field-' + input.type);
        }

        _addEvents(input);
    };

    function _addEvents(input) {
        /// <summary>
        /// Добавление обработчиков события к валидируемому полю
        /// </summary>
        /// <param name="input" type="object">Текущее валидируемое поле</param>
        var $field = _getField(input);

        $(input)
          .on('load', function (e) {
              _validate(this, true, true);
          })
          .on('change keyup', function (e) {
              if (e.which == 9 || e.which == 16) return;
              isFirstLaunch = false;
              _validate(this, true, true);
              _setFocusClassToInputGroupFieldByHisState(this);
          })
          .focus(function () {
              if (!self.isValid(this.name)) {
                  $field.find(options.error).addClass('active');
              }
              _setFocusClassToInputGroupFieldByHisState(this);
          })
          .blur(function () {
              $field.find(options.error).removeClass('active');
              _setFocusClassToInputGroupFieldByHisState(this);
          });
        $field.find(_makeItFitToJquerySelector(options.iconBaseClass)).on('click', function () {
            _showErrorHint(this);
        });
    };

    function _setFocusClassToInputGroupFieldByHisState(input) {
        /// <summary>
        /// Метод который добавляет различные классы Фокуса на inputGroup контейнер исходя из
        /// состояния валидации поля - применяется тогда когда input поле обернуто в div.input-group http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="input" type="object">поле фокус которого надо обработать</param>

        var $field = _getField(input),
            $inputGroup = _getInputGroup(input);

        if ($inputGroup && ($(input).is(":active") || $(input).is(":focus"))) {
            if ($field.hasClass(options.validClass)) {
                $inputGroup.removeClass(inputGroupSimpleFocusClass + ' ' + inputGroupErrorFocusClass)
                           .addClass(inputGroupValidFocusClass);
            } else if ($field.hasClass(options.invalidClass)) {
                $inputGroup.removeClass(inputGroupSimpleFocusClass + ' ' + inputGroupValidFocusClass)
                           .addClass(inputGroupErrorFocusClass);
            } else {
                $inputGroup.removeClass(inputGroupErrorFocusClass + ' ' + inputGroupValidFocusClass)
                           .addClass(inputGroupSimpleFocusClass);
            }
        } else if ($inputGroup) {
            $inputGroup.removeClass(inputGroupErrorFocusClass + ' ' + inputGroupValidFocusClass + ' ' + inputGroupSimpleFocusClass);
        }
    }
    function _changeIconsOfValidationState(input) {
        /// <summary>
        /// Метод который меняет вид иконки исходя из состояния валидации входного поля
        /// </summary>
        /// <param name="input" type="object">поле рядом с которым расположенна иконка</param>
        var $field = _getField(input),
            $iconOfValidationState = $field.find(_makeItFitToJquerySelector(options.iconBaseClass));

        if ($field.hasClass('ajax')) {
            $iconOfValidationState.removeClass(options.iconSuccessClass + ' ' + options.iconExclamationClass).addClass(options.iconValidatingClass);
        } else if ($field.hasClass(options.validClass)) {
            $iconOfValidationState.removeClass(options.iconValidatingClass + ' ' + options.iconExclamationClass).addClass(options.iconSuccessClass);
        } else if ($field.hasClass(options.invalidClass)) {
            $iconOfValidationState.removeClass(options.iconValidatingClass + ' ' + options.iconSuccessClass).addClass(options.iconExclamationClass);
        }
    }

    function _isRequired(input) {
        /// <summary>
        /// Метод проверяющий есть ли в правилах валидации поля ввода правило "Requered" - то есть
        /// обязательно ли данное поле для заполнения
        /// </summary>
        /// <param name="input" type="object">Текущее валидируемое поле</param>
        if ($(input).is(':checkbox, :radio, select')) return true;
        return options.rules[input.name].indexOf('required') > -1;
    };

    function _getRelated(input) {
        return _getField(input).find('[name="' + input.name + '"]');
    };

    function _getField(input) {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем обеспечивающий HTML поддержку плагина
        /// </summary>
        /// <param name="input" type="object">Текущее валидируемое поле</param>
        ///<returns type="jqueryObject">Jquery объект с классом .field-ideal</returns>
        return $(input).closest(options.field);
    };
    function _getInputGroup(input) {
        /// <summary>
        /// Возвращает Jquery объект представляющий контейнер над валидируемым полем
        /// обеспечивающий HTML поддержку фишечки бутстрапа http://getbootstrap.com/components/#input-groups
        /// </summary>
        /// <param name="input" type="object">Текущее валидируемое поле</param>
        ///<returns type="jqueryObject">Jquery объект с классом .input-group</returns>
        return $(input).closest('.input-group');
    };

    function _getFirstInvalid() {
        return self.getInvalid().first().find('input:first, textarea, select');
    };

    function _handleError(input, errorText, isValid) {
        /// <summary>
        /// В Методе собрана логика добавления нового текста Ошибки в контейнер занимающийся
        /// отображением ошибки, и показ этого контейнера исходя из состояния входящего параметра
        /// </summary>
        /// <param name="input" type="object">
        /// Текущее валидируемое поле рядом с которым нужно отработать логику
        /// </param>
        /// <param name="errorText" type="string">Текст текущей ошибки</param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>
        isValid = isValid || self.isValid(input.name);
        var $error = _getField(input).find(options.error);

        $error.removeClass('active');
        if (errorText) {
            $error.text(errorText);
            $error.addClass('active');
        }
        if (isValid) {
            $error.removeClass('active');
        }
    };

    function _handleStyle(input, isValid) {
        /// <summary>
        /// В Методе собрана логика изменения стиля валидируемого поля исходя из того валидно или не
        /// валидно данное поле.
        /// </summary>
        /// <param name="input" type="object">
        /// Текущее валидируемое поле внешность которого нужно изменить
        /// </param>
        /// <param name="isValid" type="bool">
        /// Булевый параметр показывающий валидно ли текущее поле
        /// </param>
        isValid = isValid || self.isValid(input.name);
        _getField(input).removeClass(options.validClass + ' ' + options.invalidClass)
                        .addClass(isValid ? options.validClass : options.invalidClass);
        _showValidationIcon(input);
        _changeIconsOfValidationState(input);
        _setFocusClassToInputGroupFieldByHisState(input);
    };
    function _showValidationIcon(input) {
        var $field = _getField(input),
            $validationIcon = $field.find(_makeItFitToJquerySelector(options.iconBaseClass));
        $validationIcon.addClass('active');
        //для того чтоб анимация активации сработала как полагается ждем 300мс и применяем класс с другой анимацией переходов
        setTimeout(function () {
            $validationIcon.addClass('ease-in-out-animation');
        }, 300);

    }
    function _hideValidationIcon(input) {
        var $field = _getField(input),
            $validationIcon = $field.find(_makeItFitToJquerySelector(options.iconBaseClass));
        $validationIcon.removeClass('ease-in-out-animation');
        $validationIcon.removeClass('active');
    }
    function _showErrorHint(input) {
        var errorHint = _getField(input).find(options.error);
        if (errorHint && !hasWhiteSpaceOrNull(errorHint.text())) {
            errorHint.removeClass('hidden');
            errorHint.addClass('active');
        }
    }
    function _hideErrorHint(input) {
        var errorHint = _getField(input).find(options.error);
        if (errorHint) {
            errorHint.removeClass('active');
        }
    }
    function _makeItFitToJquerySelector(cssClass) {
        /// <summary>
        /// Метод конвертирует строку подразумевающую набор css классов вида "cssClass cssClass" в строку вида ".cssClass.cssClass" для Jquery селекторов
        /// </summary>
        /// <param name="cssClass" type="string">строка из перечесления css классов через пробел: "cssClass cssClass"</param>
        ///<returns type="string">строка вида ".cssClass.cssClass"</returns>
        var joinedString = options.iconBaseClass.split(' ').join('.');
        var convertedString = joinedString.substring(0, joinedString.length);
        return '.' + convertedString;
    }
    function _fresh(input) {
        /// <summary>
        /// Приводим поле idealform к изначальному состоянию
        /// </summary>
        /// <param name="input" type="object">редактируемое поле</param>
        var $field = _getField(input);
        $field.removeClass(options.validClass + ' ' + options.invalidClass);
        $field.find(options.error).removeClass('active');
        if (_isRequired(input)) {
            _showValidationIcon(input);
        } else {
            _hideValidationIcon(input);
        }
    };

    function _validate(input, doHandleError, doHandleStyle) {
        /// <summary>
        /// Основной метод плагина - координирует всю работу по валидации текущего поля на основе
        /// правил проинициализированных при создании объекта Idealforms
        /// </summary>
        /// <param name="input" type="object">Валидируемое поле ввода данных</param>
        /// <param name="doHandleError" type="bool">
        /// Флаг отражающий необходимо ли отрабатывать логику наполнения и визуализации ошибки
        /// </param>
        /// <param name="doHandleStyle" type="bool">
        /// Флаг отражающий необходимо ли применять соотвествующий стиль к валидируемому полю
        /// </param>

        var $field = _getField(input),
            userRules = options.rules[input.name].split($.idealforms.ruleSeparator),
            oldValue = $field.data('idealforms-value'),
            isValid = true,
            rule;

        // Don't validate input if value hasn't changed
        if (!$(input).is(':checkbox, :radio') && oldValue == input.value) {
            return $field.data('idealforms-valid');
        }

        $field.data('idealforms-value', input.value);

        // Non-required input with empty value must pass validation

        if (!input.value && !_isRequired(input)) {
            $field.removeData('idealforms-valid');
            _fresh(input);

            // Inputs with value or required
        } else {
            $.each(userRules, function (i, userRule) {
                //Пользовательское правило может содержать какие либо граничные аргументы, вытаскиваем их из строки, 
                //где первым объектом будет является тип правила, а всё остальное аргументы переданные вместе с правилом
                var splitedUserRule = userRule.split($.idealforms.argSeparator);

                //вытащили само имя правила
                rule = splitedUserRule[0];
                //вытащили либо регулярное выражение соотвествующее данному правило, либо функцию из статического массива обработчиков правил
                var theRule = $.idealforms.rules[rule],
                    //Вытаскиваем все аргументы начинающие со второго стрингОбъекта 
                    args = splitedUserRule.slice(1),
                    error;

                error = _generateErrorText(input, userRule);

                //проводим валидацию содержимого поля ввода на основе текущего правила - если правило это какая-то 
                //функция то она выполняется со списком аргументов изъятых выше из правила
                isValid = typeof theRule == 'function' ?
                    theRule.apply(self, [input, input.value].concat(args))
                    : theRule.test(input.value);

                $field.data('idealforms-valid', isValid);

                //Если данное правило это не ajax отработка и есть какие либо запросы в списке запросов, то прерываем запрос по текущему полю 
                if (rule != 'ajax' && $.idealforms._requests[input.name]) {
                    $.idealforms._requests[input.name].abort();
                    _getField(input).removeClass('ajax');
                }

                if (doHandleError) _handleError(input, error, isValid);
                //Если первый запуск валидации и нет значения в поле ввода данных то не применяем стиль - во всех остальных случаях применяем
                if (doHandleStyle && (!isFirstLaunch || input.value)) {
                    _handleStyle(input, isValid);
                }
                if (isFirstLaunch) {
                    _showValidationIcon(input);
                }
                //вызываем клиентский код обработки события валидации для текущего провалидированного правила
                options.onValidate.call(self, input, rule, isValid);

                return isValid;
            });
        }

        return isValid;
    };

    function _visualAdaptationToSize() {
        /// <summary>
        /// Адаптация визуального отображения HTML оснастки плагина на основе заданного в настройках
        /// параметра adaptiveWidth
        /// </summary>
        var formParentWidth = $TargetFormOfIdealing.parent().width(),
            formWidth = $TargetFormOfIdealing.outerWidth(),
            isAdaptive = options.adaptiveWidth > formWidth && formWidth == formParentWidth;

        $TargetFormOfIdealing.toggleClass('adaptive', isAdaptive);
    }
    function _generateErrorText(input, userRule) {
        /// <summary>
        /// Метод работает как с одним параметром, так и с двумя. Если входным параметром служит
        /// только input то вытаскивается текст ошибки для случая отработки успешно ajax запроса,
        /// вытаскивается текс ошибки предыдущего перед ajax правилом правила. Если указано вторым
        /// параметром пользовательско правило валидации для поля ввода данных, то вытаскивается
        /// ошибка привязанная к этому правилу.
        /// </summary>
        var separatedUserRule;
        if (userRule) {
            separatedUserRule = userRule.split($.idealforms.argSeparator);
        } else {
            var userRulesOfInput = options.rules[input.name].split($.idealforms.ruleSeparator),
                userRuleOfInput = {},
                indexOfAjax = userRulesOfInput.indexOf('ajax');
            //пытаемся вытащить пользвательское правило которое идет до ajax правила 
            if (indexOfAjax != -1) {
                userRuleOfInput = userRulesOfInput[indexOfAjax - 1];
            }
            //если вдруг такого пользовательского правила нет - берем первое правило из списка правил
            if (userRuleOfInput === undefined) {
                userRuleOfInput = userRulesOfInput[0];
            }
            separatedUserRule = userRuleOfInput ? userRuleOfInput.split($.idealforms.argSeparator) : {};
        }

        var rule = separatedUserRule[0];

        var args = separatedUserRule.slice(1),
            error;

        //error = $.idealforms._format.apply(null, [
        //  $.idealforms._getKey('errors.' + input.name + '.' + rule, options) ||
        //  $.idealforms.errors[rule]
        //].concat(args));
        error = $.idealforms._format.apply(null, [
          $.idealforms._getKey('customInputErrors.' + input.name + '.' + rule, $.idealforms) ||
          $.idealforms.errors[rule]
        ].concat(args));
        return error;
    }
    function _ajaxExtension() {
        /// <summary>
        /// Метод добавляющий обработку ajax валидации
        /// </summary>
        //Расширяем статический класс $.idealforms новым свойством которое будет содержать список запросов
        $.extend($.idealforms, { _requests: {} });

        $.idealforms.errors.ajax = $.idealforms.errors.ajax || 'Loading...';

        $.extend($.idealforms.rules, {
            ajax: function (input) {
                var $field = _getField(input),
                    url = $(input).data('val-remote-url'),
                    userError = $.idealforms._getKey('customInputErrors.' + input.name + '.ajaxError', $.idealforms),
                    previousError = _generateErrorText(input),
                    requests = $.idealforms._requests,
                    data = {};

                data[input.name] = input.value;

                $field.addClass('ajax');

                if (requests[input.name]) requests[input.name].abort();

                requests[input.name] = $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        if (response === true) {
                            $field.data('idealforms-valid', true);
                            _handleError(input, previousError);
                            _handleStyle(input);
                        } else {
                            _handleError(input, userError);
                        }

                        options.onValidate.call(self, input, 'ajax', response);
                        $field.removeClass('ajax');

                        _changeIconsOfValidationState(input);
                    }
                });

                return false;
            }
        });
    }

    function _validateAll() {
        $inputs.each(function () { _validate(this, true, true); });
    };

    this.addValidationRules = function (rules) {
        /// <summary>
        /// Метод находит все валидируемые поля на основе информации из входяшего параметра,
        /// добавляет в переменную Rules настроек класса входящие правила валидации, запускает
        /// создание HTML экосистемы над каждым полем и проводит валидацию всех найденных полей.
        /// </summary>
        /// <param name="rules" type="array">
        /// Массив правил валидации с названиями целей валидации вида: "InputName: Rule Rule Rules..."
        /// </param>
        var validatingInputSelectors = $.map(rules, function (_, name) {
            return '[name="' + name + '"]';
        }).join(',');

        var $validatingInputs = $TargetFormOfIdealing.find(validatingInputSelectors);

        $.extend(options.rules, rules);

        //для кажого поля создаем HTML поддержку для визуализации процесса валидации
        $validatingInputs.each(function () { _buildField(this); });
        $inputs = $inputs.add($validatingInputs);

        _validateAll();
        $fields.find(options.error).removeClass('active');
    };

    this.getInvalid = function () {
        return $fields.filter(function () {
            return $(this).data('idealforms-valid') === false;
        });
    };

    this.focusFirstInvalid = function () {
        var firstInvalid = _getFirstInvalid()[0];

        if (firstInvalid) {
            _handleError(firstInvalid);
            _handleStyle(firstInvalid);

            $(firstInvalid).focus();
            _setFocusClassToInputGroupFieldByHisState(firstInvalid);
        }
    };

    this.isValid = function (name) {
        if (name) return !self.getInvalid().find('[name="' + name + '"]').length;
        return !self.getInvalid().length;
    };

    this.reset = function (name) {
        if (name) $inputs = $inputs.filter('[name="' + name + '"]');

        $inputs.filter('input:not(:checkbox, :radio)').val('');
        $inputs.filter(':checkbox, :radio').prop('checked', false);
        $inputs.filter('select').find('option').prop('selected', function () {
            return this.defaultSelected;
        });

        $inputs.change().each(function () { _fresh(this); });
    };

    this.validate = function (name) {
        return _validate(name, true);
    };

    return this;
}

//Прикрепление новой функции к Jquery. Для работы над объектом
//$.fn['idealforms'] = function (opts) {
//    var someForms = this;

//    someForms.each(function () {
//        var someForm = this;
//        // Пробегаемся по всем Jquery объектам и инициализируем для них новую Idealform с параметрами
//        var idealForm = new IdealformsOld(someForm, opts);
//        idealForm.initialization();
//        return $.data(someForm, 'idealforms', idealForm);
//    });

//    return someForms;
//};
//Глобальные функции добавленные к Jquery объект Idealform для доступа аля $.idealform.i18n
$['idealforms'] = $.extend({},
{
    _format: function (str) {
        var args = [].slice.call(arguments, 1);
        return str.replace(/\{(\d)\}/g, function (_, match) {
            return args[+match] || '';
        }).replace(/\{\*([^*}]*)\}/g, function (_, sep) {
            return args.join(sep || ', ');
        });
    },

    _getKey: function (key, obj) {
        return key.split('.').reduce(function (a, b) {
            return a && a[b];
        }, obj);
    },

    i18n: {},

    ruleSeparator: ' ',
    argSeparator: ':',

    rules: {
        required: /.+/,
        digits: /^\d+$/,
        email: /^[^@]+@[^@]+\..{2,6}$/,
        username: /^[a-z](?=[\w.]{3,31}$)\w*\.?\w*$/i,
        name: /^[^\d \$*_]{2,}$/,
        pass: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
        strongpass: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        //phone: /^[0-9]{10}$/,
        zip: /^\d{5}$|^\d{5}-\d{4}$/,
        url: /^(?:(ftp|http|https):\/\/)?(?:[\w\-]+\.)+[a-z]{2,6}([\:\/?#].*)?$/i,
        birthdate: /^[0-9]{2}.[0-9]{2}.[0-9]{4}$/,
        snils: /^[0-9]{9}$/,
        numorder: /^[0-9]{2}$/,

        phone: function (input, value) {
            //используем точно такое же регулярное выражение как на сервере проверяющие  телефон - 
            //разве что джаваскрипт не поддерживает просмотр назад в выражение поэтому регулярка изменена в обратном направлении.
            //поэтому пришлось использовать метод который реверсирует проверяемое значение
            var phoneRegex = new RegExp("^(\\d+\\s?(x|\\.txe?)\\s?)?((\\)(\\d+[\\s\\-\\.]?)?\\d+\\(|\\d+)[\\s\\-\\.]?)*(\\)([\\s\\-\\.]?\\d+)?\\d+\\+?\\((?!\\+.*)|\\d+)(\\s?\\+)?$", "i");
            var reversedValue = value.split("").reverse().join("");
            return phoneRegex.test(reversedValue);
        },
        phoneMin: function (input, value) {
            return value.length >= 8;
        },

        number: function (input, value) {
            return !isNaN(value);
        },

        range: function (input, value, min, max) {
            return Number(value) >= min && Number(value) <= max;
        },

        min: function (input, value, min) {
            return value.length >= min;
        },

        max: function (input, value, max) {
            return value.length <= max;
        },

        minoption: function (input, value, min) {
            return this._getRelated(input).filter(':checked').length >= min;
        },

        maxoption: function (input, value, max) {
            return this._getRelated(input).filter(':checked').length <= max;
        },

        minmax: function (input, value, min, max) {
            return value.length >= min && value.length <= max;
        },

        select: function (input, value, def) {
            return value != def;
        },

        extension: function (input) {
            var extensions = [].slice.call(arguments, 1)
              , valid = false;

            $.each(input.files || [{ name: input.value }], function (i, file) {
                valid = $.inArray(file.name.split('.').pop().toLowerCase(), extensions) > -1;
            });

            return valid;
        },

        equalto: function (input, value, target) {
            var self = this
              , $target = $('[name="' + target + '"]');

            if (this.getInvalid().find($target).length) return false;

            $target.off('keyup.equalto').on('keyup.equalto', function () {
                self._getField(input).removeData('idealforms-value');
                self._validate(input, false, true);
            });

            return input.value == $target.val();
        },

        date: function (input, value, format) {
            format = format || 'mm/dd/yyyy';

            var delimiter = /[^mdy]/.exec(format)[0]
              , theFormat = format.split(delimiter)
              , theDate = value.split(delimiter);

            function isDate(date, format) {
                var m, d, y;

                for (var i = 0, len = format.length; i < len; i++) {
                    if (/m/.test(format[i])) m = date[i];
                    if (/d/.test(format[i])) d = date[i];
                    if (/y/.test(format[i])) y = date[i];
                }

                if (!m || !d || !y) return false;

                return m > 0 && m < 13 &&
                  y && y.length == 4 &&
                  d > 0 && d <= (new Date(y, m, 0)).getDate();
            }

            return isDate(theDate, theFormat);
        }
    },
    errors: {},
    //персональные ошибки для какого либо поля ввода информации
    customInputErrors: {}
});