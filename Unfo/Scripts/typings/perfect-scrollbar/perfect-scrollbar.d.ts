﻿interface IPerfectScrollOptions {
    wheelSpeed?: number;
    wheelPropagation?: boolean;
    minScrollbarLength?: number;
    maxScrollbarLength?: number;
    useBothWheelAxes?: boolean;
    useKeyboard?: boolean;
    suppressScrollX?: boolean;
    suppressScrollY?: boolean;
    scrollXMarginOffset?: number;
    scrollYMarginOffset?: number;
    includePadding?: boolean;
}

interface JQuery {
    perfectScrollbar(options: IPerfectScrollOptions, command?:string): JQuery;
    perfectScrollbar(command: string): void;
}