﻿interface IReadmoreOptions {
    speed?: number;
    maxHeight?: number;
    heightMargin?: number;
    moreLink?: string;
    lessLink?: string;
    embedCSS?: boolean;
    sectionCSS?: string;
    startOpen?: boolean;
    expandedClass?: string;
    collapsedClass?: string;
    // callbacks
    beforeToggle? (trigger, element, expanded: boolean): void;
    afterToggle? (trigger, element, expanded:boolean): void;
}

interface JQuery {
    readmore(options?: IReadmoreOptions): JQuery;
} 