﻿using System.Web;
using DataModel;
using Duke.Owin.VkontakteMiddleware;
using Duke.Owin.VkontakteMiddleware.Provider;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Twitter;
using Owin;
using Owin.Security.Providers.GooglePlus;
using Owin.Security.Providers.GooglePlus.Provider;
using System;
using System.Security.Claims;
using UnfoIdentitySystem;

namespace Unfo
{
    public partial class Startup
    {
        // Дополнительные сведения о настройке проверки подлинности см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
       
            // Configure the db context, user manager and role manager to use a single instance per request
            app.CreatePerOwinContext(UnfoDbContext.Create);
            app.CreatePerOwinContext<UnfoUserManager>(UnfoUserManager.Create);
            app.CreatePerOwinContext<UnfoRoleManager>(UnfoRoleManager.Create);
            app.CreatePerOwinContext<UnfoSignInManager>(UnfoSignInManager.Create);
            

            var cookieProvider = new CookieAuthenticationProvider();
            cookieProvider.OnApplyRedirect = context =>
            {
                var t = context;
            
            };
            cookieProvider.OnResponseSignIn = context =>
            {
                var t = context;
            };
            
            cookieProvider.OnValidateIdentity =
                SecurityStampValidator.OnValidateIdentity<UnfoUserManager, UnfoUser, Guid>(
                    validateInterval: TimeSpan.FromSeconds(30),
                    regenerateIdentityCallback: (manager, user) =>
                    {
                        var someclaims = user.GenerateUserIdentityAsync(manager,
                            DefaultAuthenticationTypes.ApplicationCookie);
                        return someclaims;
                    },
                    getUserIdCallback: (claims) =>
                    {
                        var claim = claims;
                        return new Guid();
                    });
            
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
            //    LoginPath = new PathString("/Account/SignIn"),
            //    //Provider = new CookieAuthenticationProvider
            //    //{
            //    //    // Enables the application to validate the security stamp when the user logs in.
            //    //    // This is a security feature which is used when you change a password or add an external login to your account.
            //    //    OnValidateIdentity =
            //    //        SecurityStampValidator.OnValidateIdentity<UnfoUserManager, UnfoUser, Guid>(
            //    //            validateInterval: TimeSpan.FromMinutes(30),
            //    //            regenerateIdentityCallback: (manager, user) =>
            //    //            {
            //    //               var someclaims = user.GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie);
            //    //                return someclaims;
            //    //            },
            //    //            getUserIdCallback: (claims) =>
            //    //            {
            //    //                var claim = claims;
            //    //                return new Guid();
            //    //            } )
            //    //}
            //    //Provider = cookieProvider
            //});
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            app.UseVkontakteAuthentication(ExternalLoginProvidersOptions.Vkontakte);
            app.UseTwitterAuthentication(ExternalLoginProvidersOptions.Twitter);
            app.UseFacebookAuthentication(ExternalLoginProvidersOptions.Facebook);
            app.UseGooglePlusAuthentication(ExternalLoginProvidersOptions.DeployGooglePlus);
        }
    }
}