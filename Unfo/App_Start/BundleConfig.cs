﻿using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using System.Web.Optimization;

namespace Unfo
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Инициализация бандлов

            bundles.UseCdn = true;

            var nullBuilder = new NullBuilder();
            var nullOrderer = new NullOrderer();

            BundleResolver.Current = new CustomBundleResolver();

            #endregion Инициализация бандлов

            #region Бандлы с ссылками на сторонние javascript'ы и стили используемые по проекту

            bundles.Add(new CustomStyleBundle("~/Bundles/WolfThemeStyles").Include(
                "~" + Links.Content.font_awesome_css,
                "~" + Links.Content.bootstrap_css,
                "~" + Links.Content.bootstrap_override_css,
                "~" + Links.Content.css.theme_css,
                "~" + Links.Content.messenger.messenger_css,
                "~" + Links.Content.messenger.messenger_theme_flat_css,
                "~" + Links.Content.messenger.messenger_spinner_css,
                "~" + Links.Content.icheck.all_css,
                "~" + Links.Content.css.vendor.animate_css,
                "~" + Links.Content.css.vendor.brankic_css,
                "~" + Links.Content.css.vendor.ionicons_css,
                "~" + Links.Content.css.vendor.datepicker_css,
                "~" + Links.Content.css.vendor.bootstrap_switch_min_css,
                "~" + Links.Content.css.vendor.morris_css,
                "~" + Links.Content.css.vendor.summernote_css,
                "~" + Links.Content.css.vendor.select2_css,
                "~" + Links.Content.css.vendor.select2_bootstrap_css,
                "~" + Links.Content.bootstrap_datetimepicker_css,
                "~" + Links.Content.fileuploader.jquery_fileupload_css,
                "~" + Links.Content.cropper_css,
                "~" + Links.Content.jcrop.jquery_Jcrop_css
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Modernizr").Include("~/Scripts/modernizr-2.*"));

            var jQueryBundle = new CustomScriptBundle("~/Bundles/Jquery");
            jQueryBundle.Include(
                "~/Scripts/jquery-{version}.js",
                "~" + Links.Scripts.jquery_validate_js,
                "~" + Links.Scripts.jquery_validate_unobtrusive_js
                );
            jQueryBundle.Orderer = nullOrderer;
            jQueryBundle.CdnFallbackExpression = "window.jquery";
            bundles.Add(jQueryBundle);

            bundles.Add(new CustomScriptBundle("~/Bundles/CommonScripts").Include(
                "~" + Links.Scripts.bootstrap_js,
                                "~" + Links.Scripts.perfect_scrollbar.perfect_scrollbar_js,
                "~" + Links.Scripts.Template.vendor.jquery_cookie_js,
                "~" + Links.Scripts.Template.vendor.raphael_min_js,
                "~" + Links.Scripts.Template.vendor.morris_min_js,
                "~" + Links.Scripts.Template.vendor.select2_min_js,
                //"~" + Links.Scripts.Template.vendor.bootstrap_datepicker_js,
                "~" + Links.Scripts.messenger.messenger_js,
                "~" + Links.Scripts.messenger.messenger_theme_flat_js,
                "~" + Links.Scripts.icheck_js,
                //"~" + Links.Scripts.messenger.messenger_theme_future_js,
                "~" + Links.Scripts.bootstrap_switch_js,
                "~" + Links.Scripts.Template.vendor.jquery_flot.jquery_flot_js,
                "~" + Links.Scripts.Template.vendor.jquery_flot.jquery_flot_time_js,
                "~" + Links.Scripts.Template.vendor.jquery_flot.jquery_flot_tooltip_js,
                "~" + Links.Scripts.moment_js,
                "~" + Links.Scripts.bootstrap_datetimepicker_js,
                "~" + Links.Scripts.handlebars_js,
                 "~" + Links.Scripts.readmore.readmore_js,
                "~" + Links.Scripts.Spinner.spinner_options_ts,
                "~" + Links.Scripts.Spinner.spin_js,
                "~" + Links.Scripts.jquery_scrollto_js,
                "~" + Links.Scripts.Unfo.scripts_js));

            bundles.Add(new CustomScriptBundle("~/Bundles/ThemeScripts").Include(
                "~" + Links.Scripts.Template.theme_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Typeahead").Include(
                "~/Scripts/typeahead.bundle*",
                "~/Scripts/bloodhound*",
                "~/Scripts/typeahead.jquery*"
                ));

            #endregion Бандлы с ссылками на сторонние javascript'ы и стили используемые по проекту

            //==============> Джаваскрипты и стили написанные Смарткодом <==============//

            #region Бандлы Наболее обобщенных скриптов и стилей используемых во многих местах проекта

            bundles.Add(new CustomScriptBundle("~/Bundles/Idealforms/js").Include(
                "~" + Links.Scripts.Idealforms.idealforms_ts));

            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/CommonTemplates").Include(
                "~" + Links.Scripts.Unfo.CustomScripts.AlertEngine.alert_block_handlebars));

            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Common").Include(
                "~" + Links.Scripts.Unfo.CustomScripts.jquery_custom_extensions_js,
                "~" + Links.Scripts.Unfo.CustomScripts.jquery_unfo_extentions_ts,
                 "~" + Links.Scripts.Unfo.CustomScripts.scrollbars_ts,
                "~" + Links.Scripts.Unfo.CustomScripts.utilities_ts,
                "~" + Links.Scripts.Unfo.CustomScripts.CommonObjects.error_handler_js,
                "~" + Links.Scripts.Unfo.GlobalClasses.current_event_ts,
                "~" + Links.Scripts.Unfo.GlobalClasses.current_user_ts,
                "~" + Links.Scripts.Unfo.global_variables_ts,
                "~" + Links.Scripts.Unfo.unfo_js,
                "~" + Links.Scripts.Unfo.CustomScripts.handlebars_custom_helpers_js,
                "~" + Links.Scripts.Unfo.CustomScripts.AlertEngine.alert_engine_ts
                ));

            #endregion Бандлы Наболее обобщенных скриптов и стилей используемых во многих местах проекта

            //Bundles for pages

            #region Programm Bundles

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Programm/AddReport").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Programm.Add.Add_js
                ));

            #endregion Programm Bundles

            #region Events Bundles

            // настройки мероприятия
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Settings").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Events.Settings_js
                ));
            //просмотр мероприятия
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Event/Info").Include(
                // "~/Scripts/jquery.autocomplete.js",
                "~" + Links.Scripts.Unfo.Cabinet.Events.Info_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Event/Edit").Include(
                // "~/Scripts/jquery.autocomplete.js",
                "~" + Links.Scripts.Unfo.Cabinet.Events.Edit_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Event/Create").Include(
                "~" + Links.Scripts.jquery_autocomplete_js,
                "~" + Links.Scripts.Unfo.Cabinet.Events.Create_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Event/Programm").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Programm.Index_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Event/Organizators").Include(
                "~" + Links.Scripts.Template.vendor.summernote_min_js,
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Organizators_js
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Events/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Events.Events_ts
                ));
            #endregion Events Bundles

            #region Cabinet Dictionaries - Справочники Unfo

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dictionaries/Sponsors/Edit").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.AddSponsor_js));
            //редактирование секций
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/Sections").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Sections_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            //редактирование залов
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/Halls").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Halls_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            //редактирование направлений
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/Directions").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Directions_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            //редактирование мест
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/Locations").Include(
                // "~/Scripts/jquery.autocomplete.js",
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Locations_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            //редактирование докладчиков
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/Speakers").Include(
                // "~/Scripts/jquery.autocomplete.js",
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Speakers_js,
                "~" + Links.Scripts.jquery_dataTables_min_js
                ));

            #endregion Cabinet Dictionaries - Справочники Unfo

            #region Модальные окна справочников

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/LocationModal").Include(
                "~" + Links.Scripts.jquery_autocomplete_js,
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Modals.Location_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/SectionModal").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Modals.Section_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/HallModal").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Modals.Hall_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/DirectionModal").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Modals.Direction_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Dics/SpeakerModal").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Dictionaries.Modals.Speaker_js
                ));

            #endregion Модальные окна справочников

            #region Account Controller Bundles

            bundles.Add(
                new CustomScriptBundle("~/Bundles/Unfo/Account/SignIn/js").Include("~" +
                                                                                   Links.Scripts.Unfo.Account.signin_js));
            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Account/Signup/js").Include(
                "~" + Links.Scripts.Unfo.Account.external_logins_tooltip_handlebars,
                "~" + Links.Scripts.Unfo.Account.suggest_link_modal_window_handlebars,
                "~" + Links.Scripts.Unfo.Account.signup_js));

            #endregion Account Controller Bundles

            #region MyAccount Bundles

            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Cabinet/MyAccount/Profile/js").Include(
                "~" + Links.Scripts.Unfo.Cabinet.MyAccount.profile_js
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Cabinet/MyAccount/ManageExternalLogins/js").Include(
                "~" + Links.Scripts.Unfo.Cabinet.MyAccount.manage_external_logins_js
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Cabinet/MyAccount/ManagePasswords/js").Include(
                "~" + Links.Scripts.Unfo.Cabinet.MyAccount.manage_password_js
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Cabinet/MyAccount/UserPrivacyPolitcs/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.MyAccount.user_privacy_politics_ts
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Unfo/Cabinet/MyAccount/UserRequests/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.MyAccount.user_requests_ts
                ));

            #endregion MyAccount Bundles

            #region QUnit Furniture Bundles

            bundles.Add(new CustomStyleBundle("~/Bundles/Qunit/css").Include("~" + Links.Content.qunit_1_15_0_css));
            bundles.Add(new CustomScriptBundle("~/Bundles/Qunit/js").Include("~" + Links.Scripts.qunit_1_15_0_js));
            bundles.Add(
                new CustomScriptBundle("~/Bundles/QunitCommonTests/js").Include("~" +
                                                                                Links.Scripts.Unfo.CustomScripts
                                                                                    .AlertEngine.alert_engine_tests_ts));
            bundles.Add(
                new CustomScriptBundle("~/Bundles/CustomQunit/js").Include("~" +
                                                                           Links.Scripts.Unfo.CustomScripts
                                                                               .QunitFurniture.qunit_furniture_ts));

            #endregion QUnit Furniture Bundles

            #region Users bundles

            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Users/Moderators/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Users.moderators_ts,
                "~" + Links.Scripts.Unfo.Cabinet.Users.potential_moderator_handlebars,
                "~" + Links.Scripts.Unfo.Cabinet.Users.suggestion_moderator_handlebars
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/Users/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.Users.users_actions_ts,
                "~" + Links.Scripts.Unfo.Cabinet.Users.users_check_ts,
                "~" + Links.Scripts.Unfo.Cabinet.Users.users_ts
                ));

            #endregion Users bundles

            #region UsersRequests Bundles
            bundles.Add(new CustomScriptBundle("~/Bundles/Cabinet/UsersRequests/ts").Include(
                "~" + Links.Scripts.Unfo.Cabinet.UsersRequests.users_requests_ts
                ));
            #endregion

            #region Localization Bundles

            bundles.Add(new CustomScriptBundle("~/Bundles/Localization/ru").Include(
                "~" + Links.Scripts.Unfo.Localization.localization_ru_js,
                "~" + Links.Scripts.Idealforms.localization.idealforms_l8on_ru_js
                ));
            bundles.Add(new CustomScriptBundle("~/Bundles/Localization/en").Include(
                "~" + Links.Scripts.Unfo.Localization.localization_en_js,
                "~" + Links.Scripts.Idealforms.localization.idealforms_l8on_en_js
                ));
            #endregion

            bundles.Add(new CustomScriptBundle("~/Bundles/FileUploader").Include(
                "~" + Links.Scripts.fileuploader.jquery_ui_widget_js,
                "~" + Links.Scripts.fileuploader.jquery_iframe_transport_js,
                "~" + Links.Scripts.fileuploader.jquery_fileupload_js,
                "~" + Links.Scripts.fileuploader.jquery_fileupload_process_js,
                "~" + Links.Scripts.Unfo.fileuploader_js
                ));

            bundles.Add(new CustomScriptBundle("~/Bundles/ImageCropper").Include(
                "~" + Links.Scripts.jcrop.jquery_Jcrop_js,
                "~" + Links.Scripts.Unfo.cropper_script_js,
                "~" + Links.Scripts.jcrop.jquery_color_js
                ));
        }
    }
}