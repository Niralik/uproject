﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Owin;

namespace Unfo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            //var logWriterFactory = new LogWriterFactory();
            //Logger.SetLogWriter(logWriterFactory.Create()); 
        }
    }
}