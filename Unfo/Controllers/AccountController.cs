﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using DataModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Models.ViewModels;
using GlobalVariables = UnfoIdentitySystem.GlobalVariables;

namespace Unfo.Controllers
{

    public partial class AccountController : BaseController
    {
     
        // GET: /Account/SignIn
        [AllowAnonymous]
        public virtual ActionResult SignIn(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction(MVC.Cabinet.Home.Index());
            }
            return View();
        }

        // POST: /Account/SignIn
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SignIn(SignInVM model)
        {
            try
            {
                if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);

                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null) return AlertJson.Error(Resources.Account_SignIn_NotExistEmail_Alert);
              
                // This doen't count login failures towards lockout only two factor authentication
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);

                switch (result)
                {
                    case SignInStatus.Success:
                        var resultOfSetCookie = await SignInManager.SignInAsync(user, model.RememberMe);
                        return resultOfSetCookie.Succeeded ? AlertJson.Success() : AlertJson.Error(resultOfSetCookie);

                    case SignInStatus.LockedOut:
                        var endDateOfLockedOut = await UserManager.GetLockoutEndDateAsync(user.Id);
                        var convertedEndDate = TimeZoneInfo.ConvertTime(endDateOfLockedOut, User.TimeZone);
                        var errorMessage = string.Format(Resources.Account_SignIn_LockoutError_Alert, convertedEndDate.ToString("dd.MM.yy HH:mm"));
                        return AlertJson.Error(errorMessage);

                    case SignInStatus.Failure:
                        var numOfFailedAttempt = await UserManager.GetAccessFailedCountAsync(user.Id);
                        var remainingAttempts = GlobalVariables.MAX_FAILED_ATTEMPS - numOfFailedAttempt;
                        var warningMessage = string.Format(Resources.Account_SignIn_LockoutWarning_Alert, remainingAttempts);
                        var errorSection = new AlertSection(TypeOfAlert.Error, Resources.Account_SignIn_WrongPassword_Alert);
                        var warningSection = new AlertSection(TypeOfAlert.Warning, warningMessage);
                        return AlertJson.Compile(errorSection, warningSection);

                    default:
                        return AlertJson.Error(Resources.Validate_Alert_IternalServerError);
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: false, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public virtual ActionResult SignUp()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SignUp(SignUpVM signUpVm)
        {
            if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);

            var dbUser = signUpVm.GetModel();

            var resultOfIdentity = await UserManager.CreateAsync(dbUser, signUpVm.Password);
            if (!resultOfIdentity.Succeeded) return AlertJson.Error(resultOfIdentity);
           
            resultOfIdentity = await UserManager.AddToRoleAsync(dbUser, Role.User);
            if (!resultOfIdentity.Succeeded) return AlertJson.Error(resultOfIdentity);

            var resultOfSetCookie = await SignInManager.SignInAsync(dbUser, false);

            return resultOfSetCookie.Succeeded ? AlertJson.Success() : AlertJson.Error(resultOfSetCookie);
        }

        //
        // GET: /Account/ConfirmEmail
        //[AllowAnonymous]
        //public virtual async Task<ActionResult> ConfirmEmail(string userId, string code)
        //{
        //    if (userId == null || code == null)
        //    {
        //        return View("Error");
        //    }
        //    var result = await UserManager.ConfirmEmailAsync(userId, code);
        //    return View(result.Succeeded ? "ConfirmEmail" : "Error");
        //}

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public virtual ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
                ViewBag.Link = callbackUrl;
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public virtual async Task<ActionResult> SendCode(string returnUrl)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl });
        }

        /// <summary>
        /// Флаг показывающий что регистрация проходит после социальной авторизации
        /// </summary>
        public bool IsExternalLoginSignUp
        {
            get
            {
                if (ViewData["IsExternalLoginSignUp"] is bool)
                {
                    return (bool)ViewData["IsExternalLoginSignUp"];
                }
                return false;
            }
            set
            {
                if (ViewData["IsExternalLoginSignUp"] is bool || ViewData["IsExternalLoginSignUp"] != null)
                {
                    ViewData["IsExternalLoginSignUp"] = value;
                }
                else
                {
                    ViewData.Add("IsExternalLoginSignUp", value);
                }
            }
        }
        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action(MVC.Account.ExternalLoginCallback(returnUrl)));
        }

        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public virtual async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var externalLoginInfo = AuthenticationManager.GetExternalLoginInfo();

            if (externalLoginInfo == null)
            {
                return RedirectToAction(MVC.Account.SignIn());
            }
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Url.Action(MVC.Cabinet.Home.Index());
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(externalLoginInfo, isPersistent: false);

            switch (result)
            {
                case SignInStatus.Success:
                    var userId = await VmRepository.UnfoUsers.GetUserIdByProviderKey(externalLoginInfo.Login.ProviderKey);
                    var dbUser = await UserManager.FindByIdAsync(userId);
                    var resultOfSetCookie = await SignInManager.SignInAsync(dbUser, false);
                    if (resultOfSetCookie.Succeeded)
                    {
                        return Redirect(returnUrl);
                    }
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                case SignInStatus.Failure:
                default:
                    IsExternalLoginSignUp = true;
                    // Если пользователь залогинился через соц сеть - но не имеет аккаунта в системе - просим его потвердить данные подтянутые из соцсети и регаем
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = externalLoginInfo.Login.LoginProvider;
                    var externalConfirmationVm = new ExternalSignInConfirmationVM(externalLoginInfo);
                    return View(MVC.Account.Views.SignUp, externalConfirmationVm);
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ExternalLoginConfirmation(ExternalSignInConfirmationVM signUpVM)
        {
            try
            {
                if (User.Identity.IsAuthenticated) { return RedirectToAction(MVC.Cabinet.Home.Index()); }

                if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);

                // Get the information about the user from the external login provider
                var signIninfo = await AuthenticationManager.GetExternalLoginInfoAsync();

                if (signIninfo == null)
                {
                    return View("ExternalLoginFailure");
                }

                var dbUser = signUpVM.GetModel();
                
                var resultIdentity = await UserManager.CreateAsync(dbUser);
                if (!resultIdentity.Succeeded) return AlertJson.Error(resultIdentity);

                resultIdentity = await UserManager.AddToRoleAsync(dbUser, Role.User);
                if (!resultIdentity.Succeeded) return AlertJson.Error(resultIdentity);

                resultIdentity = await UserManager.AddLoginAsync(dbUser.Id, signIninfo.Login);
                if (!resultIdentity.Succeeded) return AlertJson.Error(resultIdentity);

                var resultOfSetCookie = await SignInManager.SignInAsync(dbUser, false);
                return resultOfSetCookie.Succeeded ? AlertJson.Success() : AlertJson.Error(resultOfSetCookie);
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SignInWithLinkToExternalLogin(SignInVM signInVM)
        {
            if (User.Identity.IsAuthenticated) return AlertJson.Success(); 

            if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);

            // Get the information about the user from the external login provider
            var signIninfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (signIninfo == null) return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            var dbUser = await UserManager.FindByEmailAsync(signInVM.Email);
            var hasPassword = await UserManager.CheckPasswordAsync(dbUser, signInVM.Password);
            if (hasPassword)
            {
                var result = await UserManager.AddLoginAsync(dbUser.Id, signIninfo.Login);
                if (result.Succeeded)
                {
                    var resultOfSetCookie = await SignInManager.SignInAsync(dbUser, false);
                    return resultOfSetCookie.Succeeded ? AlertJson.Success() : AlertJson.Error(resultOfSetCookie);
                }

                return AlertJson.Error(result);
            }
            return AlertJson.Error(Resources.Account_SignIn_WrongPassword_Alert);

        }


        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public virtual ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public virtual ActionResult ExternalLoginFailure()
        {
            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public virtual async Task<ActionResult> CheckHasUserPassword(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            return user == null ? Json(new { Successed = false }, JsonRequestBehavior.AllowGet) : Json(new { Successed = user.PasswordHash != null }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [AllowAnonymous]
        public virtual async Task<ActionResult> GetExternalLogins(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            if (user == null) return Json(new { Successed = false }, JsonRequestBehavior.AllowGet);

            var userLogins = await UserManager.GetLoginsAsync(user.Id);

            return Json(new { Successed = userLogins.Count != 0, ExternalLogins = userLogins.Select(ul => ul.LoginProvider) }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, Guid? userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                if (userId != null) UserId = userId.Value;
            }

            public string LoginProvider { get; set; }

            public string RedirectUri { get; set; }

            public Guid UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != Guid.Empty)
                {
                    properties.Dictionary[XsrfKey] = UserId.ToString();
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Helpers

        #region Ajax Валидаторы

        /// <summary>
        /// Проверка email'a пользователя в базе данных на совпадение
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Возвращает true если валидация прошла успешно - тоесть такого emaila нет в системе</returns>
        [AllowAnonymous]
        public virtual async Task<ActionResult> ValidateEmailOfUserInSystem(string email)
        {
            //Если почта принадлежит текущему пользователю значит она валидна
            if (email == User.Email) { return Json(true, JsonRequestBehavior.AllowGet); }

            var isValideEmailOfUser = !(await VmRepository.UnfoUsers.CheckIsExistingEmailAsync(email));
            return Json(isValideEmailOfUser, JsonRequestBehavior.AllowGet);
        }

        #endregion Ajax Валидаторы
    }
}