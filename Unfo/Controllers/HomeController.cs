﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.Extensions.IdentityExtensions;

namespace Unfo.Controllers
{
    public partial class HomeController : BaseController
    {
        // GET: /Home/
        public virtual async Task<ActionResult> Index()
        {
            Alert.Success("Main layout place alert", "LA LA LA LA LA LA LA LA");
            Alert.Error(true,"Some other place test", " LA LA LA LA LA LA LA", "LA LA LA LA LA LA LA LA", "LA LA LA LA LA LA LA LA");
            Alert.Error("ololol ololol ololol");
            Alert.Warning("waka waka waka");
            Alert.Attention(true, "Some other place test: LO LO LO LO LO LO");

            return View();
        }
    }
}