﻿using System.Web.Mvc;
using Unfo.Core;
using Unfo.Core.Constraint;

namespace Unfo.Areas.Cabinet
{
    public class CabinetAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Cabinet";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
           
            context.MapRoute(
               Route.CreateEvent,
               "Cabinet/Event/Create",
               new { controller = "Events", action = "Create", id = UrlParameter.Optional },
                new string[] { "Unfo.Areas.Cabinet.Controllers" }
           );
            context.MapRoute(
               Route.UserInfo,
               "Cabinet/User",
               new { controller = "Users", action = "UserInfo", id = UrlParameter.Optional },
                new string[] { "Unfo.Areas.Cabinet.Controllers" }
           );
            context.MapRoute(
             name: Route.InfoEvent,
             url: "Cabinet/Event/{eventName}",
             defaults: new { controller = "Info", action = "Index", id = UrlParameter.Optional, eventName = "" },
             constraints: new { eventName = new EventConstraint() },
             namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers.EventManagement" }
             );

            context.MapRoute(
              name: Route.EditEvent,
              url: "Cabinet/Event/{eventName}/Edit",
              defaults: new { controller = "Events", action = "Edit", id = UrlParameter.Optional, eventName = "" },
              constraints: new { eventName = new EventConstraint() },
              namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers" }
              );
            context.MapRoute(
              name: Route.EventModerators,
              url: "Cabinet/Event/{eventName}/Moderators",
              defaults: new { controller = "Users", action = "Moderators", id = UrlParameter.Optional, eventName = "" },
              constraints: new { eventName = new EventConstraint() },
              namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers" }
              );
            context.MapRoute(
             name: Route.EventUsers,
             url: "Cabinet/Event/{eventName}/Users",
             defaults: new { controller = "Users", action = "Users", id = UrlParameter.Optional, eventName = "" },
             constraints: new { eventName = new EventConstraint() },
             namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers" }
             );
            context.MapRoute(
                name: Route.Event,
                url: "Cabinet/Event/{eventName}/{controller}/{action}/{id}",
                defaults: new { controller = "Events", action = "Index", id = UrlParameter.Optional },
                constraints: new { eventName = new EventConstraint() },
                namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers.EventManagement" }
                );

            context.MapRoute(
                name: Route.CabinetDefault,
                url: "Cabinet/{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Unfo.Areas.Cabinet.Controllers" }
           );

        }
    }
}