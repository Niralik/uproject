﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CsQuery.ExtensionMethods;
using DataModel;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Core.GlobalClasses;
using Unfo.Core.Wrappers;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.UnfoBusinessLogic.Events;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers
{
    //[Authorize(Roles = "Admin")]
    public partial class EventsController : BaseController
    {
        public EventsController()
        {
            ViewBag.BodyId = "events";
        }
        // GET: Cabinet/Events
        public virtual async Task<ActionResult> All(EventsFilterParams filterParams)
        {
            ViewBag.Title = Resources.Events_All;
            ViewBag.BodyClass = "all-events";
            var filteredEvents = await FiltrateEvents(filterParams);
            return View(MVC.Cabinet.Events.Views.Events, filteredEvents);
        }
        public virtual async Task<ActionResult> Favorite(EventsFilterParams filterParams)
        {
            ViewBag.Title = Resources.Events_Favorite;
            ViewBag.BodyClass = "favorite-events";
            filterParams = filterParams.SetQuery(EventsFilterTypeQuery.Favorite);
            var filteredEvents = await FiltrateEvents(filterParams);
            ViewBag.IsFavorite = true;
            return View(MVC.Cabinet.Events.Views.Events, filteredEvents);
        }
        public virtual async Task<ActionResult> MyModerate(EventsFilterParams filterParams)
        {
            ViewBag.Title = Resources.Events_MyModerate;
            ViewBag.BodyClass = "moderate-events";
            filterParams = filterParams.SetRole(FilterRole.Moderator);
            var filteredEvents = await FiltrateEvents(filterParams);
            return View(MVC.Cabinet.Events.Views.Events, filteredEvents);
        }
        public virtual async Task<ActionResult> Active(EventsFilterParams filterParams)
        {
            ViewBag.Title = Resources.Events_Active;
            ViewBag.BodyClass = "active-events";
            filterParams = filterParams.SetTime(FilterTime.Active);
            var filteredEvents = await FiltrateEvents(filterParams);
            return View(MVC.Cabinet.Events.Views.Events, filteredEvents);
        }
        public async Task<List<EventVM>> FiltrateEvents(EventsFilterParams filterParams)
        {
            var filter = new EventsFilter(filterParams);

            var dbEventsTask = filter.GetResults<EventVM>();
            var amountPagesTask = filter.GetAmountOfPages();

            await Task.WhenAll(dbEventsTask, amountPagesTask);

            filterParams.SummaryAmountOfPages = amountPagesTask.Result;
            ViewBag.EventsFilterParams = filterParams;

            return dbEventsTask.Result;
        }
        public virtual ActionResult Create()
        {
            return View(MVC.Cabinet.Events.Views.Edit);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(CreateEventVM data)
        {
            if (ModelState.IsValid)
            {
                var uid = new Guid?();
                if (User.Id != Guid.Empty)
                {
                    uid = User.Id;
                }
                var id = VmRepository.Events.Create(data, uid);
                return Json(Core.JsonResponse.GetSuccess(id));
            }

            var errors = Utilities.GetArrayOfErrors(ModelState.Values);
            return Json(JsonResponse.GetError(1, errors.ToList()));
        }



        public virtual ActionResult Edit(Guid? id)
        {

            if (!id.HasValue) id = CurrentEvent.ID;
            ViewBag.Places = VmRepository.Dictionaries.GetLocations(id.Value).Select(d => new SelectListItem()
            {
                Text = string.Format("{0} {1} {2}", d.CityName, d.StreetName, d.BuildingNumber),
                Value = d.ID.ToString()
            });

            ViewBag.Directions = VmRepository.Dictionaries.GetDirections(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.FullName,
                Value = d.ID.ToString()
            });

            var @event = VmRepository.Events.GetEvent<EventVM>(id);
            return View(@event);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(UpdateEventVM data)
        {
            data.ID = CurrentEvent.ID;
            if (!ModelState.IsValid)
            {
                return View(data);
            }
            await VmRepository.UnfoUsers.AddToEventModerators(data.ID, CurrentUser.Get.Id);
            await SignInManager.UpdateCookieForCurrentUserAsync();
            VmRepository.Events.Update(data);
            return RedirectToAction(MVC.Cabinet.Info.Index());
        }

        [HttpPost]
        public virtual ActionResult Delete(Guid id)
        {
            VmRepository.Events.DeleteEvent(id);
            return RedirectToAction(MVC.Cabinet.Events.All(new EventsFilterParams()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ToFavorite(Guid eventId, string eventName)
        {
            var checkingResult = await VmRepository.Events.CheckIsExistEvent(eventId);
            if (checkingResult.IsFailed) return AlertJson.Error("Мероприятие недоступно");

            var checkingIsFavoriteAlready = await VmRepository.Events.CheckIsFavoriteForUser(CurrentUser.Get.Id, eventId);
            if (checkingIsFavoriteAlready.Succeeded) return AlertJson.Error("Мероприятие уже в избранном");

            var toFavoriteResult = await VmRepository.Events.ToFavorite(CurrentUser.Get.Id, eventId);
            if (toFavoriteResult.IsFailed) return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            return AlertJson.Success(string.Format("Мероприятие - \"{0}\" добавлено в избранное", eventName));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> UnFavorite(Guid eventId, string eventName)
        {
            var checkingResult = await VmRepository.Events.CheckIsExistEvent(eventId);
            if (checkingResult.IsFailed) return AlertJson.Error("Мероприятие недоступно");

            var checkingIsFavoriteAlready = await VmRepository.Events.CheckIsFavoriteForUser(CurrentUser.Get.Id, eventId);
            if (checkingIsFavoriteAlready.IsFailed) return AlertJson.Error("Мероприятие уже убранно из избранного");

            var unFavoriteResult = await VmRepository.Events.UnFavorite(CurrentUser.Get.Id, eventId);
            if (unFavoriteResult.IsFailed) return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            return AlertJson.Success(string.Format("Мероприятие - \"{0}\" убранно из избранного", eventName));
        }

    }

}