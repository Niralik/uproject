﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataModel;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Core.Wrappers;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;

namespace Unfo.Areas.Cabinet.Controllers
{
    public partial class UsersController : BaseController
    {
        public UsersController()
        {
            ViewBag.BodyId = "users";
        }

        public virtual async Task<ActionResult> Moderators(UsersFilterParams filterParams)
        {
            filterParams.FilterRole = FilterRole.Moderator;
            filterParams.EventId = CurrentEvent.ID;
            var filteredUsers = await FiltrateUsers(filterParams);
            return View(MVC.Cabinet.Users.Views.Moderators, filteredUsers);
        }
        public virtual async Task<ActionResult> Search(UsersFilterParams filterParams)
        {
            filterParams.DisablePagination();
            var filteredUsers = await FiltrateUsers(filterParams);
            return Json(filteredUsers, JsonRequestBehavior.AllowGet);
        }

        // GET: /Cabinet/Home/
        public virtual async Task<ActionResult> Index(UsersFilterParams filterParams)
        {    
            var filteredUsers = await FiltrateUsers(filterParams);
            return View(filteredUsers);
        }
        public virtual async Task<ActionResult> Users(UsersFilterParams filterParams)
        {
            filterParams.FilterRole = FilterRole.User;
            filterParams.EventId = CurrentEvent.ID;
            var filteredUsers = await FiltrateUsers(filterParams);
            return View(filteredUsers);
        }
        private async Task<List<UnfoUserVM>> FiltrateUsers(UsersFilterParams filterParams)
        {
            var filter = new UsersFilter(filterParams);

            var usersTask = filter.GetResults<UnfoUserVM>();
            var amountPagesTask = filter.GetAmountOfPages();
           
            await Task.WhenAll(usersTask, amountPagesTask);

            filterParams.SummaryAmountOfPages = amountPagesTask.Result;
            ViewBag.UsersFilterParams = filterParams;

            return usersTask.Result;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> RemoveFromModerators(Guid eventId, List<Guid> usersIds)
        {
            try
            {
                var ownerOfEvent = await VmRepository.UnfoUsers.GetOwnerOfEvent(eventId);
                if (usersIds.Count == 1)
                {
                    var resultOfChecking = await VmRepository.UnfoUsers.CheckIsRemovingOrganizer(ownerOfEvent, usersIds.First());
                    if (resultOfChecking.IsFailed) return AlertJson.Error(Resources.Users_Alert_RemoveOrganizerError);
                }
                usersIds = await VmRepository.UnfoUsers.ClarifyUsersIdsFromOrganizer(ownerOfEvent, usersIds);

                await VmRepository.UnfoUsers.RemoveFromEventModerators(eventId, usersIds);

                await VmRepository.UnfoUsers.ActualizeUsersModeratorsRoles(usersIds);

                var declinatedAlertMessage = Utilities.Declination(usersIds.Count, Resources.Users_Alert_ModeratorRemovedNominativeSingular, Resources.Users_Alert_ModeratorRemovedNominativePlural, Resources.Users_Alert_ModeratorRemovedGenitivePlural);
                return AlertJson.Success(declinatedAlertMessage);
            }
            catch (Exception)
            {
                 return AlertJson.Error(Resources.Validate_Alert_IternalServerError);
            }
           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> AddToModerators(Guid eventId, List<Guid> usersIds)
        {
            var clearedUsersIds = await VmRepository.UnfoUsers.ClearFromExistingEventModerators(eventId, usersIds);
            string declinatedAlertMessage;
            if (clearedUsersIds.Count == 0)
            {
                declinatedAlertMessage = Utilities.Declination(usersIds.Count, Resources.Users_Alert_TryInsertAddedModerator_Singular, Resources.Users_Alert_TryInsertAddedModerator_Plural);
                return AlertJson.Error(declinatedAlertMessage);
            }
            var resultOfAdding = await VmRepository.UnfoUsers.AddToEventModerators(eventId, clearedUsersIds.ToArray());

            if (resultOfAdding.IsFailed) return AlertJson.Error(resultOfAdding);
            declinatedAlertMessage = Utilities.Declination(clearedUsersIds.Count, Resources.Users_Alert_AddedToModerators_Nominative_Singular, Resources.Users_Alert_AddedToModerators_Nominative_Plural, Resources.Users_Alert_AddedToModerators_Genitive_Plural);
            return AlertJson.Success(declinatedAlertMessage);
        }

        public virtual async Task<ActionResult> UserInfo(Guid userId)
        {

            var user = await VmRepository.UnfoUsers.GetByIdAsync<UnfoUserVM, UnfoUser>(userId);
            return View(user);
        }
    }
}