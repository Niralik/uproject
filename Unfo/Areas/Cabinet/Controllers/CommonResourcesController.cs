﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Unfo.Core;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Core.BaseClasses;

namespace Unfo.Areas.Cabinet.Controllers
{
    public partial class CommonResourcesController : BaseController
    {
        // GET: Cabinet/Resources
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual async Task<ActionResult> Kladr(string q, string cityId, string streetId)
        {
            if (string.IsNullOrWhiteSpace(q)) return Content("{}", "application/json");

            UriBuilder uri = new UriBuilder("http://kladr-api.ru/api.php");
            var query = new NameValueCollection();
            query.Add("token", "53d0bbf2fca916fd55830a2f");
            query.Add("key", "120f477264e6fecda2544105d13374e46e5aa501");
            query.Add("query", q);

            if (!string.IsNullOrWhiteSpace(cityId))
            {
                query.Add("cityId", cityId);
                query.Add("contentType", "street");
            }
            else
            {
                query.Add("contentType", "city");
            }


            uri.Query = string.Join("&", query.AllKeys.Select(key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(query[key]))));

            var client = new HttpClient();
            var response = await client.GetAsync(uri.ToString());

            response.EnsureSuccessStatusCode();

            string content = await response.Content.ReadAsStringAsync();

            return Content(content, "application/json");
        }

        //public ActionResult FindSpeaker(string email)
        //{

        //}


        [HttpPost]
        public virtual ActionResult UploadImage()
        {
            if (Request.Files.Count == 1)
            {
                var result = Core.ImageUpload.SaveImage(Request.Files[0], VmRepository.Resources);

                return Json(JsonResponse.GetSuccess(result));

            }
            return Json(JsonResponse.GetError(1));
        }


        [HttpPost]
        public virtual ActionResult CropImage(ImageCropperVM data)
        {

            if (ModelState.IsValid)
            {
                var id = Core.ImageUpload.CropImage(data, VmRepository.Resources);
                return Json(JsonResponse.GetSuccess(id));
            }

            return Json(JsonResponse.GetError(1));
        }

    }
}