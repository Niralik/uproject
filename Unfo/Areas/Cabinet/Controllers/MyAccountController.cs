﻿using System.Threading;
using DataModel;
using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Owin.Security.Providers.ArcGISOnline.Provider;
using Unfo.App_LocalResources;
using Unfo.Controllers;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Core.Wrappers;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Areas.Cabinet.Controllers
{
    [Authorize]
    public partial class MyAccountController : BaseController
    {
        // GET: Cabinet/Profile
        public MyAccountController()
        {
            ViewBag.BodyId = "account";
            ViewBag.IsMyAccount = true;
            Alert.IsPageView = true;
        }

        public virtual ActionResult Index()
        {
            return RedirectToAction(MVC.Cabinet.MyAccount.Profile());
        }

        #region Страница редактирования профиля
        [HttpGet]
        public new virtual async Task<ActionResult> Profile()
        {
            var currentDbUser = await UserManager.FindByIdAsync(User.Id);
            var currentUser = new UnfoUserVM(currentDbUser);
            return View(currentUser);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Profile(UpdateUnfoUserVM updatingUnfoUserVm)
        {
            if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);

            var dbUser = await UserManager.FindByIdAsync(updatingUnfoUserVm.Id);

            var updatedDbUser = updatingUnfoUserVm.UpdateModel(dbUser);

            var updateResult = await UserManager.UpdateAsync(updatedDbUser);

            if (updateResult.Succeeded)
            {
                await SignInManager.UpdateCookieForCurrentUserAsync();
                return AlertJson.Success(Resources.MyAccount_Profile_Alert_UpdateSuccessed);
            }
            return AlertJson.Error(updateResult);
        }
        #endregion Страница редактирования профиля

        #region Страница управления привязками к соц сетям
        [HttpGet]
        public virtual async Task<ActionResult> ManageExternalLogins(AccountStatusMessage? statusMessage)
        {
            if (statusMessage.HasValue) GenerateAlertByAccountStatusMessage(statusMessage.Value);

            var externalLoginsOfUser = UserManager.GetLogins(User.Id);
            var manageExternalLoginsVm = new ManageExternalLoginsVM(externalLoginsOfUser);

            return View(manageExternalLoginsVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ToLinkExternalLogin(string externalLogin)
        {
            ExternalLogins enumExternalLogin;
            var isSuccessParseExternalLogin = Enum.TryParse(externalLogin, true, out enumExternalLogin);
            if (isSuccessParseExternalLogin)
            {
                return new AccountController.ChallengeResult(externalLogin, Url.Action(MVC.Cabinet.MyAccount.ToLinkExternalLoginCallback()), User.Id);
            }

            return RedirectToAction(MVC.Cabinet.MyAccount.ManageExternalLogins(AccountStatusMessage.Error));
        }
        public virtual async Task<ActionResult> ToLinkExternalLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Id.ToString());
            if (loginInfo == null) return RedirectToAction(MVC.Cabinet.MyAccount.ManageExternalLogins(AccountStatusMessage.Error));

            var addResult = await UserManager.AddLoginAsync(User.Id, loginInfo.Login);
            if (addResult.Succeeded)
            {
                return RedirectToAction(MVC.Cabinet.MyAccount.ManageExternalLogins(AccountStatusMessage.ToLinkExternalLoginSuccess));
            }
            if (addResult.Errors.First() == "A user with that external login already exists.")
            {
                return RedirectToAction(MVC.Cabinet.MyAccount.ManageExternalLogins(AccountStatusMessage.UserWithExternalLoginExistError));
            }
            return RedirectToAction(MVC.Cabinet.MyAccount.ManageExternalLogins(AccountStatusMessage.Error));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> RemoveExternalLogin(string externalLogin)
        {
            var externalLoginsOfUser = await UserManager.GetLoginsAsync(User.Id);
            if (externalLoginsOfUser.Count == 1 && !User.HasPassword) return AlertJson.Error(string.Format(Resources.MyAccount_RemoveExternalLogin_OneLoginAndNoPassword, externalLogin));
            if (externalLoginsOfUser.Count == 1 && !User.IsEmailConfirmed) return AlertJson.Error(string.Format(Resources.MyAccount_RemoveExternalLogin_OneLoginAndEmailNotConfirmed, externalLogin));

            var providerKey = externalLoginsOfUser.Where(l => l.LoginProvider == externalLogin).Select(l => l.ProviderKey).SingleOrDefault();
            var removeResult = await UserManager.RemoveLoginAsync(User.Id, new UserLoginInfo(externalLogin, providerKey));
            if (removeResult.Succeeded)
            {
                return AlertJson.Success(string.Format(Resources.MyAccount_RemoveExternalLogin_Alert_Success, externalLogin));
            }
            return AlertJson.Error(Resources.Validate_Alert_IternalServerError);
        }
        #endregion Страница управления привязками к соц сетям

        #region Страница Изменения и Добавления пароля
        [HttpGet]
        public virtual async Task<ActionResult> ManagePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ManagePassword(ManagePasswordVM managePasswordVm)
        {
            if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);
            ActionResult result;
            if (User.HasPassword)
            {
                result = await VmRepository.MyAccount.ChangePassword(managePasswordVm);
            }
            else
            {
                result = await VmRepository.MyAccount.AddPassword(managePasswordVm);
            }
            return result;
        }
        #endregion Страница Изменения и Добавления пароля

        #region Страница изменения политик приватности

        [HttpGet]
        public virtual async Task<ActionResult> UserPrivacyPolitics()
        {
            var currentDbUser = await UserManager.FindByIdAsync(User.Id);
            var userPrivacyPoliticsVm = new UserPrivacyPoliticsVM(currentDbUser);
            return View(userPrivacyPoliticsVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ChangeUserPrivacyPolitic(UserPrivacyPoliticVM userPrivacyPoliticVm)
        {
            if (!ModelState.IsValid) return AlertJson.Error(ModelState.Values);
            
            var updatingUser = await UserManager.FindByIdAsync(userPrivacyPoliticVm.UserID);
            if (updatingUser == null) return AlertJson.Error();

            var result = await VmRepository.UnfoUsers.ChangeUserPrivacyPolitic(updatingUser, userPrivacyPoliticVm);
            if (result.IsFailed) return AlertJson.Error(result);

            return userPrivacyPoliticVm.IsPublic ? AlertJson.Success(Resources.MyAccount_PrivacyPolicies_Alert_DataOpened) : AlertJson.Success(Resources.MyAccount_PrivacyPolicies_Aler_DataClosed);
        }



        #endregion Страница изменения политик приватности

        #region Страница запросов пользователя

        public virtual async Task<ActionResult> UserRequests()
        {
            var userRequestVm = await VmRepository.MyAccount.GetUserRequest(User.Id);

            return View(userRequestVm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> NewUserRequest(UserRequestVm userRequestVm)
        {
            userRequestVm.SetAuthor(User.Id);
            userRequestVm.RequestState = RequestState.InProgress;
            var creatingUserRequestResult = await VmRepository.MyAccount.CreateNewUserRequest(userRequestVm);
            if (creatingUserRequestResult.Succeeded) return AlertJson.Success(Resources.MyAccount_NewUserRequest_Success);
            return AlertJson.Error(Resources.Validate_Alert_IternalServerError);
        }

      
        #endregion

        #region private helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public void GenerateAlertByAccountStatusMessage(AccountStatusMessage statusMessage)
        {
            switch (statusMessage)
            {
                case AccountStatusMessage.ToLinkExternalLoginSuccess:
                    Alert.Success(true, Resources.MyAccount_ExternalLogins_Alert_ToLinkSuccess);
                    break;

                case AccountStatusMessage.UserWithExternalLoginExistError:
                    Alert.Error(true, Resources.MyAccount_ExternalLogins_Alert_ExistLinkError);
                    break;

                case AccountStatusMessage.Error:
                    Alert.Error(true, Resources.Validate_Alert_IternalServerError);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("statusMessage");
            }
        }
        #endregion private helpers

       
    }
 
}