﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using CsQuery.ExtensionMethods;
using DataModel;
using Unfo.App_LocalResources;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Core.Wrappers;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;

namespace Unfo.Areas.Cabinet.Controllers
{
    public partial class UserRequestsController : BaseController
    {
        public UserRequestsController()
        {
            ViewBag.BodyId = "usersRequests";
        }

        public virtual async Task<ActionResult> Index(UsersRequestsFilterParams filterParams)
        {
            var filteredUsersRequests = await FiltrateUsersRequests(filterParams);
            return View(filteredUsersRequests);
        }
        public async Task<List<UserRequestAdminVM>> FiltrateUsersRequests(UsersRequestsFilterParams filterParams)
        {
            filterParams.AmountOfElementsPerPage = 25;
            var filter = new UsersRequestsFilter(filterParams);

            var dbRequestsTask = filter.GetResults();
            var amountPagesTask = filter.GetAmountOfPages();

            await Task.WhenAll(dbRequestsTask, amountPagesTask);

            filterParams.SummaryAmountOfPages = amountPagesTask.Result;
            ViewBag.UsersFilterParams = filterParams;
            var usersRequestsVm = new List<UserRequestAdminVM>();
            dbRequestsTask.Result.GroupBy(request => request.AuthorID).ForEach(requestsOfOneAuthor => usersRequestsVm.Add(new UserRequestAdminVM(requestsOfOneAuthor)));
            return usersRequestsVm;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Approve(Guid userRequestId)
        {
            var userRequestVm = await VmRepository.UsersRequests.GetByIdAsync<UserRequestVm, Request>(userRequestId);
            userRequestVm.RequestState = RequestState.Completed;
            userRequestVm.SetAdmin(CurrentUser.Get.Id);
            var updateResult = await VmRepository.UsersRequests.AddOrUpdateAsync(userRequestVm);
            if (updateResult.IsFailed) return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            var addToRoleResult = await UserManager.AddToRoleAsync(userRequestVm.Author.Id, Role.Organizer.ToString());
            if (!addToRoleResult.Succeeded && addToRoleResult.Errors.Single() != "User already in role.") return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            return AlertJson.Success(string.Format("Пользователь \"{0}\" - одобрен в качестве организатора.", userRequestVm.Author.Fio));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Abort(Guid userRequestId)
        {
            var userRequestVm = await VmRepository.UsersRequests.GetByIdAsync<UserRequestVm, Request>(userRequestId);
            userRequestVm.RequestState = RequestState.Aborted;
            userRequestVm.SetAdmin(CurrentUser.Get.Id);
            var updateResult = await VmRepository.UsersRequests.AddOrUpdateAsync(userRequestVm);
            if (updateResult.IsFailed) return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            var removeFromRoleResult = await UserManager.RemoveFromRoleAsync(userRequestVm.Author.Id, Role.Organizer.ToString());
            if (!removeFromRoleResult.Succeeded && removeFromRoleResult.Errors.Single() != "User is not in role.") return AlertJson.Error(Resources.Validate_Alert_IternalServerError);

            return AlertJson.Success(string.Format("Пользователю \"{0}\" - отказано в получение статуса организатора.", userRequestVm.Author.Fio));
        }
    }
}