﻿using System;
using System.Web.Mvc;
using Unfo.Core.BaseClasses;

namespace Unfo.Areas.Cabinet.Controllers
{
    public partial class ContentController : BaseController
    {
        // GET: Cabinet/Content
        public virtual ActionResult LanguageTabs(Guid? eid, string lang)
        {
            return PartialView("_LanguageTabs", VmRepository.Resources.GetAccessCultures(eid));
        }
    }
}