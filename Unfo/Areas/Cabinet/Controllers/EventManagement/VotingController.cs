﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using DataModel;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class VotingController : BaseController
    {
        /// <summary>
        /// Голосования
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Index(string uid)
        {
            var data = VmRepository.Votings.GetVotes(CurrentEvent.ID);
            return View(data);
        }

        public virtual ActionResult Edit(Guid? id)
        {
            VoteViewModel data;
            if (id.HasValue && id.Value != Guid.Empty)
            {
                data = VmRepository.Votings.GetVoteWithResults(CurrentEvent.ID, id.Value);
            }
            else
            {
                var questions = new List<VoteQuestionViewModel>()
                {
                    new VoteQuestionViewModel(),
                    new VoteQuestionViewModel(),
                };

                data = new VoteViewModel(CurrentEvent.ID) {Questions = questions};
            }
          

            return View(data);
        }


        [HttpPost]
        public virtual ActionResult Edit(VoteViewModel data)
        {
            if (ModelState.IsValid)
            {
                
                

                var voteID = VmRepository.Votings.AddOrUpdate(data);
                if (CurrentEvent.GetAccessCultures().Count > 1)
                {
                    Alert.Error(true, "Голосование сохранено");
                    return RedirectToAction("Edit", new {id = voteID});
                }
                return RedirectToAction("Index");
            }

            return View(data);
        }

        [HttpGet]
        public virtual ActionResult GetVoteQuestion(string lang)
        {
            var data = new VoteQuestionViewModel();
            return PartialView(MVC.Cabinet.Shared.Views.EditorTemplates.VoteQuestionViewModel, data);
        }

        public virtual ActionResult ToogleActivate(Guid? id)
        {
            VoteViewModel data;
            if (id.HasValue && id.Value != Guid.Empty)
            {
                VmRepository.Votings.ToogleVote(id.Value);
            }

            Alert.Error(true, "Не могу найти голосование");
            return RedirectToAction("Index");
        }

        public virtual ActionResult Show(Guid id)
        {
            if (id == Guid.Empty) return RedirectToAction("Index");

            var data = VmRepository.Votings.GetVoteWithResults(CurrentEvent.ID, id);
            bool canVote = !VmRepository.Votings.HasVote(id, User.Id);
            ViewBag.CanVote = canVote;

            return View(data);
        }

        [HttpPost]
        public virtual ActionResult Delete(Guid? id)
        {
            VoteViewModel data;
            if (id.HasValue && id.Value != Guid.Empty)
            {
                VmRepository.Votings.DeleteVote(id.Value);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public virtual ActionResult Vote(Guid questionId)
        {
            var data = VmRepository.Votings.GetVoteByQuestion(questionId, CurrentLocalization.Edit);
            if (data == null && data.ID != Guid.Empty)
            {
                Alert.Error(true, "Не могу найти голосование");
                return RedirectToAction("Index");
            }

            if (!VmRepository.Votings.HasVote(questionId, User.Id))
                VmRepository.Votings.Vote(questionId, User.Id);


            return RedirectToAction("Index");
           
        }

        [HttpPost]
        public virtual ActionResult VoteWithAnswer(Guid voteID, string answer)
        {
            if (voteID != Guid.Empty && !String.IsNullOrEmpty(answer))
                VmRepository.Votings.VoteWithAnswer(voteID, answer);

            return RedirectToAction("Index", new { id = voteID });
        }
    }
    
}