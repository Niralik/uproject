﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataModel;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class ResourcesController : BaseController
    {
        // GET: Cabinet/Resources

        public virtual ActionResult GetList(string type)
        {
            switch (type)
            {
                case "speaker":
                    {
                        return
                            Json(
                                JsonResponse.GetSuccess(
                                    VmRepository.Dictionaries.GetSpeackers(CurrentEvent.ID).Select(d => new SelectListItem()
                                    {
                                        Text = d.Name,
                                        Value = d.ID.ToString()
                                    })), JsonRequestBehavior.AllowGet);


                    }
                case "hall":
                {
                    return
                        Json(
                            JsonResponse.GetSuccess(
                                VmRepository.Dictionaries.GetHalls(CurrentEvent.ID).Select(d => new SelectListItem()
                                {
                                    Text = d.FullName,
                                    Value = d.ID.ToString()
                                })), JsonRequestBehavior.AllowGet);
                    
                    
                }
                case "direction":
                {
                    return
                        Json(
                            JsonResponse.GetSuccess(
                                VmRepository.Dictionaries.GetDirections(CurrentEvent.ID).Select(d => new SelectListItem()
                                {
                                    Text = d.FullName,
                                    Value = d.ID.ToString()
                                })), JsonRequestBehavior.AllowGet);


                }
                case "section":
                {
                    return
                        Json(
                            JsonResponse.GetSuccess(
                                VmRepository.Dictionaries.GetSections(CurrentEvent.ID).Select(d => new SelectListItem()
                                {
                                    Text = d.FullName,
                                    Value = d.ID.ToString()
                                })), JsonRequestBehavior.AllowGet);



                }
                case "location":
                {
                    return
                        Json(
                            JsonResponse.GetSuccess(
                                VmRepository.Dictionaries.GetLocations(CurrentEvent.ID).Select(d => new SelectListItem()
                                {
                                    Text = string.Format("{1} {2}", d.CityName, d.StreetName, d.BuildingNumber),
                                    Value = d.ID.ToString()
                                })), JsonRequestBehavior.AllowGet);


                }
            }
            return Json(JsonResponse.GetError(1), JsonRequestBehavior.AllowGet);
        }
    }
}