﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using DataModel;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.Dictionaries;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class SponsorsController : BaseController
    {
        public virtual ActionResult Index()
        {

            var sponsors = VmRepository.Dictionaries.GetList<SponsorVM, Sponsor>(CurrentEvent.ID);
            return View(sponsors);
        }

        public virtual ActionResult Add()
        {

            return View(new SponsorVM());
        }

        [HttpPost]
        public virtual ActionResult Add(SponsorVM data)
        {
            if (ModelState.IsValid)
            {
                data.EventID = CurrentEvent.ID;
                VmRepository.Dictionaries.AddOrUpdate(data);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}
