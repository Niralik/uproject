﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Services.Description;
using DataModel;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    /// <summary>
    /// Справочники типа зал, направление и т.д.
    /// </summary>
    public partial class DictionariesController : BaseController
    {
        //
        // GET: /Cabinet/Dictionaries/
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult Locations()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Locations", VmRepository.Dictionaries.GetLocations(CurrentEvent.ID));
            }
            return View(VmRepository.Dictionaries.GetLocations(CurrentEvent.ID));
        }

        public virtual ActionResult EditLocation(Guid? id)
        {
            ViewBag.Locations = VmRepository.Resources.GetCountries();
            if (id.HasValue)
            {

                var data = VmRepository.Dictionaries.Get<LocationVM, Location>(CurrentEvent.ID, id.Value);
                return PartialView("_EditLocationModal", data);

            }

            return PartialView("_EditLocationModal", new LocationVM(CurrentEvent.ID).CreateLocalizeForm());
        }

        [HttpPost]
        public virtual ActionResult EditLocation(LocalizeForm<LocationVM> data)
        {
            if (ModelState.IsValid)
            {
                var direction = new LocationVM(CurrentEvent.ID);
                direction.SetLocalizeForms(data.Forms);
                VmRepository.Dictionaries.AddOrUpdate<Location>(direction);

                return Json(JsonResponse.GetSuccess(1));
            }
            return Json(JsonResponse.GetError(1, Core.Utilities.GetArrayOfErrors(ModelState.Values).ToList())); ;
        }


        [HttpPost]
        public virtual ActionResult DeleteLocation(Guid id)
        {
            VmRepository.Dictionaries.DeleteLocation(id, CurrentEvent.ID);
            return Json(JsonResponse.GetSuccess(1));

        }



        public virtual ActionResult Directions(int? typeView)
        {
            if (Request.IsAjaxRequest())
            {
                if (!typeView.HasValue)
                    return PartialView("_Directions", VmRepository.Dictionaries.GetDirections(CurrentEvent.ID));

                var editDirections = new EditDirections();
                editDirections.DirectionItems =
                    VmRepository.Dictionaries.GetDirections(CurrentEvent.ID)
                        .Select(d => new KeyValuePair<Guid, string>(d.ID, d.FullName))
                        .ToList();

                return PartialView("_EditDirections", editDirections);
            }
            return View(VmRepository.Dictionaries.GetDirections(CurrentEvent.ID));
        }


        public virtual ActionResult EditDirection(Guid? id)
        {
            if (id.HasValue)
            {
                var data = VmRepository.Dictionaries.Get<DirectionVM, Direction>(CurrentEvent.ID, id.Value);
                return PartialView("_EditDirectionModal", data);

            }

            return PartialView("_EditDirectionModal", new DirectionVM(CurrentEvent.ID).CreateLocalizeForm());
        }


        [HttpPost]
        public virtual ActionResult EditDirection(LocalizeForm<DirectionVM> data)
        {



            if (ModelState.IsValid)
            {
                var direction = new DirectionVM(CurrentEvent.ID);
                direction.SetLocalizeForms(data.Forms);
                VmRepository.Dictionaries.AddOrUpdate<Direction>(direction);

                return Json(JsonResponse.GetSuccess(1));
            }
            return Json(JsonResponse.GetError(1, Core.Utilities.GetArrayOfErrors(ModelState.Values).ToList())); ;


        }

        [HttpPost]
        public virtual ActionResult DeleteDirection(Guid id)
        {
            VmRepository.Dictionaries.Delete<Direction>(id, CurrentEvent.ID);
            return Json(JsonResponse.GetSuccess(1));

        }


        #region Halls


        public virtual ActionResult Halls()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Halls", VmRepository.Dictionaries.GetHalls(CurrentEvent.ID));
            }
            return View(VmRepository.Dictionaries.GetHalls(CurrentEvent.ID));
        }

        public virtual ActionResult EditHall(Guid? id)
        {
            if (id.HasValue)
            {
                var data = VmRepository.Dictionaries.Get<HallVM, Hall>(CurrentEvent.ID, id.Value);
                return PartialView("_EditHallModal", data);

            }

            return PartialView("_EditHallModal", new HallVM(CurrentEvent.ID).CreateLocalizeForm());
        }

        [HttpPost]
        public virtual ActionResult EditHall(LocalizeForm<HallVM> data)
        {
            if (ModelState.IsValid)
            {
                var hall = new HallVM(CurrentEvent.ID);
                hall.SetLocalizeForms(data.Forms);

                VmRepository.Dictionaries.AddOrUpdate<Hall>(hall);
                return Json(JsonResponse.GetSuccess(1));
            }
            return Json(JsonResponse.GetError(1, Core.Utilities.GetArrayOfErrors(ModelState.Values).ToList())); ;
        }

        [HttpPost]
        public virtual ActionResult DeleteHall(Guid id)
        {
            VmRepository.Dictionaries.Delete<Hall>(id, CurrentEvent.ID);
            return Json(JsonResponse.GetSuccess(1));

        }

        #endregion

        #region Sections


        public virtual ActionResult Sections()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Sections", VmRepository.Dictionaries.GetSections(CurrentEvent.ID));
            }
            var data = VmRepository.Dictionaries.GetSections(CurrentEvent.ID);
            return View(data);
        }

        public virtual ActionResult EditSection(Guid? id)
        {
            if (id.HasValue)
            {
                var data = VmRepository.Dictionaries.Get<SectionVM, Section>(CurrentEvent.ID, id.Value);
                return PartialView("_EditSectionModal", data);

            }

            return PartialView("_EditSectionModal", new SectionVM(CurrentEvent.ID).CreateLocalizeForm());
        }

        [HttpPost]
        public virtual ActionResult EditSection(LocalizeForm<SectionVM> data)
        {
            if (ModelState.IsValid)
            {
                var section = new SectionVM(CurrentEvent.ID);
                section.SetLocalizeForms(data.Forms);

                VmRepository.Dictionaries.AddOrUpdate<Section>(section);
                return Json(JsonResponse.GetSuccess(1));
            }
            return Json(JsonResponse.GetError(1, Core.Utilities.GetArrayOfErrors(ModelState.Values).ToList())); ;
        }

        [HttpPost]
        public virtual ActionResult DeleteSection(Guid id)
        {
            VmRepository.Dictionaries.Delete<Section>(id, CurrentEvent.ID);
            return Json(JsonResponse.GetSuccess(1));

        }

        #endregion


    }

}