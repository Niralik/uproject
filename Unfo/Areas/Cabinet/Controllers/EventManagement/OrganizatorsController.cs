﻿using System.Web.Mvc;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels.Dictionaries;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class OrganizatorsController : BaseController
    {
        // GET: Cabinet/Organizators
        public virtual ActionResult Index()
        {
            var data = VmRepository.Dictionaries.GetOrganization(CurrentEvent.ID);
            return View(data);
        }

        [HttpPost]
        public virtual ActionResult Index(PromoOrganizatorsVM data)
        {
            if (ModelState.IsValid)
            {
                data.EventID = CurrentEvent.ID;
                VmRepository.Dictionaries.UpdateOrganizators(data);
                Alert.Success("Информация сохранена");
            }
            return View(data);
        }
    }
}