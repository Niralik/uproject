﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class SettingsController : BaseController
    {
        // GET: Cabinet/Settings
        public virtual ActionResult Index()
        {
            var data = VmRepository.SettingsEvent.GetSettings(CurrentEvent.ID);

            ViewBag.SectionsList = VmRepository.Resources.GetServiceSections(data.ServiceSectionsIds.ToArray());
            ViewBag.Cultures = VmRepository.Resources.GetAccessCulturesForLocalization(data.SupportLanguages);
            return View(data);
        }

        [HttpPost]
        public virtual ActionResult Index(SettingsVM data)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0 && Request.Files[0] != null)
                {
                    var fileId = Guid.NewGuid();
                    var fileName = fileId.ToString() + Path.GetExtension(Request.Files[0].ContentType);

                    FileManager.SaveFile(Request.Files[0].InputStream, fileName, TypeOfFile.Hide);

                    VmRepository.Resources.SaveDocument(fileId, fileName, DataModel.ContentType.Certificate);
                    data.CertificateId = fileId;
                }
                else
                {
                    data.CertificateId = null;
                }

                VmRepository.SettingsEvent.UpdateSettings(data);
            }
            ViewBag.SectionsList = VmRepository.Resources.GetServiceSections();
            ViewBag.Cultures = VmRepository.Resources.GetAccessCulturesForLocalization();
            return View(data);
        }

    }
}