﻿using System;
using System.Linq;
using System.Web.Mvc;
using DataModel;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    public partial class SpeakersController : BaseController
    {
        //
        // GET: /Cabinet/Speakers/
        public virtual ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Speakers", VmRepository.Dictionaries.GetSpeackers(CurrentEvent.ID));

            }
            return View(VmRepository.Dictionaries.GetSpeackers(CurrentEvent.ID));
        }

        public virtual ActionResult Edit(Guid? id)
        {
            if (id.HasValue)
            {
                var data = VmRepository.Dictionaries.Get<SpeakerVM, Speaker>(CurrentEvent.ID, id.Value);
                return PartialView("_EditSpeakerModal", data);

            }

            return PartialView("_EditSpeakerModal", new SpeakerVM(CurrentEvent.ID).CreateLocalizeForm());
        }

        [HttpPost]
        public virtual ActionResult Edit(LocalizeForm<SpeakerVM> data)
        {
            if (ModelState.IsValid)
            {
                var speaker = new SpeakerVM(CurrentEvent.ID);
                speaker.SetLocalizeForms(data.Forms);
                VmRepository.Dictionaries.AddOrUpdate<Speaker>(speaker);

                return Json(JsonResponse.GetSuccess(1));
            }
            return Json(JsonResponse.GetError(1, Core.Utilities.GetArrayOfErrors(ModelState.Values).ToList())); ;
        }


        [HttpPost]
        public virtual ActionResult Delete(Guid id)
        {
            VmRepository.Dictionaries.Delete<Speaker>(id, CurrentEvent.ID);
            return Json(JsonResponse.GetSuccess(1));

        }

    }
   
}