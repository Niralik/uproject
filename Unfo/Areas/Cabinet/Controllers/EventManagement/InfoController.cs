﻿using System;
using System.Web.Mvc;
using DataModel;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    /// <summary>
    /// Информация о мероприятии
    /// </summary>
    public partial class InfoController : BaseController
    {
        public virtual ActionResult Index()
        {
            var @event = VmRepository.Events.GetEvent<InfoEventVM>(CurrentEvent.ID);
            return View(@event);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SendToApprove(Guid id)
        {
            return RedirectToRoute(Route.Event, new { Action = "Index" });
        }
    }
}