﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataModel;
using OrangeJetpack.Localization;
using Unfo.Core;
using Unfo.Core.BaseClasses;
using Unfo.Core.CustomValidations;
using Unfo.Core.Extensions;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    /// <summary>
    /// Программа из докладов на событии
    /// </summary>
    [CanEdit]
    public partial class ProgrammController : BaseController
    {
        public virtual ActionResult Index()
        {
            SetFilterData();

            var data = VmRepository.Programm.GetList<ReportBaseVM, Report>(CurrentEvent.ID);
            return View(data);
        }

        void SetFilterData()
        {
            var fvm = new OrderFilterVM();
            fvm.Dates =
                CurrentEvent.UEvent.StartDate.DatesToDate(CurrentEvent.UEvent.FinishDate)
                    .Select(d => new SelectListItem()
                    {
                        Text = d.ToShortDateString(),
                        Value = d.ToShortDateString()
                    }).ToList();
            //fvm.Date = Request.Params["date"];


          

            fvm.Directions =
                CurrentEvent.UEvent.Directions
                    .Select(d => new SelectListItem()
                    {
                        Text = d.Localize(CurrentLocalization.Edit, f => f.Name).Name,
                        Value = d.ID.ToString()
                    }).ToList();
           
            fvm.Halls =
                CurrentEvent.UEvent.Halls
                    .Select(d => new SelectListItem()
                    {
                        Text = d.Localize(CurrentLocalization.Edit, f => f.Name).Name,
                        Value = d.ID.ToString(),
                        Selected = Request.Params["hall"] == d.ID.ToString()
                    }).ToList();

           
            fvm.Sections =
                CurrentEvent.UEvent.Sections
                    .Select(d => new SelectListItem()
                    {
                        Text = d.Localize(CurrentLocalization.Edit, f => f.Name).Name,
                        Value = d.ID.ToString()
                    }).ToList();



            fvm.Dates.Insert(0, new SelectListItem()
            {
                Text = "Все",
                Value = ""
            });
            fvm.Directions.Insert(0, new SelectListItem()
            {
                Text = "Все",
                Value = ""
            });
            fvm.Halls.Insert(0, new SelectListItem()
            {
                Text = "Все",
                Value = ""
            });
            fvm.Sections.Insert(0, new SelectListItem()
            {
                Text = "Все",
                Value = ""
            });



            ViewBag.FilterData = fvm;
        }

        public virtual ActionResult Add(Guid? direction, Guid? hall, Guid? section)
        {
            //Guid direction = Guid.Empty;
            //if (Request.Params["direction"] != null)
            //{
            //    Guid.TryParse(Request.Params["direction"], out direction);
            //}

            //Guid hall = Guid.Empty;
            //if (Request.Params["hall"] != null)
            //{
            //    Guid.TryParse(Request.Params["hall"], out hall);
            //}
            //Guid section = Guid.Empty;
            //if (Request.Params["section"] != null)
            //{
            //    Guid.TryParse(Request.Params["section"], out section);
            //}

            SetViewBagForReport(hall.HasValue ? hall.Value : Guid.Empty, section.HasValue ? section.Value : Guid.Empty, direction);
            return View();
        }

        [HttpPost]
        public virtual ActionResult Add(ReportVM data)
        {

            if (ModelState.IsValid)
            {
                data.EventId = CurrentEvent.ID;
                VmRepository.Programm.AddOrUpdate(data);
                return RedirectToAction("Index");
            }
            SetViewBagForReport(data.Hall.Id, data.Section.Id, data.Direction.Id);

            return View(data);
        }


        public virtual ActionResult Edit(Guid id)
        {
            var data = VmRepository.Programm.Get(id);

            


            

            SetViewBagForReport(
                data.Hall != null ? data.Hall.Id : Guid.Empty,
                data.Section != null ? data.Section.Id : Guid.Empty,
                (data.Direction != null ? data.Direction.Id : Guid.Empty));

            return View(data);
        }

        [HttpPost]
        public virtual ActionResult Edit(ReportVM data)
        {
            if (ModelState.IsValid)
            {
                data.EventId = CurrentEvent.ID;
                VmRepository.Programm.AddOrUpdate(data);
                return RedirectToAction("Index");
            }

            SetViewBagForReport(
                data.Hall != null ? data.Hall.Id : Guid.Empty,
                data.Section != null ? data.Section.Id : Guid.Empty,
                (data.Direction != null ? data.Direction.Id : null));


            return View(data);
        }

        public virtual ActionResult SpeakerItem(bool hasSubreport)
        {
            ViewBag.HasSubReport = hasSubreport;

            ViewBag.Speakers = VmRepository.Dictionaries.GetSpeackers(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.Name + " (" + d.Company + ")",
                Value = d.ID.ToString(),
            });

            return PartialView("_SpeakerItem", new SpeakerItem());
        }

        void SetViewBagForReport(Guid hallId, Guid section, Guid? direction)
        {
            ViewBag.Halls = VmRepository.Dictionaries.GetHalls(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.FullName,
                Value = d.ID.ToString(),
                Selected = d.ID == hallId
            });

            ViewBag.Sections = VmRepository.Dictionaries.GetSections(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.FullName,
                Value = d.ID.ToString(),
                Selected = d.ID == section
            });

            ViewBag.Direction = VmRepository.Dictionaries.GetDirections(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.FullName,
                Value = d.ID.ToString(),
                Selected = direction.HasValue && direction == d.ID
            });

            ViewBag.Speakers = VmRepository.Dictionaries.GetSpeackers(CurrentEvent.ID).Select(d => new SelectListItem()
            {
                Text = d.Name + " (" + d.Company + ")",
                Value = d.ID.ToString(),
            });
        }


    }

}