﻿using System.Web.Mvc;
using Unfo.Core;
using Unfo.Core.BaseClasses;

namespace Unfo.Areas.Cabinet.Controllers.EventManagement
{
    /// <summary>
    /// Объявления
    /// </summary>
    public partial class AdvertsController : BaseController
    {
        //
        // GET: /Cabinet/Adverts/
        public virtual ActionResult Index()
        {
            return View();
        }
    }
  
}