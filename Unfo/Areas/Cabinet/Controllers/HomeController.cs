﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Unfo.Core;
using Unfo.Core.BaseClasses;

namespace Unfo.Areas.Cabinet.Controllers
{
    public partial class HomeController : BaseController
    {
        //
        // GET: /Cabinet/Home/
        public virtual async Task<ActionResult> Index()
        {
            return View();
        }
    }
}