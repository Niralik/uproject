﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.UnfoBusinessLogic.Events;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Core.Binders
{
    public class UnfoModelBinder: DefaultModelBinder
    {
        private HttpRequestBase CurrentRequest { get; set; }
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            CurrentRequest = controllerContext.HttpContext.Request;
            if (bindingContext.ModelType == typeof(UsersFilterParams))
            {
                return BindUsersFilterParams();
            }
             if (bindingContext.ModelType == typeof(UsersRequestsFilterParams))
            {
                return BindUsersRequestsFilterParams();
            }
             if (bindingContext.ModelType == typeof(EventsFilterParams))
            {
                return BindEventsFilterParams();
            }
            
            return base.BindModel(controllerContext, bindingContext);
        }

        /// <summary>
        /// Метод который связывает параметры Маршрута с экземпляром <see cref="UsersFilterParams"/>
        /// </summary>
        /// <returns>Наполненный связанными данными объект <see cref="UsersFilterParams"/></returns>
        private object BindUsersFilterParams()
        {
            return FillInstanceByParsedRouteData(new UsersFilterParams(), typeof(UsersFilterParams));
        }
        /// <summary>
        /// Метод который связывает параметры Маршрута с экземпляром <see cref="UsersFilterParams"/>
        /// </summary>
        /// <returns>Наполненный связанными данными объект <see cref="UsersFilterParams"/></returns>
        private object BindEventsFilterParams()
        {
            return FillInstanceByParsedRouteData(new EventsFilterParams(), typeof(EventsFilterParams));
        }
        /// <summary>
        /// Метод который связывает параметры Маршрута с экземпляром <see cref="UsersFilterParams"/>
        /// </summary>
        /// <returns>Наполненный связанными данными объект <see cref="UsersFilterParams"/></returns>
        private object BindUsersRequestsFilterParams()
        {
            return FillInstanceByParsedRouteData(new UsersRequestsFilterParams(), typeof(UsersRequestsFilterParams));
        }
        /// <summary>
        /// Заполнение экземпляра какого либо класса данными из маршрута
        /// </summary>
        /// <param name="instanceToBeFilled">Заполняемый экземпляр</param>
        /// <param name="typeOfinstance">Тип заполняемого экземпляра</param>
        /// <returns></returns>
        private object FillInstanceByParsedRouteData(object instanceToBeFilled, Type typeOfinstance)
        {
            foreach (var propertyInfo in typeOfinstance.GetProperties())
            {
                var propertyName = propertyInfo.Name;
                var valueFromRoute = GetValueByPropertyName(propertyName);
                if (string.IsNullOrWhiteSpace(valueFromRoute)) continue;

                object parsedValue = null;
                var typeOfProperty = propertyInfo.PropertyType;
                var underlyingType = Nullable.GetUnderlyingType(typeOfProperty);
                var parsingType = underlyingType ?? typeOfProperty;
                if (parsingType == typeof(DateTime) || propertyInfo.PropertyType == typeof(DateTime?))
                {
                    parsedValue = DateParser(valueFromRoute);
                }
                if (parsingType == typeof(int) || propertyInfo.PropertyType == typeof(int?))
                {
                    parsedValue = IntParser(valueFromRoute);
                }
                if (parsingType == typeof(bool))
                {
                    parsedValue = BoolParser(valueFromRoute);
                }
                if (parsingType == typeof(Guid) || propertyInfo.PropertyType == typeof(Guid?))
                {
                    parsedValue = GuidParser(valueFromRoute);
                }
                if (parsingType == typeof(string))
                {
                    parsedValue = valueFromRoute;
                }
                if (parsingType == typeof(float))
                {
                    parsedValue = FloatParser(valueFromRoute);
                }
                if (parsingType.BaseType == typeof(Enum))
                {
                    var typeUnfoModelBinder = typeof(UnfoModelBinder);
                    var emumParserMethod = typeUnfoModelBinder.GetMethod("EnumParser", BindingFlags.NonPublic | BindingFlags.Instance);
                    var generedEnumParserMethod = emumParserMethod.MakeGenericMethod(parsingType);
                    parsedValue = generedEnumParserMethod.Invoke(this, new[] { valueFromRoute });
                }
                propertyInfo.SetValue(instanceToBeFilled, parsedValue);
            }
            return instanceToBeFilled;
        }

        private string GetValueByPropertyName(string propertyName)
        {
            var values = new string[] { };
            if (CurrentRequest.Form.HasKeys())
            {
                var key = FindMatchKey(CurrentRequest.Form.AllKeys, propertyName);
                values = CurrentRequest.Form.GetValues(key);
            }
            if (CurrentRequest.Params.HasKeys())
            {
                var key = FindMatchKey(CurrentRequest.Params.AllKeys, propertyName);
                values = CurrentRequest.Params.GetValues(key);
            }
            return values != null && values.Any() ? values[0] : null;
        }

        private static string FindMatchKey(IEnumerable<string> keys, string propertyName)
        {
            var matchedKey = string.Empty;
            foreach (var key in keys)
            {
                var cleanedKey = key.ToLower().Replace("parameters.", "").Replace("parameters_", "");
                if (cleanedKey == propertyName.ToLower())
                {
                    matchedKey = key;
                }
            }
            return matchedKey;
        }

        #region Приватные парсеры
        private bool BoolParser(string boolString)
        {
            bool result;
            Boolean.TryParse(boolString, out result);
            return result;
        }

        private int IntParser(string intString)
        {
            int result;
            int.TryParse(intString, out result);
            return result;
        }

        private float FloatParser(string floatString)
        {
            float result;
            float.TryParse(floatString.Replace('.', ','), out result);
            return result;
        }
        private DateTime DateParser(string dateString)
        {
            DateTime result;
            var parseSuccessed = DateTime.TryParse(dateString, Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out result);
            if (!parseSuccessed) DateTime.TryParse(dateString, out result);
            if (!parseSuccessed) DateTime.TryParse(dateString, CultureInfo.InvariantCulture, DateTimeStyles.None, out result); ;
            return result;
        }

        private Guid GuidParser(string guidString)
        {
            Guid result;
            Guid.TryParse(guidString, out result);
            return result;
        }

        private T EnumParser<T>(string enumString)
        {
            return enumString != null && Enum.Parse(typeof(T), enumString) is T ? (T)Enum.Parse(typeof(T), enumString) : default(T);
        }
        #endregion

    }
}