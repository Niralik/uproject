﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using Unfo.Core.Attributes;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.UnfoBusinessLogic.Events;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Core.Binders
{
    public class UnfoModelUnbinder : IModelUnbinder
    {
        public void UnbindModel(RouteValueDictionary routeValueDictionary, string routeName, object routeValue)
        {
            Unbind(routeValueDictionary, routeValue, typeof (UsersFilterParams));
            Unbind(routeValueDictionary, routeValue, typeof(UsersRequestsFilterParams));
            Unbind(routeValueDictionary, routeValue, typeof(EventsFilterParams));
        }

        /// <summary>
        /// Метод преобразующий какой либо объект в Значения Маршута исключая дефолтные значения
        /// </summary>
        /// <param name="routeValueDictionary">Словарь значений маршрута который будет выводится в URL</param>
        /// <param name="paramsQuery">Экземпляр типа переданного в метод</param>
        /// <param name="typeOfParamsQuery">Тип экземпляра для которого нужна особая логика преобразования в маршрут</param>
        private void Unbind(RouteValueDictionary routeValueDictionary, object paramsQuery, Type typeOfParamsQuery)
        {
            if (paramsQuery.GetType() != typeOfParamsQuery) return;

            foreach (var propertyInfo in typeOfParamsQuery.GetProperties())
            {
                var isHasDoNotBindAtribbute = propertyInfo.CustomAttributes.Any(atr => atr.AttributeType == typeof(DoNotBindAttribute));
                if (isHasDoNotBindAtribbute) continue;

                var name = propertyInfo.Name;
                var value = propertyInfo.GetValue(paramsQuery);

                var isDefaultValue = DetermineIsValueDefault(value);
                if (isDefaultValue) continue;

                if (value is DateTime)
                {
                    routeValueDictionary.Add(name, ((DateTime)value).ToShortDateString());
                }
                else
                {
                    if (value != null) routeValueDictionary.Add(name, value.ToString());
                }
            }
        }
        private bool DetermineIsValueDefault(object value)
        {
            var isDefault = false;
            if (value is int) isDefault = (int)value == default(int);
            if (value is float) isDefault = (float)value == default(float);
            if (value is bool) isDefault = (bool)value == default(bool);
            if (value is string) isDefault = string.IsNullOrWhiteSpace((string)value);
            if (value is Guid) isDefault = (Guid)value == default(Guid);
            if (value is Enum) isDefault = (int)value == default(int);
            if (value is DateTime) isDefault = (DateTime)value == default(DateTime);
            return isDefault;
        }
    }

    public static class ReflectionUtility
    {
        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            var body = (MemberExpression)expression.Body;
            return body.Member.Name;
        }
    }
}