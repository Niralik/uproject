﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using DataModel;
using Unfo.Core;
using Unfo.Core.Extensions;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.CommonViewModels;
using Unfo.Models.ViewModels.Interfaces;
using WebGrease;


namespace Unfo.Core
{
    public static class ImageUpload
    {
        public static UploadImageVM SaveImage(HttpPostedFileBase file, IImageRepository repository, bool onlySource = false)
        {
            return SaveImage(file, Guid.NewGuid(), repository, onlySource);
        }

        public static UploadImageVM SaveImage(HttpPostedFileBase file, Guid imageId, IImageRepository repository, bool onlySource = false)
        {
            return SaveImage(file.InputStream, imageId, file.FileName, repository, onlySource);
        }


        static UploadImageVM SaveImage(Stream file, Guid imageId, string fileName, IImageRepository repository, bool onlySource = false)
        {
            return SaveImage(Image.FromStream(file), imageId, fileName, repository, onlySource);
        }


        static UploadImageVM SaveImage(Image image, Guid imageId, string fileName, IImageRepository repository, bool onlySource = false)
        {
            //var thumbsSize_ = new System.Drawing.Size(400, 400);
            var normalSize_ = onlySource ? new Size(0, 0) : new System.Drawing.Size(800, 800);
            Image source = image;

            var sourceSize = source.Size;

            //Image thumbs = source.GetThumbs(thumbsSize_); //Core.Utilities.ThumbnailImage(source, thumbsSize_);
            //source.GetThumbnailImage(thumbsSize_.Width, thumbsSize_.Height, () => false, IntPtr.Zero);
            //var thumbsSize = thumbs.Size;

            Core.StorageWrapper.Instance.Value.ImageStorageManager.SaveImage(source, imageId.ToString(), TypeOfImage.Source);


            if (!onlySource)
            {
                Image normal = source.GetResizeImage(normalSize_);
                normalSize_ = normal.Size;
                Core.StorageWrapper.Instance.Value.ImageStorageManager.SaveImage(normal, imageId.ToString(), TypeOfImage.Normal);
            }

            var result = repository.SaveImage(imageId, fileName, sourceSize.Width, source.Height);
            return new UploadImageVM()
            {
                ID = imageId,
                SourceSize = sourceSize,
                NormalSize = normalSize_
            };
        }

        public static Guid CropImage(ImageCropperVM cropper, IImageRepository repository)
        {
            var image = Core.StorageWrapper.Instance.Value.ImageStorageManager.GetImage(cropper.ID.ToString(),
                cropper.TypeCrop == CropForm.Any ?
                TypeOfImage.Source : TypeOfImage.Normal);
            var cropImage = image.CropImage(new Rectangle(new Point(cropper.PointX, cropper.PointY),
                new Size(cropper.Width, cropper.Height)));


            var id = Guid.NewGuid();
            SaveImage(cropImage, id, "", repository);
            return id;

        }

    }
}