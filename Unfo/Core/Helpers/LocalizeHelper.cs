﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrangeJetpack.Localization;
using Unfo.Core.GlobalClasses;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Core.Helpers
{
    public static class LocalizeHelper
    {
        public static string Localization(string language, string fieldData, string val)
        {
            var cultures = CurrentEvent.GetAccessCultures();
            if (cultures.All(d => d.Key != language)) return fieldData;
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LocalizedContent>>(fieldData);
                bool update = false;
                foreach (var localizedContent in list)
                {
                    if (localizedContent.Key == language)
                    {
                        localizedContent.Value = val;
                        update = true;

                    }
                }
                if (!update)
                {
                    list.Add(new LocalizedContent(language, val));
                }
                return list.ToArray().Serialize();
            }
            catch (Exception e)
            {
                var result = new List<LocalizedContent>();
                result.Add(new LocalizedContent(language, val));

                foreach (var culture in cultures)
                {
                    if (language != culture.Key)
                    {
                        result.Add(new LocalizedContent(culture.Key, val));
                    }
                }

                return result.ToArray().Serialize();
            }


        }

        public static string GetLocalizeField(string fieldData)
        {
            return GetLocalizeField(CurrentLocalization.UI, fieldData);
        }
        public static string GetLocalizeField(string language, string fieldData)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LocalizedContent>>(fieldData);
                var field = list.SingleOrDefault(d => d.Key == language);
                if (field == null)
                {
                    if (list.Any())
                        return list.First().Value;
                    return string.Empty;
                }
                return field.Value;
            }
            catch (Exception e)
            {
                return fieldData;
            }
        }
    }
}