﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using Microsoft.Ajax.Utilities;
using Unfo.Core.ValidationAttributes;

namespace Unfo.Core.Helpers
{
    public static class EnumHelper<T>
    {
        //public static IList<T> GetValues(Enum value)
        //{
        //    var enumValues = new List<T>();
        //    var enumType = typeof (T);
        //    foreach (FieldInfo fi in T.GetFields(BindingFlags.Static | BindingFlags.Public))
        //    {
        //        enumValues.Add((T)Enum.Parse(value.GetType(), fi.Name, false));
        //    }
        //    return enumValues;
        //}
        public static IList<T> GetValues()
        {
            var enumValues = new List<T>();
            var enumType = typeof(T);
            foreach (FieldInfo fi in enumType.GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                enumValues.Add((T)Enum.Parse(enumType, fi.Name, false));
            }
            return enumValues;
        }

        public static T Parse(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static IList<string> GetNames(Enum value)
        {
            return value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
        }

        public static IList<string> GetDisplayValues(Enum value)
        {
            return GetNames(value).Select(obj => GetDisplayValue(Parse(obj))).ToList();
        }

        public static string GetDisplayValue(T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            var displayNameOfValue = string.Empty;
            if (descriptionAttributes.Length == 0) return displayNameOfValue;
        
            if (descriptionAttributes[0].ResourceType != null)
            {
                foreach (var propertyInfo in descriptionAttributes[0].ResourceType.GetProperties().Where(propertyInfo => propertyInfo.Name == descriptionAttributes[0].Name))
                {
                    displayNameOfValue = (string)propertyInfo.GetValue(null, null);
                }
            }
            else
            {
                displayNameOfValue = descriptionAttributes[0].Name;

            }
            return displayNameOfValue;
        }

    }


}