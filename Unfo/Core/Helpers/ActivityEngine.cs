﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Unfo.Core.Helpers
{
    /// <summary>
    /// Класс представляющий из себя конкретный ХтмлТэг вокруг которого строится различная
    /// управляющая логика
    /// </summary>
    public class HtmlTag
    {
        /// <summary>
        /// Значение какого либо перечесления которое идентифицирует данный экземпляр
        /// </summary>
        public Enum HtmlTagEnumValue { get; set; }
        /// <summary>
        /// T4MVC путь до экшена к которому привязан данный тег
        /// </summary>
        public T4MvcActionResult T4MvcAction { get; set; }
        /// <summary>
        /// Если данный Тэг должен быть активным - после обработки - переменая будет содержать
        /// название CSS класса "active"
        /// </summary>
        public string Active { get; set; }
        /// <summary>
        /// Если данный тег Активине - то переменная будет содержать - Сss класс - Visible - в противном случае - Hidden
        /// </summary>
        public string Visibility { get; set; }
        /// <summary>
        /// Конструктор создает экземляр представляющий конкретный HTML тэг
        /// </summary>
        /// <param name="htmlTagEnumValue">
        /// Значение какого либо перечесление которое помогло бы одназначно идентифицировать данный экземпляр
        /// </param>
        public HtmlTag(Enum htmlTagEnumValue)
        {
            HtmlTagEnumValue = htmlTagEnumValue;
        }

        public HtmlTag(T4MvcActionResult t4MvcAction, string activeCssClass, string visbility)
        {
            Active = activeCssClass;
            Visibility = visbility;
            T4MvcAction = t4MvcAction;
        }
    }

    public class T4MvcActionResult:IT4MVCActionResult
    {
        public RouteValueDictionary RouteValueDictionary { get; set; }
        public string Protocol { get; set; }
        public string Areas { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public T4MvcActionResult(string action, string controller, RouteValueDictionary routeValueDictionary = null)
        {
            Action = action;
            Controller = controller;
            RouteValueDictionary = routeValueDictionary;
        }

        public override bool Equals(object obj)
        {
            var result = obj as ActionResult;
            if (result == null) return false;
            var t4MvcResult = result.GetT4MVCResult();
            return Controller == t4MvcResult.Controller && Action == t4MvcResult.Action;
        }
    }
    public partial class HtmlEngine
    {
       
        private static T4MvcActionResult T4MvcAction { get; set; }
  
        public static void SetCurrentAction(string controllerName, string actionName)
        {
            T4MvcAction = new T4MvcActionResult(actionName,controllerName);
        }

    }
    /// <summary>
    /// Класс обертка над функциональностью получения "активного" состояния какого либо HTML тега на
    /// основе данных пришедших их контроллера данные генерируются на основе перечеслений которые
    /// должны отражать все HTML теги состояние которых надо узнать
    /// </summary>
    public partial class HtmlEngine
    {
        private const string ActiveCssClass = "active";
        private const string VisibleCssClass = "visible";
        private const string HiddenCssClass = "hidden";

        private List<HtmlTag> HtmlTags { get; set; }
        /// <summary>
        /// Коллекция Типов Перечеслений на основе которых можно в дальнейшем получать данные для конкретного енум значения
        /// </summary>
        protected virtual List<Type> TargetEnumTypes { get; set; }

        public HtmlEngine()
        {
            HtmlTags = new List<HtmlTag>();
            GenerateHtmlTagsByEnums();
            GenerateHtmlTagsByT4MvcPath();
        
        }
        private void GenerateHtmlTagsByEnums()
        {
            if (TargetEnumTypes == null) return;
            foreach (var activityEnumType in TargetEnumTypes)
            {
                foreach (var enumValue in Enum.GetValues(activityEnumType))
                {
                    HtmlTags.Add(new HtmlTag(enumValue as Enum));
                }
            }
        }

        private void GenerateHtmlTagsByT4MvcPath()
        {
            if (T4MvcAction != null)
            {
                HtmlTags.Add(new HtmlTag(T4MvcAction, ActiveCssClass, VisibleCssClass));
            }
        }
        /// <summary>
        /// Метод пробегается по массиву HTML тегов сгенерированному на основе перечесления
        /// переданного в статический конструктор и устанавливает свойство <see cref="HtmlTag.Active" /> на
        /// основе заданных в <param name="activeItems"></param> значений Перечесления.
        /// </summary>
        /// <param name="activeItems">
        /// Значения перечеслений представляющие какой либо HTML тег которому необходимо добавить
        /// активный класс
        /// </param>
        public void SetCurrentActiveItem(params Enum[] activeItems)
        {
            if (activeItems == null) return;
            if (HtmlTags == null) throw new ArgumentNullException("htmlTags пуст");
            HtmlTags.ForEach(tag =>
            {
                foreach (var isActiveValue in activeItems)
                {
                    if (Equals(isActiveValue, tag.HtmlTagEnumValue))
                    {
                        tag.Active = ActiveCssClass;
                        tag.Visibility = VisibleCssClass;
                    }
                    else
                    {
                        tag.Visibility = HiddenCssClass;
                    }
                    
                }
            });
        }
        
        /// <summary>
        /// Метод пробегается по массиву HTML тегов сгенерированному на основе перечесления
        /// переданного в статический конструктор и устанавливает свойство <see cref="HtmlTag.Active" /> на
        /// основе заданных в <param name="isActiveEnumValueList"></param> значений Перечесления.
        /// </summary>
        /// <param name="isActiveEnumValueList">
        /// Значение перечесления представляющие какой либо HTML тег которому необходимо добавить
        /// активный класс
        /// </param>
        public void SetCurrentActiveItem(IList<Enum> isActiveEnumValueList)
        {
            SetCurrentActiveItem(isActiveEnumValueList.ToArray());
        }

        /// <summary>
        /// Метод возвращает название СSS класса представляющего активное состояние на основе
        /// перечесления представляющего из себя HTML теги
        /// </summary>
        /// <param name="targetsEnumValue">
        /// Значения перечеслений представляющие из себя конкретный тег активность которому
        /// необходимо установить
        /// </param>
        /// <returns> cssClass:"active"</returns>
        public string GetActivity(params Enum[] targetsEnumValue)
        {
            return HtmlTags.Where(tag => targetsEnumValue.Contains(tag.HtmlTagEnumValue)).Select(tag => tag.Active).FirstOrDefault(tag => tag != null);
        }
        public string GetVisibility(params Enum[] targetsEnumValue)
        {
            return HtmlTags.Any(htmlTag => targetsEnumValue.Contains(htmlTag.HtmlTagEnumValue)) ? VisibleCssClass : HiddenCssClass;
        }

        public string GetActivity(params ActionResult[] targetActions)
        {
            return HtmlTags.Where(htmlTag => targetActions.Any(action=> htmlTag.T4MvcAction.Equals(action))).Select(tag => tag.Active).FirstOrDefault(tag => tag != null);
        }
        public string GetVisibility(params ActionResult[] targetActions)
        {
            return HtmlTags.Any(htmlTag => targetActions.Any(action => htmlTag.T4MvcAction.Equals(action))) ? VisibleCssClass : HiddenCssClass;
        }
        public string GetActivity(params Task<ActionResult>[] targetActions)
        {
            return GetActivity(targetActions.Select(action=> action.Result).ToArray());
        }
        public string GetVisibility(params Task<ActionResult>[] targetActions)
        {
            return GetVisibility(targetActions.Select(action => action.Result).ToArray());
        }
       
    }

    public class HtmlEngine<TEnumTarget>: HtmlEngine
    {
        protected override List<Type> TargetEnumTypes
        {
            get { return new List<Type> { typeof(TEnumTarget) };}
        }

    }
}