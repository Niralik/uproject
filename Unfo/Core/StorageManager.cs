﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Unfo.Core
{
    public enum TypeOfImage
    {
        Thumbs, Normal, Source
    }

    public enum TypeOfFile
    {
        Hide, Public
    }

    public interface IFileStorageManager
    {
        string GetUrlToImage(string imageId, TypeOfImage typeImage = TypeOfImage.Normal);

        Image GetImage(string imageId, TypeOfImage typeImage = TypeOfImage.Normal);

        void SaveImage(Image image, string imageId, TypeOfImage typeImage = TypeOfImage.Normal);

        string EmptyId { get; }

        string GetUrlToFile(string fileName, TypeOfFile type = TypeOfFile.Public);

        Stream GetFile(string fileName, TypeOfFile type = TypeOfFile.Public);

        void SaveFile(Stream file, string fileName, TypeOfFile type = TypeOfFile.Public);

        void SaveFile(byte[] file, string fileName, TypeOfFile type = TypeOfFile.Public);
    }

    public class LocalStorageManager : IFileStorageManager
    {
        private const string normalPath = "/Content/upload/img/normal/";
        private const string thumbsPath = "/Content/upload/img/thumbs/";
        private const string sourcePath = "/Content/upload/img/source/";
        public LocalStorageManager()
        {
            if (HttpContext.Current == null)
            {
                return;
            }

            var path = HttpContext.Current.Server.MapPath("~");
            if (!Directory.Exists(path + thumbsPath))
            {
                System.IO.Directory.CreateDirectory(path + thumbsPath);
            }
            if (!Directory.Exists(path + normalPath))
            {
                System.IO.Directory.CreateDirectory(path + normalPath);
            }
            if (!Directory.Exists(path + sourcePath))
            {
                System.IO.Directory.CreateDirectory(path + sourcePath);
            }
        }

        public string EmptyId { get { return Guid.Empty.ToString(); } }

        public string GetUrlToImage(string imageId, TypeOfImage typeImage = TypeOfImage.Normal)
        {
            if (imageId == EmptyId) return string.Empty;

            switch (typeImage)
            {
                case TypeOfImage.Source:
                    {
                        return sourcePath + imageId + ".jpg";
                    }
                case TypeOfImage.Normal:
                    {
                        return normalPath + imageId + ".jpg";
                    }
                case TypeOfImage.Thumbs:
                    {
                        return thumbsPath + imageId + ".jpg";
                    }
            }
            return String.Empty;
        }

        public Image GetImage(string imageId, TypeOfImage typeImage = TypeOfImage.Normal)
        {
            var path = this.GetUrlToImage(imageId, typeImage);

            return Image.FromFile(HttpContext.Current.Server.MapPath("~") + path);
        }

        public void SaveImage(System.Drawing.Image image, string imageId, TypeOfImage typeImage = TypeOfImage.Normal)
        {
            var path = this.GetUrlToImage(imageId, typeImage);
            using (var stream = new FileStream(HttpContext.Current.Server.MapPath("~") + path, FileMode.Create))
            {
                image.Save(stream, ImageFormat.Jpeg);
            }
        }

        private const string hidePath = "/Content/upload/files/hide/";
        private const string publicPath = "/Content/upload/files/public/";

        public string GetUrlToFile(string fileName, TypeOfFile type = TypeOfFile.Public)
        {
            if (!string.IsNullOrWhiteSpace(fileName)) return string.Empty;

            switch (type)
            {
                case TypeOfFile.Hide:
                    {
                        return hidePath + fileName;
                    }
                case TypeOfFile.Public:
                    {
                        return publicPath + fileName;
                    }
            }
            return String.Empty;
        }

        public Stream GetFile(string fileName, TypeOfFile type = TypeOfFile.Public)
        {
            var path = this.GetUrlToFile(fileName, type);

            return File.Open(path, FileMode.OpenOrCreate);
        }

        public void SaveFile(Stream file, string fileName, TypeOfFile type = TypeOfFile.Public)
        {
            SaveFile(file.ReadFully(), fileName, type);
        }

        public void SaveFile(byte[] file, string fileName, TypeOfFile type = TypeOfFile.Public)
        {
            File.WriteAllBytes(GetUrlToFile(fileName, type), file);
        }
    }


    public class StorageWrapper
    {
        public static readonly Lazy<StorageWrapper> Instance = new Lazy<StorageWrapper>(() => new StorageWrapper());


        private readonly IFileStorageManager manager;
        public StorageWrapper()
        {
            manager = new LocalStorageManager();
            //manager = new AzureStorageManager();
        }

        public IFileStorageManager ImageStorageManager
        {
            get
            {
                return manager;
            }
        }
    }
}