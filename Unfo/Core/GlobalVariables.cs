﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;



namespace Unfo.Core
{
    public static partial class GlobalVariables
    {
    }

    public static class CurrentLocalization
    {
        public const string CookieName = "current_culture";

        public const string DefaultLang = "ru";
        /// <summary>
        /// Текущая выбранная языковая культура интерфейса - возвращает в формате TwoLetterISOLanguageName
        /// </summary>
        public static string UI
        {
            get { return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName; }
        }
        /// <summary>
        /// Текущая выбранная языковая культура в режиме редактирования - возвращает в формате TwoLetterISOLanguageName
        /// </summary>
        public static string Edit
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Request.Cookies[CookieName] == null) return DefaultLang;
                return HttpContext.Current.Request.Cookies[CookieName].Value;
            }
        }
    }
    public static class Route
    {
        public const string Event = "EventRoute";
        public const string CabinetDefault = "Cabinet_default";
        public const string UserInfo = "Cabinte_user_info";
        public const string CreateEvent = "Cabinet_create_event";
        public const string EditEvent = "Cabinet_edit_event";
        public const string EventModerators = "Cabinet_event_moderators";
        public const string EventUsers = "Cabinet_event_users";
        public const string InfoEvent = "Cabinet_info_event";
    }
}