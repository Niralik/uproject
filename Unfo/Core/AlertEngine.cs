﻿using System.Diagnostics;
using Microsoft.Ajax.Utilities.Configuration;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.EnterpriseLibrary.Common.Properties;
using Unfo.Core.Extensions;
using Unfo.App_LocalResources;
using ModelState = System.Web.Mvc.ModelState;
using Resources = Unfo.App_LocalResources.Resources;

namespace Unfo.Core
{
    public class AlertJson
    {
        public AlertJson()
        {
        }

        /// <summary>
        /// Метод формирует Json объект представляющий оповещение символизирующие Успех
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Success(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Success, alertMessages);
        }
        public static JsonResult Success(object relatedData, int code)
        {
            return Compile(TypeOfAlert.Success, relatedData, code);
        }
        /// <summary>
        /// Метод формирует Json объект представляющий оповещение символизирующие Ошибку - без параметров по умолчанию вернет ошибку InternalServerError
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Error(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Error, alertMessages);
        }
        /// <summary>
        /// Метод формирует Json объект представляющий Ошибки операций валидации модели
        /// </summary>
        /// <param name="modelStateCollection">Значения состояния модели полученные из этого свойства: <see cref="ModelState.Values"/></param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Error(ICollection<ModelState> modelStateCollection)
        {
            var errors = new List<string>();
            foreach (var modelState in modelStateCollection.Where(ms=> ms.Errors != null))
            {
                errors.AddRange(modelState.Errors.Select(modelError => modelError.ErrorMessage).ToList());
            }

            var typeOfAlertMessage = TypeOfAlert.Success;
            if (errors.Any()) typeOfAlertMessage = TypeOfAlert.Error;

            return Compile(new AlertSection(typeOfAlertMessage, errors.ToArray()));
        }
        /// <summary>
        /// Метод формирует Json объект представляющий оповещение со списком ошибок сформированными системой Identity -
        /// если результат операции будет успешным - будет сформированное пустое сообщение с флагом <see cref="Successed = true"/>
        /// </summary>
        /// <param name="identityResult">Результат Identity операции</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Error(IdentityResult identityResult)
        {
            var typeOfAlertMessage = TypeOfAlert.Error;
            if (identityResult.Succeeded) typeOfAlertMessage = TypeOfAlert.Success;
            return Compile(new AlertSection(typeOfAlertMessage, identityResult.Errors.ToArray()));
        }
        public static JsonResult Error(object relatedData, int code)
        {
            return Compile(TypeOfAlert.Error, relatedData, code);
        }
        /// <summary>
        /// Метод формирует Json объект представляющий Предупреждающие оповещение
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Warning(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Warning, alertMessages);
        }
        /// <summary>
        /// Метод формирует Json объект представляющий Информационное оповещение
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Attention(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Info, alertMessages);
        }
        /// <summary>
        /// Метод упаковывает контейнеры оповещений в один json объект - притом общий флаг Successed установится в False - если среди контейнеров сообщений 
        /// будет хоть один с типом <see cref="TypeOfAlert.Error"/>
        /// </summary>
        /// <param name="alertSections">Контейнеры оповещений</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Compile(params AlertSection[] alertSections)
        {
            return Compile(null, null, alertSections);
        }
        public static JsonResult Compile(object relatedData, params AlertSection[] alertSections)
        {
            return Compile(relatedData, null, alertSections);
        }
        public static JsonResult Compile(int code, params AlertSection[] alertSections)
        {
            return Compile(null, code, alertSections);
        }
        public static JsonResult Compile(object relatedData, int? code, params AlertSection[] alertSections)
        {
            var alert = new AlertsWrapper(new List<AlertSection>(alertSections));
            alert.RelatedData = relatedData;
            alert.Code = code.HasValue ? code.Value : 0;
            return new JsonResult { Data = alert };
        }
        public static JsonResult Compile(TypeOfAlert typeAlert, object relatedData, int code)
        {
            return Compile(typeAlert, relatedData, code, null);
        }
        /// <summary>
        /// Метод формирует Json объект представляющий оповещение заданного типа
        /// </summary>
        /// <param name="typeAlert">Тип оповещения</param>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Compile(TypeOfAlert typeAlert, params string[] alertMessages)
        {
            return Compile(null, 0, new AlertSection(typeAlert, alertMessages));
        }
        public static JsonResult Compile(TypeOfAlert typeAlert, object relatedData, int? code, params string[] alertMessages)
        {
            return Compile(relatedData, code, new AlertSection(typeAlert, alertMessages));
        }

        
    }
    /// <summary>
    /// Класс представляет собой обертку над всеми контейнерами оповещений и общий результат операции
    /// </summary>
    public class AlertsWrapper
    {
        /// <summary>
        /// Флаг говорящий об успехе выполненой операции
        /// </summary>
        public bool Successed { get; set; }
        /// <summary>
        /// Сопуствующие данные которые необходимо передать с результатом операции
        /// </summary>
        public object RelatedData { get; set; }
        /// <summary>
        /// Любой код который необходимо отработать на клиенте
        /// </summary>
        public int Code { get; set; }
        public List<AlertSection> AlertSections { get; set; }
        public AlertsWrapper(List<AlertSection> alertSections)
        {
            AlertSections = alertSections ?? new List<AlertSection>();
            SetSuccessedByContentOfSections();
        }

        private void SetSuccessedByContentOfSections()
        {
            Successed = true;

            foreach (var section in AlertSections.Where(section => section != null && section.TypeAlert == TypeOfAlert.Error))
            {
                Successed = false;
                break;
            }
        }

        public AlertsWrapper()
        {
        }
    }
    /// <summary>
    /// Класс представляющий из себя контейнер с оповещениями одного типа
    /// </summary>
    public class AlertSection
    {
        public TypeOfAlert TypeAlert { get; set; }
        public string Message { get; set; }

        public string[] Messages { get; set; }

        public AlertSection(TypeOfAlert typeAlert, params string[] alertMessages)
        {
            TypeAlert = typeAlert;
            FitIntoTemplateLogic(alertMessages);
            SetDefaultMessages();
        }
        /// <summary>
        /// Метод подгоняющий текущий объект под необходимые условия для работы Хендлбар темплейта
        /// </summary>
        /// <param name="alertMessages"></param>
        private void FitIntoTemplateLogic(params string[] alertMessages)
        {
            if (alertMessages == null) return;
            if (alertMessages.Count() > 1)
            {
                Messages = alertMessages;
            }
            else
            {
                Message = alertMessages.FirstOrDefault();
            }
          
        }

        private void SetDefaultMessages()
        {
            var isAllMessagesEmpty = string.IsNullOrWhiteSpace(Message) && (Messages == null || Messages.All(string.IsNullOrWhiteSpace));
            if (isAllMessagesEmpty)
            {
                switch (TypeAlert)
                {
                    case TypeOfAlert.Info:
                        break;
                    case TypeOfAlert.Warning:
                        break;
                    case TypeOfAlert.Success:

                        break;
                    case TypeOfAlert.Error:
                        Message = Resources.Validate_Alert_IternalServerError;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public AlertSection()
        {
            
        }

        public static AlertSection Success
        {
            get
            {
                return new AlertSection()
                    {
                        TypeAlert = TypeOfAlert.Success
                    };
            }
        }
    }

    /// <summary>
    /// Класс обслуживащий логику генерации HTML разметки из шаблона alert-block.handlebars
    /// </summary>
    public class AlertTemplate
    {
        private static string _alertTemplatePath;
        private string _html;

        public static string AlertTemplatePath
        {
            get
            {
                return _alertTemplatePath ??  HttpContext.Current.Server.MapPath(Links.Scripts.Unfo.CustomScripts.AlertEngine.alert_block_handlebars);
            }
            set
            {
                _alertTemplatePath = value;
            }
        }

        private Func<object, string> CompiledTemplate
        {
            get
            {

                var rawTemplate = Convert.ToString(Utilities.ReadHtmlFile(AlertTemplatePath));
                var compiledTemplate = Handlebars.Handlebars.Compile(rawTemplate);
                return compiledTemplate;
            }
        }
        /// <summary>
        /// Объект хранящий в себе все контейнеры оповещений и общий результат операции
        /// </summary>
        private AlertsWrapper AlertsDataWrapper { get; set; }

        /// <summary>
        /// Html разметка на основе <see cref="alert-block.handlebars"/> темплейта
        /// </summary>
        public string Html
        {
            get
            {
                GenerateHtml();
                return _html;
            }
            private set { _html = value; }
        }

        public bool IsPageView { get; set; }
        /// <summary>
        /// Создает единную HTML разметку для всех контейнеров оповещений подданых на вход
        /// </summary>
        /// <param name="alertSections">Контейнеры оповещений</param>
        public AlertTemplate(params AlertSection[] alertSections)
        {
            ExtractAlertDataWrapper(AlertJson.Compile(alertSections));
        }
        /// <summary>
        /// Создает HTML разметку на основе результатов Identity операций
        /// </summary>
        /// <param name="identityResult">Представляет результата Identity операции</param>
        public AlertTemplate(IdentityResult identityResult)
        {
            ExtractAlertDataWrapper(AlertJson.Error(identityResult));
        }
        /// <summary>
        /// Создает HTML разметку оповещения одного заданного типа с заданными сообщениями
        /// </summary>
        /// <param name="typeAlert">Тип оповещения</param>
        /// <param name="alertMessages">Содержимое оповещения</param>
        public AlertTemplate(TypeOfAlert typeAlert, params string[] alertMessages)
        {
            ExtractAlertDataWrapper(AlertJson.Compile(typeAlert, alertMessages));
        }
        /// <summary>
        /// Создает HTML разметку на основе результатов валидации Модели по средствам атрибутов
        /// </summary>
        /// <param name="modelStateCollection">
        /// Значения состояния модели полученыне из этого свойства: <see cref="ModelState.Values"/>
        /// </param>
        public AlertTemplate(ICollection<ModelState> modelStateCollection)
        {
            ExtractAlertDataWrapper(AlertJson.Error(modelStateCollection));
        }
        public AlertTemplate()
        {
            Html = String.Empty;
        }
        /// <summary>
        /// Генерирует HTML разметку на основе шаблона alert-block.handlebars и проиницализрованных
        /// в конструкторе данных если конструктор был пустой, вернется пустая строка - логично же))
        /// </summary>
        /// <returns>HTML разметка упакованная в строку</returns>
        private void GenerateHtml() 
        {
            var generatedHtml = string.Empty;
            foreach (var alertSection in AlertsDataWrapper.AlertSections.Where(section => section.Messages!= null || section.Message != null))
            {
                var generatedHtmlOfAlertSection = CompiledTemplate(GenerateDataForTemplate(alertSection));
                generatedHtml += generatedHtmlOfAlertSection;
            }
            Html = generatedHtml;
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="json"></param>
        private void ExtractAlertDataWrapper(JsonResult json)
        {
            AlertsDataWrapper = new AlertsWrapper();
            if (json != null && json.Data is AlertsWrapper)
            {
                AlertsDataWrapper = json.Data as AlertsWrapper;
            }
        }
        /// <summary>
        /// Генерация данных для заполнения шаблона
        /// </summary>
        /// <param name="alertSection">Контейнер оповещений представляющих один тип</param>
        /// <returns></returns>
        private object GenerateDataForTemplate(AlertSection alertSection)
        {
            var alertData = new AlertHtmlData()
            {
                AlertCss = GetAlertCssClasses(alertSection),
                AlertHeader = GenerateHeaderOfAlertSection(alertSection),
            };
            //Из за особенностей отображения массива сообщений и одного сообщения через ХендлсБар темплейты,
            //пришлось использовать два разных свойства представляющий из себя массив и еденичное сообщение.
           
                alertData.AlertMessages = alertSection.Messages;
                alertData.AlertMessage = alertSection.Message;
            return alertData;
        }

        private string GetAlertCssClasses(AlertSection alertSection)
        {
            var alertCss = "active ";
            if (IsPageView)
            {
                alertCss += "page-alert ";
            }
            alertCss += alertSection.TypeAlert.GetCssClass();
            return alertCss;
        }

        /// <summary>
        /// Генерация заголовка оповещения исходя из типа оповещения
        /// </summary>
        /// <param name="alertSection">Контейнер оповещений представляющих один тип</param>
        /// <returns></returns>
        private string GenerateHeaderOfAlertSection(AlertSection alertSection)
        {
            string headerOfAlertSection;
            switch (alertSection.TypeAlert)
            {
                case TypeOfAlert.Info:
                    headerOfAlertSection = "Пожалуйста, обратите внимание!";
                    break;

                case TypeOfAlert.Warning:
                    headerOfAlertSection = "Предупреждение!";
                    break;

                case TypeOfAlert.Success:
                    headerOfAlertSection = "Поздравляем вас. Операция завершена успешно!";
                    break;

                case TypeOfAlert.Error:
                    headerOfAlertSection = alertSection.Messages != null ? "Внимание! Обнаружены ошибки!" : "Внимание! Обнаружена ошибка!";
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return headerOfAlertSection;
        }

        /// <summary>
        /// Класс представляющий собой данные используемые на шаблоне handlebars в качестве ключей
        /// выглядиших как {{AlertCss}}
        /// </summary>
        internal class AlertHtmlData
        {
            /// <summary>
            /// Свойство должно содержать список существующих CSS классов в формате "some-class
            /// some-class some-class" для контейнера оповещения
            /// </summary>
            public string AlertCss { get; set; }
            /// <summary>
            /// Данное свойство нужно для разделения логики визуализации Оповещения - если
            /// заполненно - значит отобразится в обычном режиме
            /// </summary>
            public string AlertMessage { get; set; }
            /// <summary>
            /// Данное свойство нужно для разделения логики визуализации Оповещения - если
            /// заполненно - значит отобразится в списочном режиме
            /// </summary>
            public string[] AlertMessages { get; set; }
            /// <summary>
            /// Заголовок оповещения
            /// </summary>
            public string AlertHeader { get; set; }

            public AlertHtmlData()
            {
            }
        }
    }
    /// <summary>
    /// Статические методы организующую систему оповещения вьюшки о результатых операции после
    /// инициализации в контроллере на вьюшке необоходимо вызвать <see cref="Html.ShowAlert()"/>
    /// </summary>
    public static class Alert
    {
        public static bool IsPageView { get; set; }
        /// <param name="alertMessages">
        /// В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.
        /// </param>
        public static void Success(params string[] alertMessages)
        {
            Success(false, alertMessages);
        }
 
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Success(bool toCustomPlaceOnView, params string[] alertMessages)
        {
            AssembleAlert(TypeOfAlert.Success, toCustomPlaceOnView, alertMessages);
        }
        /// <param name="alertMessages">
        /// В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.
        /// </param>
        public static void Error(params string[] alertMessages)
        {
            Error(false, alertMessages);
        }
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Error(bool toCustomPlaceOnView, params string[] alertMessages)
        {
            AssembleAlert(TypeOfAlert.Error, toCustomPlaceOnView, alertMessages);
        }
        /// <summary>
        /// Формирует оповещения об ошибке на основе результатов валидации модели через атрибуты
        /// </summary>
        /// <param name="modelStateValueCollection">Коллекция ошибок валидации получаемая из<see cref="ModelState.Values"/></param>
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        public static void Error(ICollection<ModelState> modelStateValueCollection, bool toCustomPlaceOnView = false)
        {
            AssembleAlert(toCustomPlaceOnView, modelStateValueCollection);
        }
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Warning(params string[] alertMessages)
        {
            Warning(false, alertMessages);
        }
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Warning(bool toCustomPlaceOnView, params string[] alertMessages)
        {
            AssembleAlert(TypeOfAlert.Warning, toCustomPlaceOnView, alertMessages);
        }
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Attention(params string[] alertMessages)
        {
            Attention(false, alertMessages);
        }
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        /// <param name="alertMessages">В случае одного сообщения будет отображенно в обычном виде - если передан массив строк -
        /// будет отображенно в виде списка.</param>
        public static void Attention(bool toCustomPlaceOnView, params string[] alertMessages)
        {
            AssembleAlert(TypeOfAlert.Info, toCustomPlaceOnView, alertMessages);
        }
        /// <summary>
        /// Формирует оповещение на основе результатов Identity операций
        /// </summary>
        /// <param name="toCustomPlaceOnView">Флаг отмечающий что это оповещение будет отображено в произвольном месте</param>
        public static void FromIdentityResult(IdentityResult identityResult, bool toCustomPlaceOnView = false)
        {
            AssembleAlert(toCustomPlaceOnView, identityResult);
        }

        private static TempDataDictionary TempData { get; set; }
        static Alert()
        {
            TempData = new TempDataDictionary();
        }

        public static void InitializeTempDataContext(ControllerContext controler)
        {
            TempData.Load(controler, new SessionStateTempDataProvider());
        }

        private static bool IsCustomPlaceOnView { get; set; }

        private static void AssembleAlert(TypeOfAlert typeAlert, bool toCustomPlaceOnView, params string[] alertMessages)
        {
            IsCustomPlaceOnView = toCustomPlaceOnView;
            var alertTemplate = new AlertTemplate(typeAlert, alertMessages) {IsPageView = IsPageView};
            SaveToTempData(alertTemplate);
        }
        private static void AssembleAlert(bool toCustomPlaceOnView, ICollection<ModelState> modelStateErrors)
        {
            IsCustomPlaceOnView = toCustomPlaceOnView;
            var alertTemplate = new AlertTemplate(modelStateErrors) {IsPageView = IsPageView};
            SaveToTempData(alertTemplate);
        }
        private static void AssembleAlert(bool toCustomPlaceOnView, IdentityResult identityResult)
        {
            IsCustomPlaceOnView = toCustomPlaceOnView;
            var alertTemplate = new AlertTemplate(identityResult) {IsPageView = IsPageView};
            SaveToTempData(alertTemplate);
        }

        private const string AlertHtmlKeyOfMainPlace = "alert_html_main_place";
        private const string AlertHtmlKeyOfSomePlace = "alert_html_some_place";
        private static void SaveToTempData(AlertTemplate alertTemplate)
        {
            var specificSaveKey = IsCustomPlaceOnView ? AlertHtmlKeyOfSomePlace : AlertHtmlKeyOfMainPlace;

            if (TempData.ContainsKey(specificSaveKey))
            {
                var storedAlertHtml = TempData.Peek(specificSaveKey) as string;
                var updatedAlertHtml = storedAlertHtml + alertTemplate.Html;
                TempData[specificSaveKey] = updatedAlertHtml;
            }
            else
            {
                TempData.Add(specificSaveKey, alertTemplate.Html);
            }
        }
        /// <summary>
        /// Данный метод добавит на вьюшку разметку оповещения если оно было объявлено в Экшене хочу
        /// заметить что желательно бы помещать в див контейнер так как HTML разметка контейнеро зависима
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="isCustomPlaceOnView">Оповещения отображаются в произвольном месте?</param>
        /// <returns></returns>
        public static IHtmlString ShowAlert(this HtmlHelper helper, bool isCustomPlaceOnView = false)
        {
            var specificKey = isCustomPlaceOnView ? AlertHtmlKeyOfSomePlace : AlertHtmlKeyOfMainPlace;

            if (TempData.ContainsKey(specificKey))
            {
                var alertHtml = TempData[specificKey] as string;
                return MvcHtmlString.Create(alertHtml);
            }
            return MvcHtmlString.Empty;
        }
    }
}