﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Unfo.Core.GlobalClasses;
using UnfoIdentitySystem.GlobalStaticClasses;
using UnfoIdentitySystem.UnfoPrincipal;


namespace Unfo.Core.CustomValidations
{
    public class VMLocalize : Attribute
    {
        public string ModelProperty { get; set; }
        public VMLocalize(string modelProperty)
        {
            ModelProperty = modelProperty;
        }
    }

    public class CanEditAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            return true; // для тестов

            if (httpContext.User.Identity.IsAuthenticated)
            {
                if (httpContext.User.IsInRole("Admin")) return true;
                var user = (UnfoPrincipal) httpContext.User;

                return CurrentEvent.UEvent.Moderators.Any(d => d.UserID == user.Id);

            }
            return false;
        }

       
    }
}