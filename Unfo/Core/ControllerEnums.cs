﻿namespace Unfo.Core
{
    public enum MyAccount
    {
        Profile,
        ManageExternalLogins,
        ManagePassword,
        UserPrivacyPolitics
    }
    public enum Events
    {
        Index,
        Edit,
        Create
    }
    public enum Voting
    {
        Index
    }
    public enum Speakers
    {
        Index
    }

    public enum Programm
    {
        Index
    }
    public enum Info
    {
        Index
    }
    public enum Dictionaries
    {
        Sections,
        Halls,
        Directions,
        Locations,
        Sponsors
    }
    public enum Adverts
    {
        Index
    }

    public enum Settings
    {
        Index   
    }
}