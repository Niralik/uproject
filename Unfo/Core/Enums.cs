﻿using Unfo.Core.ValidationAttributes;

namespace Unfo.Core
{
    /// <summary>
    /// Тип мероприятий в системе
    /// </summary>
    public enum TypeOfEvents
    {
        MyEvents,
        Active,
        Archive,
        All
    }

    /// <summary>
    /// Перечесление для системы оповещения пользователя о статусе каких либо операций или возникаемых ошибках
    /// В Джава скрипте существует такое же перечесление и много клиентского кода зависит от идентичесности порядка между нимим. Будьте осторожны.
    /// </summary>
    public enum TypeOfAlert
    {
        [CssClass("alert-info")] Info,
        [CssClass("alert-warning")] Warning,
        [CssClass("alert-success")] Success,
        [CssClass("alert-danger")] Error
    }

}