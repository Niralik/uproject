﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Unfo.Models;
using Unfo.Models.ViewModels;

namespace Unfo.Core.ValidationAttributes
{
    public class EventNameValidationAttribute : ValidationAttribute
    {
        public EventNameValidationAttribute()
        {
        }

        public EventNameValidationAttribute(string errorMessage) : base(errorMessage)
        {

        }

        //protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        //{
        //    var model = ((EventVM) validationContext.ObjectInstance);
        //    if (value == null)
        //    {
        //        return new ValidationResult("Введите название");
        //    }
        //    var isExists = ViewModelRepository.Instance.Events.CheckIfEventNameExists(value as string, model.ID);
        //    return isExists ? ValidationResult.Success : new ValidationResult("Уже есть событие с таким названием");
        //}
    }
}