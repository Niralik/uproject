﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unfo.Core.Attributes
{
    /// <summary>
    /// Аттрибут для свойств связываемых объектов - говорит что данное свойство не нужно связывать с таблицей маршрутов
    /// </summary>
    public class DoNotBindAttribute : Attribute
    {

    }
}