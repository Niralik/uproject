﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Unfo.Models;
using Unfo.Models.ViewModels;

namespace Unfo.Core.ValidationAttributes
{
    /// <summary>
    /// Атрибут хранящий название Css классов для каких либо объектов
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum | AttributeTargets.Property)]
    public class CssClassAttribute : Attribute
    {
        public string CssClass { get; set; }
        public CssClassAttribute(string cssClass)
        {
            CssClass = cssClass;
        }

    }
}