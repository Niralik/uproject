﻿using DataModel;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Unfo.Core.GlobalClasses;

namespace Unfo.Core.Constraint
{
    public class EventConstraint : IRouteConstraint
    {
        public DataModel.IRepositoryFabric Fabric { get; set; }
        public EventConstraint()
        {
            Fabric = new RepositoryFabric();
        }

        public bool Match(HttpContextBase httpContext, System.Web.Routing.Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            if (!values.ContainsKey(parameterName)) return false;

            var eventName = values[parameterName] as string;
            return !string.IsNullOrWhiteSpace(eventName) && IsEventExist(eventName);
        }

        private bool IsEventExist(string translatedEventName)
        {
            var isExist = Fabric.Table<UEvent>().DbSet.Any(d => d.TranslitName == translatedEventName);
            return isExist;

        }
    }
}