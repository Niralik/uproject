﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unfo.Core.GlobalClasses;
using Unfo.Models;

namespace Unfo.Core.Initializers
{
    public class CurrentEventInitializer: BaseInitializer
    {
        public static void Execute()
        {
            InitializeCurrentEvent();
            InitializeCookieCurrentEvent();
        }
        private static void InitializeCurrentEvent()
        {
            var eventRouteName = Request.RequestContext.RouteData.Values["eventName"] as string;
            if (string.IsNullOrWhiteSpace(eventRouteName))
            {
                CurrentEvent.Dispose();
            }
            else
            {
                CurrentEvent.Initialize(ViewModelRepository.Instance.Events.GetByRouteName(eventRouteName));
            }
        }

        private static void InitializeCookieCurrentEvent()
        {
            var currentEventCookie = new HttpCookie("_current_event", "");
            if (CurrentEvent.ID != default(Guid))
            {
                currentEventCookie.Value = CurrentEvent.ToJson();
            }
            Response.Cookies.Set(currentEventCookie);
        }
    }
}