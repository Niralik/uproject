﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.Initializers
{
    public class BaseInitializer
    {
        protected static UnfoSignInManager SignInManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<UnfoSignInManager>();
            }

        }
        protected static HttpContext HttpContext
        {
            get
            {
                return HttpContext.Current;
            }
        }
        protected static HttpResponse Response
        {
            get
            {
                return HttpContext.Response;
            }
        }
        protected static HttpRequest Request
        {
            get
            {
                return HttpContext.Request;
            }
        }
        protected static UnfoPrincipal CurrentUser
        {
            get { return HttpContext.User as UnfoPrincipal; }
            set { HttpContext.User = value; }
        }
    }
}