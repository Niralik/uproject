﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Owin.Security.Providers.ArcGISOnline.Provider;
using Unfo.Core.Extensions.IdentityExtensions;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.Initializers
{
    public class CurrentUserInitializer : BaseInitializer
    {
        public static void Execute()
        {
            InitializeCurrentUser();
            InitializeCookieCurrentUser();
            InitializeTimezone();
        }
        private static void InitializeCurrentUser()
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            CurrentUser = new UnfoPrincipal();
            if (authCookie != null)
            {
                try
                {
                    var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket == null) return;
                    var deserializedModel = JsonConvert.DeserializeObject<UnfoPrincipalSerializeModel>(authTicket.UserData);

                    var userFromCookie = new UnfoPrincipal(deserializedModel);
                    CurrentUser = userFromCookie;

                    if (!authTicket.Expired) return;

                    var resultOfUpdate = SignInManager.UpdateCookieForCurrentUser();
                    if (resultOfUpdate.Succeeded)
                    {
                        InitializeCurrentUser();              
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                    }
                }
                catch (CryptographicException ex)
                {
                    FormsAuthentication.SignOut();
                }
            }
            else
            {
                FormsAuthentication.SignOut();
            }
        }

        private static void InitializeCookieCurrentUser()
        {
            var currentUserCookie = new HttpCookie("_current_user", "");
            if (CurrentUser.Identity.IsAuthenticated)
            {
                currentUserCookie.Value = CurrentUser.ToJson();
            }
            Response.Cookies.Set(currentUserCookie);
        }

        private static void InitializeTimezone()
        {
            var timeZoneCookie = Request.Cookies["_timezone"];
            var defaultTimeZone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(tz => tz.Id == "Ekaterinburg Standard Time");
            if (timeZoneCookie != null)
            {
                var userTimeZoneStandartName = HttpUtility.UrlDecode(timeZoneCookie.Value);
                var userTimeZone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(tz => tz.StandardName == userTimeZoneStandartName);

                CurrentUser.TimeZone = userTimeZone ?? defaultTimeZone;
            }
            else
            {
                CurrentUser.TimeZone = defaultTimeZone;
            }
        }
    }
}