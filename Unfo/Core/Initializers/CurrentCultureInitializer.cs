﻿using System.Globalization;
using System.Threading;
using Unfo.Core.Helpers;

namespace Unfo.Core.Initializers
{
    public class CurrentCultureInitializer : BaseInitializer
    {
        public static void Execute()
        {
            InitializeCurrentCulture();
        }

        private static void InitializeCurrentCulture()
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            var cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
            {
                // obtain it from HTTP header AcceptLanguages
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0
                    ? Request.UserLanguages[0] : null;
            }
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe
            var currentCultureInfo = new CultureInfo(cultureName);

            // Modify current thread's cultures
            Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}