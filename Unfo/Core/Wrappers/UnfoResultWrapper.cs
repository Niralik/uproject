﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Unfo.Core.Wrappers
{
    public class UnfoResult: IdentityResult
    {
        public bool IsFailed { get { return !Succeeded; } }
        public UnfoResult(params string[] errors) : base(errors)
        {
        }

        public UnfoResult(IEnumerable<string> errors) : base(errors)
        {
        }

        protected UnfoResult(bool success) : base(success)
        {
        }

        public new static UnfoResult Success
        {
            get { return new UnfoResult(true); }
        }

        public new static UnfoResult Failed(params string[] errors)
        {
            return new UnfoResult(errors);
        }
    }
}