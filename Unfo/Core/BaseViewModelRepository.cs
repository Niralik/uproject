﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using DataModel.ModelInterfaces;
using Unfo.Core.GlobalClasses;
using Unfo.Models.ViewModels;
using Unfo.Models.ViewModels.Dictionaries;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core
{
    public class BaseViewModelRepository
    {
        public  UnfoPrincipal User
        {
            get { return HttpContext.Current.User as UnfoPrincipal; }
        }

        public DataModel.IRepositoryFabric Fabric { get; set; }

        public BaseViewModelRepository()
        {
        }

        public BaseViewModelRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }

        public virtual void AddOrUpdate<TModel>(IModelBuilder<TModel> data) where TModel : EventBaseModel, new()
        {
            var rep = Fabric.Get<TModel>();
            var model = data.GetModel();
            if (model.ID == Guid.Empty)
            {
                rep.Insert(model);
                rep.Save();
            }
            else
            {
                var report = rep.GetById(model.ID);
                if (report.EventID == CurrentEvent.ID)
                {
                    report = data.UpdateModel(report);
                    rep.Update(report);
                    rep.Save();
                }
            }
        }
      

    }
}