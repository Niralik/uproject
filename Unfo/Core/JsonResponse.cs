﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unfo.Core
{
    public class JsonResponse
    {
        public int Result { get; set; }

        public object Response { get; set; }

        public static JsonResponse GetSuccess(object resopnse)
        {
            return new JsonResponse()
            {
                Response = resopnse,
                Result = 1
            };
        }

        /// <summary>
        /// Используется в api
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static JsonResponse GetError(int code)
        {
            var errors = new List<string>()
            {
                "Ошибка запроса."
            };

            return new JsonResponse()
            {
                Response = new JsonErrorResponse() { ErrorCode = code, Errors = errors },
                Result = 0
            };
        }

        public static JsonResponse GetError(int code, string error)
        {
            var errors = new List<string>()
            {
                error
            };

            return new JsonResponse()
            {
                Response = new JsonErrorResponse() { ErrorCode = code, Errors = errors },
                Result = 0
            };
        }


        /// <summary>
        /// Используется в api
        /// </summary>
        /// <param name="code"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static JsonResponse GetError(int code, IList<string> desc)
        {
            return new JsonResponse()
            {
                Response = new JsonErrorResponse() { ErrorCode = code, Errors = desc },
                Result = 0
            };
        }

    }


    public class JsonErrorResponse
    {
        public int ErrorCode { get; set; }

        public IList<string> Errors { get; set; }
    }
}