﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Web;

namespace Unfo.Core.Extensions
{


    public static class ImageExtetions
    {
        static Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.High;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        static Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
            //img.Dispose();
            return (Image)(bmpCrop);
        }

        public static Image GetThumbs(this Image source, Size size)
        {
            if (source.Size.Width < size.Width || source.Size.Height < size.Height)
            {
                int sW = source.Size.Width;
                int sH = source.Size.Height;

                double percent = 0;
                double p1 = size.Height / (double)sH;
                double p2 = size.Width / (double)sW;
                if (p1 > p2) percent = p1;
                else percent = p2;

                source = resizeImage(source, new Size((int)(Math.Ceiling(sW * percent)), (int)(Math.Ceiling(sH * percent))));

            }
            Rectangle rect = new Rectangle(new Point(((source.Size.Width / 2) - (size.Width / 2)), ((source.Size.Height / 2) - (size.Height / 2))), size);

            return cropImage(source, rect);
        }

        public static Image GetResizeImage(this Image source, Size newSize)
        {
            return resizeImage(source, newSize);
        }

        public static Image CropImage(this Image source, Rectangle cropArea)
        {
            return cropImage(source, cropArea);
        }

        
    }
}