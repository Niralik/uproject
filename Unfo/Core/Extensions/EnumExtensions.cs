﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using DataModel.Core.Attributes;
using Unfo.Core.Attributes;
using Unfo.Core.ValidationAttributes;

namespace Unfo.Core.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Возвращает css класс зашитый в атриббуте [CssClass] у объекта над которым выполняется данный метод
        /// </summary>
        /// <returns>Если будут найдены аттрибуты то вернется значение зашитое, если же ничего не будет вернется пустая строка</returns>
        public static Guid GetDbId(this Enum value)
        {
            var roleId = RoleIdAttribute.RolesIds[(Role) value];
            return roleId;
        }
    }
}