﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unfo.Core.Extensions
{
    public static class DateTimeExtantions
    {
        public static List<DateTime> DatesToDate(this DateTime date, DateTime toDate)
        {
            List<DateTime> dates = new List<DateTime>();

            if (date.Date > toDate.Date) return dates;
            if (date.Date == toDate.Date)
            {
                dates.Add(date.Date);
                return dates;
            }
            DateTime tmp = date;
            while (tmp.Date<=toDate.Date)
            {
                dates.Add(tmp.Date);
                tmp = tmp.AddDays(1);
            }

            return dates;
        }
    }
}