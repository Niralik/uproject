﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Unfo.Models.ViewModelRepositories;
using Unfo.Models.ViewModels;

namespace Unfo.Core.Extensions
{
    public static class UrlHelperExtension
    {
        private static RouteValueDictionary RouteDictionaryWithEventName(ActionResult result, EventVM eventVm, bool isDeleteArea = false)
        {
            var routeValueDictionary = result.GetRouteValueDictionary();
            routeValueDictionary.Add("eventName", eventVm.TranslitName);
            if (isDeleteArea)
            {
                routeValueDictionary.Remove("Area");
            }
            return routeValueDictionary;
        }
        private static RouteValueDictionary RouteDictionaryWithoutAreaName(ActionResult result)
        {
            var routeValueDictionary = result.GetRouteValueDictionary();
            routeValueDictionary.Remove("Area");
            return routeValueDictionary;
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, ActionResult result)
        {
            return urlHelper.RouteUrl(null, result, null, null);
        }
        public static string UnfoEventUrl(this UrlHelper urlHelper, ActionResult result, EventVM eventVm)
        {
            return urlHelper.RouteUrl(null, RouteDictionaryWithEventName(result, eventVm), null, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, ActionResult result)
        {
            return urlHelper.RouteUrl(routeName, RouteDictionaryWithoutAreaName(result), null, null);
        }
        public static string UnfoEventUrl(this UrlHelper urlHelper, string routeName, ActionResult result, EventVM eventVm)
        {
            return urlHelper.RouteUrl(routeName, RouteDictionaryWithEventName(result, eventVm, true), null, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, ActionResult result, string protocol)
        {
            return urlHelper.RouteUrl(routeName, result, protocol, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, ActionResult result, string protocol, string hostName)
        {
            return urlHelper.RouteUrl(routeName, result.GetRouteValueDictionary(), protocol ?? result.GetT4MVCResult().Protocol, hostName);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, Task<ActionResult> taskResult)
        {
            return urlHelper.RouteUrl(null, RouteDictionaryWithoutAreaName(taskResult.Result), null, null);
        }
        public static string UnfoEventUrl(this UrlHelper urlHelper, Task<ActionResult> taskResult, EventVM eventVm)
        {
            return urlHelper.RouteUrl(null, RouteDictionaryWithEventName(taskResult.Result, eventVm), null, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, Task<ActionResult> taskResult)
        {
            return urlHelper.RouteUrl(routeName, RouteDictionaryWithoutAreaName(taskResult.Result), null, null);
        }
        public static string UnfoEventUrl(this UrlHelper urlHelper, string routeName, Task<ActionResult> taskResult, EventVM eventVm)
        {
            return urlHelper.RouteUrl(routeName, RouteDictionaryWithEventName(taskResult.Result, eventVm, true), null, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, Task<ActionResult> taskResult, string protocol)
        {
            return urlHelper.RouteUrl(routeName, taskResult.Result, protocol, null);
        }

        public static string UnfoRouteUrl(this UrlHelper urlHelper, string routeName, Task<ActionResult> taskResult, string protocol, string hostName)
        {
            return urlHelper.RouteUrl(routeName, taskResult.Result, protocol, hostName);
        }
        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, object htmlAttributes, string protocol)
        {
            return htmlHelper.RouteLink(linkText, routeName, result, htmlAttributes, protocol, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, object htmlAttributes, string protocol, string hostName)
        {
            return htmlHelper.RouteLink(linkText, routeName, result, htmlAttributes, protocol, hostName, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, ActionResult result, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, null, result, htmlAttributes, null, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, result, htmlAttributes, null, null, null);
        }
        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, ActionResult result)
        {
            return htmlHelper.RouteLink(linkText, result, null);
        }
        public static MvcHtmlString UnfoEventLink(this HtmlHelper htmlHelper, string linkText,  ActionResult result, EventVM eventVm)
        {
            return htmlHelper.RouteLink(linkText, RouteDictionaryWithEventName(result, eventVm, false), null);
        }
        public static MvcHtmlString UnfoEventLink(this HtmlHelper htmlHelper, string linkText, ActionResult result, EventVM eventVm, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, RouteDictionaryWithEventName(result, eventVm, true), htmlAttributes);
        }
        public static MvcHtmlString UnfoEventLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, EventVM eventVm)
        {
            return htmlHelper.RouteLink(linkText, routeName, RouteDictionaryWithEventName(result, eventVm, true), null);
        }
        public static MvcHtmlString UnfoEventLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, EventVM eventVm, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, RouteDictionaryWithEventName(result, eventVm, true), htmlAttributes);
        }
        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, IDictionary<string, object> htmlAttributes, string protocol)
        {
            return htmlHelper.RouteLink(linkText, routeName, result, htmlAttributes, protocol, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, ActionResult result, IDictionary<string, object> htmlAttributes, string protocol, string hostName)
        {
            return htmlHelper.RouteLink(linkText, routeName, result, htmlAttributes, protocol, hostName, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, Task<ActionResult> taskResult, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, null, taskResult.Result, htmlAttributes, null, null, null);
        }
        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, Task<ActionResult> taskResult)
        {
            return htmlHelper.RouteLink(linkText, null, taskResult.Result, null, null, null, null);
        }
        public static MvcHtmlString UnfoEventLink(this HtmlHelper htmlHelper, string linkText, Task<ActionResult> taskResult, EventVM eventVm, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, null, RouteDictionaryWithEventName(taskResult.Result, eventVm, true), htmlAttributes);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, null, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, object htmlAttributes, string protocol)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, protocol, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, object htmlAttributes, string protocol, string hostName)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, protocol, hostName, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, object htmlAttributes, string protocol, string hostName, string fragment)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), protocol, hostName, fragment);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, Task<ActionResult> taskResult, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, null, taskResult.Result, htmlAttributes, null, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, null, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, IDictionary<string, object> htmlAttributes, string protocol)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, protocol, null, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, IDictionary<string, object> htmlAttributes, string protocol, string hostName)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, protocol, hostName, null);
        }

        public static MvcHtmlString UnfoRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, Task<ActionResult> taskResult, IDictionary<string, object> htmlAttributes, string protocol, string hostName, string fragment)
        {
            return htmlHelper.RouteLink(linkText, routeName, taskResult.Result, htmlAttributes, protocol, hostName, fragment);
        }






        #region Cabinet Default Route Urls
        
        /// <summary>
        /// Создает урлы на основе дефолтного кабинет роута - <see cref="Route.CabinetDefault"/>
        /// </summary>
        /// <returns></returns>
         public static string UnfoCabinetRouteUrl(this UrlHelper urlHelper, ActionResult result)
        {
            return urlHelper.RouteUrl(Route.CabinetDefault, RouteDictionaryWithoutAreaName(result), null, null);
        }
         /// <summary>
         /// Создает урлы на основе дефолтного кабинет роута - <see cref="Route.CabinetDefault"/>
         /// </summary>
         /// <returns></returns>
         public static string UnfoCabinetRouteUrl(this UrlHelper urlHelper, Task<ActionResult> taskResult)
         {
             Task.WaitAll(taskResult);
             return urlHelper.RouteUrl(Route.CabinetDefault, RouteDictionaryWithoutAreaName(taskResult.Result), null, null);
         }
        #endregion

        #region Users Filter Route Urls
         public static string UnfoUsersSortUrl(this UrlHelper urlHelper, UsersFilterParams filterParams)
         {
             switch (filterParams.FilterRole)
             {
                 case FilterRole.None:
                     break;
                 case FilterRole.Admin:
                     break;
                 case FilterRole.Organizer:
                     break;
                 case FilterRole.Moderator:
                    return UnfoRouteUrl(urlHelper, MVC.Cabinet.Users.Moderators(filterParams));
                 case FilterRole.User:
                     break;
             }
             return UnfoRouteUrl(urlHelper, MVC.Cabinet.Users.Index(filterParams));
         }
        #endregion

    }
}