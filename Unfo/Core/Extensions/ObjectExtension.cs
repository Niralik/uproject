﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Unfo.Core.ValidationAttributes;
using Unfo.Models.ViewModels;

namespace Unfo.Core.Extensions
{
    public static class ObjectExtension
    {

        /// <summary>
        /// Возвращает css класс зашитый в атриббуте [CssClass] у объекта над которым выполняется данный метод
        /// </summary>
        /// <returns>Если будут найдены аттрибуты то вернется значение зашитое, если же ничего не будет вернется пустая строка</returns>
        public static string GetCssClass(this Object value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var cssAttribute = fieldInfo.GetCustomAttributes(typeof(CssClassAttribute), false) as CssClassAttribute[];
            if (cssAttribute == null) return string.Empty;
            return (cssAttribute.Length > 0) ? cssAttribute[0].CssClass : string.Empty;
        }
    }

    
}