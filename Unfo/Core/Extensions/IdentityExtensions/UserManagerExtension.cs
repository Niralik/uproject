﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CsQuery.ExtensionMethods;
using DataModel;
using Microsoft.AspNet.Identity;
using UnfoIdentitySystem;

namespace Unfo.Core.Extensions.IdentityExtensions
{
    public static class UserManagerExtension
    {
        public static async Task<IdentityResult> AddToRoleAsync(this UnfoUserManager userManager, UnfoUser user, Role role)
        {
            return await userManager.AddToRoleAsync(user.Id, role.ToString());
        }
        public static async Task<IdentityResult> AddToRolesAsync(this UnfoUserManager userManager, UnfoUser user, params Role[] roles)
        {
            var stringRoles = roles.Select(r => r.ToString()).ToArray();
            return await userManager.AddToRolesAsync(user.Id, stringRoles);
        }
    }
}