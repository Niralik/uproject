﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using DataModel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Unfo.App_LocalResources;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.Extensions.IdentityExtensions
{
    public static class UnfoSingInManagerExtension
    {
        /// <summary>
        /// Метод создает Куки представляющие из себя расширенные данные о текущем пользователе и 
        /// </summary>
        /// <param name="dbUser">данные о пользователе представленные бизнес-моделью</param>
        /// <param name="rememberMe">Запомнить пользователя</param>
        /// <param name="userRoles">Список ролей текущего пользователя которые необходимо сохранить в куках</param>
        /// <returns></returns>
        public static async Task<IdentityResult> SignInAsync(this UnfoSignInManager signInManager, UnfoUser dbUser, bool rememberMe, params Role[] userRoles)
        {
            try
            {
                IEnumerable<Role> roles = null;
                if (userRoles == null || userRoles.Count() == 0)
                {
                    var dbRoles = await signInManager.UserManager.GetRolesAsync(dbUser.Id);
                    roles = dbRoles.Select(r => Enum.Parse(typeof(Role), r)).Cast<Role>().ToList();
                }
                else
                {
                    roles = userRoles;
                }
                CreateTicket(dbUser, rememberMe, roles.ToArray());
                return IdentityResult.Success;
            }
            catch (Exception)
            {
                return new IdentityResult(Resources.Validate_Alert_IternalServerError);
            }
        }
        /// <summary>
        /// Метод создает Куки представляющие из себя расширенные данные о текущем пользователе и 
        /// </summary>
        /// <param name="dbUser">данные о пользователе представленные бизнес-моделью</param>
        /// <param name="rememberMe">Запомнить пользователя</param>
        /// <param name="userRoles">Список ролей текущего пользователя которые необходимо сохранить в куках</param>
        /// <returns></returns>
        public static IdentityResult SignIn(this UnfoSignInManager signInManager, UnfoUser dbUser, bool rememberMe, params Role[] userRoles)
        {
            try
            {
                IEnumerable<Role> roles = null;
                if (userRoles == null || userRoles.Count() == 0)
                {
                    var dbRoles = signInManager.UserManager.GetRoles(dbUser.Id);
                    roles = dbRoles.Select(r => Enum.Parse(typeof(Role), r)).Cast<Role>().ToList();
                }
                else
                {
                    roles = userRoles;
                }
                CreateTicket(dbUser, rememberMe, roles.ToArray());
                return IdentityResult.Success;
            }
            catch (Exception)
            {
                return new IdentityResult(Resources.Validate_Alert_IternalServerError);
            }
        }

        private static void CreateTicket(UnfoUser dbUser, bool rememberMe, Role[] roles)
        {
            var serializeModel = new UnfoPrincipalSerializeModel(dbUser, roles);

            var serializedUserData = JsonConvert.SerializeObject(serializeModel);

            var authenticationTicket = new FormsAuthenticationTicket(
                1,
                dbUser.UserName,
                DateTime.Now,
                DateTime.Now.AddMinutes(60),
                rememberMe,
                serializedUserData);
            var encryptTicket = FormsAuthentication.Encrypt(authenticationTicket);
            var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptTicket);
            HttpContext.Current.Response.Cookies.Set(faCookie);
            HttpContext.Current.Request.Cookies.Set(faCookie);
        }


        /// <summary>
        /// Обновление куков текущего пользователя(и куда он только течет Х)) )
        /// </summary>
        /// <returns></returns>
        public static async Task<IdentityResult> UpdateCookieForCurrentUserAsync(this UnfoSignInManager signInManager)
        {
            var currentUser = await signInManager.UserManager.FindByNameAsync(HttpContext.Current.User.Identity.Name);
            if (currentUser != null)
            {
                var result = await signInManager.SignInAsync(currentUser, true);
                return result;
            }
            return new IdentityResult("Вай вай - почему то нету юзера там где должен быть он =_=");
        }
        /// <summary>
        /// Обновление куков текущего пользователя(и куда он только течет Х)) )
        /// </summary>
        /// <returns></returns>
        public static IdentityResult UpdateCookieForCurrentUser(this UnfoSignInManager signInManager)
        {
            var currentUser = signInManager.UserManager.FindByName(HttpContext.Current.User.Identity.Name);
            if (currentUser != null)
            {
                currentUser.LastSignInDate = DateTime.Now;
                var result = signInManager.SignIn(currentUser, true);
                return result;
            }
            return new IdentityResult("Вай вай - почему то нету юзера там где должен быть он =_=");
        }
    }
}