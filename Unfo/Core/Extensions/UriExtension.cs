﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using DataModel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Unfo.App_LocalResources;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.Extensions.IdentityExtensions
{
    public static class UriExtension
    {
        /// <summary>
        /// Метод формирует красивую ссылку с https 
        /// </summary>
        /// <param name="uriBuilder"></param>
        /// <returns></returns>
        public static string ToStringWithHttps(this UriBuilder uriBuilder)
        {
            uriBuilder.Scheme = Uri.UriSchemeHttps;
            uriBuilder.Port = -1;
            return uriBuilder.Uri.ToString();
        }
        /// <summary>
        /// Метод формирует красивую ссылку с http
        /// </summary>
        /// <param name="uriBuilder"></param>
        /// <returns></returns>
        public static string ToStringWithoutPorts(this UriBuilder uriBuilder)
        {
            uriBuilder.Port = -1;
            return uriBuilder.Uri.ToString();
        }
    }
}