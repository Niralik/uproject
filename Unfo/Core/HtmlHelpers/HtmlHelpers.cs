﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Unfo.Core;

namespace System.Web.Mvc
{
    public static class HtmlHelpers
    {
        /// <summary>
        /// Метод возвражает словосочитание нужного склонения на основе значения контрольной цифры -
        /// строки должны быть подходящими для обработки методом String.Format()
        /// </summary>
        /// <param name="count">Контрольная цифра</param>
        /// <param name="a">единственное число (именительный падеж)</param>
        /// <param name="b">множественное число (именительный падеж)</param>
        /// <param name="c">множественное число (родительный падеж)</param>
        public static MvcHtmlString Declination(this HtmlHelper helper, int count, string a, string b, string c)
        {
            return new MvcHtmlString(Utilities.Declination(count, a,b,c));
        }  
    }

}