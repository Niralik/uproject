﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Unfo.Core.Filters;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Core.HtmlHelpers
{
    public static class HtmlHelperExtensions
    {
   
        public static MvcHtmlString Pager(this HtmlHelper helper, IPageableParams pageParams, Func<int, string> action, int amountLinks = 5)
        {
            if (pageParams.SummaryAmountOfPages <= 1)
            {
                return MvcHtmlString.Empty;
            }
            var currentPage = pageParams.CurrentPage;
            var lastPageNumber = (int)Math.Ceiling((double)currentPage / amountLinks) * amountLinks;
            var firstPageNumber = lastPageNumber - (amountLinks - 1);
            var hasPreviousPage = currentPage > 1;
            var hasNextPage = currentPage < pageParams.SummaryAmountOfPages;
            if (lastPageNumber > pageParams.SummaryAmountOfPages)
            {
                lastPageNumber = pageParams.SummaryAmountOfPages;
            }
            var ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");
            ul.InnerHtml += AddLink(1, action, currentPage == 1, "disabled", "<<", "First Page");
            ul.InnerHtml += AddLink(currentPage - 1, action, !hasPreviousPage, "disabled", "<", "Previous Page");
            for (int i = firstPageNumber; i <= lastPageNumber; i++)
            {
                ul.InnerHtml += AddLink(i, action, i == currentPage, "active", i.ToString(), i.ToString());
            }
            ul.InnerHtml += AddLink(currentPage + 1, action, !hasNextPage, "disabled", ">", "Next Page");
            ul.InnerHtml += AddLink(pageParams.SummaryAmountOfPages, action, currentPage == pageParams.SummaryAmountOfPages, "disabled", ">>", "Last Page");
            return MvcHtmlString.Create(ul.ToString());
        }
        private static TagBuilder AddLink(int index, Func<int, string> action, bool condition, string classToAdd, string linkText, string tooltip)
        {
            var li = new TagBuilder("li");
            li.MergeAttribute("title", tooltip);
            if (condition)
            {
                li.AddCssClass(classToAdd);
            }
            var a = new TagBuilder("a");
            a.MergeAttribute("href", !condition ? action(index) : "javascript:");
            a.SetInnerText(linkText);
            li.InnerHtml = a.ToString();
            return li;
        }

    }

    public class PagerViewModel
    {
        public int CurrentPage { get; set; }

        public int CountPage { get; set; }
    }
}