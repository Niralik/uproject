﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unfo.Core.Attributes;
using Unfo.Models.ViewModelRepositories;

namespace Unfo.Core.Filters
{
    public abstract class BaseUnfoFilterParams<TNestedParams> : LinkPagesParams, IBaseFilterParams where TNestedParams : class, new()
    {

        public string SearchQuery { get; set; }
        /// <summary>
        /// Общее количество найденных совпадений после выполнения запроса на основе параметров фильтра
        /// </summary>
        [DoNotBind]
        public int ResultCount { get; set; }
        
        /// <summary>
        /// Возвращает полную копию ParamsOfQueryToEvent
        /// </summary>
        /// <returns></returns>
        protected TNestedParams Clone()
        {
            var filterParams = new TNestedParams();
            var typeFilterParams = typeof(TNestedParams);
            foreach (var propertyInfo in typeFilterParams.GetProperties())
            {
                var valueOfThisInstance = propertyInfo.GetValue(this);
                propertyInfo.SetValue(filterParams, valueOfThisInstance);
            }
            return filterParams;
        }
    }
    public abstract class LinkPagesParams : IPageableParams
    {
        private int _currentPage;
        private int _amountOfElementsPerPage;
        private const int DefaultCurrentPage = 1;
        private const int DefaultAmountElemetsPerPage = 25;

        public int CurrentPage
        {
            get { return _currentPage == default(int) ? DefaultCurrentPage : _currentPage; }
            set { _currentPage = value; }
        }

        public int AmountOfElementsPerPage
        {
            get { return _amountOfElementsPerPage == default(int) ? DefaultAmountElemetsPerPage : _amountOfElementsPerPage; }
            set { _amountOfElementsPerPage = value; }
        }
        [DoNotBind]
        public int SummaryAmountOfPages { get; set; }
        [DoNotBind]
        public bool IsDisablePaginating { get; set; }

        public void DisablePagination()
        {
            IsDisablePaginating = true;
        }

        public void EnablePagination()
        {
            IsDisablePaginating = false;
        }
        public void SetDefaultCurrentPage()
        {
            CurrentPage = DefaultCurrentPage;
        }

    }

    public interface IBaseFilterParams: IPageableParams
    {
        string SearchQuery { get; set; }
        int ResultCount { get; set; }
    }
    public interface IPageableParams
    {
        int CurrentPage { get; set; }
        int AmountOfElementsPerPage { get; set; }
        int SummaryAmountOfPages { get; set; }
        bool IsDisablePaginating { get; }
        void DisablePagination();
        void EnablePagination();
        void SetDefaultCurrentPage();

    }
    public interface IPageableParams<out TParamsClass> : IPageableParams
    {
        TParamsClass SetCurrentPage(int numCurrentPage);
    }
  
}