﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DataModel;
using DataModel.ModelInterfaces;

namespace Unfo.Core.Filters
{
    /// <summary>
    /// База для создания фильтров - логика фильтра использует паттерн Шаблонный метод для фиксации алгоритма
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class BaseUnfoFilter<TEntity> where TEntity : class
    {
        protected IRepositoryFabric _fabric = new RepositoryFabric();

        protected IRepositoryFabric RepositoryFabric
        {
            get { return _fabric; }
            set { _fabric = value; }
        }
        protected IQueryable<TEntity> FilteredQuery { get; set; }
        protected IBaseFilterParams FilterParams { get; set; }
        protected UnfoDbContext CurrentDbContext { get; set; }

        protected BaseUnfoFilter(IBaseFilterParams parameters)
        {
            if (parameters == null) throw new NoNullAllowedException("Filter");
            FilterParams = parameters;
        }
        protected void AssembleQuery()
        {
            CurrentDbContext = RepositoryFabric.NewContext();
            FiltrateQuery();
            SortQuery();
        }
        protected abstract void FiltrateQuery();
        protected abstract void SortQuery();

        private void PaginateQuery()
        {
            if (FilterParams.IsDisablePaginating) return;

            var paginatedQuery = FilteredQuery.Skip((FilterParams.CurrentPage - 1) * FilterParams.AmountOfElementsPerPage).Take(FilterParams.AmountOfElementsPerPage);
            FilteredQuery = paginatedQuery;
        }

        public async Task<List<TViewModel>> GetResults<TViewModel>() where TViewModel : class, IViewModelBuilder<TEntity>, new()
        {
            AssembleQuery();
            PaginateQuery();
            var requestedList = await FilteredQuery.ToListAsync();
            var wrappedRequestedList = Fill<TViewModel>(requestedList);
            return wrappedRequestedList;
        }
        public async Task<List<TEntity>> GetResults()
        {
            AssembleQuery();
            PaginateQuery();
            var requestedList = await FilteredQuery.ToListAsync();
            return requestedList;
        }
        private static List<TViewModel> Fill<TViewModel>(List<TEntity> entities) where TViewModel : class, IViewModelBuilder<TEntity>, new()
        {
            var viewModels = new List<TViewModel>();
            entities.ForEach(entity =>
            {
                var viewModel = new TViewModel();
                viewModel.Fill(entity);
                viewModels.Add(viewModel);
            });
            return viewModels;
        }

        public async Task<int> GetAmountOfPages()
        {
            AssembleQuery();
            var amountOfEntities = await FilteredQuery.CountAsync();
            FilterParams.ResultCount = amountOfEntities;
            var amountOfPages = (double)amountOfEntities / FilterParams.AmountOfElementsPerPage;
            return (int)Math.Ceiling(amountOfPages);
        }

        protected IQueryable<TEntity> FiltrateBySearchQuery(IQueryable<TEntity> incommingQuery)
        {
            if (string.IsNullOrWhiteSpace(FilterParams.SearchQuery)) return incommingQuery;
            var terms = FilterParams.SearchQuery.Split(Convert.ToChar(" ")).Where(term => !string.IsNullOrWhiteSpace(term)).ToArray();
            var filteredQueryBySearchQuery = SearchByCriteria(incommingQuery, terms);
            return filteredQueryBySearchQuery;
        }

        protected abstract IQueryable<TEntity> SearchByCriteria(IQueryable<TEntity> incommingQuery, string[] searchTerms);
    }
}