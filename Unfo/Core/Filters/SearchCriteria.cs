﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataModel;

namespace Unfo.Core.Filters
{
    /// <summary>
    /// Класс инкапсулирует в статических свойствах различные поисковые критерии
    /// </summary>
    /// <returns>Список критериев</returns>
    public class SearchCriteria
    {
        public static Expression<Func<UnfoUser, string>>[] Users
        {
            get
            {
                var searchCriteria = new Expression<Func<UnfoUser, string>>[5];
                searchCriteria[0] = user => user.Email;
                searchCriteria[1] = user => user.FirstName;
                searchCriteria[2] = user => user.SurName;
                searchCriteria[3] = user => user.FatherName;
                searchCriteria[4] = user => user.PhoneNumber;
                return searchCriteria;
            }
        }
        public static Expression<Func<UEvent, string>>[] Events
        {
            get
            {
                var searchCriteria = new Expression<Func<UEvent, string>>[1];
                searchCriteria[0] = uEvent => uEvent.Name;
                return searchCriteria;
            }
        }
    }
}