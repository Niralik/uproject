﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace Unfo.Core
{
    public static partial class Utilities
    {
        public static byte[] ReadFully(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static string GetAllErrors(ICollection<ModelState> modelStates)
        {
            var sb = new StringBuilder();
            foreach (ModelState modelState in modelStates)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    sb.Append(error.ErrorMessage);
                }
            }
            return sb.ToString();
        }

        public static IEnumerable<string> GetArrayOfErrors(ICollection<ModelState> modelStates)
        {
            var errors = new List<string>();
            var sb = new StringBuilder();
            foreach (ModelState modelState in modelStates)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    errors.Add(error.ErrorMessage);
                }
            }
            return errors;
        }

        /// <summary>
        /// Метод читай html разметку из файла
        /// </summary>
        /// <param name="htmlFileNameWithPath">Физический путь до файла</param>
        /// <returns></returns>
        public static StringBuilder ReadHtmlFile(string htmlFileNameWithPath)
        {
            var htmlContent = new StringBuilder();
            string line;
            try
            {
                using (var htmlReader = new StreamReader(htmlFileNameWithPath))
                {

                    while ((line = htmlReader.ReadLine()) != null)
                    {
                        htmlContent.Append(line);
                    }
                }
            }
            catch (Exception objError)
            {
                throw objError;
            }

            return htmlContent;
        }

        /// <summary>
        /// Метод возвражает словосочитание нужного склонения на основе значения контрольной цифры -
        /// строки должны быть подходящими для обработки методом String.Format()
        /// </summary>
        /// <param name="count">Контрольная цифра</param>
        /// <param name="a">единственное число (именительный падеж)</param>
        /// <param name="b">множественное число (именительный падеж)</param>
        /// <param name="c">множественное число (родительный падеж)</param>
        public static string Declination(int count, string a, string b, string c)
        {
            int n = count % 100;
            int n1 = n % 10;
            if (n > 10 && n < 20) return String.Format(c, count);
            if (n1 > 1 && n1 < 5) return String.Format(b, count);
            if (n1 == 1) return String.Format(a, count);
            return String.Format(c, count);
        }
        /// <summary>
        /// Метод возвражает словосочитание нужного склонения на основе значения контрольной цифры -
        /// строки должны быть подходящими для обработки методом String.Format()
        /// </summary>
        /// <param name="count">Контрольная цифра</param>
        /// <param name="a">форма единственного числа</param>
        /// <param name="b">форма множественного числа</param>
        public static string Declination(int count, string a, string b)
        {
            return Declination(count, a, b, b);
        }
    }
}