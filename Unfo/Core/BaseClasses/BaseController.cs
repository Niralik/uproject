﻿using System;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Unfo.Core.Helpers;
using Unfo.Core.Initializers;
using Unfo.Models;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.BaseClasses
{
    //[Authorize]
    //[RequireHttps]
    public class BaseController : Controller
    {
        #region Свойства

        protected new virtual UnfoPrincipal User
        {
            get { return HttpContext.User as UnfoPrincipal; }
        }

        public UnfoSignInManager SignInManager
        {
            get { return HttpContext.GetOwinContext().Get<UnfoSignInManager>(); }
        }

        public UnfoUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<UnfoUserManager>(); }
        }

        public UnfoRoleManager RoleManager
        {
            get { return HttpContext.GetOwinContext().Get<UnfoRoleManager>(); }
        }

        public IFileStorageManager FileManager { get; private set; }

        public ViewModelRepository VmRepository { get; private set; }

        #endregion Свойства

        public BaseController()
        {
            VmRepository = new ViewModelRepository();
            FileManager = new LocalStorageManager();
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            Alert.InitializeTempDataContext(ControllerContext);
            CurrentUserInitializer.Execute();
            CurrentEventInitializer.Execute();
            CurrentCultureInitializer.Execute();
            return base.BeginExecuteCore(callback, state);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SetCurrentActiveAction(filterContext);
            base.OnActionExecuting(filterContext);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && VmRepository != null) VmRepository.Dispose();

            base.Dispose(disposing);
        }

        /// <summary>
        /// Установка текущего активного экшена к которому осуществляется запрос в HtmlEnginge
        /// </summary>
        /// <param name="filterContext">Фильтерконтекст метода OnActionExecuting</param>
        private void SetCurrentActiveAction(ActionExecutingContext filterContext)
        {
            HtmlEngine.SetCurrentAction(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName);
        }
    }
}