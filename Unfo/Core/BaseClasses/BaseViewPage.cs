﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unfo.Core.Helpers;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.BaseClasses
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new UnfoPrincipal User
        {
            get { return base.User as UnfoPrincipal; }
        }
    
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new UnfoPrincipal User
        {
            get { return base.User as UnfoPrincipal; }
        }

       
    }
}