﻿using DataModel;
using DataModel.ModelInterfaces;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Unfo.Core.GlobalClasses;
using Unfo.Core.Wrappers;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core
{
    public class BaseViewModelRepository
    {
        public DataModel.IRepositoryFabric Fabric { get; set; }

        public UnfoPrincipal CurrentUser { get { return HttpContext.Current.User as UnfoPrincipal; } }

        public UnfoSignInManager SignInManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<UnfoSignInManager>();
            }
        }

        public UnfoUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<UnfoUserManager>();
            }
        }

        public UnfoRoleManager RoleManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<UnfoRoleManager>();
            }
        }

        public BaseViewModelRepository()
        {
            Fabric = new RepositoryFabric();
        }

        public BaseViewModelRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }

        public virtual IEnumerable<TViewModel> GetList<TViewModel, TModel>(Guid eventID)
            where TViewModel : IViewModelBuilder<TModel>, new()
            where TModel : EventBaseModel, new()
        {
            var result = new List<TViewModel>();

            var list = Fabric.Table<TModel>().Get(d => !d.IsDelete && d.EventID == eventID);
            foreach (var item in list)
            {
                var t = new TViewModel();
                t.Fill(item);
                result.Add(t);
            }
            return result;
        }

        public virtual TViewModel Get<TViewModel, TModel>(object id)
            where TViewModel : IViewModelBuilder<TModel>, new()
            where TModel : class , new()
        {
            var result = new TViewModel();

            var list = Fabric.Table<TModel>().GetById(id);
            result.Fill(list);
            return result;
        }

        public virtual async Task<TViewModel> GetByIdAsync<TViewModel, TModel>(object id)
            where TViewModel : IViewModelBuilder<TModel>, new()
            where TModel : class , new()
        {
            var result = new TViewModel();
            var dbEntity = await Fabric.Table<TModel>().GetByIdAsync(id);
            result.Fill(dbEntity);
            return result;
        }

        public virtual void AddOrUpdate<TModel>(IModelBuilder<TModel> data) where TModel : EventBaseModel, new()
        {
            var rep = Fabric.Table<TModel>();
            var model = data.GetModel();
            if (model.ID == Guid.Empty)
            {
                rep.Insert(model);
                rep.Save();
            }
            else
            {
                var item = rep.GetById(model.ID);
                if (item.EventID == CurrentEvent.ID)
                {
                    item = data.UpdateModel(item);
                    rep.Update(item);
                    rep.Save();
                }
            }
        }

        public virtual async Task<UnfoResult> AddOrUpdateAsync<TModel>(IModelBuilder<TModel> viewModel) where TModel : class, IBaseModel, new()
        {
            try
            {
                var repository = Fabric.Table<TModel>();
                var model = viewModel.GetModel();
                if (model.ID == Guid.Empty)
                {
                    repository.Insert(model);

                }
                else
                {
                    var entity = await repository.GetByIdAsync(model.ID);
                    entity = viewModel.UpdateModel(entity);
                    repository.Update(entity);
                }
                await repository.SaveAsync();
                return UnfoResult.Success;
            }
            catch (Exception)
            {
                return UnfoResult.Failed();
            }
        }
    }
}