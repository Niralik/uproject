using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DataModel;
using Newtonsoft.Json;
using Unfo.Core.Helpers;
using UnfoIdentitySystem.GlobalStaticClasses;

namespace Unfo.Core.GlobalClasses
{
    /// <summary>
    /// Текущее глобальное событие
    /// </summary>
    public static class CurrentEvent
    {
        public static Guid ID { get; set; }

        public static UEvent UEvent { get; set; }

        public static bool HasAccess { get; set; }

        public static string GetLocalizeName()
        {
            return LocalizeHelper.GetLocalizeField(CurrentLocalization.UI, UEvent.Name);
        }

        public static void Initialize(UEvent @event)
        {
            IdentityCurrentEvent.Initialize(@event);
            if (@event == null) return;
            UEvent = @event;
            ID = @event.ID;

            HasAccess = false;

            //if (HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    if (HttpContext.Current.User.IsInRole("Admin"))
            //    {
            //        HasAccess = true;
            //        return;
            //    }
            //    var user = ((UnfoPrincipal) HttpContext.Current.User);
            //    @event.OrganizatorsForEvent.Any(d=>d.)

            //}

        }

        //public static IEnumerable<KeyValuePair<string, string>> GetAccessCultures()
        //{

        //    var culture = UEvent != null&&UEvent.IsLocalization ? UEvent.SupportLanguages : string.Empty;
        //    if (string.IsNullOrWhiteSpace(culture))
        //        return new List<KeyValuePair<string, string>>()
        //        {
        //            new KeyValuePair<string, string>( "ru",new CultureInfo("ru").DisplayName)
                   
        //        };
        //    culture = culture.Trim().ToLower();
        //    var cultures = culture.Split(',').Select(d => d.Trim()).ToList();

        //    var result = new List<KeyValuePair<string, string>>();
        //    foreach (var item in cultures)
        //    {
        //        result.Add(new KeyValuePair<string, string>(item, new CultureInfo(item).DisplayName));
        //    }
        //    return result;
        //}


        public static Dictionary<string,string> GetAccessCultures()
        {
            var dic = new Dictionary<string, string>();
            
            var culture = UEvent != null && UEvent.IsLocalization ? UEvent.SupportLanguages : string.Empty;
            if (string.IsNullOrWhiteSpace(culture))
            {
                dic.Add("ru", new CultureInfo("ru").DisplayName);
                return dic;
            }
               
            culture = culture.Trim().ToLower();
            var cultures = culture.Split(',').Select(d => d.Trim()).ToList();

            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in cultures)
            {
                dic.Add(item, new CultureInfo(item).DisplayName);
            }
            return dic;
        }

        public static void Dispose()
        {
            ID = Guid.Empty;
            UEvent = null;
        }

        #region Сериализация текущего события в джсон
        public static string ToJson()
        {
            return JsonConvert.SerializeObject(new CookieCurrentEvent(UEvent));
        }

        private class CookieCurrentEvent
        {
            public Guid ID { get; set; }
            public string Name { get; set; }
            public string TranslitName { get; set; }
            public CookieCurrentEvent(UEvent uEvent)
            {
                ID = uEvent.ID;
                Name = LocalizeHelper.GetLocalizeField(uEvent.Name);
                TranslitName = uEvent.TranslitName;
            }

        } 
        #endregion
    }
}