﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unfo.Core.Helpers;
using UnfoIdentitySystem.UnfoPrincipal;

namespace Unfo.Core.GlobalClasses
{
    public static class CurrentUser
    {
        public static UnfoPrincipal Get { get { return HttpContext.Current.User as UnfoPrincipal; } }
    }

}