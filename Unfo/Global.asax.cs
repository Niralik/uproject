﻿using System;
using System.Security.Cryptography;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using DataModel;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Handlebars;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Unfo.Core.Binders;
using Unfo.Core.Extensions.IdentityExtensions;
using Unfo.Models.UnfoBusinessLogic;
using Unfo.Models.UnfoBusinessLogic.Events;
using Unfo.Models.ViewModelRepositories;
using UnfoIdentitySystem;
using UnfoIdentitySystem.UnfoPrincipal;


namespace Unfo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;
            SetBindersAndUnbinders();
        }

        private void SetBindersAndUnbinders()
        {
            ModelBinders.Binders.Add(typeof(UsersFilterParams), new UnfoModelBinder());
            ModelUnbinderHelpers.ModelUnbinders.Add(typeof(UsersFilterParams), new UnfoModelUnbinder());

            ModelBinders.Binders.Add(typeof(UsersRequestsFilterParams), new UnfoModelBinder());
            ModelUnbinderHelpers.ModelUnbinders.Add(typeof(UsersRequestsFilterParams), new UnfoModelUnbinder());

            ModelBinders.Binders.Add(typeof(EventsFilterParams), new UnfoModelBinder());
            ModelUnbinderHelpers.ModelUnbinders.Add(typeof(EventsFilterParams), new UnfoModelUnbinder());
        }

     

    }
}
