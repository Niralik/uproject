﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity.Owin;
using UnfoIdentitySystem;

namespace WebApi.Core
{
    public class WebApiBaseController: ApiController
    {
        private UnfoSignInManager _signInManager;

        public UnfoSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<UnfoSignInManager>();
            }
            private set { _signInManager = value; }
        }
        private UnfoUserManager _userManager;
        public UnfoUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<UnfoUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private UnfoRoleManager _roleManager;
        public UnfoRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<UnfoRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
    }

    

}