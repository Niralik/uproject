﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using DataModel;

namespace WebApi.Core.Helpers
{
    public class CommonHelpers
    {
        public static Dictionary<string, string> GetAccessCulturesForEvent(UEvent uEvent)
        {
            var dic = new Dictionary<string, string>();

            var culture = uEvent != null && uEvent.IsLocalization ? uEvent.SupportLanguages : string.Empty;
            if (string.IsNullOrWhiteSpace(culture))
            {
                dic.Add("ru", new CultureInfo("ru").DisplayName);
                return dic;
            }

            culture = culture.Trim().ToLower();
            var cultures = culture.Split(',').Select(d => d.Trim()).ToList();

            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in cultures)
            {
                dic.Add(item, new CultureInfo(item).DisplayName);
            }
            return dic;
        }


        public static List<string> GetErrorsFromModelState(ICollection<System.Web.Http.ModelBinding.ModelState> values)
        {
            var errors = new List<string>();
            foreach (var modelState in values)
            {
                foreach (var error in modelState.Errors)
                {
                   errors.Add(error.ErrorMessage);
                }
            }

            return errors;
        }

    }
}