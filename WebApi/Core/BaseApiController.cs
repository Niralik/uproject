﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using UnfoIdentitySystem;
using WebApi.Models;
using WebApi.Models.CommonModels;
using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;

namespace WebApi.Core
{
    public class BaseApiController : ApiController
    {
        public ViewModelRepository VmRepository { get; private set; }

        public BaseApiController()
        {
            VmRepository = new ViewModelRepository();
        }

        private UnfoSignInManager _signInManager;

        public UnfoSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<UnfoSignInManager>();
            }
            private set { _signInManager = value; }
        }
        private UnfoUserManager _userManager;
        public UnfoUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<UnfoUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private UnfoRoleManager _roleManager;
        public UnfoRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<UnfoRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

    }


    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            HttpRequestMessage request = actionContext.Request;
            var erorMessage = JsonResponse.GetError(-1);

             var responseMessage = new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(erorMessage)) 
            };
            responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            actionContext.Response = responseMessage;

        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }
    }

}