﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using Owin.Security.Providers.ArcGISOnline.Provider;
using WebApi.Models.VMRepositories;

namespace WebApi.Models.Repositories
{
    public class EventRepository : RepositoryBase
    {
        public EventRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }


        #region События
        public List<Api_EventVM> GetEvents(int count , int page  , string lang = "ru")
        {
            var sections = Fabric.Table<ServiceSection>().DbSet.ToList();
            var events =
                Fabric.Table<UEvent>()
                    .DbSet.Where(d => !d.IsDelete)
                    .OrderByDescending(d => d.StartDate)
                    .Take(count)
                    .Skip(page)
                    .ToList();

            var result = events.Select(d=> new Api_EventVM(d, lang, sections)).ToList();

            return result;
        }

        public Api_EventVM GetEvent(Guid id, string lang = "ru")
        {
            var sections = Fabric.Table<ServiceSection>().DbSet.ToList();

            var @event = Fabric.Table<UEvent>().GetById(id);
            return new Api_EventVM(@event, lang, sections);
        }

        public void DeleteEvent(Guid id)
        {
            Fabric.Table<UEvent>().Delete(id);
            Fabric.Table<UEvent>().Save();
        }
        #endregion

        public void SubscribeToEvent(Guid eventId, string userID)
        {
            Guid userIDGuid = Guid.Parse(userID);

            var userExists = Fabric.Table<UnfoUser>().DbSet.Any(d => d.Id == userIDGuid);
            var eventExists = Fabric.Table<UEvent>().DbSet.Any(d => d.ID == eventId);
            if (userExists && eventExists)
            {
                var subscription = Fabric.Table<SubscriptionToEvent>().DbSet.FirstOrDefault(d => d.UserID == userIDGuid && d.EventID == eventId);
                if (subscription != null)
                {
                    subscription.IsDelete = !subscription.IsDelete;
                    Fabric.Table<SubscriptionToEvent>().Update(subscription);
                    Fabric.Table<SubscriptionToEvent>().Save();
                }
            }
        }

        public void RateEvent(Guid reportId, Guid userId, double rate, string commentText)
        {
            var reportExist = Fabric.Table<Report>().DbSet.Any(d => d.ID == reportId);
            if (!reportExist) return;

            var reportRating = Fabric.Table<ReportRating>().DbSet.FirstOrDefault(d => d.UserID == userId && d.ReportID == reportId);
            if (reportRating == null)
            {
                Fabric.Table<ReportRating>().Insert(new ReportRating()
                {
                    RatingPoints = rate,
                    ReportID = reportId,
                    UserID = userId,
                    CommentText = commentText
                });
            }
            else
            {
                reportRating.RatingPoints = rate;
                reportRating.CommentText = commentText;
                Fabric.Table<ReportRating>().Insert(reportRating);
            }

            Fabric.Table<ReportRating>().Save();
        }


    }
}