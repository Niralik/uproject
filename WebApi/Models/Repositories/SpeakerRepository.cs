﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using WebApi.Models.VMRepositories;

namespace WebApi.Models.Repositories
{
    public class SpeakerRepository : RepositoryBase
    {
        public SpeakerRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }

        public List<Api_SpeakerVM> GetSpeakersForSpeech(Guid speechID, string lang = "ru")
        {
            var speakers = Fabric.Table<SpeakerForReport>().DbSet.Where(d => d.ReportID == speechID && !d.Speaker.IsDelete).Select(d => d.Speaker)
                .ToList().Select(d => new Api_SpeakerVM(d, lang)).ToList();

            return speakers;
        }

        public Api_SpeakerVM GetSpeaker(Guid speakerID, string lang = "ru")
        {
            var speaker = Fabric.Table<Speaker>().DbSet.Where(d => d.ID == speakerID && !d.IsDelete)
                .ToList().Select(d => new Api_SpeakerVM(d, lang)).SingleOrDefault();

            return speaker;
        }


        public List<Api_SpeakerVM> GetAllSpeakers(Guid eventID, string lang = "ru")
        {
            var speakers = Fabric.Table<SpeakerForReport>().DbSet.Where(d => d.Speaker.EventID == eventID && !d.Speaker.IsDelete && !d.Report.IsDelete).Select(d => d.Speaker)
                .ToList().Select(d => new Api_SpeakerVM(d, lang)).ToList();

            return speakers;
        }


    }
}