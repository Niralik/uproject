﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;

namespace WebApi.Models.VMRepositories
{
    public class RepositoryBase
    {
        public DataModel.IRepositoryFabric Fabric { get; set; }

        public RepositoryBase()
        {
        }

        public RepositoryBase(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
        }
    }
}