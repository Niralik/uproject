﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using WebApi.Models.VMRepositories;

namespace WebApi.Models.Repositories
{
    public class DictionariesRepositoriy : RepositoryBase
    {
        public DictionariesRepositoriy(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }

        public List<Api_HallVM> GetHalls(Guid eventID, string lang = "ru")
        {
            var halls = Fabric.Table<Hall>().DbSet.Where(d => d.EventID == eventID && !d.IsDelete)
                .ToList().Select(d => new Api_HallVM(d, lang)).ToList();

            return halls;
        }

        public List<Api_DirectionVM> GetDirections(Guid eventID, string lang = "ru")
        {
            var directions = Fabric.Table<Direction>().DbSet.Where(d => d.EventID == eventID && !d.IsDelete)
                .ToList().Select(d => new Api_DirectionVM(d, lang)).ToList();

            return directions;
        }

        public List<Api_SectionVM> GetSections(Guid eventID, string lang = "ru")
        {
            var sections = Fabric.Table<Section>().DbSet.Where(d => d.EventID == eventID && !d.IsDelete) .ToList()
                .Select(d => new Api_SectionVM(d, lang)).ToList();

            return sections;
        }


    }
}