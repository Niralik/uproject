﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using WebApi.Models.VMRepositories;

namespace WebApi.Models.Repositories
{
    public class ReportRepositoriy : RepositoryBase
    {
        public ReportRepositoriy(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }

        public List<Api_ReportVM> GetReports(Guid eventID, string lang = "ru")
        {

            var speeches = Fabric.Table<Report>().DbSet.Where(d => d.EventID == eventID && !d.IsDelete)
                .ToList().Select(d => new Api_ReportVM(d, lang)).ToList();

            return speeches;
        }


        public void SubscribeToEvent()
        {

        }


        public void DeleteEvent(Guid id)
        {
            Fabric.Table<UEvent>().Delete(id);
            Fabric.Table<UEvent>().Save();
        }


    }
}