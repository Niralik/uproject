﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel;
using WebApi.Models.VMRepositories;

namespace WebApi.Models.Repositories
{
    public class MemberRepository : RepositoryBase
    {
        public MemberRepository(IRepositoryFabric repFabric)
            : base(repFabric)
        {

        }

        public List<Api_MemberVM> GetMembers(Guid eventID)
        {
            var members = Fabric.Table<SubscriptionToEvent>().DbSet.Where(d => d.EventID == eventID && !d.Event.IsDelete && !d.UnfoUser.IsDelete).Select(d => d.UnfoUser)
                .ToList().Select(d => new Api_MemberVM(d)).ToList();

            return members;
        }



    }
}