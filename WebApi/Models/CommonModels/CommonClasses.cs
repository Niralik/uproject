﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models.CommonModels
{

        public class JsonResponse
        {


            private static readonly Dictionary<string, string> errors = new Dictionary<string, string>
            {
                {"-1", "Ошибка авторизации"},
                {"1", "Не указан айдишник"},
                {"500", "Внутренняя ошибка сервера"}
            };


            public int Result { get; set; }

            public object Response { get; set; }

            public static JsonResponse GetSuccess(object response)
            {
                return new JsonResponse()
                {
                    Response = response,
                    Result = 1
                };
            }

            public static JsonResponse GetError(int code)
            {
                return new JsonResponse()
                {
                    Response = new ErrorResponse() { ErrorCode = code, Errors = new List<string>(){ errors[code.ToString()] }},
                    Result = 0
                };
            }

            public static JsonResponse GetError(string message)
            {
                return new JsonResponse()
                {
                    Response = new ErrorResponse() { ErrorCode = 500, Errors = new List<string>() { message } },
                    Result = 0
                };
            }

            public static JsonResponse GetError(int code, List<string> errors)
            {
                return new JsonResponse()
                {
                    Response = new ErrorResponse() { ErrorCode = code, Errors = errors },
                    Result = 0
                };
            }
        }

        public class ErrorResponse
        {
            public int ErrorCode { get; set; }

            public List<string> Errors { get; set; }
        }


}