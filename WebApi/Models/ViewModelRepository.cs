using System;
using DataModel;
using WebApi.Models.Repositories;

namespace WebApi.Models
{
    public class ViewModelRepository : IDisposable
    {

        private static ViewModelRepository instance { get; set; }

        public ViewModelRepository()
        {
            Fabric = new RepositoryFabric();
            InitReps();
        }

        public ViewModelRepository(IRepositoryFabric repFabric)
        {
            Fabric = repFabric;
            InitReps();
        }

        public IRepositoryFabric Fabric { get; private set; }


        public EventRepository Event { get; private set; }
        public DictionariesRepositoriy Dictionaries { get; private set; }
        public ReportRepositoriy Report { get; private set; }
        public SpeakerRepository Speaker { get; private set; }


        /// <summary>
        /// Singleton
        /// </summary>
        public static ViewModelRepository Instance
        {
            get { return instance ?? (instance = new ViewModelRepository()); }
        }

        private void InitReps()
        {
            Event = new EventRepository(Fabric);
            Report = new ReportRepositoriy(Fabric);
            Dictionaries = new DictionariesRepositoriy(Fabric);
            Speaker = new SpeakerRepository(Fabric);
            instance = this;

        }

        public void Dispose()
        {
            if (Fabric != null)
                Fabric.Dispose();
        }
    }
}