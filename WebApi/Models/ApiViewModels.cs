﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DataModel;
using Microsoft.Ajax.Utilities;
using OrangeJetpack.Localization;
using WebApi.Core.Helpers;

namespace WebApi.Models
{
    public class Api_BaseVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }


    public class NameValueVM
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public NameValueVM()
        {
        }
    }

    public class Api_EventVM : Api_BaseVM
    {
        public Api_EventVM()
        {

        }

        public Api_EventVM(UEvent data, string lang = "ru")
        {
            FillEvent(data, lang);
        }

        public Api_EventVM(UEvent data, string lang, IEnumerable<ServiceSection> sections )
        {
            FillEvent(data, lang);
            EventSections = data.ServiceSections != null && data.ServiceSections.Count != 0? 
                data.ServiceSections.Select(d => d.ServiceSectionId).ToList() 
                :
                sections.Select(d=>d.ID).ToList();

            var languages = CommonHelpers.GetAccessCulturesForEvent(data);

            Languages = languages.Select(d=> new Dictionary<string, string>()
            {
               { "Name",  d.Value },
               { "Value",  d.Key }
            }).ToList();
        }

        private void FillEvent(UEvent data, string lang = "ru")
        {
            Id = data.ID;
            Name = data.Localize(lang, d => d.Name).Name;

            Description = data.Description != null? data.Localize(lang, d => d.Description).Description : "";
            if (data.Location != null)
            {
                LocationCity = data.Location.Localize(lang, d => d.CityName).CityName;
                LocationFull = data.Location.Localize(lang, d => d.Name).Name + " " +
                               data.Location.Localize(lang, d => d.StreetName).StreetName + " " +
                               data.Location.Localize(lang, d => d.BuildingNumber).BuildingNumber;
            }
            else
            {
                LocationCity = "";
                LocationFull = "";
            }

            if (!String.IsNullOrWhiteSpace(data.PromoCode))
            {
                MD5 md5 = MD5.Create();
                byte[] inputBytes = Encoding.ASCII.GetBytes(data.PromoCode);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                foreach (byte t in hashBytes)
                    sb.Append(t.ToString("X2"));

                PromoCode = sb.ToString();
            }
            else
            {
                PromoCode = "";
            }

            StartDateTime = data.StartDate;
            FinishDateTime = data.FinishDate;
            IsPrivate = data.IsPrivate;
            var languages = CommonHelpers.GetAccessCulturesForEvent(data);

            Languages = languages.Select(d => new Dictionary<string, string>()
            {
               { "Name",  d.Value },
               { "Value",  d.Key }
            }).ToList();
        }



        public string Description { get; set; }

        public string PromoCode { get; set; }
        public string LocationCity { get; set; }
        public string LocationFull { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime FinishDateTime { get; set; }
        public bool IsPrivate { get; set; }
        public List<Dictionary<string, string>> Languages { get; set; }

        public List<int> EventSections { get; set; }
    }

    public class Api_ReportsWrapper
    {
        public Api_ReportsWrapper()
        {
        }

        public List<Api_ReportVM> Reports;
        public List<Api_DirectionVM> Directions;
        public List<Api_HallVM> Halls;
        public List<Api_SectionVM> Sections;

    }

    public class Api_ReportVM : Api_BaseVM
    {

        public Api_ReportVM()
        {

        }

        public Api_ReportVM(Report data, string lang = "ru")
        {
            Id = data.ID;
            Name = data.Localize(lang, d=>d.Name).Name;
            Description = data.Localize(lang, d=>d.FullDescription).FullDescription;
            StartDateTime = data.StartDateTime;
            FinishDateTime = data.FinishDateTime;
            IsPrivate = data.IsPrivateEvent;

            Speakers = data.SpeakersForReport.Select(d=> new Api_SpeakerVM(d.Speaker, lang)).ToList();
            DirectionID = data.DirectionID;
            HallID = data.HallID;
            SectionID = data.SectionID;
        }


        public string Description { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime FinishDateTime { get; set; }

        public bool IsPrivate { get; set; }

        public List<Api_SpeakerVM> Speakers { get; set; }

        public Guid? DirectionID { get; set; }
        public Guid? SectionID { get; set; }

        public Guid? HallID { get; set; }
    }

    public class Api_SpeakerVM : Api_BaseVM
    {
        public Api_SpeakerVM()
        {

        }

        public Api_SpeakerVM(Speaker data, string lang)
        {
            Id = data.ID;
            Name = data.Localize(lang, d=>d.Name).Name;
            Email = data.Email;
            Company = data.Localize(lang, d => d.Company).Company;
            Location = data.Localize(lang, d => d.City).City;
            AdditionalInfo = data.AdditionalInformation;
            Phone = data.Phone;
        }

        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string AdditionalInfo { get; set; }
        public Guid PhotoId { get; set; }
    }

    public class Api_MemberVM : Api_BaseVM
    {
        public Api_MemberVM()
        {
        }

        public Api_MemberVM(UnfoUser data)
        {
        }

        public string Email { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string AdditionalInfo { get; set; }
        public Guid PhotoId { get; set; }
    }

    public class Api_PartnerVM : Api_BaseVM
    {
        public string Location { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string Email { get; set; }
        public Guid PhotoId { get; set; }
    }

    public class Api_DirectionVM : Api_BaseVM
    {
        public Api_DirectionVM()
        {
        }

        public Api_DirectionVM(Direction data, string lang)
        {
            Id = data.ID;
            Name = data.Localize(lang, d=>d.Name).Name;
        }
    }


    public class Api_OrganizerVM : Api_BaseVM
    {

    }

    public class Api_AdvertVM : Api_BaseVM
    {

    }

    public class Api_HallVM : Api_BaseVM
    {

        public Api_HallVM()
        {
        }

        public Api_HallVM(Hall data, string lang)
        {
            Id = data.ID;
            Name = data.Localize(lang, d => d.Name).Name;
        }
    }

    public class Api_SectionVM : Api_BaseVM
    {

        public Api_SectionVM()
        {
        }

        public Api_SectionVM(Section data, string lang)
        {
            Id = data.ID;
            Name = data.Localize(lang, d => d.Name).Name;
        }
    }
}
