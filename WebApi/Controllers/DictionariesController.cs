﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Parser;
using Microsoft.Ajax.Utilities;
using WebApi.Core;
using WebApi.Models.CommonModels;

namespace WebApi.Controllers
{
    public class DictionariesController : BaseApiController
    {
        // GET: Dictionaries
        public JsonResponse Halls(Guid eventID, string lang = "ru" )
        {
            try
            {
                var halls = VmRepository.Dictionaries.GetHalls(eventID, lang);
                return JsonResponse.GetSuccess(halls);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }


        // GET: Dictionaries
        public JsonResponse Dictionaries(Guid eventID, string lang = "ru")
        {
            try
            {
                var halls = VmRepository.Dictionaries.GetHalls(eventID, lang);
                return JsonResponse.GetSuccess(halls);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }


        // GET: Dictionaries
        public JsonResponse Sections(Guid eventID, string lang = "ru")
        {
            try
            {
                var halls = VmRepository.Dictionaries.GetHalls(eventID, lang);
                return JsonResponse.GetSuccess(halls);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }
    }
}