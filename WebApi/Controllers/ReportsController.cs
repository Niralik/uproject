﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApi.Core;
using WebApi.Models;
using WebApi.Models.CommonModels;

namespace WebApi.Controllers
{
    public class ReportsController : BaseApiController
    {
        // GET api/Speech
        /// <summary>
        /// Получить все доклады
        /// </summary>
        /// <returns></returns>
        public JsonResponse GetAll(Guid eventID, string lang = "ru")
        {
            try
            {
                var reports = VmRepository.Report.GetReports(eventID, lang);
                var sections = VmRepository.Dictionaries.GetSections(eventID, lang);
                var halls = VmRepository.Dictionaries.GetHalls(eventID, lang);
                var direction = VmRepository.Dictionaries.GetDirections(eventID, lang);

                var reportsWithDictionaries = new Api_ReportsWrapper()
                {
                    Sections = sections,
                    Halls = halls,
                    Reports = reports,
                    Directions = direction
                };
                return JsonResponse.GetSuccess(reportsWithDictionaries);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
           
        }


        public bool AddToSchedule(Guid speechId)
        {
            VmRepository.Report.SubscribeToEvent();
            return true;
        }


        public bool RemoveFromMySchedule()
        {
            return true;
        }

        public void SendQuestion()
        {
            
        }

    }
}