﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApi.Core;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class PartnerController : BaseApiController
    {
        // GET: Partner
        public IEnumerable<Api_PartnerVM> Get()
        {
            return new List<Api_PartnerVM>();
        }
    }
}