﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Core;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class OrganizerController : BaseApiController
    {
        public IEnumerable<Api_OrganizerVM> Get()
        {
            return new List<Api_OrganizerVM>();
        }
    }
}
