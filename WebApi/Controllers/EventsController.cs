﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DataModel;
using Microsoft.AspNet.Identity;
using WebApi.Core;
using WebApi.Models;
using WebApi.Models.CommonModels;
using WebGrease;



namespace WebApi.Controllers
{
    [System.Web.Mvc.RoutePrefix("api/Events")]
    public class EventsController : BaseApiController
    {

        // GET api/Events
        /// <summary>
        /// Тест комментария
        /// </summary>
        /// <returns></returns> 
        public JsonResponse Get(int count = 30, int page = 0, string lang = "ru")
        {
            try
            {
                var events = VmRepository.Event.GetEvents(count, page, lang);
                return JsonResponse.GetSuccess(events);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() {ex.InnerException.ToString() });
            }
        }


        public JsonResponse Get(Guid? id, string lang = "ru")
        {
            if (!id.HasValue) return JsonResponse.GetError(1);
            try
            {
                var ev = VmRepository.Event.GetEvent(id.Value, lang);

                if (ev.Id == Guid.Empty)
                    return JsonResponse.GetError(1);

                return JsonResponse.GetSuccess(ev);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }


        public JsonResponse AddToPlan(Guid? eventID)
        {
            if (!eventID.HasValue) return JsonResponse.GetError(1);
            if (!User.Identity.IsAuthenticated) return JsonResponse.GetError(-1);

            try
            {
                VmRepository.Event.SubscribeToEvent(eventID.Value, User.Identity.GetUserId());
                return JsonResponse.GetSuccess(null);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }
    }
}