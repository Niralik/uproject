﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApi.Core;
using WebApi.Models.CommonModels;

namespace WebApi.Controllers
{
    public class SpeakersController : BaseApiController
    {
        public JsonResponse Get(Guid eventID, string lang = "ru")
        {
            try
            {
                var speakers = VmRepository.Speaker.GetAllSpeakers(eventID, lang);
                return JsonResponse.GetSuccess(speakers);
            }
            catch (Exception ex)
            {
                return JsonResponse.GetError(500, new List<string>() { ex.InnerException.ToString() });
            }
        }

     
    }
}