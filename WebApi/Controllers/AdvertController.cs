﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Core;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class AdvertController : BaseApiController
    {
        public IEnumerable<Api_AdvertVM> Get()
        {
            return new List<Api_AdvertVM>();
        }
    }
}
